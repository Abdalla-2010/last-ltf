<header>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Navbar Section -->


    <nav class="navbar navbar-fixed-top " style="margin: auto;">

        <div class="container-fluid">

            <!--second nav button -->

            <div id="menu_bars" class="right menu_bars">

                <span class="t1"></span>

                <span class="t2"></span>

                <span class="t3"></span>

            </div>

            <!-- Brand and toggle get grouped for better mobile display -->


            <div class="container">

                <div class="navbar-header">

                    <div class="crypto_logo_main">


                        <p class="swiper-slider-heading raleway font_600 " style="color: #007bff;">LTF </p>


                    </div>

                    <!--<a class="navbar-brand yellow_logo_crypto" href="#"><img src="images/logo_crypto.png" alt="logo"></a>-->

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->

                <div class="collapse navbar-collapse navbar-ex1-collapse  ">

                    <ul class="nav navbar-nav navbar-right">

                        <li class="{{ (request()->is('/'))  ? 'active' : '' }}"><a href="{{url('/')}}">Home</a></li>
<li class="{{ (request()->is('visitor')) ? 'active' : '' }}"><a href="{{url('visitor')}}">Visitors</a>
                        </li>
                         <li class="{{ (request()->is('exhibitor')) ? 'active' : '' }}"><a href="{{url('exhibitor')}}">Exhibitors</a>
                        </li>
                        <li class="{{ (request()->is('sponsored')) ? 'active' : '' }}"><a href="{{url('sponsored')}}">Sponseres&Partners</a>
                        </li>

                        <li class="{{ (request()->is('tripfam')) ? 'active' : '' }}"><a href="{{url('tripfam')}}">FAM Trips</a></li>

                        

                       

                        
                        @auth
                            <li class="{{ (request()->is('admin/dashboard')) ? 'active' : '' }}"><a href="{{route('admin.dashboard.index')}}">My Profile</a>
                            </li>
                        @else
                            <li class="{{ (request()->is('login')) ? 'active' : '' }}"><a href="{{route('login')}}">Login</a>
                            </li>
                        @endauth
                    </ul>


                </div>

                <!-- /.navbar-collapse -->

            </div>

            <div class="sidebar_menu">

                <nav class="pushmenu pushmenu-right">

                    <p class="swiper-slider-heading raleway font_600 " style="color:#007bff;">LTF </p>

                    <ul class="push_nav centered">

                        <li class="clearfix">

                            <a href="{{url('/')}}"><span>01.</span>Home</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('visitor')}}"> <span>02.</span>Visitors</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('exhibitor')}}"> <span>03.</span>Exhibitors</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('outer-news')}}"> <span>04.</span>News</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('library')}}"> <span>05.</span>library</a>


                        </li>

                        <li class="clearfix">

                            <a href="{{url('about')}}"> <span>06.</span>About</a>


                        </li>

                       


                        

                        


                        


                        <li class="clearfix">

                            <a href="{{url('contact')}}"> <span>07.</span>Contact</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('tripfam')}}"> <span>08.</span>FAM Trips</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('sponsored')}}"> <span>9.</span>sponsores&Partners</a>


                        </li>
                        <li class="clearfix">

                            <a href="{{url('destinationpartner')}}"> <span>9.</span>Destination Partners</a>


                        </li>

                        @auth
                            <li class="clearfix">

                                <a href="{{route('admin.dashboard.index')}}"> <span>12.</span>My Profile</a>

                            </li>
                        @else

                            <li class="clearfix">

                                <a href="{{route('login')}}"> <span>11.</span>login</a>

                            </li>

                        @endauth

                        

                        </li>

                        

                    </ul>

                    <div class="clearfix"></div>

                    <ul class="social_icon black top25 bottom20 list-inline">


                        <li><a href="#"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a></li>

                        <li><a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a></li>

                        <li><a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a></li>

                        <li><a href="#"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i>

                            </a></li>


                    </ul>

                </nav>

            </div>

        </div>

    </nav>


    <!-- /Navbar Section -->

</header>
