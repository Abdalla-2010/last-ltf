<!DOCTYPE html>

<html>



<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">



    <title>{{ trans('global.site_title') }}</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />

    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

    <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" />

    <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" />

    <link href="https://unpkg.com/@coreui/coreui@2.1.16/dist/css/coreui.min.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />

    @yield('styles')

</head>



<style>
    .list-notificacao{
  min-width: 400px;
  background: #ffffff;
}

.list-notificacao li{
   border-bottom : 1px #d8d8d8 solid;
   text-align    : justify;
   padding       : 5px 10px 5px 10px;
   cursor: pointer;
   font-size: 12px;
}

.list-notificacao li:hover{
background: #f1eeee;
}

.list-notificacao li:hover .exclusaoNotificacao{
display: block;
}

.list-notificacao li  p{
 color: black;
 width: 305px;
}
.dropdown-toggle::after{
    display:none;
}

.list-notificacao li .exclusaoNotificacao{
    width: 25px;
    min-height: 40px;
    position: absolute;
    right: 0;
    display: none;
}

.list-notificacao .media img{
    width: 40px;
    height: 40px;
    float:left;
    margin-right: 10px;
}

.badgeAlert {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 12px;
    font-weight: 700;
    color: #fff;
    line-height: 1;
    vertical-align: baseline;
    white-space: nowrap;
    text-align: center;
    background-color: #d9534f;
    border-radius: 10px;
    position: absolute;
    margin-top: -10px;
    margin-left: -10px
}

</style>

<body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done sidebar-lg-show">

    <header class="app-header navbar">

        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">

            <span class="navbar-toggler-icon"></span>

        </button>

        <a class="navbar-brand" href="#">

            <span class="navbar-brand-full">LTF</span>

            <span class="navbar-brand-minimized">P</span>

        </a>

        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">

            <span class="navbar-toggler-icon"></span>

        </button>



        <ul class="nav navbar-nav ml-auto">

            @if(count(config('panel.available_languages', [])) > 1)

                <li class="nav-item dropdown d-md-down-none">

                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                        {{ strtoupper(app()->getLocale()) }}

                    </a>

                    <div class="dropdown-menu dropdown-menu-right">

                        @foreach(config('panel.available_languages') as $langLocale => $langName)

                            <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a>

                        @endforeach

                    </div>

                </li>

            @endif
            <li class="dropdown" style="margin-right: 32px;">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                     <i class="far fa-bell" style="font-size: 25px;"></i>
                    <span class='badgeAlert'>2</span>
                    <span class="caret"></span></a>
                  <ul class="list-notificacao dropdown-menu" style="margin-left: -372px;" >
                    <li id='item_notification_1'>
                        <div class="media">
                           <div class="media-left"> 
                              <a href="#"> 
 <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="{{asset('Frontend/images/not_1.png')}}" data-holder-rendered="true">                            </div>
                           <div class="media-body">
                              <div class='exclusaoNotificacao'><button class='btn btn-danger btn-xs button_exclusao' id='1' onclick='excluirItemNotificacao(this)'>x</button>
                              </div>
                              <h4 class="media-heading">ITEM 1</h4>
                              <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                           </div>
                        </div>
                     </li>    
                     <li id='item_notification_2'>
                        <div class="media">
                           <div class="media-left"> 
                              <a href="#"> 
                              <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="{{asset('Frontend/images/not_1.png')}}" data-holder-rendered="true"> </a> 
                           </div>
                           <div class="media-body">
                              <div class='exclusaoNotificacao'><button class='btn btn-danger btn-xs' id='2' onclick='excluirItemNotificacao(this)'>x</button>
                              </div>
                              <h4 class="media-heading">ITEM 2</h4>
                              <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                           </div>
                        </div>
                     </li>
                  </ul>
               </li>
              
        
        </ul>

        

    </header>



    <div class="app-body">

        @include('partials.menu')

        <main class="main">





            <div style="padding-top: 20px" class="container-fluid">



                @yield('content')



            </div>





        </main>

        <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">

            {{ csrf_field() }}

        </form>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <script src="https://unpkg.com/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>

    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>

    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>

    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>

    <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

    <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <script src="{{ asset('js/main.js') }}"></script>

    <script>

        $(function() {

  let copyButtonTrans = '{{ trans('global.datatables.copy') }}'

  let csvButtonTrans = '{{ trans('global.datatables.csv') }}'

  let excelButtonTrans = '{{ trans('global.datatables.excel') }}'

  let pdfButtonTrans = '{{ trans('global.datatables.pdf') }}'

  let printButtonTrans = '{{ trans('global.datatables.print') }}'

  let colvisButtonTrans = '{{ trans('global.datatables.colvis') }}'



  let languages = {

    'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'

  };



  $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

  $.extend(true, $.fn.dataTable.defaults, {

    language: {

      url: languages.{{ app()->getLocale() }}

    },

    columnDefs: [{

        orderable: false,

        className: 'select-checkbox',

        targets: 0

    }, {

        orderable: false,

        searchable: false,

        targets: -1

    }],

    select: {

      style:    'multi+shift',

      selector: 'td:first-child'

    },

    order: [],

    scrollX: true,

    pageLength: 100,

    dom: 'lBfrtip<"actions">',

    buttons: [

      {

        extend: 'copy',

        className: 'btn-default',

        text: copyButtonTrans,

        exportOptions: {

          columns: ':visible'

        }

      },

      {

        extend: 'csv',

        className: 'btn-default',

        text: csvButtonTrans,

        exportOptions: {

          columns: ':visible'

        }

      },

      {

        extend: 'excel',

        className: 'btn-default',

        text: excelButtonTrans,

        exportOptions: {

          columns: ':visible'

        }

      },

      {

        extend: 'pdf',

        className: 'btn-default',

        text: pdfButtonTrans,

        exportOptions: {

          columns: ':visible'

        }

      },

      {

        extend: 'print',

        className: 'btn-default',

        text: printButtonTrans,

        exportOptions: {

          columns: ':visible'

        }

      },

      {

        extend: 'colvis',

        className: 'btn-default',

        text: colvisButtonTrans,

        exportOptions: {

          columns: ':visible'

        }

      }

    ]

  });



  $.fn.dataTable.ext.classes.sPageButton = '';

});



    </script>

<script>
        function excluirItemNotificacao(e){
  $('#item_notification_'+e.id).remove()
}

    </script>   

    @yield('scripts')

</body>



</html>

