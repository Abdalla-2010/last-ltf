<html lang="en"><head><style>.dismissButton{background-color:#fff;border:1px solid #dadce0;color:#1a73e8;border-radius:4px;font-family:Roboto,sans-serif;font-size:14px;height:36px;cursor:pointer;padding:0 24px}.dismissButton:hover{background-color:rgba(66,133,244,0.04);border:1px solid #d2e3fc}.dismissButton:focus{background-color:rgba(66,133,244,0.12);border:1px solid #d2e3fc;outline:0}.dismissButton:hover:focus{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd}.dismissButton:active{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd;box-shadow:0 1px 2px 0 rgba(60,64,67,0.3),0 1px 3px 1px rgba(60,64,67,0.15)}.dismissButton:disabled{background-color:#fff;border:1px solid #f1f3f4;color:#3c4043}
</style><style>.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}
</style><style>.gm-control-active>img{box-sizing:content-box;display:none;left:50%;pointer-events:none;position:absolute;top:50%;transform:translate(-50%,-50%)}.gm-control-active>img:nth-child(1){display:block}.gm-control-active:hover>img:nth-child(1),.gm-control-active:active>img:nth-child(1){display:none}.gm-control-active:hover>img:nth-child(2),.gm-control-active:active>img:nth-child(3){display:block}
</style><link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700"><style>.gm-ui-hover-effect{opacity:.6}.gm-ui-hover-effect:hover{opacity:1}
</style><style>.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px;box-sizing:border-box}
</style><style>@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style>.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
</style><style>.gm-style img{max-width: none;}.gm-style {font: 400 11px Roboto, Arial, sans-serif; text-decoration: none;}</style>
<style>
body {
    background-image:url(Frontend/images/Mercator_Blank_Map_World.png);
    background-size:cover !important;
  
}
.navbar .navbar-nav li a {
    color: #191919 !important;
}
.swiper-slider-heading {
    font-size:35px !important;
}
.menu ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
.navbar-fixed-top{
    background: white;
   margin: auto;

}
.menu li {
  padding: 8px;
  margin-bottom: 7px;
  background-color: #33b5e5;
  color: #ffffff;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}

.menu li:hover {
  background-color: #0099cc;
}
.flex-container {
  display: flex;
  flex-wrap: nowrap;
margin-left: -34px;    
}

.flex-container > div {
  background-color: #f1f1f1;
  width: 300px;
  margin: 10px;
  text-align: center;
  line-height: 75px;
 font-size: 17px;
    font-weight: 700;
    height: 106px;
    padding-top: 16px;
    margin-left:58px;
}
@media (max-width: 575.98px) { 
    .btn-primary {
        width: 326px !important;
}
.flex-container {
     display: block !important;
    
    }
    .boxes{
       margin-left: -32px;  
    }
.content-offer img{
    width: 323px !important;
}
.contact_form_inner {
    width: 100%;
    overflow-x: hidden;
}
 
   
    
}
body{
    overflow-x:hidden;
}
    
}


@media (min-width: 576px) and (max-width: 767.98px) {  }


@media (min-width: 768px) and (max-width: 991.98px) {  }


@media (min-width: 992px) and (max-width: 1199.98px) {  }


@media (min-width: 1200px) { }
</style>
    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

     <meta name="description" content="">
    
  <meta name="keywords" content="">

    <!-- Insert Favicon Here -->
    <link href="{{asset('Frontend/images/favicon.png')}}" rel="icon">

    <!-- Page Title(Name)-->
    <title>LTF</title>

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/bootstrap.css')}}">

    <!-- Font-Awesome CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/font-awesome.css')}}">

    <!-- Slider Revolution CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/settings.css')}}">

    <!--  Fancy Box CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/jquery.fancybox.css')}}">

    <!-- Circleful CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/jquery.circliful.css')}}">

    <!-- Animate CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/animate.css')}}">

    <!-- Cube Portfolio CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/cubeportfolio.min.css')}}">

    <!-- Owl Carousel CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('Frontend/css/owl.theme.default.min.css')}}">

    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/swiper.min.css')}}">

    <!-- Custom Style CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/css/style.css')}}">

    <!-- Crypto Currency CSS File -->
    <link rel="stylesheet" href="{{asset('Frontend/cryptocurrency-assets/css/style.css')}}">

    <!-- Color StyleSheet CSS File -->
    <link href="{{asset('Frontend/css/yellow.css')}}" rel="stylesheet" id="color" type="text/css">
    
    <link rel="stylesheet" type="text/css" href="{{asset('Frontend/fonts/line-icons.css')}}">

    <link rel="stylesheet" href="{{asset('Frontend/css/font-six.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



<script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script><style type="text/css">@-webkit-keyframes _gm7906 {
0% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }
50% { -webkit-transform: translate3d(0px,-20px,0); -webkit-animation-timing-function: ease-in; }
100% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }
}
</style><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script></head>


<body>




<!-- Loader -->

<!-- Loader -->

<div class="loader" style="display: none;">

    <div class="loader-inner">

        <div class="spinner">

            <div class="dot1"></div>

            <div class="dot2"></div>

        </div>

    </div>

</div>
<!-- Parent Section -->

<section class="page_content_parent_section">


    <!-- Header Section -->

  @include('nav')

   

      
</section>

<section class="contact_form_section" id="contact-form">

        <div class="container">

            <div class="row">

                <div class="contact_form_inner big_padding clearfix">

                   <h1>Terms and conditions <br></h1>
  <h3><span style="font-weight: 400;">Of processing personal data of end users, serves also as</span><br>
<span style="font-weight: 400;">“End User Agreement”</span></br></br></h3>

			<p><span style="font-weight: 400;">Please read this document carefully and give your consent at the bottom.</span></p>
<p><span style="font-weight: 400;">This document contains the terms and conditions of processing personal data by Oview (herein after referred to as the “Terms and Conditions”). Additional provisions concern the different aspects of Oview activities presented in <a href="https://oviewapp.com/disclaimer-1/">Oview Privacy Policy</a>.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">In order to gain access to Oview, we ask you to provide your personal information (“Personal Data”). All information that you disclose is confidential and protected from unauthorized disclosure, tampering, or damage.</span></p>
<p><span style="font-weight: 400;">Your participation is voluntary. According to the Terms and Conditions you give your consent to use your personal data in accordance with the following conditions:</span></p>	
<br>
  
  


			<h2><span style="font-weight: 400;">Subject and purpose of data collection/data processing</span></h2>

<p><span style="font-weight: 400;">Oview, limited liability partnership (LLP), including its subsidiaries, affiliates, divisions, contractors, and all data sources and suppliers, (collectively “Oview”, “we”, “us” or “our”) – a global online platform where opinions are created, acknowledged and transformed into universally accessible visualised data with the intention to induce a civilization that is inspired by the freely created opinions of the majority, which are supported by unbiased information. </span></p>
<p><span style="font-weight: 400;">Oview provides access to a system where users can:</span></p>
<p><span style="font-weight: 400;">– create polar questions (Yes/No questions) about current articles (the “</span><span style="font-weight: 400;">survey questions</span><span style="font-weight: 400;">”). This is done via the backend;</span></p>
<p><span style="font-weight: 400;">– answer </span><span style="font-weight: 400;">the survey questions.</span></p>
<p><span style="font-weight: 400;">It is important to note that all analysis of the survey results such as reports, statistical tables, charts, graphics or other visual forms (“statistical reports”), are always aggregated into statistical information without compromising individual votes.</span></p>
<p><span style="font-weight: 400;">For these purposes Oview asks you to provide your personal data which will be used by Oview to create statistical reports based on personal opinions about different news articles, items, issues, topics, and other internet sources which users link while creating their survey questions in order to help other </span><span style="font-weight: 400;">users </span><span style="font-weight: 400;">to understand the general context of these survey questions</span> <span style="font-weight: 400;">(the “articles”). Oview provides you with access to our services only after you create an account in Oview as defined in the Privacy Policy in the part “Registration in Oview”. As soon as you sign the Terms and Conditions and finish the whole registration process in the Oview <a href="https://cms.oviewapp.com">backend of the app</a> or in <a href="https://play.google.com/store/apps/details?id=nl.oview.app">the mobile app</a> (the “registration”), your personal data will be transferred to Oview and you will become an Oview user (the “user”). Becoming an Oview user contains the right to express your opinion by voluntary participation in survey questions and/or the right to create survey questions. </span></p>
<p><span style="font-weight: 400;">Oview controls personal data. It collects and processes personal data only for the purposes of Oview activities. Oview respects your privacy and complies with Dutch&nbsp;and international data protection law.</span></p>
<br>




			<h2><span style="font-weight: 400;">Personal data</span></h2>
<p><span style="font-weight: 400;">During the registration in Oview we ask you to provide personal data which includes your name, birth date, gender, education level, location (city), language preference and/or other categories of personal data which Oview will decide to include in the future. Providing this personal data is optional, and it determines the amount of information in statistical reports the user will be able to see after responding to the survey questions; meaning; providing your personal data gives access to the corresponding aggregated data in the statistics reports. It is possible at any point to remove and/or add personal data, and that will immediately change the extent of access to the relevant statistical reports.</span></p>
<p><span style="font-weight: 400;">Oview can collect, process, store and use your technical information</span> <span style="font-weight: 400;">such as cookies, and/or other technologies, and subsidiary analytical software as described in our Cookies Policy and in accordance to Dutch and international law. The statistical information of your activities in your Oview account can be used for improvement of our system including security policies, but not limited to it alone.</span></p>
<p><span style="font-weight: 400;">We collect information from your mobile device about the system and model of your device, version of your operating system, and statistical information about the equipment of your device such as the processor, </span><span style="font-weight: 400;">short-term memory, </span><span style="font-weight: 400;">internal memory, application versions and similar. This information could be used as statistical data for improvement of our system including security policies, but not limited to it alone.</span></p>
<p><span style="font-weight: 400;">You can use third party services (such as Facebook and others) to share personal data while creating an account in Oview. Only personal data with public access will be collected in the case of using third party services. Oview does not have direct or indirect intention to collect more personal data than we ask of you during the registration</span><span style="font-weight: 400;">.</span><span style="font-weight: 400;"> You can manage the information that we collect through third party services by changing the extent of public access to your personal data in those services according to their Privacy Policies.</span></p>

  <br>
    

			<h2><span style="font-weight: 400;">Processing of Personal Data</span></h2>
<p><span style="font-weight: 400;">Without infringement of personal protection and in accordance to Dutch, European, and international Data Protection Law. Oview </span><span style="font-weight: 400;">has the right to use all data collected for actions or a set of actions, including the collection, recording, organization, storage, updating or modification, retrieval, consultation, use, dissemination by means of transmission, distribution or display in any other form, merging, linking, as well as blocking, erasure or destruction of data (“processing”).</span></p>
<p><span style="font-weight: 400;">Your personal data is processed according to the Terms and Conditions and Privacy Policy of Oview.</span></p>
<p><span style="font-weight: 400;">Oview will store personal data on a special database in the server of third parties (the “provider”). This provider stores personal data in accordance with the Terms and Conditions of the special agreement between us and this provider. All provider activity related to personal data is regulated by the Terms and Conditions, our Privacy Policy&nbsp;and the provider’s Privacy Policy. The provider guarantees additional security measures to your personal data with relation to the server where all data are saved.</span></p>
<p><span style="font-weight: 400;">Your personal data is protected from unauthorized access also from the third parties through our encryption during transmission, storage, and other processing of personal data to increase the level of safety of your personal data.</span></p>
<p><span style="font-weight: 400;">With respect to your privacy and following the requirements of data protection, your personal data are used by Oview only after its depersonalization, mixing and modification. The modification of information is the final step of creating statistical reports. </span></p>
<p><span style="font-weight: 400;">All results of public opinion become public only after the user</span><b>`</b><span style="font-weight: 400;">s voluntarily participation in survey questions, with the option of user</span><b>`</b><span style="font-weight: 400;">s preliminary research into the topic discussed in the different articles.</span></p>
<br>


			<h2><span style="font-weight: 400;">Users</span><b>’</b><span style="font-weight: 400;"> &nbsp;rights &nbsp;</span></h2>
<p><span style="font-weight: 400;">You have the right to know what kind of information related to you is being processed by Oview, and/or whether your personal data is being processed or not. You have the right to send a request to Oview in order to correct, supplement, delete or block information related to you according to your specification based on legitimate grounds and according to the Dutch&nbsp;and international data protection law.</span> <span style="font-weight: 400;">Oview is obliged to inform you when we finish all actions related to such a withdrawal, </span><span style="font-weight: 400;">in accordance with</span><span style="font-weight: 400;"> the term fixed by the law.</span></p>
<p><span style="font-weight: 400;">Users have the right to react to this document and notify Oview, via email, what they would like to change in the Terms and Conditions and why. Oview reserves the right to change the Terms and Conditions in accordance with these suggestions, but it is not obliged to do so.</span></p>
<br>


			<h2><span style="font-weight: 400;">Providing personal data to third parties</span></h2>
<p><span style="font-weight: 400;">We keep your personal data in accordance with confidentiality requirements by law and without infringement of your privacy. </span></p>
<p><span style="font-weight: 400;">We encrypt your personal data during transmission, storage, and backup to increase the level of safety of your personal data.</span></p>
<p><span style="font-weight: 400;">Specific employees of Oview</span> <span style="font-weight: 400;">have a key to the codified personal data for the purposes of improvement of the application system including, but not limited to security policy, for ensuring proper operation of the platform, and for other legitimate purposes related to Oview activities.</span></p>
<p><span style="font-weight: 400;">Third parties can view the statistical reports without any personal identification of the users.</span></p>
<br>



			<h2><span style="font-weight: 400;">Limitations of the validity of the </span><span style="font-weight: 400;">the Terms and Conditions</span></h2>
<p><span style="font-weight: 400;">You agree, for an unlimited time, to give your consent regarding your personal data to Oview according to the terms and conditions as described in the Terms and Conditions.</span></p>
<p><span style="font-weight: 400;">Oview has the right to change the Terms and Conditions without any notification unless these changes reduce users</span><b>’</b><span style="font-weight: 400;"> rights; it will not be done without the notice and additional consent of the users.</span></p>
<br>




			<h2><span style="font-weight: 400;">General</span></h2>
<p><span style="font-weight: 400;">All the definitions in the Terms and Conditions, including “Oview”, “user”, “survey question”, “personal data”, “statistical report”, “registration”, “provider”, and others can be used in the plural form as well as in the singular form without it having any effect on the definitions themselves. The pronouns used herein shall include, where appropriate, either gender and/or both singular and plural.</span></p>
<p><span style="font-weight: 400;">All terms and their definitions in the Terms and Conditions can be used in the Privacy Policy document, and vice versa, without any changes to their meaning/s. </span></p>
<p><span style="font-weight: 400;">In the event that any clause in this Terms and Conditions should be and/or become null and void, this mere fact will not have any effect on the validity of the other clauses.</span></p>
<p><span style="font-weight: 400;">This Terms and Conditions shall be governed and interpreted through and under the Dutch Law and the law of the court in Utrecht.</span></p>
<p><span style="font-weight: 400;">If you have any questions, suggestions or comments regarding Oview</span><b>’</b><span style="font-weight: 400;">s processing of personal data, please contact us at </span><a href="mailto:info@oviewapp.com"><span style="font-weight: 400;">info@oviewapp.com</span></a><span style="font-weight: 400;">. </span></p>
<br>




    </div>
    </div>
    </div>
    
    </section>
    
    <!-- /Parent Section Ended -->

<!-- jQuery 2.2.0-->


<script src="{{asset('Frontend/js/jquery.js')}}"></script>

<!-- Google Map Api -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U" type="text/javascript"></script>
<script src="{{asset('Frontend/js/map.js')}}" type="text/javascript"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{asset('Frontend/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('Frontend/js/actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('Frontend/js/video.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('Frontend/js/bootstrap.min.js')}}"></script>

<!-- Owl Carousel 2 Core JavaScript -->
<script src="{{asset('Frontend/js/owl.carousel.js')}}"></script>
<script src="{{asset('Frontend/js/owl.animate.js')}}"></script>
<script src="{{asset('Frontend/js/owl.autoheight.js')}}"></script>
<script src="{{asset('Frontend/js/owl.autoplay.js')}}"></script>
<script src="{{asset('Frontend/js/owl.autorefresh.js')}}"></script>
<script src="{{asset('Frontend/js/owl.hash.js')}}"></script>
<script src="{{asset('Frontend/js/owl.lazyload.js')}}"></script>
<script src="{{asset('Frontend/js/owl.navigation.js')}}"></script>
<script src="{{asset('Frontend/js/owl.support.js')}}"></script>
<script src="{{asset('Frontend/js/owl.video.js')}}"></script>

<!-- Fancy Box Javacript -->
<script src="{{asset('Frontend/js/jquery.fancybox.js')}}"></script>

<!-- Wow Js -->
<script src="{{asset('Frontend/js/wow.min.js')}}"></script>

<!-- Appear Js-->
<script src="{{asset('Frontend/js/jquery.appear.js')}}"></script>

<!-- Countdown Js -->
<script src="{{asset('Frontend/js/jquery.countdown.js')}}"></script>

<!-- Parallax Js -->
<script src="{{asset('Frontend/js/parallax.min.js')}}"></script>

<!-- Particles Core Js -->
<script src="{{asset('Frontend/js/particles.js')}}"></script>

<!-- Cube Portfolio Core JavaScript -->
<script src="{{asset('Frontend/js/jquery.cubeportfolio.min.js')}}"></script>

<!-- Circliful Core JavaScript -->
<script src="{{asset('Frontend/js/jquery.circliful.min.js')}}"></script>

<!-- Swiper Slider Core JavaScript -->
<script src="{{asset('Frontend/js/swiper.min.js')}}"></script>

<!-- Crypto Currency Core Javascript -->
<script src="{{asset('Frontend/cryptocurrency-assets/js/script.js')}}"></script>

<!-- Custom JavaScript -->
<script src="{{asset('Frontend/js/script.js')}}"></script>



</body></html>