<div class="sidebar">
    <nav class="sidebar-nav ps ps--active-y">


        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.dashboard.index") }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>


            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-user nav-icon">

                    </i>
                    {{ trans('global.profile.title') }}
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.show.profile") }}"
                           class="nav-link {{ request()->is('admin/show/profile') ? 'active' : '' }}">
                            <i class="fas fa-eye nav-icon">

                            </i>
                            {{ trans('global.profile.action.show') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route("admin.edit.profile") }}"
                           class="nav-link {{ request()->is('admin/edit/profile') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-edit">

                            </i>
                            {{ trans('global.profile.action.edit') }}
                        </a>
                    </li>
                </ul>

            @if(Auth::user()->status == 0)


                <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle">
                        <i class="fas fa-users nav-icon">

                        </i>
                        {{ trans('global.userManagement.title') }}
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route("admin.permissions.index") }}"
                               class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <i class="fas fa-unlock-alt nav-icon">

                                </i>
                                {{ trans('global.permission.title') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route("admin.roles.index") }}"
                               class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <i class="fas fa-briefcase nav-icon">

                                </i>
                                {{ trans('global.role.title') }}
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="{{ route("admin.users.index") }}"
                               class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <i class="fas fa-user nav-icon">

                                </i>
                                {{ trans('global.user.title') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route("admin.leaders.index") }}"
                               class="nav-link {{ request()->is('admin/leaders') || request()->is('admin/leaders/*') ? 'active' : '' }}">
                                <i class="fas fa-user-friends nav-icon">

                                </i>
                                {{ trans('global.leaders.title') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route("admin.tourguides.index") }}"
                               class="nav-link {{ request()->is('admin/tourguides') || request()->is('admin/tourguides/*') ? 'active' : '' }}">
                                <i class="fas fa-user-friends nav-icon">

                                </i>
                                {{ trans('global.tourguides.title') }}
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="{{ route("admin.visitors.index") }}"
                               class="nav-link {{ request()->is('admin/visitors') || request()->is('admin/visitors/*') ? 'active' : '' }}">
                                <i class="fas fa-users nav-icon">

                                </i>
                                {{ trans('global.visitor.title') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#"
                               class="nav-link ">
                                <i class="fas fa-users nav-icon">

                                </i>
                                {{ trans('global.trad_visitor.title') }}
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="{{ route("admin.exhibitors.index") }}"
                               class="nav-link {{ request()->is('admin/exhibitors') || request()->is('admin/exhibitors/*') ? 'active' : '' }}">
                                <i class="fas fa-home nav-icon">

                                </i>
                                {{ trans('global.exhibitor.title') }}
                            </a>
                        </li>


                    </ul>
                </li>







                <li class="nav-item">
                    <a href="{{route('admin.searchs.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-search">

                        </i>
                        {{ trans('global.search.title') }}
                    </a>
                </li>



                <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle">
                        <i class="fas fa-user-cog nav-icon">

                        </i>
                        {{ trans('global.setting.title') }}
                    </a>
                    <ul class="nav-dropdown-items">

                        <li class="nav-item">
                            <a href="{{ route("admin.abouts.index") }}"
                               class="nav-link {{ request()->is('admin/abouts') || request()->is('admin/abouts/*') ? 'active' : '' }}">
                                <i class="fas fa-address-card nav-icon">

                                </i>
                                {{ trans('global.about.title') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route("admin.articles.index") }}"
                               class="nav-link {{ request()->is('admin/articles') || request()->is('admin/articles/*') ? 'active' : '' }}">
                                <i class="fas fa-edit nav-icon">

                                </i>
                                {{ trans('global.news.title') }}
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="{{ route("admin.metas.index") }}"
                               class="nav-link {{ request()->is('admin/metas') || request()->is('admin/metas/*') ? 'active' : '' }}">
                                <i class="fas fa-smile-wink nav-icon">

                                </i>
                                Meta
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route("admin.sliders.index") }}"
                               class="nav-link {{ request()->is('admin/sliders') || request()->is('admin/sliders/*') ? 'active' : '' }}">
                                <i class="fas fa-smile-wink nav-icon">
                                </i>
                                @lang('global.sliders.title')
                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="{{ route("admin.librarys.index") }}"
                               class="nav-link {{ request()->is('admin/librarys') || request()->is('admin/librarys/*') ? 'active' : '' }}">
                                <i class="fas fa-book-open nav-icon">

                                </i>
                                {{ trans('global.librarys.title') }}
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="nav-item">
                    <a href="{{route('admin.favourites.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-heart">

                        </i>
                        {{ trans('global.favourites.title') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route("admin.contactus.index") }}"
                       class="nav-link {{ request()->is('admin/contactus') || request()->is('admin/contactus/*') ? 'active' : '' }}">
                        <i class="fas fa-phone nav-icon">

                        </i>
                        {{ trans('global.contactus.title') }}
                    </a>
                </li>




                <li class="nav-item">
                    <a href="{{ route("admin.offers.index") }}"
                       class="nav-link {{ request()->is('admin/offers') || request()->is('admin/offers/*') ? 'active' : '' }}">
                        <i class="fas fa-box nav-icon">

                        </i>
                        {{ trans('global.offers.title') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route("admin.myoffers.index") }}"
                       class="nav-link {{ request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : '' }}">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{ route("admin.fans.index") }}"
                       class="nav-link {{ request()->is('admin/fans') || request()->is('admin/fans/*') ? 'active' : '' }}">
                        <i class="fas fa-bank nav-icon">

                        </i>
                        Trip Fam
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{ route("admin.rates.index") }}"
                       class="nav-link {{ request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : '' }}">
                        <i class="fas fa-star nav-icon">

                        </i>
                        {{ trans('global.rates.title') }}
                    </a>
                </li>



            @endif


        <!--<li class="nav-item">-->
            <!--    <a href="#" class="nav-link"-->
            <!--      >-->
            <!--        <i class="nav-icon fas fa-sign-out-alt">-->

            <!--        </i>-->
            <!--        Appointment-->
            <!--    </a>-->
            <!--</li>-->


            <!--<li class="nav-item nav-dropdown">-->
            <!--    <a class="nav-link  nav-dropdown-toggle">-->
            <!--        <i class="fas fa-star nav-icon">-->

            <!--        </i>-->
        <!--        {{ trans('global.rates.title') }}-->
            <!--    </a>-->
            <!--    <ul class="nav-dropdown-items">-->
            <!--        <li class="nav-item">-->
        <!--            <a href="{{ route("admin.rates.index") }}"-->
        <!--               class="nav-link {{ request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : '' }}">-->
            <!--                <i class="fas fa-star nav-icon">-->

            <!--                </i>-->
        <!--                {{ trans('global.rates.title') }}-->
            <!--            </a>-->
            <!--        </li>-->
            <!--        <li class="nav-item">-->
        <!--            <a href="{{ route("admin.forms.index") }}"-->
        <!--               class="nav-link {{ request()->is('admin/forms') || request()->is('admin/forms/*') ? 'active' : '' }}">-->
            <!--                <i class="fas fa-book nav-icon">-->

            <!--                </i>-->
        <!--                {{ trans('global.forms.title') }}-->
            <!--            </a>-->
            <!--        </li>-->
            <!--    </ul>-->
            <!--</li>-->


        <!-- <li class="nav-item">
                <a href="{{ route("admin.media.index") }}"
                   class="nav-link {{ request()->is('admin/media') || request()->is('admin/media/*') ? 'active' : '' }}">
                    <i class="fas fa-square nav-icon">

                    </i>
                    {{ trans('global.media.title') }}
            </a>
        </li> -->


            @if(Auth::user()->status == 1 )




                <li class="nav-item">
                    <a href="#" class="nav-link"
                    >
                        <i class="nav-icon fas fa-sign-out-alt">

                        </i>
                        Appointment
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{route('admin.searchs.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-search">

                        </i>
                        {{ trans('global.search.title') }}
                    </a>
                </li>



                <li class="nav-item">
                    <a href="{{ route("admin.myoffers.index") }}"
                       class="nav-link {{ request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : '' }}">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>



                <li class="nav-item">
                    <a href="{{route('admin.favourites.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-heart">

                        </i>
                        {{ trans('global.favourites.title') }}
                    </a>
                </li>



                <li class="nav-item">
                    <a href="{{ route("admin.rates.index") }}"
                       class="nav-link {{ request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : '' }}">
                        <i class="fas fa-star nav-icon">

                        </i>
                        {{ trans('global.rates.title') }}
                    </a>
                </li>


            @endif



            @if(Auth::user()->status == 2)

                <li class="nav-item">
                    <a href="{{ route("admin.offers.index") }}"
                       class="nav-link {{ request()->is('admin/offers') || request()->is('admin/offers/*') ? 'active' : '' }}">
                        <i class="fas fa-box nav-icon">

                        </i>
                        {{ trans('global.offers.title') }}
                    </a>
                </li>

            @endif

            @if(Auth::user()->status == 3 || Auth::user()->status == 5)




                <li class="nav-item">
                    <a href="#" class="nav-link"
                    >
                        <i class="nav-icon fas fa-sign-out-alt">

                        </i>
                        Appointment
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{route('admin.searchs.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-search">

                        </i>
                        {{ trans('global.search.title') }}
                    </a>
                </li>



                <li class="nav-item">
                    <a href="{{ route("admin.myoffers.index") }}"
                       class="nav-link {{ request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : '' }}">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>



                <li class="nav-item">
                    <a href="{{route('admin.favourites.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-heart">

                        </i>
                        {{ trans('global.favourites.title') }}
                    </a>
                </li>



                <li class="nav-item">
                    <a href="{{ route("admin.rates.index") }}"
                       class="nav-link {{ request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : '' }}">
                        <i class="fas fa-star nav-icon">

                        </i>
                        {{ trans('global.rates.title') }}
                    </a>
                </li>


            @endif




            @if(Auth::user()->status == 4 )





                <li class="nav-item">
                    <a href="#" class="nav-link"
                    >
                        <i class="nav-icon fas fa-sign-out-alt">

                        </i>
                        Appointment
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{route('admin.searchs.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-search">

                        </i>
                        {{ trans('global.search.title') }}
                    </a>
                </li>




                <li class="nav-item">
                    <a href="{{ route("admin.myoffers.index") }}"
                       class="nav-link {{ request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : '' }}">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{route('admin.favourites.index')}}" class="nav-link"
                    >
                        <i class="nav-icon fas fa-heart">

                        </i>
                        {{ trans('global.favourites.title') }}
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{ route("admin.rates.index") }}"
                       class="nav-link {{ request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : '' }}">
                        <i class="fas fa-star nav-icon">

                        </i>
                        {{ trans('global.rates.title') }}
                    </a>
                </li>







            @endif


            <li class="nav-item">
                <a href="#" class="nav-link"
                   onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>


        </ul>

        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
