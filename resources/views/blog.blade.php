<html lang="en"><head><style>.dismissButton{background-color:#fff;border:1px solid #dadce0;color:#1a73e8;border-radius:4px;font-family:Roboto,sans-serif;font-size:14px;height:36px;cursor:pointer;padding:0 24px}.dismissButton:hover{background-color:rgba(66,133,244,0.04);border:1px solid #d2e3fc}.dismissButton:focus{background-color:rgba(66,133,244,0.12);border:1px solid #d2e3fc;outline:0}.dismissButton:hover:focus{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd}.dismissButton:active{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd;box-shadow:0 1px 2px 0 rgba(60,64,67,0.3),0 1px 3px 1px rgba(60,64,67,0.15)}.dismissButton:disabled{background-color:#fff;border:1px solid #f1f3f4;color:#3c4043}
</style><style>.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}
</style><style>.gm-control-active>img{box-sizing:content-box;display:none;left:50%;pointer-events:none;position:absolute;top:50%;transform:translate(-50%,-50%)}.gm-control-active>img:nth-child(1){display:block}.gm-control-active:hover>img:nth-child(1),.gm-control-active:active>img:nth-child(1){display:none}.gm-control-active:hover>img:nth-child(2),.gm-control-active:active>img:nth-child(3){display:block}
</style><link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700"><style>.gm-ui-hover-effect{opacity:.6}.gm-ui-hover-effect:hover{opacity:1}
</style><style>.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px;box-sizing:border-box}
</style><style>@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style>.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
</style><style>.gm-style img{max-width: none;}.gm-style {font: 400 11px Roboto, Arial, sans-serif; text-decoration: none;}</style>
    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

     <meta name="description" content="">
    
  <meta name="keywords" content="">
    <!-- Insert Favicon Here -->
    <link href="images/favicon.png" rel="icon">

    <!-- Page Title(Name)-->
    <title>LTF</title>

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/bootstrap.css') }}">

    <!-- Font-Awesome CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/font-awesome.css') }}">
    <link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free.min.css" media="all">
    <link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-font-face.min.css" media="all">
    <link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css" media="all">

    <!-- Slider Revolution CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/settings.css') }}">

    <!--  Fancy Box CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/jquery.fancybox.css') }}">

    <!-- Circleful CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/jquery.circliful.css') }}">

    <!-- Animate CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/animate.css') }}">

    <!-- Cube Portfolio CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/cubeportfolio.min.css') }}">

    <!-- Owl Carousel CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/owl.theme.default.min.css') }}">

    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/swiper.min.css') }}">

    <!-- Custom Style CSS File -->
    <link rel="stylesheet" href="{{ asset('Frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/counters.css') }}">

    <!-- Crypto Currency CSS File -->
    <link rel="stylesheet" href="cryptocurrency-assets/css/style.css">

    <!-- Color StyleSheet CSS File -->
    <link href="{{ asset('Frontend/css/yellow.css') }}" rel="stylesheet" id="color" type="text/css">
    
    <link rel="stylesheet" type="text/css" href="fonts/line-icons.css">

    <link rel="stylesheet" href="{{ asset('Frontend/css/font-six.css') }}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script>
   const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;
  

let countDown = new Date('Sep 30, 2020 00:00:00').getTime(),
    x = setInterval(function() {    

      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
        
      document.getElementById('day').innerText = Math.floor(distance / (day)),
        document.getElementById('hour').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minute').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('second').innerText = Math.floor((distance % (minute)) / second);

      //do something later when date is reached
      //if (distance < 0) {
      //  clearInterval(x);
      //  'IT'S MY BIRTHDAY!;
      //}

    }, second)
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



<script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script><style type="text/css">@-webkit-keyframes _gm7906 {
0% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }
50% { -webkit-transform: translate3d(0px,-20px,0); -webkit-animation-timing-function: ease-in; }
100% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }
}
</style><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script></head>


<body>




<!-- Loader -->

<!-- Loader -->

<div class="loader" style="display: none;">

    <div class="loader-inner">

        <div class="spinner">

            <div class="dot1"></div>

            <div class="dot2"></div>

        </div>

    </div>

</div>
<!-- Parent Section -->

<section class="page_content_parent_section">


    <!-- Header Section -->

    <header>

        <!-- Navbar Section -->

        <nav class="navbar navbar-fixed-top ">
            <div class="container-fluid">
                <!--second nav button -->
                <div id="menu_bars" class="right menu_bars">
                    <span class="t1"></span>
                    <span class="t2"></span>
                    <span class="t3"></span>
                </div>
                <!-- Brand and toggle get grouped for better mobile display -->


                <div class="container">
                    <div class="navbar-header">
                         <div class="crypto_logo_main">

                                       
                                        <p class="swiper-slider-heading raleway font_600 " style="    color: #ffcb4c;" >LTF  </p>

                                    </div>
                        <!--<a class="navbar-brand yellow_logo_crypto" href="#"><img src="images/logo_crypto.png" alt="logo"></a>-->
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse  ">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="https://cvctravel.tech/OTS/travel.html" >Home</a></li>
                            <li><a href="https://cvctravel.tech/OTS/Aboutus/about.html">About</a></li>
                            <li><a href="https://cvctravel.tech/OTS/visitor.html" >Visitors</a></li>
                            <li><a href="https://cvctravel.tech/OTS/Exhaibitur.html" >Exhibitors</a></li>
                            <li><a href="https://cvctravel.tech/OTS/news.html" >News</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="https://cvctravel.tech/OTS/login.html"></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <div class="sidebar_menu">
                    <nav class="pushmenu pushmenu-right">
                          <p class="swiper-slider-heading raleway font_600 "  style="color:#ffb600;">LTF  </p>
                        <ul class="push_nav centered">
                            <li class="clearfix">
                                <a href="https://cvctravel.tech/OTS/travel.html"><span>01.</span>Home</a>

                            </li>
                            <li class="clearfix">
                                <a href="https://cvctravel.tech/OTS/Aboutus/about.html"> <span>02.</span>About</a>

                            </li>
                            <li class="clearfix">
                                <a href="https://cvctravel.tech/OTS/visitor.html" > <span>03.</span>Visitors</a>

                            </li>

                            <li class="clearfix">
                                <a href="https://cvctravel.tech/OTS/Exhaibitur.html"> <span>04.</span>Exhibitors</a>

                            </li>
                            <li class="clearfix">
                                <a href="https://cvctravel.tech/OTS/news.html" > <span>05.</span>News</a>

                            </li>
                            <li class="clearfix">
                                <a href="#"> <span>06.</span>Contact</a>

                            </li>
                            <li class="clearfix">
                                <a href="https://cvctravel.tech/OTS/login.html"> <span>06.</span>login</a>

                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <ul class="social_icon black top25 bottom20 list-inline">

                            <li><a href="#" ><i class="fa fa-fw fa-facebook"></i></a></li>
                            <li><a href="#" class="navy_blue twitter"><i class="fa fa-fw fa-twitter"></i></a></li>
                            <li><a href="#" class="navy_blue pinterest"><i class="fa fa-fw fa fa-pinterest"></i></a></li>
                            <li><a href="#" class="navy_blue linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                        </ul>
                    </nav>
                </div>
            </div>
        </nav>

        <!-- /Navbar Section -->
</header>

   

      
</section>

<div class="owl-carousel hero-slide owl-style owl-loaded owl-drag">

        

        


      <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-4557px, 0px, 0px); transition: all 1s ease 0s; width: 9116px;"><div class="owl-item cloned" style="width: 1519.2px;"><div class="site-section">
          <div class="container">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div class="img-bg" style="background-image: url('images/big_img_1.jpg')"></div>
              <div class="contents">
                <span class="caption">Editor's Pick</span>
                <h2><a href="#">News Needs to Meet Its Audiences Where They Are</a></h2>
                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?</p>
                
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">Food</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">•</span> 3 min read <span class="icon-star2"></span></span>
                </div>

              </div>
            </div>
          </div>
        </div></div><div class="owl-item cloned" style="width: 1519.2px;"><div class="site-section">
          <div class="container">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div class="img-bg" style="background-image: url('images/big_img_1.jpg')"></div>
              <div class="contents">
                <span class="caption">Editor's Pick</span>
                <h2><a href="#">News Needs to Meet Its Audiences Where They Are</a></h2>
                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?</p>
                
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">Food</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">•</span> 3 min read <span class="icon-star2"></span></span>
                </div>

              </div>
            </div>
          </div>
        </div></div><div class="owl-item" style="width: 1519.2px;"><div class="site-section">
          <div class="container">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div class="img-bg" style="background-image: url('images/big_img_1.jpg')"></div>
              <div class="contents">
                <span class="caption">Editor's Pick</span>
                <h2><a href="#">News Needs to Meet Its Audiences Where They Are</a></h2>
                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?</p>
                
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">Food</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">•</span> 3 min read <span class="icon-star2"></span></span>
                </div>

              </div>
            </div>
          </div>
        </div></div><div class="owl-item active" style="width: 1519.2px;"><div class="site-section">
          <div class="container">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div class="img-bg" style="background-image: url('images/big_img_1.jpg')"></div>
              <div class="contents">
                <span class="caption">Editor's Pick</span>
                <h2><a href="#">News Needs to Meet Its Audiences Where They Are</a></h2>
                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?</p>
                
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">Food</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">•</span> 3 min read <span class="icon-star2"></span></span>
                </div>

              </div>
            </div>
          </div>
        </div></div><div class="owl-item cloned" style="width: 1519.2px;"><div class="site-section">
          <div class="container">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div class="img-bg" style="background-image: url('images/big_img_1.jpg')"></div>
              <div class="contents">
                <span class="caption">Editor's Pick</span>
                <h2><a href="#">News Needs to Meet Its Audiences Where They Are</a></h2>
                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?</p>
                
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">Food</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">•</span> 3 min read <span class="icon-star2"></span></span>
                </div>

              </div>
            </div>
          </div>
        </div></div><div class="owl-item cloned" style="width: 1519.2px;"><div class="site-section">
          <div class="container">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div class="img-bg" style="background-image: url('images/big_img_1.jpg')"></div>
              <div class="contents">
                <span class="caption">Editor's Pick</span>
                <h2><a href="#">News Needs to Meet Its Audiences Where They Are</a></h2>
                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?</p>
                
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">Food</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">•</span> 3 min read <span class="icon-star2"></span></span>
                </div>

              </div>
            </div>
          </div>
        </div></div></div></div><div class="owl-nav"><div class="owl-prev"><span class="icon-arrow_back"></span></div><div class="owl-next"><span class="icon-arrow_forward"></span></div></div><div class="owl-dots"><div class="owl-dot"><span></span></div><div class="owl-dot active"><span></span></div></div></div>
    
    <!-- /Parent Section Ended -->

<!-- jQuery 2.2.0-->


<script src="{{ asset('Frontend/js/jquery.js') }}"></script>

<!-- Google Map Api -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U" type="text/javascript"></script>
<script src="{{ asset('Frontend/js/map.js') }}" type="text/javascript"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{ asset('Frontend/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/jquery.themepunch.revolution.min.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{ asset('Frontend/js/actions.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/video.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('Frontend/js/jquery.downCount.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl Carousel 2 Core JavaScript -->
<script src="{{ asset('Frontend/js/owl.carousel.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.animate.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.autoheight.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.autoplay.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.autorefresh.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.hash.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.lazyload.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.navigation.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.support.js') }}"></script>
<script src="{{ asset('Frontend/js/owl.video.js') }}"></script>

<!-- Fancy Box Javacript -->
<script src="{{ asset('Frontend/js/jquery.fancybox.js')}}"></script>

<!-- Wow Js -->
<script src="{{ asset('Frontend/js/wow.min.js') }}"></script>

<!-- Appear Js-->
<script src="{{ asset('Frontend/js/jquery.appear.js') }}"></script>

<!-- Countdown Js -->
<script src="{{ asset('Frontend/js/jquery.countdown.js') }}"></script>

<!-- Parallax Js -->
<script src="{{ asset('Frontend/js/parallax.min.js') }}"></script>

<!-- Particles Core Js -->
<script src="{{ asset('Frontend/js/particles.js') }}"></script>

<!-- Cube Portfolio Core JavaScript -->
<script src="{{ asset('Frontend/js/jquery.cubeportfolio.min.js') }}"></script>

<!-- Circliful Core JavaScript -->
<script src="{{ asset('Frontend/js/jquery.circliful.min.js') }}"></script>

<!-- Swiper Slider Core JavaScript -->
<script src="{{ asset('Frontend/js/swiper.min.js') }}"></script>

<!-- Crypto Currency Core Javascript -->
<script src="cryptocurrency-assets/js/script.js"></script>

<!-- Custom JavaScript -->
<script src="{{ asset('Frontend/js/script.js') }}"></script>



</body></html>