
@extends('layouts.admin')
@section('content')
<style>
.card-pricing.popular {
    z-index: 1;
    border: 3px solid #007bff;
}
.card-pricing .list-unstyled li {
    padding: .5rem 0;
    color: #6c757d;
}
.card-deck {
    display:inline-flex !important;
}
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="card">
    <div class="card-header">
        {{ trans('global.rates.title_singular') }} {{ trans('global.list') }}
           <hr>
      <div class="container mb-5 mt-5">
    <div class="pricing card-deck flex-column flex-md-row mb-3">
        <!--<div class="card card-pricing text-center px-3 mb-4">-->
        <!--    <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Starter</span>-->
        <!--    <div class="bg-transparent card-header pt-4 border-0">-->
        <!--        <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15">$<span class="price">3</span><span class="h6 text-muted ml-2">/ per month</span></h1>-->
        <!--    </div>-->
        <!--    <div class="card-body pt-0">-->
        <!--        <ul class="list-unstyled mb-4">-->
        <!--            <li>Up to 5 users</li>-->
        <!--            <li>Basic support on Github</li>-->
        <!--            <li>Monthly updates</li>-->
        <!--            <li>Free cancelation</li>-->
        <!--        </ul>-->
        <!--        <a class="btn btn-outline-secondary mb-3" href="{{ route('admin.forms.index') }}">Order now</a>-->
                <!--<button type="button" class=""></button>-->
        <!--    </div>-->
        <!--</div>-->
        
        
        
        @if(Auth::user()->status == 0)

        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30">$<span class="price">10</span><span class="h6 text-muted ml-2"></span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li style="font-size:16px;color:red"><strike>$ 100</strike></li>
                    <li>Unlimited Appointment</li>
                    <li>Unlimited Exhibitor Search</li>
                  
                </ul>
                <a class="btn btn-primary mb-3" target="_blank" href="{{ route('admin.forms.index') }}">Order now</a>
                <!--<a href="https://www.totoprayogo.com" target="_blank" class="">Order Now</a>-->
            </div>
        </div>
        
        
                <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="45">$<span class="price">100</span><span class="h6 text-muted ml-2"></span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li style="font-size:16px;color:red"><strike>$ 500</strike></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <a class="btn btn-primary mb-3" href="{{ route('admin.forms.index') }}">Order now</a>
               
            </div>
        </div>
@endif        




        
@if(Auth::user()->status == 1)

        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30">$<span class="price">10</span><span class="h6 text-muted ml-2"></span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li style="font-size:16px;color:red"><strike>$ 100</strike></li>
                    <li>Unlimited Appointment</li>
                    <li>Unlimited Exhibitor Search</li>
                  
                </ul>
                <a class="btn btn-primary mb-3" target="_blank" href="{{ route('admin.forms.index') }}">Order now</a>
                <!--<a href="https://www.totoprayogo.com" target="_blank" class="">Order Now</a>-->
            </div>
        </div>
@endif        
        
        
@if(Auth::user()->status == 3)

        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30">$<span class="price">10</span><span class="h6 text-muted ml-2"></span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li style="font-size:16px;color:red"><strike>$ 100</strike></li>
                    <li>Unlimited Appointment</li>
                    <li>Unlimited Exhibitor Search</li>
                  
                </ul>
                <a class="btn btn-primary mb-3" target="_blank" href="{{ route('admin.forms.index') }}">Order now</a>
                <!--<a href="https://www.totoprayogo.com" target="_blank" class="">Order Now</a>-->
            </div>
        </div>
@endif        


@if(Auth::user()->status == 5)

        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30">$<span class="price">10</span><span class="h6 text-muted ml-2"></span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li style="font-size:16px;color:red"><strike>$ 100</strike></li>
                    <li>Unlimited Appointment</li>
                    <li>Unlimited Exhibitor Search</li>
                  
                </ul>
                <a class="btn btn-primary mb-3" target="_blank" href="{{ route('admin.forms.index') }}">Order now</a>
                <!--<a href="https://www.totoprayogo.com" target="_blank" class="">Order Now</a>-->
            </div>
        </div>
@endif        

        
  @if(Auth::user()->status == 4)
      
        
        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Professional</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="45">$<span class="price">100</span><span class="h6 text-muted ml-2"></span></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li style="font-size:16px;color:red"><strike>$ 500</strike></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <a class="btn btn-primary mb-3" href="{{ route('admin.forms.index') }}">Order now</a>
               
            </div>
        </div>
        
        @endif
        <!--<div class="card card-pricing text-center px-3 mb-4">-->
        <!--    <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">Enterprise</span>-->
        <!--    <div class="bg-transparent card-header pt-4 border-0">-->
        <!--        <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="60">$<span class="price">12</span><span class="h6 text-muted ml-2">/ per month</span></h1>-->
        <!--    </div>-->
        <!--    <div class="card-body pt-0">-->
        <!--        <ul class="list-unstyled mb-4">-->
        <!--            <li>Up to 5 users</li>-->
        <!--            <li>Basic support on Github</li>-->
        <!--            <li>Monthly updates</li>-->
        <!--            <li>Free cancelation</li>-->
        <!--        </ul>-->
        <!--        <a class="btn btn-outline-secondary mb-3" href="{{ route('admin.forms.index') }}">Order now</a>-->
                <!--<button type="button" class=""></button>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
</div>


@endsection