@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.librarys.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.librarys.fields.name') }}
                    </th>
                    <td>
                        {{ $library->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.librarys.fields.video') }}
                    </th>
                    <td>
                        {{ $library->video }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection