@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.fans.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.fans.fields.name') }}
                    </th>
                    <td>
                        {{ $fan->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Designation
                    </th>
                    <td>
                        {!! $fan->designation !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Organization
                    </th>
                    <td>
                        {!! $fan->organization !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Title
                    </th>
                    <td>
                        {!! $fan->title !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Email
                    </th>
                    <td>
                        {!! $fan->email !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Address_Line
                    </th>
                    <td>
                        {!! $fan->address_Line !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        City
                    </th>
                    <td>
                        {!! $fan->city !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        State
                    </th>
                    <td>
                        {!! $fan->state !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Zip_Code
                    </th>
                    <td>
                        {!! $fan->zip_code !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Province
                    </th>
                    <td>
                        {!! $fan->province !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Country
                    </th>
                    <td>
                        {!! $fan->country !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Phone
                    </th>
                    <td>
                        {!! $fan->phone !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Fax
                    </th>
                    <td>
                        {!! $fan->fax !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Website
                    </th>
                    <td>
                        {!! $fan->website !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        How many meetings does your company plan annually?
                    </th>
                    <td>
                        {!! $fan->q1 !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Name of meeting you could bring to Albuquerque*
                    </th>
                    <td>
                        {!! $fan->q2 !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Month and year of meeting or conference*
                    </th>
                    <td>
                        {!! $fan->q3 !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Number of rooms on peak*
                    </th>
                    <td>
                        {!! $fan->q4 !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Total room nights or estimate*
                    </th>
                    <td>
                        {!! $fan->q5 !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Net sq.ft. of exhibit space needed
                    </th>
                    <td>
                        {!! $fan->q6 !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        Other comments or requests
                    </th>
                    <td>
                        {!! $fan->q7 !!}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.fans.fields.created_at') }}
                    </th>
                    <td>
                         {{ $fan->created_at ?? '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection