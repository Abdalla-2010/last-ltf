@extends('layouts.admin')
@section('content')
   
<div class="card">
    <div class="card-header">
        {{ trans('global.about.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.about.fields.name') }}
                        </th>
                        <th>
                            {{ trans('global.about.fields.body') }}
                        </th>

                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($abouts as $key => $about)
                        <tr data-entry-id="{{ $about->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $about->name ?? '' }}
                            </td>
                            <td>
                                {{ $about->body ?? '' }}
                            </td>
                            <td>
                                @can('about_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.abouts.show', $about->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('about_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.abouts.edit', $about->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.abouts.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection