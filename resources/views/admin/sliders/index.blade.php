@extends('layouts.admin')
@section('content')
@can('meta_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.metas.create") }}">
                {{ trans('global.add') }} {{ trans('global.metas.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('global.metas.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.sliders.fields.image') }}
                        </th>
                         <th>
                            {{ trans('global.sliders.fields.title') }}
                        </th>
                        <th>
                            {{ trans('global.sliders.fields.content') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($sliders as $key => $slider)
                        <tr data-entry-id="{{ $slider->id }}">
                            <td>

                            </td>
                            <td>
                                <img src="{{$slider->image}}" style="width: 100px; height: 100px;">
                            </td>
                            <td>
                                {{ $slider->title ?? '' }}
                            </td>
                            <td>
                                {{ $slider->content ?? '' }}
                            </td>
                            <td>
                                @can('slider_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.sliders.edit', $slider->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
