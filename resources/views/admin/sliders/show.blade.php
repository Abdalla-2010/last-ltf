@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.metas.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.metas.fields.name') }}
                    </th>
                    <td>
                        {{ $meta->title }}
                    </td>
                </tr>
                 <tr>
                    <th>
                        {{ trans('global.metas.fields.group') }}
                    </th>
                    <td>
                        {{ $meta->group }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.metas.fields.description') }}
                    </th>
                    <td>
                        {!! $meta->body !!}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection