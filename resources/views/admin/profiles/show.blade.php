@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.show') }} {{ trans('global.user.title') }}
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <th>
                        {{ trans('global.user.fields.type') }}
                    </th>
                    <td>
                        {{ $user->type }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.name') }}
                    </th>
                    <td>
                        {{ $user->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.last-name') }}
                    </th>
                    <td>
                        {{ $user->last_name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.email') }}
                    </th>
                    <td>
                        {{ $user->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.phone') }}
                    </th>
                    <td>
                        {{ $user->phone }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.imgPersonal') }}
                    </th>
                    <td>
                        <img src="{{ asset($user->imgPersonal) }}" style="height: 50px; width: 100px">
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.company') }}
                    </th>
                    <td>
                        {{ $user->company }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.company-email') }}
                    </th>
                    <td>
                        {{ $user->company_email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.imgCompany') }}
                    </th>
                    <td>
                        <img src="{{asset( $user->imgCompany) }}" style="height: 50px; width: 100px">
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.website') }}
                    </th>
                    <td>
                        <a href="{{ $user->website }}" target="_blank">{{$user->website}}</a>
                    </td>
                </tr>
                @if($user->facebook != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.facebook') }}
                        </th>
                        <td>
                            <a href="{{ $user->facebook }}" target="_blank">Facebook</a>
                        </td>
                    </tr>
                @endif
                @if($user->linkedin != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.linkedin') }}
                        </th>
                        <td>
                            <a href="{{ $user->linkedin }}" target="_blank">LinkedIn</a>
                        </td>
                    </tr>
                @endif
                @if($user->twitter != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.twitter') }}
                        </th>
                        <td>
                            <a href="{{ $user->twitter }}" target="_blank">Twitter</a>
                        </td>
                    </tr>
                @endif
                @if($user->instgram != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.instgram') }}
                        </th>
                        <td>
                            <a href="{{ $user->instgram }}" target="_blank">Instagram</a>
                        </td>
                    </tr>
                @endif
                <tr>
                    <th>
                        {{ trans('global.user.fields.designation') }}
                    </th>
                    <td>
                        {{ $user->designation }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.zip-code') }}
                    </th>
                    <td>
                        {{ $user->zip_code }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.country') }}
                    </th>
                    <td>
                        {{ $user->country }}
                    </td>
                </tr>
                @if($user->travel != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.travel') }}
                        </th>

                        <td>
                            <ul>

                                @foreach($user->travel as $travel)
                                    <li>{{$travel}}</li>
                                @endforeach

                            </ul>
                        </td>
                    </tr>
                @endif
                @if($user->languages != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.languages') }}
                        </th>
                        <td>
                            <ul>
                                @foreach($user->languages as $language)
                                    <li>{{$language}}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endif
                @if($user->destination != null)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.destination') }}
                        </th>
                        <td>
                            <ul>
                                @foreach($user->destination as $destination)
                                    <li>{{$destination}}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endif
                @if($user->payment_status == 1)
                    <tr>
                        <th>
                            {{ trans('global.user.fields.company_profile') }}
                        </th>
                        <td>
                            @if(!empty($user->company_profile))
                                <form id="download" action="{{route('admin.companyProfile.download', $user->id)}}"
                                      method="POST"
                                      style="display: inline-block;">
                                    @csrf
                                    <a href="#"
                                       onclick="event.preventDefault(); document.getElementById('download').submit();">Download
                                        Company Profile</a>
                                </form>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('global.user.fields.brochure') }}
                        </th>
                        <td>
                            <img src="{{asset( $user->brochure) }}" style="height: 50px; width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('global.user.fields.youtubeLink') }}
                        </th>
                        <td class="text-sm-center">
                            @php $videoId = getYoutubeEmbedUrl($user->youtubeLink) @endphp

                            @if($videoId)
                                <iframe width="50%" height="300"
                                        src="https://www.youtube-nocookie.com/embed/{{$videoId}}"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            @endif
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection
