@extends('layouts.admin')

@section('content')



    <div class="card">

        <div class="card-header">

            {{ trans('global.edit') }} {{ trans('global.about.title_singular') }}

        </div>


        <div class="card-body">

            <form action="{{route('admin.update.profile')}}" method="POST" enctype="multipart/form-data">
                <div>
                    @if($errors)
                        @foreach($errors as $error)
                            <p>{{$error->firs()}}</p>
                        @endforeach
                    @endif
                </div>
                @csrf

                @method('patch')

                <div class="container">

                    <div class="row" style="margin-top:20px">


                        <div class="col-md-6">


                            <div class="form-group">


                                <label for="first">First Name</label>


                                <input type="text" class="form-control" placeholder="" id="name"

                                       name="name" value="{{ $user->name }}"

                                       required>


                            </div>


                        </div>


                        <!--  col-md-6   -->


                        <div class="col-md-6">


                            <div class="form-group">


                                <label for="last">Last Name</label>


                                <input type="text" class="form-control" placeholder="" id="last"

                                       name="last_name" value="{{$user->last_name}}" required>


                            </div>


                        </div>


                        <!--  col-md-6   -->


                    </div>


                    <div class="row">


                        <div class="col-md-6">


                            <div class="form-group">


                                <label for="company">Company Name</label>


                                <input type="text" class="form-control" placeholder=" Company Name"

                                       id="company" name="company" value="{{$user->company}}" required>


                            </div>


                        </div>


                        <!--  col-md-6   -->


                        <div class="col-md-6">


                            <div class="form-group">


                                <label for="phone"> Cell Phone </label>


                                <br>


                                <input type="tel" id="phone" name="phone" class="form-control phone"

                                       aria-describedby="" placeholder=""

                                       value="{{$user->phone}}" required>


                            </div>


                        </div>


                        <!--  col-md-6   -->


                        <!--  row   -->


                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="email">Email address</label>


                                <input type="email" class="form-control" id="email" name="email"

                                       value="{{ $user->email}}"

                                       placeholder="company email">

                            </div>

                        </div>


                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="url">Your Website <small>Please include http:// or

                                        https://</small></label>

                                <input type="url" class="form-control" id="url" placeholder="url"

                                       name="website" value="{{$user->website}}">

                            </div>

                        </div>


                        <!--  col-md-6   -->


                        <div class="col-md-6">


                            <label for=""> Country </label>


                            <select name="country" class="form-control" required>

                                <option

                                    {{$user->country == 'United States' ? 'selected' : ''}} value="United States">

                                    United States

                                </option>


                                <option {{$user->country == 'Canada' ? 'selected' : ''}}value="Canada">Canada

                                </option>


                                <option

                                    {{$user->country == 'Afghanistan' ? 'selected' : ''}}value="Afghanistan">

                                    Afghanistan

                                </option>


                                <option

                                    {{$user->country == 'Aland Islands' ? 'selected' : ''}}value="Aland Islands">

                                    Aland Islands

                                </option>


                                <option {{$user->country == 'Albania' ? 'selected' : ''}} value="Albania">

                                    Albania

                                </option>


                                <option {{$user->country == 'Algeria' ? 'selected' : ''}} value="Algeria">

                                    Algeria

                                </option>


                                <option

                                    {{$user->country == 'American Samoa' ? 'selected' : ''}} value="American Samoa">

                                    American Samoa

                                </option>


                                <option {{$user->country == 'Andorra' ? 'selected' : ''}}value="Andorra">

                                    Andorra

                                </option>


                                <option {{$user->country == 'Angola' ? 'selected' : ''}} value="Angola">Angola

                                </option>


                                <option {{$user->country == 'Anguilla' ? 'selected' : ''}}value="Anguilla">

                                    Anguilla

                                </option>


                                <option {{$user->country == 'Antarctica' ? 'selected' : ''}}value="Antarctica">

                                    Antarctica

                                </option>


                                <option {{$user->country == 'Antigua' ? 'selected' : ''}}value="Antigua">

                                    Antigua

                                </option>


                                <option

                                    {{$user->country == 'Antigua and Barbuda' ? 'selected' : ''}}value="Antigua and Barbuda">

                                    Antigua and Barbuda

                                </option>


                                <option {{$user->country == 'Argentina' ? 'selected' : ''}}value="Argentina">

                                    Argentina

                                </option>


                                <option {{$user->country == 'Armenia' ? 'selected' : ''}}value="Armenia">

                                    Armenia

                                </option>


                                <option {{$user->country == 'Aruba' ? 'selected' : ''}}value="Aruba">Aruba

                                </option>


                                <option {{$user->country == 'Australia' ? 'selected' : ''}}value="Australia">

                                    Australia

                                </option>


                                <option {{$user->country == 'Austria' ? 'selected' : ''}}value="Austria">

                                    Austria

                                </option>


                                <option {{$user->country == 'Azerbaijan' ? 'selected' : ''}}value="Azerbaijan">

                                    Azerbaijan

                                </option>


                                <option {{$user->country == 'Bahamas' ? 'selected' : ''}}value="Bahamas">

                                    Bahamas

                                </option>


                                <option {{$user->country == 'Bahrain' ? 'selected' : ''}}value="Bahrain">

                                    Bahrain

                                </option>


                                <option {{$user->country == 'Bangladesh' ? 'selected' : ''}}value="Bangladesh">

                                    Bangladesh

                                </option>


                                <option {{$user->country == 'Barbados' ? 'selected' : ''}}value="Barbados">

                                    Barbados

                                </option>


                                <option {{$user->country == 'Belarus' ? 'selected' : ''}}value="Belarus">

                                    Belarus

                                </option>


                                <option {{$user->country == 'Belgium' ? 'selected' : ''}}value="Belgium">

                                    Belgium

                                </option>


                                <option {{$user->country == 'Belize' ? 'selected' : ''}}value="Belize">Belize

                                </option>


                                <option {{$user->country == 'Benin' ? 'selected' : ''}}value="Benin">Benin

                                </option>


                                <option {{$user->country == 'Bermuda' ? 'selected' : ''}}value="Bermuda">

                                    Bermuda

                                </option>


                                <option {{$user->country == 'Bharain' ? 'selected' : ''}}value="Bharain">

                                    Bharain

                                </option>


                                <option {{$user->country == 'Bhutan' ? 'selected' : ''}}value="Bhutan">Bhutan

                                </option>


                                <option {{$user->country == 'Bolivia' ? 'selected' : ''}}value="Bolivia">

                                    Bolivia

                                </option>


                                <option {{$user->country == 'Bonaire' ? 'selected' : ''}}value="Bonaire">

                                    Bonaire

                                </option>


                                <option

                                    {{$user->country == 'Bosnia and Herzegovina' ? 'selected' : ''}}value="Bosnia and Herzegovina">

                                    Bosnia and Herzegovina

                                </option>


                                <option {{$user->country == 'Botswana' ? 'selected' : ''}}value="Botswana">

                                    Botswana

                                </option>


                                <option

                                    {{$user->country == 'Bouvet Island' ? 'selected' : ''}}value="Bouvet Island">

                                    Bouvet Island

                                </option>


                                <option {{$user->country == 'Brazil' ? 'selected' : ''}}value="Brazil">Brazil

                                </option>


                                <option

                                    {{$user->country == 'British Indian Ocean Terr' ? 'selected' : ''}}value="British Indian Ocean Terr">

                                    British Indian Ocean

                                    Terr

                                </option>


                                <option

                                    {{$user->country == 'British Virgin Islands' ? 'selected' : ''}}value="British Virgin Islands">

                                    British Virgin Islands

                                </option>


                                <option {{$user->country == 'Brunei' ? 'selected' : ''}}value="Brunei">Brunei

                                </option>


                                <option

                                    {{$user->country == 'Brunei Darussalam' ? 'selected' : ''}}value="Brunei Darussalam">

                                    Brunei Darussalam

                                </option>


                                <option {{$user->country == 'Bulgaria' ? 'selected' : ''}}value="Bulgaria">

                                    Bulgaria

                                </option>


                                <option

                                    {{$user->country == 'Burkina Faso' ? 'selected' : ''}}value="Burkina Faso">

                                    Burkina Faso

                                </option>


                                <option {{$user->country == 'Burma' ? 'selected' : ''}}value="Burma">Burma

                                </option>


                                <option {{$user->country == 'Burundi' ? 'selected' : ''}}value="Burundi">

                                    Burundi

                                </option>


                                <option {{$user->country == 'Cambodia' ? 'selected' : ''}}value="Cambodia">

                                    Cambodia

                                </option>


                                <option {{$user->country == 'Cameroon' ? 'selected' : ''}}value="Cameroon">

                                    Cameroon

                                </option>


                                <option {{$user->country == 'Canada' ? 'selected' : ''}}value="Canada">Canada

                                </option>


                                <option

                                    {{$user->country == 'Canary Islands' ? 'selected' : ''}}value="Canary Islands">

                                    Canary Islands

                                </option>


                                <option {{$user->country == 'Cape Verde' ? 'selected' : ''}}value="Cape Verde">

                                    Cape Verde

                                </option>


                                <option

                                    {{$user->country == 'Cayman Islands' ? 'selected' : ''}}value="Cayman Islands">

                                    Cayman Islands

                                </option>


                                <option

                                    {{$user->country == 'Central African Republic' ? 'selected' : ''}}value="Central African Republic">

                                    Central African Republic

                                </option>


                                <option {{$user->country == 'Chad' ? 'selected' : ''}}value="Chad">Chad</option>


                                <option {{$user->country == 'Chile' ? 'selected' : ''}}value="Chile">Chile

                                </option>


                                <option {{$user->country == 'China' ? 'selected' : ''}}value="China">China

                                </option>


                                <option

                                    {{$user->country == 'Chinese Taipei' ? 'selected' : ''}}value="Chinese Taipei">

                                    Chinese Taipei

                                </option>


                                <option

                                    {{$user->country == 'Christmas Island' ? 'selected' : ''}}value="Christmas Island">

                                    Christmas Island

                                </option>


                                <option

                                    {{$user->country == 'Cocos (Keeling) Islands' ? 'selected' : ''}}value="Cocos (Keeling) Islands">

                                    Cocos (Keeling) Islands

                                </option>


                                <option {{$user->country == 'Colombia' ? 'selected' : ''}}value="Colombia">

                                    Colombia

                                </option>


                                <option {{$user->country == 'Comoros' ? 'selected' : ''}}value="Comoros">

                                    Comoros

                                </option>


                                <option {{$user->country == 'Congo' ? 'selected' : ''}}value="Congo">Congo

                                </option>


                                <option

                                    {{$user->country == 'Congo, The Dem Rep Of The' ? 'selected' : ''}}value="Congo, The Dem Rep Of The">

                                    Congo, The Dem Rep Of

                                    The

                                </option>


                                <option

                                    {{$user->country == 'Cook Islands' ? 'selected' : ''}}value="Cook Islands">

                                    Cook Islands

                                </option>


                                <option {{$user->country == 'Costa Rica' ? 'selected' : ''}}value="Costa Rica">

                                    Costa Rica

                                </option>


                                <option

                                    {{$user->country == "Cote D'Ivoire" ? 'selected' : ''}}value="Cote D'Ivoire">

                                    Cote D'Ivoire

                                </option>


                                <option {{$user->country == 'Croatia' ? 'selected' : ''}}value="Croatia">

                                    Croatia

                                </option>


                                <option {{$user->country == 'Cuba' ? 'selected' : ''}}value="Cuba">Cuba</option>


                                <option {{$user->country == 'Curacao' ? 'selected' : ''}} value="Curacao">

                                    Curacao

                                </option>


                                <option {{$user->country == 'Cyprus' ? 'selected' : ''}}value="Cyprus">Cyprus

                                </option>


                                <option

                                    {{$user->country == 'Czech Republic' ? 'selected' : ''}}value="Czech Republic">

                                    Czech Republic

                                </option>


                                <option {{$user->country == 'Denmark' ? 'selected' : ''}}value="Denmark">

                                    Denmark

                                </option>


                                <option {{$user->country == 'Djibouti' ? 'selected' : ''}}value="Djibouti">

                                    Djibouti

                                </option>


                                <option {{$user->country == 'Dominica' ? 'selected' : ''}}value="Dominica">

                                    Dominica

                                </option>


                                <option

                                    {{$user->country == 'Dominican Republic' ? 'selected' : ''}}value="Dominican Republic">

                                    Dominican Republic

                                </option>


                                <option {{$user->country == 'Ecuador' ? 'selected' : ''}}value="Ecuador">

                                    Ecuador

                                </option>


                                <option {{$user->country == 'Egypt' ? 'selected' : ''}} value="Egypt">Egypt

                                </option>


                                <option

                                    {{$user->country == 'El Salvador' ? 'selected' : ''}}value="El Salvador">El

                                    Salvador

                                </option>


                                <option

                                    {{$user->country == 'Equatorial Guinea' ? 'selected' : ''}}value="Equatorial Guinea">

                                    Equatorial Guinea

                                </option>


                                <option {{$user->country == 'Eritrea' ? 'selected' : ''}}value="Eritrea">

                                    Eritrea

                                </option>


                                <option {{$user->country == 'Estonia' ? 'selected' : ''}}value="Estonia">

                                    Estonia

                                </option>


                                <option {{$user->country == 'Ethiopia' ? 'selected' : ''}}value="Ethiopia">

                                    Ethiopia

                                </option>


                                <option

                                    {{$user->country == 'Falkland Is (Malvinas)' ? 'selected' : ''}}value="Falkland Is (Malvinas)">

                                    Falkland Is (Malvinas)

                                </option>


                                <option

                                    {{$user->country == 'Faroe Islands' ? 'selected' : ''}}value="Faroe Islands">

                                    Faroe Islands

                                </option>


                                <option {{$user->country == 'Fiji' ? 'selected' : ''}}value="Fiji">Fiji</option>


                                <option {{$user->country == 'Finland' ? 'selected' : ''}}value="Finland">

                                    Finland

                                </option>


                                <option {{$user->country == 'France' ? 'selected' : ''}}value="France">France

                                </option>


                                <option

                                    {{$user->country == 'French Guiana' ? 'selected' : ''}}value="French Guiana">

                                    French Guiana

                                </option>


                                <option

                                    {{$user->country == 'French Polynesia' ? 'selected' : ''}}value="French Polynesia">

                                    French Polynesia

                                </option>


                                <option

                                    {{$user->country == 'French Southern Terr' ? 'selected' : ''}}value="French Southern Terr">

                                    French Southern Terr

                                </option>


                                <option {{$user->country == 'Gabon' ? 'selected' : ''}} value="Gabon">Gabon

                                </option>


                                <option {{$user->country == 'Gambia' ? 'selected' : ''}}value="Gambia">Gambia

                                </option>


                                <option {{$user->country == 'Georgia' ? 'selected' : ''}}value="Georgia">

                                    Georgia

                                </option>


                                <option {{$user->country == 'Germany' ? 'selected' : ''}}value="Germany">

                                    Germany

                                </option>


                                <option {{$user->country == 'Ghana' ? 'selected' : ''}}value="Ghana">Ghana

                                </option>


                                <option {{$user->country == 'Gibraltar' ? 'selected' : ''}}value="Gibraltar">

                                    Gibraltar

                                </option>


                                <option {{$user->country == 'Greece' ? 'selected' : ''}}value="Greece">Greece

                                </option>


                                <option {{$user->country == 'Greenland' ? 'selected' : ''}}value="Greenland">

                                    Greenland

                                </option>


                                <option {{$user->country == 'Grenada' ? 'selected' : ''}}value="Grenada">

                                    Grenada

                                </option>


                                <option {{$user->country == 'Guadeloupe' ? 'selected' : ''}}value="Guadeloupe">

                                    Guadeloupe

                                </option>


                                <option {{$user->country == 'Guam' ? 'selected' : ''}}value="Guam">Guam</option>


                                <option {{$user->country == 'Guatemala' ? 'selected' : ''}}value="Guatemala">

                                    Guatemala

                                </option>


                                <option {{$user->country == 'Guinea' ? 'selected' : ''}}value="Guinea">Guinea

                                </option>


                                <option

                                    {{$user->country == 'Guinea-Bissau' ? 'selected' : ''}}value="Guinea-Bissau">

                                    Guinea-Bissau

                                </option>


                                <option {{$user->country == 'Guyana' ? 'selected' : ''}}value="Guyana">Guyana

                                </option>


                                <option {{$user->country == 'Haiti' ? 'selected' : ''}}value="Haiti">Haiti

                                </option>


                                <option

                                    {{$user->country == 'Heard Is and McDonald Is' ? 'selected' : ''}}value="Heard Is and McDonald Is">

                                    Heard Is and McDonald Is

                                </option>


                                <option

                                    {{$user->country == 'Holy See (Vatican City' ? 'selected' : ''}}value="Holy See (Vatican City)">

                                    Holy See (Vatican City)

                                </option>


                                <option {{$user->country == 'Honduras' ? 'selected' : ''}}value="Honduras">

                                    Honduras

                                </option>


                                <option {{$user->country == 'Hong Kong' ? 'selected' : ''}}value="Hong Kong">

                                    Hong Kong

                                </option>


                                <option

                                    {{$user->country == 'Hong Kong, China' ? 'selected' : ''}}value="Hong Kong, China">

                                    Hong Kong, China

                                </option>


                                <option {{$user->country == 'Hungary' ? 'selected' : ''}}value="Hungary">

                                    Hungary

                                </option>


                                <option {{$user->country == 'Iceland' ? 'selected' : ''}}value="Iceland">

                                    Iceland

                                </option>


                                <option {{$user->country == 'India' ? 'selected' : ''}}value="India">India

                                </option>


                                <option {{$user->country == 'Indonesia' ? 'selected' : ''}}value="Indonesia">

                                    Indonesia

                                </option>


                                <option {{$user->country == 'Iran' ? 'selected' : ''}}value="Iran">Iran</option>


                                <option

                                    {{$user->country == 'Iran, Islamic Republic of' ? 'selected' : ''}}value="Iran, Islamic Republic of">

                                    Iran, Islamic Republic

                                    of

                                </option>


                                <option {{$user->country == 'Iraq' ? 'selected' : ''}}value="Iraq">Iraq</option>


                                <option {{$user->country == 'Ireland' ? 'selected' : ''}}value="Ireland">

                                    Ireland

                                </option>


                                <option {{$user->country == 'Israel' ? 'selected' : ''}}value="Israel">Israel

                                </option>


                                <option {{$user->country == 'Italy' ? 'selected' : ''}}value="Italy">Italy

                                </option>


                                <option {{$user->country == 'Jamaica' ? 'selected' : ''}}value="Jamaica">

                                    Jamaica

                                </option>


                                <option {{$user->country == 'Japan' ? 'selected' : ''}}value="Japan">Japan

                                </option>


                                <option {{$user->country == 'Jordan' ? 'selected' : ''}}value="Jordan">Jordan

                                </option>


                                <option {{$user->country == 'Kazakhstan' ? 'selected' : ''}}value="Kazakhstan">

                                    Kazakhstan

                                </option>


                                <option {{$user->country == 'Kazakstan' ? 'selected' : ''}}value="Kazakstan">

                                    Kazakstan

                                </option>


                                <option {{$user->country == 'Kenya' ? 'selected' : ''}}value="Kenya">Kenya

                                </option>


                                <option {{$user->country == 'Kiribati' ? 'selected' : ''}}value="Kiribati">

                                    Kiribati

                                </option>


                                <option

                                    {{$user->country == "Korea, Dem People's Rep" ? 'selected' : ''}}value="Korea, Dem People's Rep">

                                    Korea, Dem People's Rep

                                </option>


                                <option

                                    {{$user->country == 'Korea, Rebublic Of' ? 'selected' : ''}}value="Korea, Rebublic Of">

                                    Korea, Rebublic Of

                                </option>


                                <option

                                    {{$user->country == 'Korea, Republic of' ? 'selected' : ''}}value="Korea, Republic of">

                                    Korea, Republic of

                                </option>


                                <option {{$user->country == 'Kosovo' ? 'selected' : ''}}value="Kosovo">Kosovo

                                </option>


                                <option {{$user->country == 'Kuwait' ? 'selected' : ''}}value="Kuwait">Kuwait

                                </option>


                                <option {{$user->country == 'Kyrgyzstan' ? 'selected' : ''}}value="Kyrgyzstan">

                                    Kyrgyzstan

                                </option>


                                <option

                                    {{$user->country == "Lao People's Dem Republic" ? 'selected' : ''}}value="Lao People's Dem Republic">

                                    Lao People's Dem

                                    Republic

                                </option>


                                <option {{$user->country == 'Laos' ? 'selected' : ''}}value="Laos">Laos</option>


                                <option {{$user->country == 'Latvia' ? 'selected' : ''}} value="Latvia">Latvia

                                </option>


                                <option {{$user->country == 'Lebanon' ? 'selected' : ''}}value="Lebanon">

                                    Lebanon

                                </option>


                                <option {{$user->country == 'Lesotho' ? 'selected' : ''}}value="Lesotho">

                                    Lesotho

                                </option>


                                <option {{$user->country == 'Liberia' ? 'selected' : ''}}value="Liberia">

                                    Liberia

                                </option>


                                <option {{$user->country == 'Libya' ? 'selected' : ''}}value="Libya">Libya

                                </option>


                                <option

                                    {{$user->country == 'Libyan Arab Jamahiriya' ? 'selected' : ''}}value="Libyan Arab Jamahiriya">

                                    Libyan Arab Jamahiriya

                                </option>


                                <option

                                    {{$user->country == 'Liechtenstein' ? 'selected' : ''}}value="Liechtenstein">

                                    Liechtenstein

                                </option>


                                <option {{$user->country == 'Lithuania' ? 'selected' : ''}}value="Lithuania">

                                    Lithuania

                                </option>


                                <option {{$user->country == 'Luxembourg' ? 'selected' : ''}}value="Luxembourg">

                                    Luxembourg

                                </option>


                                <option {{$user->country == 'Macao' ? 'selected' : ''}}value="Macao">Macao

                                </option>


                                <option

                                    {{$user->country == 'Macau, China' ? 'selected' : ''}}value="Macau, China">

                                    Macau, China

                                </option>


                                <option {{$user->country == 'Macedonia' ? 'selected' : ''}}value="Macedonia">

                                    Macedonia

                                </option>


                                <option {{$user->country == 'Madagascar' ? 'selected' : ''}}value="Madagascar">

                                    Madagascar

                                </option>


                                <option {{$user->country == 'Malawi' ? 'selected' : ''}}value="Malawi">Malawi

                                </option>


                                <option {{$user->country == 'Malaysia' ? 'selected' : ''}}value="Malaysia">

                                    Malaysia

                                </option>


                                <option {{$user->country == 'Maldives' ? 'selected' : ''}}value="Maldives">

                                    Maldives

                                </option>


                                <option {{$user->country == 'Mali' ? 'selected' : ''}}value="Mali">Mali</option>


                                <option {{$user->country == 'Malta' ? 'selected' : ''}}value="Malta">Malta

                                </option>


                                <option

                                    {{$user->country == 'Marshall' ? 'selected' : ''}}value="Marshall Islands">

                                    Marshall Islands

                                </option>


                                <option {{$user->country == 'Martinique' ? 'selected' : ''}}value="Martinique">

                                    Martinique

                                </option>


                                <option {{$user->country == 'Mauritania' ? 'selected' : ''}}value="Mauritania">

                                    Mauritania

                                </option>


                                <option {{$user->country == 'Mauritius' ? 'selected' : ''}}value="Mauritius">

                                    Mauritius

                                </option>


                                <option {{$user->country == 'Mayotte' ? 'selected' : ''}}value="Mayotte">

                                    Mayotte

                                </option>


                                <option {{$user->country == 'Mexico' ? 'selected' : ''}}value="Mexico">Mexico

                                </option>


                                <option

                                    {{$user->country == 'Micronesia, Fed States Of' ? 'selected' : ''}}value="Micronesia, Fed States Of">

                                    Micronesia, Fed States

                                    Of

                                </option>


                                <option {{$user->country == 'Moldova' ? 'selected' : ''}}value="Moldova">

                                    Moldova

                                </option>


                                <option

                                    {{$user->country == 'Moldova, Republic Of' ? 'selected' : ''}}value="Moldova, Republic Of">

                                    Moldova, Republic Of

                                </option>


                                <option {{$user->country == 'Monaco' ? 'selected' : ''}}value="Monaco">Monaco

                                </option>


                                <option {{$user->country == 'Mongolia' ? 'selected' : ''}}value="Mongolia">

                                    Mongolia

                                </option>


                                <option {{$user->country == 'Montenegro' ? 'selected' : ''}}value="Montenegro">

                                    Montenegro

                                </option>


                                <option {{$user->country == 'Montserrat' ? 'selected' : ''}}value="Montserrat">

                                    Montserrat

                                </option>


                                <option {{$user->country == 'Morocco' ? 'selected' : ''}}value="Morocco">

                                    Morocco

                                </option>


                                <option {{$user->country == 'Mozambique' ? 'selected' : ''}}value="Mozambique">

                                    Mozambique

                                </option>


                                <option {{$user->country == 'Myanmar' ? 'selected' : ''}}value="Myanmar">

                                    Myanmar

                                </option>


                                <option {{$user->country == 'Namibia' ? 'selected' : ''}}value="Namibia">

                                    Namibia

                                </option>


                                <option {{$user->country == 'Nauru' ? 'selected' : ''}}value="Nauru">Nauru

                                </option>


                                <option {{$user->country == 'Nepal' ? 'selected' : ''}}value="Nepal">Nepal

                                </option>


                                <option

                                    {{$user->country == 'Netherlands' ? 'selected' : ''}}value="Netherlands">

                                    Netherlands

                                </option>


                                <option

                                    {{$user->country == 'Netherlands Antilles' ? 'selected' : ''}}value="Netherlands Antilles">

                                    Netherlands Antilles

                                </option>


                                <option

                                    {{$user->country == 'New Caledonia' ? 'selected' : ''}}value="New Caledonia">

                                    New Caledonia

                                </option>


                                <option {{$user->country == 'New Guinea' ? 'selected' : ''}}value="New Guinea">

                                    New Guinea

                                </option>


                                <option

                                    {{$user->country == 'New Zealand' ? 'selected' : ''}}value="New Zealand">New

                                    Zealand

                                </option>


                                <option {{$user->country == 'Nicaragua' ? 'selected' : ''}}value="Nicaragua">

                                    Nicaragua

                                </option>


                                <option {{$user->country == 'Niger' ? 'selected' : ''}}value="Niger">Niger

                                </option>


                                <option {{$user->country == 'Nigeria' ? 'selected' : ''}}value="Nigeria">

                                    Nigeria

                                </option>


                                <option {{$user->country == 'Niue' ? 'selected' : ''}}value="Niue">Niue</option>


                                <option

                                    {{$user->country == 'Norfolk Island' ? 'selected' : ''}}value="Norfolk Island">

                                    Norfolk Island

                                </option>


                                <option

                                    {{$user->country == 'North Cyprus' ? 'selected' : ''}}value="North Cyprus">

                                    North Cyprus

                                </option>


                                <option

                                    {{$user->country == 'Northern Mariana Islands' ? 'selected' : ''}}value="Northern Mariana Islands">

                                    Northern Mariana Islands

                                </option>


                                <option {{$user->country == 'Norway' ? 'selected' : ''}}value="Norway">Norway

                                </option>


                                <option {{$user->country == 'Oman' ? 'selected' : ''}} value="Oman">Oman

                                </option>


                                <option {{$user->country == 'Pakistan' ? 'selected' : ''}}value="Pakistan">

                                    Pakistan

                                </option>


                                <option {{$user->country == 'Palau' ? 'selected' : ''}}value="Palau">Palau

                                </option>


                                <option {{$user->country == 'Palestine' ? 'selected' : ''}}value="Palestine">

                                    Palestine

                                </option>


                                <option

                                    {{$user->country == 'Palestinian Territory' ? 'selected' : ''}}value="Palestinian Territory">

                                    Palestinian Territory

                                </option>


                                <option {{$user->country == 'Panama' ? 'selected' : ''}}value="Panama">Panama

                                </option>


                                <option

                                    {{$user->country == 'Papua New Guinea' ? 'selected' : ''}}value="Papua New Guinea">

                                    Papua New Guinea

                                </option>


                                <option {{$user->country == 'Paraguay' ? 'selected' : ''}}value="Paraguay">

                                    Paraguay

                                </option>


                                <option

                                    {{$user->country == 'Peoples Republic of Congo' ? 'selected' : ''}}value="Peoples Republic of Congo">

                                    Peoples Republic of

                                    Congo

                                </option>


                                <option {{$user->country == 'Peru' ? 'selected' : ''}}value="Peru">Peru</option>


                                <option

                                    {{$user->country == 'Philippines' ? 'selected' : ''}}value="Philippines">

                                    Philippines

                                </option>


                                <option {{$user->country == 'Pitcairn' ? 'selected' : ''}}value="Pitcairn">

                                    Pitcairn

                                </option>


                                <option {{$user->country == 'Poland' ? 'selected' : ''}}value="Poland">Poland

                                </option>


                                <option {{$user->country == 'Portugal' ? 'selected' : ''}}value="Portugal">

                                    Portugal

                                </option>


                                <option

                                    {{$user->country == 'Puerto Rico' ? 'selected' : ''}}value="Puerto Rico">

                                    Puerto Rico

                                </option>


                                <option {{$user->country == 'Qatar' ? 'selected' : ''}}value="Qatar">Qatar

                                </option>


                                <option {{$user->country == 'Reunion' ? 'selected' : ''}}value="Reunion">

                                    Reunion

                                </option>


                                <option {{$user->country == 'Romania' ? 'selected' : ''}}value="Romania">

                                    Romania

                                </option>


                                <option {{$user->country == 'Russia' ? 'selected' : ''}}value="Russia">Russia

                                </option>


                                <option

                                    {{$user->country == 'Russian Federation' ? 'selected' : ''}}value="Russian Federation">

                                    Russian Federation

                                </option>


                                <option {{$user->country == 'Rwanda' ? 'selected' : ''}}value="Rwanda">Rwanda

                                </option>


                                <option

                                    {{$user->country == 'Saint Barthelemy' ? 'selected' : ''}}value="Saint Barthelemy">

                                    Saint Barthelemy

                                </option>


                                <option

                                    {{$user->country == 'Saint Eustatius' ? 'selected' : ''}}value="Saint Eustatius">

                                    Saint Eustatius

                                </option>


                                <option

                                    {{$user->country == 'Saint Helena' ? 'selected' : ''}}value="Saint Helena">

                                    Saint Helena

                                </option>


                                <option

                                    {{$user->country == 'Saint Kitts &amp; Nevis' ? 'selected' : ''}}value="Saint Kitts &amp; Nevis">

                                    Saint Kitts &amp; Nevis

                                </option>


                                <option

                                    {{$user->country == 'Saint Kitts and Nevis' ? 'selected' : ''}}value="Saint Kitts and Nevis">

                                    Saint Kitts and Nevis

                                </option>


                                <option

                                    {{$user->country == 'Saint Lucia' ? 'selected' : ''}}value="Saint Lucia">

                                    Saint Lucia

                                </option>


                                <option

                                    {{$user->country == 'Saint Martin' ? 'selected' : ''}}value="Saint Martin">

                                    Saint Martin

                                </option>


                                <option

                                    {{$user->country == 'Saint Pierre and Miquelon' ? 'selected' : ''}}value="Saint Pierre and Miquelon">

                                    Saint Pierre and

                                    Miquelon

                                </option>


                                <option

                                    {{$user->country == 'Saint Vincent' ? 'selected' : ''}}value="Saint Vincent">

                                    Saint Vincent

                                </option>


                                <option {{$user->country == 'Samoa' ? 'selected' : ''}}value="Samoa">Samoa

                                </option>


                                <option {{$user->country == 'San Marino' ? 'selected' : ''}}value="San Marino">

                                    San Marino

                                </option>


                                <option

                                    {{$user->country == 'Sao Tome and Principe' ? 'selected' : ''}}value="Sao Tome and Principe">

                                    Sao Tome and Principe

                                </option>


                                <option

                                    {{$user->country == 'Saudi Arabia' ? 'selected' : ''}} value="Saudi Arabia">

                                    Saudi Arabia

                                </option>


                                <option {{$user->country == 'Senegal' ? 'selected' : ''}}value="Senegal">

                                    Senegal

                                </option>


                                <option {{$user->country == 'Serbia' ? 'selected' : ''}}value="Serbia">Serbia

                                </option>


                                <option

                                    {{$user->country == 'Serbia and Montenegro' ? 'selected' : ''}}value="Serbia and Montenegro">

                                    Serbia and Montenegro

                                </option>


                                <option {{$user->country == 'Seychelles' ? 'selected' : ''}}value="Seychelles">

                                    Seychelles

                                </option>


                                <option

                                    {{$user->country == 'Sierra Leone' ? 'selected' : ''}}value="Sierra Leone">

                                    Sierra Leone

                                </option>


                                <option {{$user->country == 'Singapore' ? 'selected' : ''}}value="Singapore">

                                    Singapore

                                </option>


                                <option

                                    {{$user->country == 'Sint Maarten' ? 'selected' : ''}}value="Sint Maarten">

                                    Sint Maarten

                                </option>


                                <option

                                    {{$user->country == 'Slovak Republic' ? 'selected' : ''}}value="Slovak Republic">

                                    Slovak Republic

                                </option>


                                <option {{$user->country == 'Slovakia' ? 'selected' : ''}}value="Slovakia">

                                    Slovakia

                                </option>


                                <option {{$user->country == 'Slovenia' ? 'selected' : ''}}value="Slovenia">

                                    Slovenia

                                </option>


                                <option

                                    {{$user->country == 'Solomon Islands' ? 'selected' : ''}}value="Solomon Islands">

                                    Solomon Islands

                                </option>


                                <option {{$user->country == 'Somalia' ? 'selected' : ''}}value="Somalia">

                                    Somalia

                                </option>


                                <option

                                    {{$user->country == 'South Africa' ? 'selected' : ''}}value="South Africa">

                                    South Africa

                                </option>


                                <option

                                    {{$user->country == 'South Georgia and the SSI' ? 'selected' : ''}}value="South Georgia and the SSI">

                                    South Georgia and the

                                    SSI

                                </option>


                                <option {{$user->country == 'Spain' ? 'selected' : ''}}value="Spain">Spain

                                </option>


                                <option {{$user->country == 'Sri Lanka' ? 'selected' : ''}}value="Sri Lanka">Sri

                                    Lanka

                                </option>


                                <option

                                    {{$user->country == 'St Vincent and Grenadines' ? 'selected' : ''}}value="St Vincent and Grenadines">

                                    St Vincent and

                                    Grenadines

                                </option>


                                <option {{$user->country == 'Sudan' ? 'selected' : ''}}value="Sudan">Sudan

                                </option>


                                <option {{$user->country == 'Suriname' ? 'selected' : ''}}value="Suriname">

                                    Suriname

                                </option>


                                <option

                                    {{$user->country == 'Svalbard and Jan Mayen' ? 'selected' : ''}}value="Svalbard and Jan Mayen">

                                    Svalbard and Jan Mayen

                                </option>


                                <option {{$user->country == 'Swaziland' ? 'selected' : ''}}value="Swaziland">

                                    Swaziland

                                </option>


                                <option {{$user->country == 'Sweden' ? 'selected' : ''}}value="Sweden">Sweden

                                </option>


                                <option

                                    {{$user->country == 'Switzerland' ? 'selected' : ''}}value="Switzerland">

                                    Switzerland

                                </option>


                                <option {{$user->country == 'Syria' ? 'selected' : ''}}value="Syria">Syria

                                </option>


                                <option

                                    {{$user->country == 'Syrian Arab Republic' ? 'selected' : ''}}value="Syrian Arab Republic">

                                    Syrian Arab Republic

                                </option>


                                <option {{$user->country == 'Tahiti' ? 'selected' : ''}}value="Tahiti">Tahiti

                                </option>


                                <option {{$user->country == 'Taiwan' ? 'selected' : ''}} value="Taiwan">Taiwan

                                </option>


                                <option

                                    {{$user->country == 'Taiwan, Province of China' ? 'selected' : ''}}value="Taiwan, Province of China">

                                    Taiwan, Province of

                                    China

                                </option>


                                <option {{$user->country == 'Tajikistan' ? 'selected' : ''}}value="Tajikistan">

                                    Tajikistan

                                </option>


                                <option {{$user->country == 'Tanzania' ? 'selected' : ''}}value="Tanzania">

                                    Tanzania

                                </option>


                                <option

                                    {{$user->country == 'Tanzania, United Republic' ? 'selected' : ''}}value="Tanzania, United Republic">

                                    Tanzania, United

                                    Republic

                                </option>


                                <option {{$user->country == 'Thailand' ? 'selected' : ''}}value="Thailand">

                                    Thailand

                                </option>


                                <option

                                    {{$user->country == 'Timor-Leste' ? 'selected' : ''}}value="Timor-Leste">

                                    Timor-Leste

                                </option>


                                <option {{$user->country == 'Togo' ? 'selected' : ''}}value="Togo">Togo</option>


                                <option {{$user->country == 'Tokelau' ? 'selected' : ''}}value="Tokelau">

                                    Tokelau

                                </option>


                                <option {{$user->country == 'Tonga' ? 'selected' : ''}}value="Tonga">Tonga

                                </option>


                                <option

                                    {{$user->country == 'Trinidad' ? 'selected' : ''}}value="Trinidad &amp; Tobago">

                                    Trinidad &amp; Tobago

                                </option>


                                <option

                                    {{$user->country == 'Trinidad and Tobago' ? 'selected' : ''}}value="Trinidad and Tobago">

                                    Trinidad and Tobago

                                </option>


                                <option {{$user->country == 'Tunisia' ? 'selected' : ''}}value="Tunisia">

                                    Tunisia

                                </option>


                                <option {{$user->country == 'Turkey' ? 'selected' : ''}}value="Turkey">Turkey

                                </option>


                                <option

                                    {{$user->country == 'Turkmenistan' ? 'selected' : ''}}value="Turkmenistan">

                                    Turkmenistan

                                </option>


                                <option

                                    {{$user->country == 'Turks &amp; Caicos' ? 'selected' : ''}} value="Turks &amp; Caicos">

                                    Turks &amp; Caicos

                                </option>


                                <option

                                    {{$user->country == 'Turks and Caicos Islands' ? 'selected' : ''}}value="Turks and Caicos Islands">

                                    Turks and Caicos Islands

                                </option>


                                <option {{$user->country == 'Tuvalu' ? 'selected' : ''}}value="Tuvalu">Tuvalu

                                </option>


                                <option {{$user->country == 'UAE' ? 'selected' : ''}}value="UAE">UAE</option>


                                <option {{$user->country == 'Uganda' ? 'selected' : ''}}value="Uganda">Uganda

                                </option>


                                <option {{$user->country == 'Ukraine' ? 'selected' : ''}}value="Ukraine">

                                    Ukraine

                                </option>


                                <option

                                    {{$user->country == 'United Arab Emirates' ? 'selected' : ''}}value="United Arab Emirates">

                                    United Arab Emirates

                                </option>


                                <option

                                    {{$user->country == 'United Kingdom' ? 'selected' : ''}}value="United Kingdom">

                                    United Kingdom

                                </option>


                                <option

                                    {{$user->country == 'United States' ? 'selected' : ''}}value="United States">

                                    United States

                                </option>


                                <option {{$user->country == 'Uruguay' ? 'selected' : ''}}value="Uruguay">

                                    Uruguay

                                </option>


                                <option

                                    {{$user->country == 'US Minor Outlying Islands' ? 'selected' : ''}}value="US Minor Outlying Islands">

                                    US Minor Outlying

                                    Islands

                                </option>


                                <option {{$user->country == 'Uzbekistan' ? 'selected' : ''}}value="Uzbekistan">

                                    Uzbekistan

                                </option>


                                <option {{$user->country == 'Vanuatu' ? 'selected' : ''}}value="Vanuatu">

                                    Vanuatu

                                </option>


                                <option {{$user->country == 'Venezuela' ? 'selected' : ''}}value="Venezuela">

                                    Venezuela

                                </option>


                                <option {{$user->country == 'Viet Nam' ? 'selected' : ''}}value="Viet Nam">Viet

                                    Nam

                                </option>


                                <option {{$user->country == 'Vietnam' ? 'selected' : ''}}value="Vietnam">

                                    Vietnam

                                </option>


                                <option

                                    {{$user->country == 'Virgin Islands, British' ? 'selected' : ''}}value="Virgin Islands, British">

                                    Virgin Islands, British

                                </option>


                                <option

                                    {{$user->country == 'Virgin Islands, US' ? 'selected' : ''}}value="Virgin Islands, US">

                                    Virgin Islands, US

                                </option>


                                <option

                                    {{$user->country == 'Wallis and Futuna' ? 'selected' : ''}}value="Wallis and Futuna">

                                    Wallis and Futuna

                                </option>


                                <option

                                    {{$user->country == 'Western Sahara' ? 'selected' : ''}} value="Western Sahara">

                                    Western Sahara

                                </option>


                                <option {{$user->country == 'Yemen' ? 'selected' : ''}}value="Yemen">Yemen

                                </option>


                                <option {{$user->country == 'Yugoslavia' ? 'selected' : ''}}value="Yugoslavia">

                                    Yugoslavia

                                </option>


                                <option {{$user->country == 'Zambia' ? 'selected' : ''}}value="Zambia">Zambia

                                </option>


                                <option {{$user->country == 'Zimbabwe' ? 'selected' : ''}}value="Zimbabwe">

                                    Zimbabwe

                                </option>


                            </select>


                        </div>


                        <div class="col-md-6">


                            <label for="email">Zip code</label>


                            <input type="" class="form-control" placeholder="code" name="zip_code"

                                   value="{{$user->zip_code}}">

                        </div>


                        <div class="col-sm-12">

                            <br>

                        </div>


                        <div class="col-md-6">

                            <label for="">Title </label>

                            <select name="title" class="form-control">

                                <option {{$user->title == 'Mr' ? 'selected' : ''}}value="Mr"> Mr

                                </option>

                                <option {{$user->title == 'Mrs' ? 'selected' : ''}}value="Mrs">Mrs

                                </option>

                            </select>

                        </div>


                        <div class="col-md-6">

                            <label for="">Designation</label>

                            <input type="" class="form-control" id="" aria-describedby="emailHelp"

                                   placeholder=""

                                   name="designation" value="{{$user->designation}}" required>


                        </div>

                        <div class="col-md-6">

                            @if($user->status == 3)

                                <label for="exampleInputPassword1"> Organization Email</label>



                            @else

                                <label for="exampleInputPassword1"> Company Email</label>



                            @endif


                            <input type="" class="form-control" id="exampleInputPassword1"

                                   placeholder="Email"

                                   value="{{$user->company_email}}" name="company_email" required>

                        </div>


                        <div class="col-md-6">

                            @if($user->status == 3)

                                <label for="exampleInputPassword1"> Organization Email</label>



                            @else

                                <label for="exampleInputPassword1"> Company Email</label>



                            @endif


                            <input type="" class="form-control" id="exampleInputPassword1"

                                   placeholder="Email"

                                   value="{{$user->company_email}}" name="company_email" required>

                        </div>


                        <div class="col-md-6">

                            <label for=""> Company Type </label>

                            @if($user->status == 3)

                                <h3 style="color: #337ab7;">My Organization</h3>

                            @else

                                <h3 style="color: #337ab7;display:none">My Company</h3>

                            @endif


                            <select name="company_type" class="form-control" required>

                                <option value="InBound"> InBound</option>

                                <option value="OutBound">OutBound</option>

                                <option value="InBound / OutBound">InBound / OutBound</option>


                            </select>

                        </div>


                        <div class="col-md-6">

                            <label for="imgPersonal">Select Personal Image : </label>

                            <input type="file" class="form-control" id="imgPersonal"

                                   name="imgPersonal"

                                   accept="image/*" style="height:40px">

                        </div>


                        <div class="col-md-6">

                            @if($user->status == 3)

                                <label for="img">Select Organization Logo Image:</label>



                            @else

                                <label for="imgCompany">Select Company Logo Image:</label>



                            @endif


                            <input type="file" id="imgCompany" name="imgCompany"

                                   class="form-control"

                                   accept="image/*" style="height: 40px;">

                        </div>


                        <div class="col-md-6">

                            <label for=""> Facebook</label>

                            <input type="text" class="form-control" id=""

                                   aria-describedby="emailHelp"

                                   placeholder="Facebook link" name="facebook" value="{{$user->facebook}}" required>

                        </div>


                        <div class="col-md-6">

                            <label for=""> Twitter </label>

                            <input type="text" class="form-control" id=""

                                   aria-describedby="emailHelp"

                                   placeholder="Twitter link" name="twitter" value="{{$user->twitter}}">

                        </div>


                        <div class="col-md-6">

                            <label for=""> LinkedIn</label>

                            <input type="text" class="form-control" id=""

                                   aria-describedby="emailHelp"

                                   placeholder="Linkedin link" name="linkedin" value="{{$user->linkedin}}">

                        </div>


                        <div class="col-md-6">

                            <label for="">Instagram</label>

                            <input type="text" class="form-control" id=""

                                   aria-describedby="emailHelp"

                                   placeholder="Instagram link" name="instagram" value="{{$user->instagram}}">

                        </div>

                        @if(auth()->user()->payment_status == 1 && auth()->user()->status == 4)
                            <div class="col-md-6">

                                <label for="youtubeLink">Youtube Vidoe</label>

                                <input type="text" class="form-control" id=""

                                       aria-describedby="emailHelp"

                                       placeholder="Youtube Video Link" name="youtubeLink"
                                       value="{{!empty($user->youtubeLink) ? $user->youtubeLink : old('youtubeLink') }}">

                            </div>

                            <div class="col-md-6">

                                <label for="company_profile">Company Profile</label>

                                <input type="file" class="form-control" id=""

                                       aria-describedby="emailHelp"

                                       accept="application/pdf" name="company_profile">

                            </div>

                            <div class="col-md-6">

                                <label for="brochure">Brochure</label>

                                <input type="file" class="form-control" id=""

                                       aria-describedby="emailHelp"

                                       accept="image/*" name="brochure">

                            </div>
                        @endif


                        <div class="col-sm-12">

                            <br>

                        </div>

                        <div class="col-sm-12">

                            <table class="table table-striped">


                                <thead>


                                <tr>

                                    <th scope="col">Destination Specialization <br> <span

                                            style="font-size:12px; font-weight:200"> (select up to 15 )</span></th>


                                    <th scope="col">Travel Specialization <br> <span

                                            style="font-size:12px; font-weight:200"> (select up to 15 )</span></th>


                                    <th scope="col">Languages Known: <br> <span

                                            style="font-size:12px; font-weight:200"> (select up to 15 )</span>

                                    </th>


                                </tr>


                                </thead>


                                <tbody>


                                <tr>


                                    <td scope="row"><input type="checkbox"
                                                           @if(!empty($user->destination)) {{ in_array('Africa', $user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                                           name="destination[]"

                                                           value="Africa">

                                        <label for="vehicle1"> Africa</label></td>

                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Accessible/Special Needs',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Accessible/Special Needs">


                                        <label for="vehicle1"> Accessible/Special Needs


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Arabic',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Arabic">


                                        <label for="vehicle1"> Arabic</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Antarctica/Arctic Region',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Antarctica/Arctic Region">


                                        <label for="vehicle1"> Antarctica/Arctic Region</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Adoption',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Adoption">


                                        <label for="vehicle1"> Adoption</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Bengali',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Bengali">


                                        <label for="vehicle1"> Bengali


                                        </label></td>


                                </tr>


                                <tr>


                                    <td scope="row"><input type="checkbox"
                                                           @if(!empty($user->destination)) {{ in_array('Asia - Central Asia',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                                           name="destination[]"

                                                           value="Asia - Central Asia">


                                        <label for="vehicle1"> Asia - Central Asia</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Adventure Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Adventure Travel">


                                        <label for="vehicle1"> Adventure Travel


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Bengali',$user->languages) ? 'checked' : ''}}@endif  id="vehicle1"
                                               name="languages[]" value="Bulgarian">


                                        <label for="vehicle1"> Bulgarian </label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Asia - China, Japan, Korea Mongolia',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name=" destination[]"

                                               value="Asia - China, Japan, Korea Mongolia">


                                        <label for="vehicle1"> Asia - China, Japan, Korea Mongolia


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Airline',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Airline">


                                        <label for="vehicle1"> Airline</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Catalan',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Catalan">


                                        <label for="vehicle1"> Catalan</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Australia/New Zealand',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Australia/New Zealand">


                                        <label for="vehicle1"> Australia/New Zealand


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('All Inclusive',$user->travel) ? 'checked' : ''}}@endif  id="vehicle1"
                                               name="travel[]" value="All Inclusive">

                                        <label for="vehicle1"> All Inclusive

                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Chinese',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Chinese">


                                        <label for="vehicle1"> Chinese </label></td>


                                </tr>


                                <tr>


                                    <td scope="row"><input type="checkbox"
                                                           @if(!empty($user->destination)) {{ in_array('Canada',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                                           name="destination[]"

                                                           value="Canada">


                                        <label for="vehicle1"> Canada</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Amusement/Theme Parks',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Amusement/Theme Parks">


                                        <label for="vehicle1">Amusement/Theme Parks


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Croatian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Croatian">


                                        <label for="vehicle1"> Croatian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Caribbean',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Caribbean">


                                        <label for="vehicle1"> Caribbean</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Archeology',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Archeology">


                                        <label for="vehicle1">Archeology


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Danish',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Danish">


                                        <label for="vehicle1"> Danish</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)){{in_array('Croatia',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Croatia">


                                        <label for="vehicle1"> Croatia</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Art & Culture/Music',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Art & Culture/Music">


                                        <label for="vehicle1"> Art & Antiques


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Dutch',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Dutch">


                                        <label for="vehicle1"> Dutch</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Cuba',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Cuba">


                                        <label for="vehicle1"> Cuba</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Art & Culture/Music',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Art & Culture/Music">


                                        <label for="vehicle1"> Art & Culture/Music


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('English',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="English">


                                        <label for="vehicle1"> English</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Egypt',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Egypt">


                                        <label for="vehicle1"> Egypt</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('LTF Volunteer Responders',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="LTF Volunteer Responders">


                                        <label for="vehicle1">LTF Volunteer Responders


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Estonian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Estonian">


                                        <label for="vehicle1"> Estonian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('England - United Kingdom',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="England - United Kingdom">


                                        <label for="vehicle1"> England - United Kingdom


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)) {{in_array('Barge/Canal/River Cruises',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Barge/Canal/River Cruises">


                                        <label for="vehicle1"> Barge/Canal/River Cruises


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('French',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="French">


                                        <label for="vehicle1"> French</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Europe - Eastern',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Europe - Eastern">


                                        <label for="vehicle1"> Europe - Eastern


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Beach Vacations',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Beach Vacations">


                                        <label for="vehicle1"> Beach Vacations


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('German',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="German">


                                        <label for="vehicle1"> German</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Europe - Northern',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Europe - Northern">


                                        <label for="vehicle1"> Europe - Northern


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Boating/Yacht/Sailing',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Boating/Yacht/Sailing">


                                        <label for="vehicle1"> Boating/Yacht/Sailing


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Greek',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Greek">


                                        <label for="vehicle1"> Greek</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Europe - Western',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Europe - Western">


                                        <label for="vehicle1"> Europe - Western


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Budget Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Budget Travel">


                                        <label for="vehicle1"> Budget Travel


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Hebrew',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Hebrew">


                                        <label for="vehicle1"> Hebrew</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('France',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="France">


                                        <label for="vehicle1"> France


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Business Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Business Travel">


                                        <label for="vehicle1"> Business Travel


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Hindi',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Hindi">


                                        <label for="vehicle1"> Hindi</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Germany',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Germany">


                                        <label for="vehicle1"> Germany</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Camping/Hiking',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Camping/Hiking">


                                        <label for="vehicle1"> Camping/Hiking


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Hungarian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Hungarian">


                                        <label for="vehicle1"> Hungarian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Greece',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Greece">


                                        <label for="vehicle1"> Greece</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Castles/Villas',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Castles/Villas">


                                        <label for="vehicle1"> Castles/Villas


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Indonesian',$user->languages) ? 'checked' : ''}}@endif  id="vehicle1"
                                               name="languages[]" value="Indonesian">


                                        <label for="vehicle1"> Indonesian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('India',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="India">


                                        <label for="vehicle1"> India</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Corporate/Government',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Corporate/Government">


                                        <label for="vehicle1"> Corporate/Government


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Italian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Italian">


                                        <label for="vehicle1"> Italian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Israel',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Israel">


                                        <label for="vehicle1"> Israel</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Cruising - River',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Cruising - River">


                                        <label for="vehicle1"> Cruising - River


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Japanese',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Japanese">


                                        <label for="vehicle1"> Japanese</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)){{in_array('Italy',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Italy">


                                        <label for="vehicle1"> Italy</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Cruising/Cruise Lines',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Cruising/Cruise Lines">


                                        <label for="vehicle1">Cruising/Cruise Lines


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Korean',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Korean">


                                        <label for="vehicle1"> Korean</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Latin America & Mexico',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Latin America & Mexico">


                                        <label for="vehicle1"> Latin America & Mexico


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Culinary/Cooking/Gastronomy',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Culinary/Cooking/Gastronomy">


                                        <label for="vehicle1">Culinary/Cooking/Gastronomy


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Latvian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Latvian">


                                        <label for="vehicle1"> Latvian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Middle East',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Middle East">


                                        <label for="vehicle1"> Middle East


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Customized Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Customized Travel">


                                        <label for="vehicle1">Customized Travel


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Norwegian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Norwegian">


                                        <label for="vehicle1"> Norwegian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Pacific Islands',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="Pacific Islands">


                                        <label for="vehicle1"> Pacific Islands - Tahiti, Fiji, Bali, etc.


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Destination Weddings',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Destination Weddings">


                                        <label for="vehicle1"> Destination Weddings


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Polish',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Polish">


                                        <label for="vehicle1"> Polish</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Russia',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Russia">


                                        <label for="vehicle1"> Russia</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Disney',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Disney">


                                        <label for="vehicle1">Disney


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Portuguese',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Portuguese">


                                        <label for="vehicle1"> Portuguese</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('South America',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="South America">


                                        <label for="vehicle1"> South America


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Eco-Tourism',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Eco-Tourism">


                                        <label for="vehicle1"> Eco-Tourism


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Punjabi',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Punjabi">


                                        <label for="vehicle1"> Punjabi</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('Spain',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="Spain">


                                        <label for="vehicle1"> Spain</label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Educational',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Educational">


                                        <label for="vehicle1"> Educational


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Russian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Russian">


                                        <label for="vehicle1"> Russian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('U.S. - Alaska',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="U.S. - Alaska">


                                        <label for="vehicle1"> U.S. - Alaska


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Family Fun',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Family Fun">


                                        <label for="vehicle1"> Family Fun


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Serbian',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Serbian">


                                        <label for="vehicle1"> Serbian</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('U.S. - Florida',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="U.S. - Florida">


                                        <label for="vehicle1"> U.S. - Florida


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Family/Multi-generational',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Family/Multi-generational">


                                        <label for="vehicle1"> Family/Multi-generational


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Spanish',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Spanish">


                                        <label for="vehicle1"> Spanish</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('U.S. - Hawaii',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="U.S. - Hawaii">


                                        <label for="vehicle1"> U.S. - Hawaii


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Ferry/Riverboat',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Ferry/Riverboat">


                                        <label for="vehicle1"> Ferry/Riverboat


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Swedish',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Swedish">


                                        <label for="vehicle1"> Swedish</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('U.S. - Las Vegas',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="U.S. - Las Vegas">


                                        <label for="vehicle1"> U.S. - Las Vegas


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Honeymoon',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Honeymoon">


                                        <label for="vehicle1"> Honeymoon


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Tagalog',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Tagalog">


                                        <label for="vehicle1"> Tagalog</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('U.S. - Midwest',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]"

                                               value="U.S. - Midwest">


                                        <label for="vehicle1"> U.S. - Midwest


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Historical',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Historical">


                                        <label for="vehicle1"> Historical


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Turkish',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Turkish">


                                        <label for="vehicle1"> Turkish</label></td>


                                </tr>


                                <tr>


                                    <td><input type="checkbox"
                                               @if(!empty($user->destination)) {{ in_array('U.S. - West',$user->destination) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="destination[]" value="U.S. - West">


                                        <label for="vehicle1"> U.S. - West


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Incentive Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Incentive Travel">


                                        <label for="vehicle1"> Incentive Travel


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Urdu',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Urdu">


                                        <label for="vehicle1"> Urdu</label></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Incentive Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Incentive Travel">


                                        <label for="vehicle1"> Incentive Travel


                                        </label></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->languages)) {{ in_array('Vietnamese',$user->languages) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="languages[]" value="Vietnamese">


                                        <label for="vehicle1"> Vietnamese</label></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('LGBT',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="LGBT">


                                        <label for="vehicle1"> LGBT


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Lifestyle/Family/Specialty',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Lifestyle/Family/Specialty">


                                        <label for="vehicle1">Lifestyle/Family/Specialty


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Luxury Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Luxury Travel">


                                        <label for="vehicle1"> Luxury Travel


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Medical Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Medical Travel">


                                        <label for="vehicle1"> Medical Travel


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Incentive Travel',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Incentive Travel">


                                        <label for="vehicle1"> Incentive Travel


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Minority - African American',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Minority - African American">


                                        <label for="vehicle1"> Minority - African American


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Minority - Asian',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Minority - Asian">


                                        <label for="vehicle1"> Minority - Asian


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Motor coach/Bus',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Motor coach/Bus">


                                        <label for="vehicle1">Motor coach/Bus


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Nature',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Nature">


                                        <label for="vehicle1"> Nature


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Rafting',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Rafting">


                                        <label for="vehicle1"> Rafting


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Rail',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Rail">


                                        <label for="vehicle1"> Rail


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Religious/Faith/Spiritual',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]"

                                               value="Religious/Faith/Spiritual">


                                        <label for="vehicle1"> Religious/Faith/Spiritual


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Resorts',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Resorts">


                                        <label for="vehicle1"> Resorts


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Reunions',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Reunions">


                                        <label for="vehicle1"> Reunions


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Safari & Wildlife',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Safari & Wildlife">


                                        <label for="vehicle1"> Safari & Wildlife


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Singles',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Singles">


                                        <label for="vehicle1"> Singles


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array('Speciality',$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Speciality">


                                        <label for="vehicle1"> Speciality


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ (in_array('Sports/Exercise',$user->travel) ? 'checked' : '')}} @endif id="vehicle1"
                                               name="travel[]" value="Sports/Exercise">


                                        <label for="vehicle1"> Sports/Exercise


                                        </label></td>


                                    <td></td>


                                </tr>


                                <tr>


                                    <td></td>


                                    <td><input type="checkbox"
                                               @if(!empty($user->travel)){{ in_array("Women's Travel",$user->travel) ? 'checked' : ''}}@endif id="vehicle1"
                                               name="travel[]" value="Women's Travel">


                                        <label for="vehicle1"> Women's Travel


                                        </label></td>


                                    <td></td>


                                </tr>


                                </tbody>


                            </table>


                        </div>

                    </div>

                </div>

                <!--  col-md-6   -->


                <button type="submit" class="btn btn-primary" style="margin-top: 25px; float: left;

                         margin-right: 72px; width: 91px;float:right;"> Update

                </button>


            </form>

        </div>

    </div>



@endsection

