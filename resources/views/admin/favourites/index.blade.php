@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            {{ trans('global.favourites.title') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable">
                    <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.exhibitor.fields.company') }}
                        </th>

                        <th>
                            {{ trans('global.user.fields.name') }}
                        </th>
                        <th>
                            {{ trans('global.user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('global.user.fields.phone') }}
                        </th>
                        <th>
                            {{ trans('global.user.fields.country') }}
                        </th>
                        <th>
                            Control &nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($favourites as $favourite)
                        <tr data-entry-id="{{ $favourite->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $favourite->name.' '.$favourite->last_name ?? '' }}
                            </td>
                            <td>
                                {{ $favourite->company ?? '' }}
                            </td>

                            <td>
                                {{ $favourite->email ?? '' }}
                            </td>
                            <td>
                                {{ $favourite->phone ?? '' }}
                            </td>
                            <td>
                                {{ $favourite->country ?? '' }}
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary"
                                   href="{{ route('admin.favourites.show', $favourite->id) }}">
                                    {{ trans('global.view') }}
                                </a>
                                <form action="{{ route('admin.favourites.delete', ['id'=>$favourite->id]) }}" method="post"
                                      onsubmit="return confirm('{{ trans('global.areYouSure') }}');"
                                      style="display: inline-block;">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="btn btn-xs btn-danger"
                                           value="{{ trans('global.delete') }}">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@section('scripts')
    @parent
    <script>
        $(function () {
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('admin.favourites.massDestroy') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({selected: true}).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: {ids: ids, _method: 'DELETE'}
                        })
                            .done(function () {
                                location.reload()
                            })
                    }
                }
            }
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            dtButtons.push(deleteButton)
            $('.datatable:not(.ajaxTable)').DataTable({buttons: dtButtons})
        })

    </script>
@endsection
@endsection
