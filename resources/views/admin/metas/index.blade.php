@extends('layouts.admin')
@section('content')
@can('meta_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.metas.create") }}">
                {{ trans('global.add') }} {{ trans('global.metas.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('global.metas.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.metas.fields.group') }}
                        </th>
                        <th>
                            {{ trans('global.metas.fields.name') }}
                        </th>
                         <th>
                            {{ trans('global.metas.fields.keywords') }}
                        </th>
                        <th>
                            {{ trans('global.metas.fields.description') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($metas as $key => $meta)
                        <tr data-entry-id="{{ $meta->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $meta->group ?? '' }}
                            </td>
                            <td>
                                {{ $meta->title ?? '' }}
                            </td>
                            <td>
                                {{ $meta->key_words ?? '' }}
                            </td>
                            <td>
                                {{ $meta->body ?? '' }}
                            </td>
                            <td>
                                @can('meta_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.metas.show', $meta->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('meta_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.metas.edit', $meta->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('meta_delete')
                                    <form action="{{ route('admin.metas.destroy', $meta->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.metas.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('meta_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
