@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.metas.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.metas.update", [$meta->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                <label for="group">{{ trans('global.metas.fields.group') }}</label>
                <input type="text" disabled id="group" name="group" class="form-control" value="{{ old('group', isset($meta) ? $meta->group : '') }}">
                @if($errors->has('group'))
                    <em class="invalid-feedback">
                        {{ $errors->first('group') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.metas.fields.group_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('global.metas.fields.name') }}*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($meta) ? $meta->title : '') }}">
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.metas.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('key_words') ? 'has-error' : '' }}">
                <label for="body">{{ trans('global.metas.fields.keywords') }}</label>
                <input type="text" id="title" name="key_words" class="form-control" value="{{ old('key_words', isset($meta) ? $meta->key_words : '') }}">                @if($errors->has('key_words'))
                    <em class="invalid-feedback">
                        {{ $errors->first('key_words') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.metas.fields.keywords_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                <label for="body">{{ trans('global.metas.fields.description') }}</label>
                <textarea id="body" name="body" class="form-control ">{{ old('body', isset($meta) ? $meta->body : '') }}</textarea>
                @if($errors->has('body'))
                    <em class="invalid-feedback">
                        {{ $errors->first('body') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.metas.fields.description_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection
