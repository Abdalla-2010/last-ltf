<style>

    @media only screen and (max-width: 768px) {

        .card {

            width: 100% !important;

            overflow-x: scroll !important;

        }


    }

</style>

@extends('layouts.admin')

@section('content')



    <div class="card">

        <div class="card-header">

            {{ trans('global.results.title_singular') }} {{ trans('global.list') }}

        </div>

        <div class="container">

            <div class="result">

                <table class="table table-striped ">

                    <thead>

                    <tr>

                        <th></th>

                        <th scope="col">Company</th>

                        <th scope="col">Full Name</th>

                        <th scope="col">Title</th>

                        <th scope="col">Country</th>

                        <th scope="col">View Profile</th>

                        <th scope="col">Make Favourite</th>

                        <th scope="col">Appointment Booking</th>

                        1

                    </tr>

                    </thead>

                    <tbody>


                    @php $i = 1 @endphp

                    @forelse($companies as $company)

                        <tr>

                            <td>{{$i}}</td>

                            <td>{{$company->company}}</td>

                            <td>{{$company->name . ' '. $company->last_name}}</td>

                            <td>{{$company->title}}</td>

                            <td>{{$company->country}}</td>

                            <td><a href="{{route('admin.results.show.profile', $company->id)}}">Profile</a></td>

                            <td>

                                @php $exist = false @endphp

                                @foreach(Auth::user()->favourites as $favourite)

                                    @if($company->id == $favourite->id)

                                        @php $exist = true @endphp

                                        @break

                                    @else

                                        @php $exist = false @endphp

                                    @endif

                                @endforeach

                                @if($exist)

                                    <button type="submit" data-id="{{$company->id}}"

                                            title="remove from favourites list"

                                            class="remove heart btn btn-border">

                                        <i style="color: red" class="fas fa-heart"></i></button>

                                @else

                                    <button type="submit" data-id="{{$company->id}}" title="add to favourite list"

                                            class="add heart btn btn-border">

                                        <i style="color: red" class="far fa-heart"></i></button>

                                @endif

                            </td>


                            {{$i++}}


                            <td>
                                <a href="#" class="btn btn-success">Book an appointment</a>
                            </td>
                        </tr>

                    @empty

                        <th>

                            No Results

                        </th>

                    @endforelse

                    </tbody>

                </table>


            </div>


        </div>

    </div>



@endsection

@section('scripts')



    <script>

        $(function () {


            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $('.heart').click(function () {

                if ($(this).hasClass("add")) {

                    var company_id = $(this).data('id');

                    var btn = $(this);

                    $.ajax({

                        data: {company_id: company_id, _token: '{{csrf_token()}}'},

                        url: "{{ route('admin.favourites.add') }}",

                        type: "POST",

                        success: function (data) {

                            btn.removeClass('add');

                            btn.addClass('remove');

                            btn.attr('title', 'remove from favourites list');

                            btn.children().removeClass('far').addClass('fas');

                        },

                    });

                } else if ($(this).hasClass("remove")) {

                    var company_id = $(this).data('id');

                    var btn = $(this);

                    $.ajax({

                        type: "DELETE",

                        url: "{{ route('admin.favourites.add') }}" + '/destroy/' + company_id,

                        success: function (data) {

                            btn.removeClass('remove');

                            btn.attr('title', 'add to favourites list');

                            btn.addClass('add');

                            btn.children().removeClass('fas').addClass('far');

                        }

                    });

                }


            });

        });


    </script>

@endsection

