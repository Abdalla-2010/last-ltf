@extends('layouts.admin')
@section('content')
@can('user_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.myoffers.create') }}">
                {{ trans('global.add') }} {{ trans('global.myoffers.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('global.myoffers.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <!--<th>-->
                        <!--    {{ trans('global.myoffers.fields.userId') }}-->
                        <!--</th>-->
                        <th>
                            {{ trans('global.myoffers.fields.name') }}
                        </th>
                        <th>
                            {{ trans('global.myoffers.fields.img') }}
                        </th>
                        <th>
                            {{ trans('global.myoffers.fields.duration') }}
                        </th>

                        <th>
                            {{ trans('global.myoffers.fields.destination') }}
                        </th>

                        <th>
                            {{ trans('global.myoffers.fields.price') }}
                        </th>

                        <th>
                            {{ trans('global.myoffers.fields.url') }}
                        </th>
                        <th>
                            Control <!--&nbsp;-->
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($myoffers as $key => $myoffer)
                        <tr data-entry-id="{{ $myoffer->id }}">
                            <td>
                                
                            </td>
                            <!--<td>-->
                            <!--    {{ $users->name ?? '' }}-->
                            <!--</td>-->
                            <td>
                                {{ $myoffer->name ?? '' }}
                            </td>
                            <td>
                              <img src="{{ asset ('uploads/offers/'.$myoffer->img) ?? '' }}" width="100" alt=" {{ trans('global.myoffers.title_singular') }}"> 
                            </td>
                            <td>
                                {{ $myoffer->duration ?? '' }}
                            </td>

                            <td>
                                {{ $myoffer->destination ?? '' }}
                            </td>

                            <td>
                               $ {{ $myoffer->price ?? '' }}
                            </td>

                            <td>
                                <a href="{{ $myoffer->url ?? '' }}" target="_blank">{{ $myoffer->url ?? '' }}</a>
                            </td>
                            <td>
                                @can('myoffer_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.myoffers.show', $myoffer->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('myoffer_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.myoffers.edit', $myoffer->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('myoffer_delete')
                                    <form action="{{ route('admin.myoffers.destroy', $myoffer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.myoffers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('myoffer_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection