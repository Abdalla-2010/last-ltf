@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.myoffers.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.myoffers.fields.name') }}
                    </th>
                    <td>
                        {{  $myoffer->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.myoffers.fields.img') }}
                    </th>
                    <td>
                        <img src="{{ asset ('uploads/offers/'.$myoffer->img) ?? '' }}" width="100" alt=" {{ trans('global.myoffers.title_singular') }}"> 
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.myoffers.fields.duration') }}
                    </th>
                    <td>
                        {{  $myoffer->duration }}
                    </td>
                </tr>



                <tr>
                    <th>
                        {{ trans('global.myoffers.fields.destination') }}
                    </th>
                    <td>
                        {{  $myoffer->destination }}
                    </td>
                </tr>



                <tr>
                    <th>
                        {{ trans('global.myoffers.fields.price') }}
                    </th>
                    <td>
                        $ {{  $myoffer->price }}
                    </td>
                </tr>

                

                <tr>
                    <th>
                        {{ trans('global.myoffers.fields.url') }}
                    </th>
                    <td>
                        {{  $myoffer->url }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection