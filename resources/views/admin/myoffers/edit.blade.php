@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.myoffers.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route('admin.myoffers.update', [$myoffer->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('global.myoffers.fields.name') }}*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($myoffer) ? $myoffer->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.myoffers.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('img') ? 'has-error' : '' }}">
                <label for="img">{{ trans('global.myoffers.fields.img') }}*</label>
                <input type="file" id="img" name="img" class="form-control" value="{{ old('img', isset($myoffer) ? $myoffer->img : '') }}">
                @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.myoffers.fields.img_helper') }}
                </p>
            </div>


            <div class="form-group {{ $errors->has('duration') ? 'has-error' : '' }}">
                <label for="duration">{{ trans('global.myoffers.fields.duration') }}</label>
                <input type="text" id="duration" name="duration" class="form-control" value="{{ old('duration', isset($myoffer) ? $myoffer->duration : '') }}">
                @if($errors->has('duration'))
                    <em class="invalid-feedback">
                        {{ $errors->first('duration') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.myoffers.fields.duration_helper') }}
                </p>
            </div>



            <div class="form-group {{ $errors->has('destination') ? 'has-error' : '' }}">
                <label for="destination">{{ trans('global.myoffers.fields.destination') }}</label>
                <input type="text" id="destination" name="destination" class="form-control" value="{{ old('destination', isset($myoffer) ? $myoffer->destination : '') }}">
                @if($errors->has('destination'))
                    <em class="invalid-feedback">
                        {{ $errors->first('destination') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.myoffers.fields.destination_helper') }}
                </p>
            </div>



            <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                <label for="price">{{ trans('global.myoffers.fields.price') }}</label>
                <input type="text" id="price" name="price" class="form-control" value="{{ old('price', isset($myoffer) ? $myoffer->price : '') }}">
                @if($errors->has('price'))
                    <em class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.myoffers.fields.price_helper') }}
                </p>
            </div>



            <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                <label for="url">{{ trans('global.myoffers.fields.url') }}</label>
                <input type="text" id="url" name="url" class="form-control" value="{{ old('url', isset($myoffer) ? $myoffer->url : '') }}">
                @if($errors->has('url'))
                    <em class="invalid-feedback">
                        {{ $errors->first('url') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.myoffers.fields.url_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection