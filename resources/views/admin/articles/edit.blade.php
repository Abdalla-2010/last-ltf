@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.news.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.articles.update", [$article->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('global.news.fields.title') }}*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($article) ? $article->title : '') }}">
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.news.fields.title_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">{{ trans('global.news.fields.description') }}</label>
                <textarea id="description" name="description" class="form-control ">{{ old('description', isset($article) ? $article->description : '') }}</textarea>
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.news.fields.description_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                <label for="images">{{ trans('global.news.fields.images') }}</label>
                <input type="file" id="images" name="images" class="form-control" value="{{ old('images', isset($article) ? $article->images : '') }}">
                @if($errors->has('images'))
                    <em class="invalid-feedback">
                        {{ $errors->first('images') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.news.fields.images_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection