@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.news.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.news.fields.title') }}
                    </th>
                    <td>
                        {{ $article->title }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.news.fields.description') }}
                    </th>
                    <td>
                        {!! $article->description !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.news.fields.images') }}
                    </th>
                    <td>
                         <img src="{{ asset ('uploads/articles/'. $article->images) }}" width="100" alt=" {{ trans('global.news.title_singular') }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.news.fields.created_at') }}
                    </th>
                    <td>
                         {{ $article->created_at ?? '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection