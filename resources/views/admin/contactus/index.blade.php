@extends('layouts.admin')
@section('content')
   
<div class="card">
    <div class="card-header">
        {{ trans('global.contactus.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.contactus.fields.Fname') }}
                        </th>
                        <th>
                            {{ trans('global.contactus.fields.Lname') }}
                        </th>
                        <th>
                            {{ trans('global.contactus.fields.email') }}
                        </th>
                        <th>
                            {{ trans('global.contactus.fields.phone') }}
                        </th>
                        <th>
                            {{ trans('global.contactus.fields.msg') }}
                        </th>
                        <th>
                        {{ trans('global.contactus.fields.created_at') }}
                        </th>
                        <th>
                             Control<!--&nbsp;-->
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $key => $contact)
                        <tr data-entry-id="{{ $contact->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $contact->Fname ?? '' }}
                            </td>
                            <td>
                                {{ $contact->Lname ?? '' }}
                            </td>
                            <td>
                                {{ $contact->email ?? '' }}
                            </td>
                            <td>
                                {{ $contact->phone ?? '' }}
                            </td>
                            <td> 
                                @if (strlen($contact->massage) >= 50 )
                                
                                {{ substr(strip_tags($contact->massage),0,50) ?? ''}}
                                @can('contactus_show')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.contactus.show', $contact->id) }}">
                                        Read more..
                                    </a>
                                @endcan
                            
                            @else
                                {{ $contact->massage ?? '' }}
                            @endif
                            
                            </td>
                            <td>
                                {{ $contact->created_at ?? '' }}
                            </td>
                            <td>
                                @can('contactus_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.contactus.show', $contact->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                <a class="btn btn-xs btn-success" href="#">
                                        Reply
                                    </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.contactus.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
 let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('contactus_delete')
  dtButtons.push(deleteButton)
@endcan
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection