@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.contactus.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.contactus.fields.Fname') }}
                    </th>
                    <td>
                        {{ $contactus->Fname }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.contactus.fields.Lname') }}
                    </th>
                    <td>
                        {{ $contactus->Lname }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.contactus.fields.email') }}
                    </th>
                    <td>
                        {{ $contactus->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.contactus.fields.phone') }}
                    </th>
                    <td>
                        {{ $contactus->phone }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.contactus.fields.msg') }}
                    </th>
                    <td>
                        {{ $contactus->massage }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.contactus.fields.created_at') }}
                    </th>
                    <td>
                         {{ $contactus->created_at ?? '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection