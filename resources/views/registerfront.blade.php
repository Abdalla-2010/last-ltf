<html lang="en"><head>

<link type="text/css" rel="stylesheet" href="{{asset('Frontend/css/login.css')}}">

<style>.dismissButton{background-color:#fff;border:1px solid #dadce0;color:#1a73e8;border-radius:4px;font-family:Roboto,sans-serif;font-size:14px;height:36px;cursor:pointer;padding:0 24px}.dismissButton:hover{background-color:rgba(66,133,244,0.04);border:1px solid #d2e3fc}.dismissButton:focus{background-color:rgba(66,133,244,0.12);border:1px solid #d2e3fc;outline:0}.dismissButton:hover:focus{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd}.dismissButton:active{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd;box-shadow:0 1px 2px 0 rgba(60,64,67,0.3),0 1px 3px 1px rgba(60,64,67,0.15)}.dismissButton:disabled{background-color:#fff;border:1px solid #f1f3f4;color:#3c4043}

</style><style>.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}

</style><style>.gm-control-active>img{box-sizing:content-box;display:none;left:50%;pointer-events:none;position:absolute;top:50%;transform:translate(-50%,-50%)}.gm-control-active>img:nth-child(1){display:block}.gm-control-active:hover>img:nth-child(1),.gm-control-active:active>img:nth-child(1){display:none}.gm-control-active:hover>img:nth-child(2),.gm-control-active:active>img:nth-child(3){display:block}

</style><link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700"><style>.gm-ui-hover-effect{opacity:.6}.gm-ui-hover-effect:hover{opacity:1}

</style><style>.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px;box-sizing:border-box}

</style><style>@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style>.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}

</style><style>.gm-style img{max-width: none;}.gm-style {font: 400 11px Roboto, Arial, sans-serif; text-decoration: none;}</style>

<style>

body {

    overflow-y: hidden;

    background-image:url(Frontend/images/Mercator_Blank_Map_World.png);

    background-size:cover !important;



}

.navbar .navbar-nav li a {

    color: #191919 !important;

}

.swiper-slider-heading {

    font-size:35px !important;

}

.menu ul {

  list-style-type: none;

  margin: 0;

  padding: 0;

}



.menu li {

  padding: 8px;

  margin-bottom: 7px;

  background-color: #33b5e5;

  color: #ffffff;

  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);

}



.menu li:hover {

  background-color: #0099cc;

}

.flex-container {

  display: flex;

  flex-wrap: nowrap;



}



.flex-container > div {

  background-color: #f1f1f1;

  width: 254px;

  margin: 10px;

  text-align: center;

  line-height: 75px;

 font-size: 17px;

    font-weight: 700;

    height: 106px;

    padding-top: 16px;

}

.loginbox {

    height:495px;

}





.clearfix:after {

  content: "";

  display: block;

  clear: both;

  visibility: hidden;

  height: 0;

}



.form_wrapper {

  background: #fff;

  width: 400px;

  max-width: 100%;

  box-sizing: border-box;

  padding: 25px;

  margin: 8% auto 0;

  position: relative;

  z-index: 1;

  border-top: 5px solid #007bff;

  -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);

  -moz-box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);

  box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);

  -webkit-transform-origin: 50% 0%;

  transform-origin: 50% 0%;

  -webkit-transform: scale3d(1, 1, 1);

  transform: scale3d(1, 1, 1);

  -webkit-transition: none;

  transition: none;

  -webkit-animation: expand 0.8s 0.6s ease-out forwards;

  animation: expand 0.8s 0.6s ease-out forwards;

  opacity: 0;

}

.form_wrapper h2 {

  font-size: 1.5em;

  line-height: 1.5em;

  margin: 0;

}

.form_wrapper .title_container {

  text-align: center;

  padding-bottom: 15px;

}

.form_wrapper h3 {

  font-size: 1.1em;

  font-weight: normal;

  line-height: 1.5em;

  margin: 0;

}

.form_wrapper label {

  font-size: 12px;

}

.form_wrapper .row {

  margin: 10px -15px;

}

.form_wrapper .row > div {

  padding: 0 15px;

  box-sizing: border-box;

}

.form_wrapper .col_half {

  width: 50%;

  float: left;

}

.form_wrapper .input_field {

  position: relative;

  margin-bottom: 20px;

  -webkit-animation: bounce 0.6s ease-out;

  animation: bounce 0.6s ease-out;

}

.form_wrapper .input_field > span {

  position: absolute;

  left: 0;

  top: 0;

  color: #333;

  height: 100%;

  border-right: 1px solid #cccccc;

  text-align: center;

  width: 30px;

}

.form_wrapper .input_field > span > i {

  padding-top: 10px;

}

.form_wrapper .textarea_field > span > i {

  padding-top: 10px;

}

.form_wrapper input[type="text"], .form_wrapper input[type="email"], .form_wrapper input[type="password"] {

  width: 100%;

  padding: 8px 10px 9px 35px;

  height: 35px;

  border: 1px solid #cccccc;

  box-sizing: border-box;

  outline: none;

  -webkit-transition: all 0.30s ease-in-out;

  -moz-transition: all 0.30s ease-in-out;

  -ms-transition: all 0.30s ease-in-out;

  transition: all 0.30s ease-in-out;

}

.form_wrapper input[type="text"]:hover, .form_wrapper input[type="email"]:hover, .form_wrapper input[type="password"]:hover {

  background: #fafafa;

}

.form_wrapper input[type="text"]:focus, .form_wrapper input[type="email"]:focus, .form_wrapper input[type="password"]:focus {

  -webkit-box-shadow: 0 0 2px 1px rgba(255, 169, 0, 0.5);

  -moz-box-shadow: 0 0 2px 1px rgba(255, 169, 0, 0.5);

  box-shadow: 0 0 2px 1px rgba(255, 169, 0, 0.5);

  border: 1px solid #f5ba1a;

  background: #fafafa;

}

.form_wrapper input[type="submit"] {

  background: #007bff;

  height: 35px;

  line-height: 35px;

  width: 100%;

  border: none;

  outline: none;

  cursor: pointer;

  color: #fff;

  font-size: 1.1em;

  margin-bottom: 10px;

  -webkit-transition: all 0.30s ease-in-out;

  -moz-transition: all 0.30s ease-in-out;

  -ms-transition: all 0.30s ease-in-out;

  transition: all 0.30s ease-in-out;

}





.form_wrapper input[type="checkbox"], .form_wrapper input[type="radio"] {

  border: 0;

  clip: rect(0 0 0 0);

  height: 1px;

  margin: -1px;

  overflow: hidden;

  padding: 0;

  position: absolute;

  width: 1px;

}



.form_container .row .col_half.last {

  border-left: 1px solid #cccccc;

}



.checkbox_option label {

  margin-right: 1em;

  position: relative;

}

.checkbox_option label:before {

  content: "";

  display: inline-block;

  width: 0.5em;

  height: 0.5em;

  margin-right: 0.5em;

  vertical-align: -2px;

  border: 2px solid #cccccc;

  padding: 0.12em;

  background-color: transparent;

  background-clip: content-box;

  transition: all 0.2s ease;

}

.checkbox_option label:after {

  border-right: 2px solid #000000;

  border-top: 2px solid #000000;

  content: "";

  height: 20px;

  left: 2px;

  position: absolute;

  top: 7px;

  transform: scaleX(-1) rotate(135deg);

  transform-origin: left top;

  width: 7px;

  display: none;

}

.checkbox_option input:hover + label:before {

  border-color: #000000;

}

.checkbox_option input:checked + label:before {

  border-color: #000000;

}

.checkbox_option input:checked + label:after {

  -moz-animation: check 0.8s ease 0s running;

  -webkit-animation: check 0.8s ease 0s running;

  animation: check 0.8s ease 0s running;

  display: block;

  width: 7px;

  height: 20px;

  border-color: #000000;

}



.radio_option label {

  margin-right: 1em;

}

.radio_option label:before {

  content: "";

  display: inline-block;

  width: 0.5em;

  height: 0.5em;

  margin-right: 0.5em;

  border-radius: 100%;

  vertical-align: -3px;

  border: 2px solid #cccccc;

  padding: 0.15em;

  background-color: transparent;

  background-clip: content-box;

  transition: all 0.2s ease;

}

.radio_option input:hover + label:before {

  border-color: #000000;

}

.radio_option input:checked + label:before {

  background-color: #000000;

  border-color: #000000;

}



.select_option {

  position: relative;

  width: 100%;

}

.select_option select {

  display: inline-block;

  width: 100%;

  height: 35px;

  padding: 0px 15px;

  cursor: pointer;

  color: #7b7b7b;

  border: 1px solid #cccccc;

  border-radius: 0;

  background: #fff;

  appearance: none;

  -webkit-appearance: none;

  -moz-appearance: none;

  transition: all 0.2s ease;

}

.select_option select::-ms-expand {

  display: none;

}

.select_option select:hover, .select_option select:focus {

  color: #000000;

  background: #fafafa;

  border-color: #000000;

  outline: none;

}



.select_arrow {

  position: absolute;

  top: calc(50% - 4px);

  right: 15px;

  width: 0;

  height: 0;

  pointer-events: none;

  border-width: 8px 5px 0 5px;

  border-style: solid;

  border-color: #7b7b7b transparent transparent transparent;

}



.select_option select:hover + .select_arrow, .select_option select:focus + .select_arrow {

  border-top-color: #000000;

}



.credit {

  position: relative;

  z-index: 1;

  text-align: center;

  padding: 15px;

  color: #f5ba1a;

}

.credit a {

  color: #e1a70a;

}



@-webkit-keyframes check {

  0% {

    height: 0;

    width: 0;

  }

  25% {

    height: 0;

    width: 7px;

  }

  50% {

    height: 20px;

    width: 7px;

  }

}

@keyframes check {

  0% {

    height: 0;

    width: 0;

  }

  25% {

    height: 0;

    width: 7px;

  }

  50% {

    height: 20px;

    width: 7px;

  }

}

@-webkit-keyframes expand {

  0% {

    -webkit-transform: scale3d(1, 0, 1);

    opacity: 0;

  }

  25% {

    -webkit-transform: scale3d(1, 1.2, 1);

  }

  50% {

    -webkit-transform: scale3d(1, 0.85, 1);

  }

  75% {

    -webkit-transform: scale3d(1, 1.05, 1);

  }

  100% {

    -webkit-transform: scale3d(1, 1, 1);

    opacity: 1;

  }

}

@keyframes expand {

  0% {

    -webkit-transform: scale3d(1, 0, 1);

    transform: scale3d(1, 0, 1);

    opacity: 0;

  }

  25% {

    -webkit-transform: scale3d(1, 1.2, 1);

    transform: scale3d(1, 1.2, 1);

  }

  50% {

    -webkit-transform: scale3d(1, 0.85, 1);

    transform: scale3d(1, 0.85, 1);

  }

  75% {

    -webkit-transform: scale3d(1, 1.05, 1);

    transform: scale3d(1, 1.05, 1);

  }

  100% {

    -webkit-transform: scale3d(1, 1, 1);

    transform: scale3d(1, 1, 1);

    opacity: 1;

  }

}

@-webkit-keyframes bounce {

  0% {

    -webkit-transform: translate3d(0, -25px, 0);

    opacity: 0;

  }

  25% {

    -webkit-transform: translate3d(0, 10px, 0);

  }

  50% {

    -webkit-transform: translate3d(0, -6px, 0);

  }

  75% {

    -webkit-transform: translate3d(0, 2px, 0);

  }

  100% {

    -webkit-transform: translate3d(0, 0, 0);

    opacity: 1;

  }

}

@keyframes bounce {

  0% {

    -webkit-transform: translate3d(0, -25px, 0);

    transform: translate3d(0, -25px, 0);

    opacity: 0;

  }

  25% {

    -webkit-transform: translate3d(0, 10px, 0);

    transform: translate3d(0, 10px, 0);

  }

  50% {

    -webkit-transform: translate3d(0, -6px, 0);

    transform: translate3d(0, -6px, 0);

  }

  75% {

    -webkit-transform: translate3d(0, 2px, 0);

    transform: translate3d(0, 2px, 0);

  }

  100% {

    -webkit-transform: translate3d(0, 0, 0);

    transform: translate3d(0, 0, 0);

    opacity: 1;

  }

}

@media (max-width: 600px) {

  .form_wrapper .col_half {

    width: 100%;

    float: none;

  }



  .bottom_row .col_half {

    width: 50%;

    float: left;

  }



  .form_container .row .col_half.last {

    border-left: none;

  }



  .remember_me {

    padding-bottom: 20px;

  }

}



</style>

    <!-- Meta Tags For Seo + Page Optimization -->

   <meta charset="utf-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1">



     <meta name="description" content="">



  <meta name="keywords" content="">



    <!-- Insert Favicon Here -->

    <link href="{{asset('images/favicon.png')}}" rel="icon">



    <!-- Page Title(Name)-->

    <title>LTF</title>



    <!-- Bootstrap CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/bootstrap.css')}}">



    <!-- Font-Awesome CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/font-awesome.css')}}">



    <!-- Slider Revolution CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/settings.css')}}">



    <!--  Fancy Box CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/jquery.fancybox.css')}}">



    <!-- Circleful CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/jquery.circliful.css')}}">



    <!-- Animate CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/animate.css')}}">



    <!-- Cube Portfolio CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/cubeportfolio.min.css')}}">



    <!-- Owl Carousel CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="css/owl.theme.default.min.css">



    <!-- Swiper CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/swiper.min.css')}}">



    <!-- Custom Style CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/css/style.css')}}">



    <!-- Crypto Currency CSS File -->

    <link rel="stylesheet" href="{{asset('Frontend/cryptocurrency-assets/css/style.css')}}">



    <!-- Color StyleSheet CSS File -->

    <link href="{{asset('Frontend/css/yellow.css')}}" rel="stylesheet" id="color" type="text/css">

        <link rel="stylesheet" href="{{asset('Frontend/media/itb/itb_layout/itb_layout_styles/project_itb.css')}}" media="screen">





    <link rel="stylesheet" href="{{asset('Frontend/css/font-six.css')}}">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">



<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>



<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>







<script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script><style type="text/css">@-webkit-keyframes _gm7906 {

0% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }

50% { -webkit-transform: translate3d(0px,-20px,0); -webkit-animation-timing-function: ease-in; }

100% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }

}

</style><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script></head>









<body>









<!-- Loader -->



<!-- Loader -->



    <div id="overlay" style="opacity: 1.5;"></div>

    <div class="form_wrapper">

  <div class="form_container">

    <div class="title_container">

      <h2> Registration</h2>

    </div>

    <div class="row clearfix">

      <div class="">

        <form>

          <div class="input_field"> <span><i aria-hidden="true" class="fa fa-envelope"></i></span>

            <input type="email" name="email" placeholder="Email" required />

          </div>

          <div class="input_field"> <span><i aria-hidden="true" class="fa fa-lock"></i></span>

            <input type="password" name="password" placeholder="Password" required />

          </div>

          <div class="input_field"> <span><i aria-hidden="true" class="fa fa-lock"></i></span>

            <input type="password" name="password" placeholder="Re-type Password" required />

          </div>

          <div class="row clearfix">

            <div class="col_half">

              <div class="input_field"> <span><i aria-hidden="true" class="fa fa-user"></i></span>

                <input type="text" name="name" placeholder="First Name" />

              </div>

            </div>

            <div class="col_half">

              <div class="input_field"> <span><i aria-hidden="true" class="fa fa-user"></i></span>

                <input type="text" name="name" placeholder="Last Name" required />

              </div>

            </div>

          </div>





            <div class="col-md-6">

          <input class="button" type="submit" value="Register" />

          </div>

          <div class="col-md-6">

            <input class="button"  onClick="window.location.href='{{route('login.facebook')}}'"  type="submit" value="Facebook Register" />

          </div>

          <a href="{{url('/')}}" style="margin-left: 125px;">Back To  Home</a>

        </form>

      </div>

    </div>

  </div>

</div>













    <!-- /Parent Section Ended -->



<!-- jQuery 2.2.0-->





<script src="{{asset('Frontend/js/jquery.js')}}"></script>



<!-- Google Map Api -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U" type="text/javascript"></script>

<script src="{{asset('Frontend/js/map.js')}}" type="text/javascript"></script>



<!-- REVOLUTION JS FILES -->

<script type="text/javascript" src="{{asset('Frontend/js/jquery.themepunch.tools.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/jquery.themepunch.revolution.min.js')}}"></script>



<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

<script type="text/javascript" src="{{asset('Frontend/js/actions.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/carousel.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/kenburn.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/layeranimation.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/migration.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/navigation.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/parallax.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/slideanims.min.js')}}"></script>

<script type="text/javascript" src="{{asset('Frontend/js/video.min.js')}}"></script>



<!-- Bootstrap Core JavaScript -->

<script src="{{asset('Frontend/js/bootstrap.min.js')}}"></script>



<!-- Owl Carousel 2 Core JavaScript -->

<script src="{{asset('Frontend/js/owl.carousel.js')}}"></script>

<script src="{{asset('Frontend/js/owl.animate.js')}}"></script>

<script src="{{asset('Frontend/js/owl.autoheight.js')}}"></script>

<script src="{{asset('Frontend/js/owl.autoplay.js')}}"></script>

<script src="{{asset('Frontend/js/owl.autorefresh.js')}}"></script>

<script src="{{asset('Frontend/js/owl.hash.js')}}"></script>

<script src="{{asset('Frontend/js/owl.lazyload.js')}}"></script>

<script src="{{asset('Frontend/js/owl.navigation.js')}}"></script>

<script src="{{asset('Frontend/js/owl.support.js')}}"></script>

<script src="{{asset('Frontend/js/owl.video.js')}}"></script>



<!-- Fancy Box Javacript -->

<script src="{{asset('Frontend/js/jquery.fancybox.js')}}"></script>



<!-- Wow Js -->

<script src="{{asset('Frontend/js/wow.min.js')}}"></script>



<!-- Appear Js-->

<script src="{{asset('Frontend/js/jquery.appear.js')}}"></script>



<!-- Countdown Js -->

<script src="{{asset('Frontend/js/jquery.countdown.js')}}"></script>



<!-- Parallax Js -->

<script src="{{asset('Frontend/js/parallax.min.js')}}"></script>



<!-- Particles Core Js -->

<script src="{{asset('Frontend/js/particles.js')}}"></script>



<!-- Cube Portfolio Core JavaScript -->

<script src="{{asset('Frontend/js/jquery.cubeportfolio.min.js')}}"></script>



<!-- Circliful Core JavaScript -->

<script src="{{asset('Frontend/js/jquery.circliful.min.js')}}"></script>



<!-- Swiper Slider Core JavaScript -->

<script src="{{asset('Frontend/js/swiper.min.js')}}"></script>



<!-- Crypto Currency Core Javascript -->

<script src="{{asset('Frontend/cryptocurrency-assets/js/script.js')}}"></script>



<!-- Custom JavaScript -->

<script src="{{asset('Frontend/js/script.js')}}"></script>







</body></html>
