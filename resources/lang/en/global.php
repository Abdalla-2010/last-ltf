<?php



return [

    'add' => 'Add',

    'create' => 'Create',

    'list' => 'List',

    'show' => 'Show',

    'entries' => 'Entries',

    // 'search'                               => 'Search',

    'view' => 'View',

    'edit' => 'Edit',

    'delete' => 'Delete',

    'save' => 'Save',

    'pleaseSelect' => 'Please select',

    'areYouSure' => 'Are you sure?',

    'allRightsReserved' => 'All rights reserved.',

    'downloadFile' => 'Download file',

    'toggleNavigation' => 'Toggle navigation',

    'home' => 'Home',

    'youAreLoggedIn' => 'You are logged in!',

    'user_name' => 'Name',

    'reset_password' => 'Reset Password',

    'login' => 'Login',

    'remember_me' => 'Remember me',

    'forgot_password' => 'Forgot your password?',

    'register' => 'Register',

    'login_email' => 'Email',

    'login_password' => 'Password',

    'login_password_confirmation' => 'Password confirmation',

    'yes' => 'Yes',

    'no' => 'No',

    'view_file' => 'View file',

    'verifyYourUser' => 'To finish your registration - site asks you to verify your email',

    'clickHereToVerify' => 'Click here to verify',

    'thankYouForUsingOurApplication' => 'Thank you for using our website',

    'verifyYourEmail' => 'Please verify your email',

    'emailVerificationSuccess' => 'User email verified successfully',

    'yourAccountNeedsAdminApproval' => 'Your accounts needs an administrator approval in order to log in',

    'timeFrom' => 'From',

    'timeTo' => 'To',

    'filterDate' => 'Filter by date',

    'reports' => 'Reports',

    'year' => 'Year',

    'month' => 'Month',

    'logout' => 'Logout',

    'calendar' => 'Calendar',

    'dashboard' => 'Dashboard',

    'datatables' => [

        'copy' => 'Copy',

        'csv' => 'CSV',

        'excel' => 'Excel',

        'pdf' => 'PDF',

        'print' => 'Print',

        'colvis' => 'Column visibility',

        'delete' => 'Delete selected',

        'zero_selected' => 'No rows selected',

    ],

    'restore' => 'Restore',

    'permadel' => 'Delete Permanently',

    'all' => 'All',

    'trash' => 'Trash',

    'update' => 'Update',

    'no_entries_in_table' => 'No entries in table',

    'custom_controller_index' => 'Custom controller index.',

    'back_to_list' => 'Back to list',

    'category' => 'Category',

    'categories' => 'Categories',

    'sample_category' => 'Sample category',

    'questions' => 'Questions',

    'question' => 'Question',

    'answer' => 'Answer',

    'sample_question' => 'Sample question',

    'sample_answer' => 'Sample answer',

    'faq_management' => 'FAQ Management',

    'administrator_can_create_other_users' => 'Administrator (can create other users)',

    'simple_user' => 'Simple user',

    'title' => 'Title',

    'roles' => 'Roles',

    'role' => [

        'title' => 'Roles',

        'title_singular' => 'Role',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'title' => 'Title',

            'title_helper' => '',

            'permissions' => 'Permissions',

            'permissions_helper' => '',

            'created_at' => 'Created at',

            'created_at_helper' => '',

            'updated_at' => 'Updated at',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'profile' => [
        'title' => 'Profile',
        'action' =>[
            'show' => 'Show Profile',
            'edit' => 'Edit Profile',
        ]
    ],

    'user_management' => 'User management',

    'users' => 'Users',

    'user' => [

        'title' => 'Users',

        'title_singular' => 'User',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'First Name',

            'name_helper' => '',

            'email' => 'Email',

            'email_helper' => '',

            'email_verified_at' => 'Email verified at',

            'email_verified_at_helper' => '',

            'password' => 'Password',

            'password_helper' => '',

            'roles' => 'Roles',

            'roles_helper' => '',

            'remember_token' => 'Remember Token',

            'remember_token_helper' => '',

            'created_at' => 'Created at',

            'created_at_helper' => '',

            'updated_at' => 'Updated at',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

            'last-name' => 'Last Name',

            'type' => 'Type',

            'phone' => 'Phone',

            'imgPersonal' => 'Personal Image',

            'website' => 'Website',

            'facebook' => 'Facebook',

            'linkedin' => 'LinkedIn',

            'twitter' => 'Twitter',

            'instgram' => 'Instagram',

            'company' => 'Company Name',

            'imgCompany' => 'Company Logo',

            'zip-code' => 'Zip Code',

            'title' => 'Title',

            'company-email' => 'Company Email',

            'designation' => 'Designation',

            'country' => 'Country',

            'travel' => 'Travel Specially',

            'languages' => 'Languages',

            'destination' => 'Destinations',
            'brochure' => 'Brochure',
            'company_profile' => 'Company Profile',
            'youtubeLink' => 'Youtube Video',

        ],

    ],

    'name' => 'Name',

    'remember_token' => 'Remember token',

    'permissions' => 'Permissions',

    'user_actions' => 'User actions',

    'action' => 'Action',

    'action_model' => 'Action model',

    'action_id' => 'Action id',

    'time' => 'Time',

    'campaign' => 'Campaign',

    'campaigns' => 'Campaigns',

    'description' => 'Description',

    'valid_from' => 'Valid from',

    'valid_to' => 'Valid to',

    'discount_amount' => 'Discount amount',

    'discount_percent' => 'Discount percent',

    'coupons_amount' => 'Coupons amount',

    'coupons' => 'Coupons',

    'code' => 'Code',

    'redeem_time' => 'Redeem time',

    'coupon_management' => 'Coupon Management',

    'time_management' => 'Time management',

    'projects' => 'Projects',

    'time_entries' => 'Time entries',

    'work_type' => 'Work type',

    'work_types' => 'Work types',

    'project' => 'Project',

    'start_time' => 'Start time',

    'end_time' => 'End time',

    'expense_category' => 'Expense Category',

    'expense_categories' => 'Expense Categories',

    'expense_management' => 'Expense Management',

    'expenses' => 'Expenses',

    'expense' => 'Expense',

    'entry_date' => 'Entry date',

    'amount' => 'Amount',

    'income_categories' => 'Income categories',

    'monthly_report' => 'Monthly report',

    'companies' => 'Companies',

    'company_name' => 'Company name',

    'address' => 'Address',

    'website' => 'Website',

    'contact_management' => 'Contact management',

    'contacts' => 'Contacts',

    'company' => 'Company',

    'first_name' => 'First name',

    'last_name' => 'Last name',

    'phone' => 'Phone',

    'phone1' => 'Phone 1',

    'phone2' => 'Phone 2',

    'skype' => 'Skype',

    'photo' => 'Photo (max 8mb)',

    'category_name' => 'Category name',

    'product_management' => 'Product management',

    'products' => 'Products',

    'product_name' => 'Product name',

    'price' => 'Price',

    'tags' => 'Tags',

    'tag' => 'Tag',

    'photo1' => 'Photo1',

    'photo2' => 'Photo2',

    'photo3' => 'Photo3',

    'statuses' => 'Statuses',

    'task_management' => 'Task management',

    'tasks' => 'Tasks',

    'status' => 'Status',

    'attachment' => 'Attachment',

    'due_date' => 'Due date',

    'assigned_to' => 'Assigned to',

    'assets' => 'Assets',

    'asset' => 'Asset',

    'serial_number' => 'Serial number',

    'location' => 'Location',

    'locations' => 'Locations',

    'assigned_user' => 'Assigned (user)',

    'notes' => 'Notes',

    'assets_history' => 'Assets history',

    'assets_management' => 'Assets management',

    'slug' => 'Slug',

    'content_management' => 'Content management',

    'text' => 'Text',

    'excerpt' => 'Excerpt',

    'featured_image' => 'Featured image',

    'pages' => 'Pages',

    'axis' => 'Axis',

    'group_by' => 'Group by',

    'chart_type' => 'Chart type',

    'create_new_report' => 'Create new report',

    'no_reports_yet' => 'No reports yet.',

    'created_at' => 'Created at',

    'updated_at' => 'Updated at',

    'deleted_at' => 'Deleted at',

    'reports_x_axis_field' => 'X-axis - please choose one of date/time fields',

    'reports_y_axis_field' => 'Y-axis - please choose one of number fields',

    'select_crud_placeholder' => 'Please select one of your CRUDs',

    'select_dt_placeholder' => 'Please select one of date/time fields',

    'aggregate_function_use' => 'Aggregate function to use',

    'x_axis_group_by' => 'X-axis group by',

    'x_axis_field' => 'X-axis field (date/time)',

    'y_axis_field' => 'Y-axis field',

    'integer_float_placeholder' => 'Please select one of integer/float fields',

    'change_notifications_field_1_label' => 'Send email notification to User',

    'change_notifications_field_2_label' => 'When Entry on CRUD',

    'select_users_placeholder' => 'Please select one of your Users',

    'is_created' => 'is created',

    'is_updated' => 'is updated',

    'is_deleted' => 'is deleted',

    'notifications' => 'Notifications',

    'notify_user' => 'Notify User',

    'when_crud' => 'When CRUD',

    'create_new_notification' => 'Create new Notification',

    'stripe_transactions' => 'Stripe Transactions',

    'upgrade_to_premium' => 'Upgrade to Premium',

    'messages' => 'Messages',

    'you_have_no_messages' => 'You have no messages.',

    'all_messages' => 'All Messages',

    'new_message' => 'New message',

    'outbox' => 'Outbox',

    'inbox' => 'Inbox',

    'recipient' => 'Recipient',

    'subject' => 'Subject',

    'message' => 'Message',

    'send' => 'Send',

    'reply' => 'Reply',

    'calendar_sources' => 'Calendar sources',

    'new_calendar_source' => 'Create new calendar source',

    'crud_title' => 'Crud title',

    'crud_date_field' => 'Crud date field',

    'prefix' => 'Prefix',

    'label_field' => 'Label field',

    'suffix' => 'Sufix',

    'no_calendar_sources' => 'No calendar sources yet.',

    'crud_event_field' => 'Event label field',

    'create_new_calendar_source' => 'Create new Calendar Source',

    'edit_calendar_source' => 'Edit Calendar Source',

    'client_management' => 'Client management',

    'client_management_settings' => 'Client management settings',

    'country' => 'Country',

    'client_status' => 'Client status',

    'clients' => 'Clients',

    'client_statuses' => 'Client statuses',

    'currencies' => 'Currencies',

    'main_currency' => 'Main currency',

    'documents' => 'Documents',

    'file' => 'File',

    'income_source' => 'Income source',

    'income_sources' => 'Income sources',

    'fee_percent' => 'Fee percent',

    'note_text' => 'Note text',

    'client' => 'Client',

    'start_date' => 'Start date',

    'budget' => 'Budget',

    'project_status' => 'Project status',

    'project_statuses' => 'Project statuses',

    'transactions' => 'Transactions',

    'transaction_types' => 'Transaction types',

    'transaction_type' => 'Transaction type',

    'transaction_date' => 'Transaction date',

    'currency' => 'Currency',

    'current_password' => 'Current password',

    'new_password' => 'New password',

    'change_password' => 'Change password',

    'reset_password_woops' => '<strong>Whoops!</strong> There were problems with input:',

    'email_line1' => 'You are receiving this email because we received a password reset request for your account.',

    'email_line2' => 'If you did not request a password reset, no further action is required.',

    'email_greet' => 'Hello',

    'email_regards' => 'Regards',

    'confirm_password' => 'Confirm password',

    'if_you_are_having_trouble' => 'If you’re having trouble clicking the',

    'copy_paste_url_bellow' => 'button, copy and paste the URL below into your web browser:',

    'registration' => 'Registration',

    'not_approved_title' => 'You are not approved',

    'not_approved_p' => 'Your account is still not approved by administrator. Please, be patient and try again later.',

    'there_were_problems_with_input' => 'There were problems with input',

    'whoops' => 'Whoops!',

    'file_contains_header_row' => 'File contains header row?',

    'csvImport' => 'CSV Import',

    'csv_file_to_import' => 'CSV file to import',

    'parse_csv' => 'Parse CSV',

    'import_data' => 'Import data',

    'imported_rows_to_table' => 'Imported :rows rows to :table table',

    'subscription-billing' => 'Subscriptions',

    'subscription-payments' => 'Payments',

    'basic_crm' => 'Basic CRM',

    'customers' => 'Customers',

    'customer' => 'Customer',

    'select_all' => 'Select all',

    'deselect_all' => 'Deselect all',

    'team-management' => 'Teams',

    'team-management-singular' => 'Team',

    'site_title' => 'LTF',

    'userManagement' => [

        'title' => 'User Management',

        'title_singular' => 'User Management',

        'fields' => [],

    ],

    'permission' => [

        'title' => 'Permissions',

        'title_singular' => 'Permission',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'title' => 'Title',

            'title_helper' => '',

            'created_at' => 'Created at',

            'created_at_helper' => '',

            'updated_at' => 'Updated at',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'home' => [

        'title' => 'Home',

        'title_singular' => 'Home',

    ],

    'product' => [

        'title' => 'Products',

        'title_singular' => 'Product',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'visitor' => [

        'title' => 'Trade  Visitors',

        'title_singular' => 'Visitor',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'company' => 'Company',

            'company_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'trad_visitor' => [

        'title' => 'public Visitors',

        'title_singular' => 'Visitors',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'exhibitor' => [

        'title' => 'Exhibitors',

        'title_singular' => 'Exhibitors',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'company' => 'Company',

            'company_helper' => '',

            'country' => 'Country',

            'country_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'search' => [

        'title' => 'Search',

        'title_singular' => 'Seach',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'rates' => [

        'title' => 'Rates',

        'title_singular' => 'Rates',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'forms' => [

        'title' => 'Form',

        'title_singular' => 'Form',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'results' => [

        'title' => 'Results',

        'title_singular' => 'Result',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'offers' => [

        'title' => 'Offers',

        'title_singular' => 'Offer',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',





            'price' => 'Price',

            'price_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'setting' => [

        'title' => 'Setting',

        'title_singular' => 'Setting',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

        ],

    ],

    'about' => [

        'title' => 'About',

        'title_singular' => 'About',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'body' => 'Body',

            'body_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],



    'media' => [

        'title' => 'Media',

        'title_singular' => 'Media',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'body' => 'Body',

            'body_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],



    'slides' => [

        'title' => 'Slide_Imgs',

        'title_singular' => 'Slide_Imgs',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'image' => 'Image',

            'image_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'leaders' => [

        'title' => 'Group Leader',

        'title_singular' => 'Group_Leader',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'company' => 'Organiztion',

            'company_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'image' => 'Image',

            'image_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],



    'tourguides' => [

        'title' => 'Tour Guides',

        'title_singular' => 'Tour Guide',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'company' => 'Organiztion',

            'company_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'image' => 'Image',

            'image_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'librarys' => [

        'title' => 'Library',

        'title_singular' => 'Library',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Title',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'video' => 'Video',

            'video_helper' => '',

            'created_at' => 'Created At',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'contactus' => [

        'title' => 'Contact Us',

        'title_singular' => 'Contact Us',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'Fname' => 'First Name',

            'Fname_helper' => '',

            'Lname' => 'Last Name',

            'Lname_helper' => '',

            'msg' => 'Massage',

            'msg_helper' => '',

            'phone' => 'Phone',

            'phone_helper' => '',

            'email' => 'Email',

            'email_helper' => '',

            'created_at' => 'Sent_at',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'news' => [

        'title' => 'News',

        'title_singular' => 'News',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'title' => 'Title',

            'title_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'images' => 'Images',

            'images_helper' => '',

            'created_at' => 'Posted_at',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'fans' => [

        'title' => 'Trip Fan',

        'title_singular' => 'Trip Fan',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Title',

            'name_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'created_at' => 'Sent_at',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],

    'metas' => [

        'title' => 'Meta',

        'title_singular' => 'Meta',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'name' => 'Title',

            'name_helper' => '',

            'group' => 'Group',

            'group_helper' => '',

            'keywords' => 'Key Words',

            'keywords_helper' => '',

            'description' => 'Description',

            'description_helper' => '',

            'created_at' => 'Sent_at',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],



    ],

    'sliders' => [

        'title' => 'Sliders',

        'title_singular' => 'Slider',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'title' => 'Title',

            'title_helper' => '',

            'image' => 'Image',

            'image_helper' => '',

            'content' => 'Content',

            'content_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

        ],

    ],

    'myoffers' => [

        'title' => 'My Offer',

        'title_singular' => 'Offer',

        'fields' => [

            'id' => 'ID',

            'id_helper' => '',

            'userId' => 'User_ID',

            'userId_helper' => '',

            'name' => 'Name',

            'name_helper' => '',

            'img' => 'Image',

            'img_helper' => '',

            'duration' => 'Duration',

            'duration_helper' => '',

            'destination' => 'Destination',

            'destination_helper' => '',

            'price' => 'Price',

            'price_helper' => '',



            'url' => 'URL',

            'url_helper' => '',

            'created_at' => 'Sent_at',

            'created_at_helper' => '',

            'updated_at' => 'Updated At',

            'updated_at_helper' => '',

            'deleted_at' => 'Deleted at',

            'deleted_at_helper' => '',

        ],

    ],



    'favourites' => [

        'title' => 'My Favourites',

        'title_singular' => 'My Favourite',

    ],

];

