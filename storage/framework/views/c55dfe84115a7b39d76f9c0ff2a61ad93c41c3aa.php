<?php $__env->startSection('content'); ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('meta_create')): ?>
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="<?php echo e(route("admin.metas.create")); ?>">
                <?php echo e(trans('global.add')); ?> <?php echo e(trans('global.metas.title_singular')); ?>

            </a>
        </div>
    </div>
<?php endif; ?>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.metas.title_singular')); ?> <?php echo e(trans('global.list')); ?>

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            <?php echo e(trans('global.sliders.fields.image')); ?>

                        </th>
                         <th>
                            <?php echo e(trans('global.sliders.fields.title')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.sliders.fields.content')); ?>

                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($slider->id); ?>">
                            <td>

                            </td>
                            <td>
                                <img src="<?php echo e($slider->image); ?>" style="width: 100px; height: 100px;">
                            </td>
                            <td>
                                <?php echo e($slider->title ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($slider->content ?? ''); ?>

                            </td>
                            <td>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('slider_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('admin.sliders.edit', $slider->id)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/sliders/index.blade.php ENDPATH**/ ?>