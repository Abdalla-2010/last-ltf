<html lang="en">
<head>
    <style>.dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }

        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }

        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }

        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }

        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }

        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }

    </style>
    <style>.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }

    </style>
    <style>.gm-control-active > img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active > img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover > img:nth-child(1), .gm-control-active:active > img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover > img:nth-child(2), .gm-control-active:active > img:nth-child(3) {
            display: block
        }

    </style>
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700">
    <style>.gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

    </style>
    <style>.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }

    </style>
    <style>@media  print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media  screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>

    <style>

        body {

            width: 100%;

            min-height: 100vh;

            background-color: #f4f4f4;

            min-height: calc(100vh - var(--vh-offset, 0px));


            overflow-x: hidden;

            font-weight: 400;

            top: 0;

            color: #252525;

            font-size: 14px;

            line-height: 1.8;

            letter-spacing: 0.025em;

        }


        .header-design {

            background-color: #a4508b;

            background-image: linear-gradient(326deg, #a4508b 0%, #5f0a87 74%);

            height: 120px;

            text-align: center;

        }


        .listar-map-button {

            width: 100%;

            position: absolute;

            display: table;

            right: 0;

            top: 0;

            height: 200px;

            line-height: 164px;

            white-space: nowrap;

            font-size: 0;

        }


        .listar-map-button {

            right: auto;

            /***

              box-shadow: 0 0 65px rgba(0,0,0,.07) inset, 0 0 35px rgba(0,0,0,.05) inset;**/

        }


        .listar-map-button-text span {

            background-color: #fff;

            color: #252525;

            text-shadow: none;

        }


        .listar-page-header-content .listar-map-button-text span {

            position: relative;

            display: inline-block;

            vertical-align: middle;

            font-size: 15px;

            height: 44px;

            line-height: 1.6;

            padding: 10px 25px;

            box-shadow: 0 0 300px rgba(0, 0, 0, 0.65), 0 0 30px rgba(0, 0, 0, 0.06);

            border-radius: 50px;

            background-color: rgba(35, 40, 45, 0.9);

            color: #fff;

            text-shadow: 1px 1px rgba(0, 0, 0, 0.2);

        }


        header .footer-wave {

            max-width: 102%;

            width: 100%;

            height: 187.8px;

            left: 0;

            z-index: 1;

            bottom: -67px;

            background: url(https://1.bp.blogspot.com/-NYl6L8pz8B4/XoIVXwfhlNI/AAAAAAAAU3k/nxJKiLT706Mb7jUFiM5vdCsOSNnFAh0yQCLcBGAsYHQ/s1600/hero-wave.png) repeat-x;

            animation: wave 10s cubic-bezier(0.44, 0.66, 0.67, 0.37) infinite;

        }


        @keyframes  wave {

            0% {

                background-position: 0;

            }

            100% {

                background-position: 1440px;

            }

        }


        .listar-feature-item-wrapper {

            margin-bottom: 120px;

            padding-left: 27px;

            padding-right: 27px;

        }


        .listar-feature-item {

            height: calc(100% - 20px);

        }


        .listar-feature-item.listar-feature-has-link
        ~ .listar-feature-fix-bottom-padding.listar-fix-feature-arrow-button-height {

            position: relative;

            display: block;

            width: 100%;

            height: 30px;

        }


        .listar-feature-item a {

            position: absolute;

            top: -12px;

            left: 15px;

            width: calc(100% - 30px);

            height: calc(100% + 4px);

            border-radius: 1000px;

            z-index: 10;

        }


        .listar-feature-with-image .listar-feature-item a:before {

            content: "";

            position: absolute;

            top: -74px;

            left: 50%;

            margin-left: -74px;

            width: 148px;

            height: 148px;

            border-radius: 1000px;

            z-index: 10;

        }


        .listar-feature-item a:after {

            content: "";

            position: absolute;

            bottom: -7px;

            left: 50%;

            margin-left: -25px;

            width: 50px;

            height: 50px;

            border-radius: 1000px;

            z-index: 10;

            animation: ripple 0.7s linear infinite;

            box-shadow: 5px 5px 10px rgba(163, 177, 198, 0.6),
            -5px -5px 10px rgba(255, 255, 255, 0.5);

        }

        @media  only screen and (max-width: 992px) {

            .menu_bars.right {

                /*margin-right:200px;*/

            }

            .listar-feature-right-border {

                display: none;

            }

            .listar-feature-items {

                width: 100%;

                height: 60%;

            }

        }


        @keyframes  ripple {

            0% {

                box-shadow: 0 0 0 0 rgba(163, 177, 198, 0.3), 0 0 0 1em rgba(163, 177, 198, 0.3), 0 0 0 3em rgba(163, 177, 198, 0.03), 0 0 0 5em rgba(163, 177, 198, 0.01);

            }

            100% {

                box-shadow: 0 0 0 1em rgba(163, 177, 198, 0.3), 0 0 0 3em rgba(163, 177, 198, 0.03), 0 0 0 5em rgba(163, 177, 198, 0.03), 0 0 0 8em rgba(163, 177, 198, 0.01);

            }

        }


        .listar-feature-item .listar-feature-item-inner {

            padding: 50px 30px;

            border-radius: 6px;

            z-index: 5;

            position: relative;

            height: 100%;

        }


        .listar-feature-item .listar-feature-item-inner {

            padding-top: 60px;

            padding-bottom: 60px;

        }


        .listar-feature-item .listar-feature-item-inner:before {

            border: 0;

            box-shadow: 120px 0px 150px rgba(80, 80, 80, 0.15),
            10px 0px 10px rgba(80, 80, 80, 0.02);

            border-radius: 1000px;

            background: rgba(255, 255, 255, 0);

            background: -moz-linear-gradient(
                left,
                rgba(255, 255, 255, 0) 0%,
                rgba(255, 255, 255, 0) 40%,
                rgba(255, 255, 255, 1) 100%
            );

            background: -webkit-gradient(
                left top,
                right top,
                color-stop(0%, rgba(255, 255, 255, 0)),
                color-stop(40%, rgba(255, 255, 255, 0)),
                color-stop(100%, rgba(255, 255, 255, 1))
            );

            background: -webkit-linear-gradient(
                left,
                rgba(255, 255, 255, 0) 0%,
                rgba(255, 255, 255, 0) 40%,
                rgba(255, 255, 255, 1) 100%
            );

            background: -o-linear-gradient(
                left,
                rgba(255, 255, 255, 0) 0%,
                rgba(255, 255, 255, 0) 40%,
                rgba(255, 255, 255, 1) 100%
            );

            background: -ms-linear-gradient(
                left,
                rgba(255, 255, 255, 0) 0%,
                rgba(255, 255, 255, 0) 40%,
                rgba(255, 255, 255, 1) 100%
            );

            background: linear-gradient(
                to right,
                rgba(255, 255, 255, 0) 0%,
                rgba(255, 255, 255, 0) 40%,
                rgba(255, 255, 255, 1) 100%
            );

            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ffffff', GradientType=1);

        }


        .listar-feature-item .listar-feature-item-inner:before {

            content: "";

            position: absolute;

            top: 0;

            left: 0;

            width: 100%;

            height: 100%;

        }


        .listar-feature-right-border {

            position: absolute;

            width: calc(123% + 24px);

            height: calc(100% + 24px);

            top: -12px;

            left: 50%;

            overflow: hidden;

        }


        .listar-feature-items {

            text-align: center;

            display: -webkit-box;

            display: -moz-box;

            display: -ms-flexbox;

            display: -webkit-flex;

            display: flex;

            -webkit-flex-wrap: wrap;

            -moz-flex-wrap: wrap;

            -ms-flex-wrap: wrap;

            flex-wrap: wrap;

        }


        .listar-feature-right-border:before,
        .listar-hovering-features
        .listar-feature-item
        a:hover
        ~ .listar-feature-item-inner
        .listar-feature-right-border:before {

            border: 11px solid #2f53bf;

        }


        .listar-feature-right-border:before,
        .listar-hovering-features
        .listar-feature-item
        a:hover
        ~ .listar-feature-item-inner
        .listar-feature-right-border:before,
        .listar-hovering-features-grey
        .listar-feature-item
        a:hover
        ~ .listar-feature-item-inner
        .listar-feature-right-border:before {

            content: "";

            position: absolute;

            width: 100%;

            height: 100%;

            top: 0;

            left: 0;

            border: 11px solid #258bd5;

            border-radius: 800px;

            left: -50%;

        }


        .listar-feature-item .listar-feature-block-content-wrapper {

            position: relative;

            display: block;

            text-decoration: none;

            margin: -30px;

            padding: 30px;

            height: calc(100% + 100px);

        }


        .listar-features-design-2 .listar-feature-item-wrapper {

            margin-bottom: 120px;

            padding-left: 27px;

            padding-right: 27px;

        }


        .listar-feature-item.listar-feature-has-link .listar-feature-item-inner:after {

            content: "";

            background-color: #fff;

            z-index: 6;

            position: absolute;

            background-repeat: no-repeat;

            background-position: center center;

        }


        .listar-feature-icon-wrapper {

            width: 148px;

            height: 148px;

            line-height: 148px;

            border-radius: 500px;

            position: relative;

            background-color: #fff;

            box-shadow: 8px 8px 30px rgba(0, 0, 0, 0.06);

            -webkit-transform: rotate(0deg);

            -moz-transform: rotate(0deg);

            -ms-transform: rotate(0deg);

            -o-transform: rotate(0deg);

            transform: rotate(0deg);

            left: 50%;

            vertical-align: middle;

            margin-left: -74px;

            top: -66px;

            margin-top: -74px;

        }


        .listar-feature-icon-inner {

            width: 120px;

            height: 120px;

            line-height: 120px;

            border-radius: 500px;

            position: relative;

            display: inline-block;

            vertical-align: middle;

            box-shadow: 10px 10px 20px rgba(0, 0, 0, 0.14);

            background: #fff;

        }


        .listar-feature-icon-inner div {

            -webkit-transform: rotate(0deg);

            -moz-transform: rotate(0deg);

            -ms-transform: rotate(0deg);

            -o-transform: rotate(0deg);

            transform: rotate(0deg);

            display: inline-block;

            position: relative;

            width: 120px;

            height: 120px;

            line-height: 120px;

            text-align: left;

        }


        .listar-feature-icon-inner img {

            position: relative;

            margin: 0 auto;

            vertical-align: middle;

            display: inline-block;

            width: auto;

            left: 50%;

            -webkit-transform: translate(-50%);

            -moz-transform: translate(-50%);

            -ms-transform: translate(-50%);

            -o-transform: translate(-50%);

            transform: translate(-50%);

            height: 55px;

        }


        .listar-feature-item img,
        .listar-feature-item i {

            position: relative;

            display: inline-block;

            border-bottom: 0;

            font-size: 40px;

            top: 0;

            padding: 0;

            color: #258bd5;

        }


        .listar-feature-with-image .listar-feature-content-wrapper {

            top: 0;

        }

        .listar-feature-content-wrapper {

            position: relative;

            margin-top: -15.5px;

        }


        .listar-feature-item .listar-feature-item-title {

            padding-top: 0;

            margin: 0 0 30px;

            text-align: center;

            line-height: 1.5;

        }


        .listar-feature-item
        .listar-feature-item-title.listar-feature-counter-added
        > span {

            margin-left: 25px;

            border-radius: 0 50px 50px 0;

        }


        .listar-feature-item .listar-feature-item-title > span {

            box-shadow: 15px 20px 30px rgba(80, 80, 80, 0.12),
            5px 0px 40px rgba(80, 80, 80, 0.1);

            padding: 10px 20px;

            display: inline-block;

            position: relative;

            background-color: #fff;

        }


        .listar-feature-item .listar-feature-item-title > span span {

            box-shadow: 15px 15px 30px rgba(80, 80, 80, 0.2),
            5px 0px 80px rgba(80, 80, 80, 0.15);

            padding: 10px 0;

            display: inline-block;

            width: 50px;

            height: 50px;

            line-height: 30px;

            white-space: nowrap;

            position: absolute;

            top: -3px;

            left: -40px;

            border-radius: 50px;

            background-color: #fff;

        }


        .listar-feature-item .listar-feature-item-title span {

            display: inline-block;

            font-size: 14px;

            line-height: normal;

            font-weight: 400;

            text-shadow: none;

            border-radius: 50px;

            color: #252525;

            font-size: 18px;

        }


        .listar-feature-item-excerpt {

            padding: 0 20px;

        }


        .listar-feature-item-excerpt {

            color: #252525;

            position: relative;

            top: 0;

            width: 100%;

            margin: 0 auto;

            text-align: center;

        }


        .pset {

            padding-top: 180px;

            padding-bottom: 80px;

        }


        .listar-map-button-text span {

            position: relative;

            display: inline-block;

            vertical-align: middle;

            font-size: 15px;

            height: 44px;

            line-height: 1.6;

            padding: 10px 25px;

            box-shadow: 0 0 31px rgba(0, 0, 0, 0.65), 0 0 4px rgba(0, 0, 0, 0.06);

            border-radius: 50px;

            background-color: rgba(35, 40, 45, 0.9);

            color: #fff;

            text-shadow: 1px 1px rgba(0, 0, 0, 0.2);

        }


        .listar-map-button-text span {

            background-color: #fff;

            color: #252525;

            text-shadow: none;

            -webkit-transition: all 0.2s ease-in;

            -moz-transition: all 0.2s ease-in;

            -ms-transition: all 0.2s ease-in;

            -o-transition: all 0.2s ease-in;

            transition: all 0.2s ease-in;

        }


        .listar-map-button-text span:after {

            border: 12px solid #fff;

        }


        .listar-map-button-text span:after {

            content: "";

            position: absolute;

            top: -14px;

            left: -14px;

            width: calc(100% + 28px);

            height: calc(100% + 28px);

            border-radius: 36px;

        }


        .listar-map-button:hover .listar-map-button-text span {

            padding: 10px 43px 10px 51px;

            -webkit-transition: all 0.2s ease-in;

            -moz-transition: all 0.2s ease-in;

            -ms-transition: all 0.2s ease-in;

            -o-transition: all 0.2s ease-in;

            transition: all 0.2s ease-in;

        }


        .listar-feature-with-image .listar-feature-item a:before {

            content: "";

            position: absolute;

            top: -74px;

            left: 50%;

            margin-left: -74px;

            width: 148px;

            height: 148px;

            border-radius: 1000px;

            z-index: 10;

        }


        .listar-feature-item.listar-feature-has-link .listar-feature-item-inner:after {

            margin-left: -25px;

            width: 50px;

            height: 50px;

            border-radius: 50px;

            box-shadow: 10px 10px 40px rgba(80, 80, 80, 0.15),
            15px 15px 30px rgba(80, 80, 80, 0.05), 0 0 120px rgba(80, 80, 80, 0.6);

            bottom: -30px;

            background-image: url("https://image.flaticon.com/icons/svg/2316/2316674.svg");

            background-size: 40%;

            color: #555;

            line-height: 52px;

        }


        .col-lg-1,
        .col-lg-2,
        .col-lg-3,
        .col-lg-4,
        .col-lg-5,
        .col-lg-6,
        .col-lg-7,
        .col-lg-8,
        .col-lg-9,
        .col-lg-10,
        .col-lg-11,
        .col-lg-12,
        .col-md-1,
        .col-md-2,
        .col-md-3,
        .col-md-4,
        .col-md-5,
        .col-md-6,
        .col-md-7,
        .col-md-8,
        .col-md-9,
        .col-md-10,
        .col-md-11,
        .col-md-12,
        .col-sm-1,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12,
        .col-xs-1,
        .col-xs-2,
        .col-xs-3,
        .col-xs-4,
        .col-xs-5,
        .col-xs-6,
        .col-xs-7,
        .col-xs-8,
        .col-xs-9,
        .col-xs-10,
        .col-xs-11,
        .col-xs-12 {

            align-self: flex-start;

        }


        .navbar .navbar-nav li a {

            color: #191919 !important;

        }

        .swiper-slider-heading {

            font-size: 35px !important;

        }

        .menu ul {

            list-style-type: none;

            margin: 0;

            padding: 0;

        }


        .menu li {

            padding: 8px;

            margin-bottom: 7px;

            background-color: #33b5e5;

            color: #ffffff;

            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);

        }


        .menu li:hover {

            background-color: #0099cc;

        }

        .flex-container {

            display: flex;

            flex-wrap: nowrap;


        }


        .flex-container > div {

            background-color: #f1f1f1;

            width: 254px;

            margin: 10px;

            text-align: center;

            line-height: 75px;

            font-size: 17px;

            font-weight: 700;

            height: 106px;

            padding-top: 16px;

        }


        .first-content {

            list-style: none;

        }

    </style>

    <!-- Meta Tags For Seo + Page Optimization -->

    <meta charset="utf-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="title" content="<?php echo e(!empty($meta->title) ? $meta->title : ''); ?>">
    <meta name="description" content="<?php echo e(!empty($meta->body) ? $meta->body : ''); ?>">
    <meta name="keywords" content="<?php echo e(!empty($meta->key_words) ? $meta->key_words : ''); ?>">


    <!-- Insert Favicon Here -->

    <link href="<?php echo e(asset('images/favicon.png')); ?>" rel="icon">


    <!-- Page Title(Name)-->

    <title>LTF</title>


    <!-- Bootstrap CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">


    <!-- Font-Awesome CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">


    <!-- Slider Revolution CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">


    <!--  Fancy Box CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">


    <!-- Circleful CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">


    <!-- Animate CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">


    <!-- Cube Portfolio CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">


    <!-- Owl Carousel CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">

    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <!-- Swiper CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">


    <!-- Custom Style CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">


    <!-- Crypto Currency CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">


    <!-- Color StyleSheet CSS File -->

    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/media/itb/itb_layout/itb_layout_styles/project_itb.css')); ?>"
          media="screen">


    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script>
    <style type="text/css">@-webkit-keyframes _gm7906 {

                               0% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                               50% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }

                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                           }

    </style>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script>
</head>


<body>

<!--<div class="body">-->

<!--    <img src="<?php echo e(asset('Frontend/images/Mercator_Blank_Map_World.png')); ?>" style="  background-size:cover;">-->


<!-- Loader -->


<!-- Loader -->


<div class="loader" style="display: none;">


    <div class="loader-inner">


        <div class="spinner">


            <div class="dot1"></div>


            <div class="dot2"></div>


        </div>


    </div>


</div>

<!-- Parent Section -->


<section class="page_content_parent_section">


    <!-- Header Section -->


    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</section>


<section class="contact_form_section" id="contact-form">


    <div class="container">


        <div class="row">


            <div class="contact_form_inner big_padding clearfix">


                <div class="col-md-12  " style="visibility: visible; ">


                    <header>


                        <div></div>

                    </header>

                    <div class="pset">

                        <div class="container">

                            <div class="row listar-feature-items">


                                <div
                                    class="col-xs-12 col-sm-6 col-md-3 listar-feature-item-wrapper listar-feature-with-image listar-height-changed"
                                    data-aos="fade-zoom-in" data-aos-group="features" data-line-height="25.2px">

                                    <div class="listar-feature-item listar-feature-has-link">


                                        <a href="<?php echo e(url('registerfront')); ?>"></a>


                                        <div class="listar-feature-item-inner">


                                            <div class="listar-feature-right-border"></div>


                                            <div class="listar-feature-block-content-wrapper">

                                                <div class="listar-feature-icon-wrapper">

                                                    <div class="listar-feature-icon-inner">

                                                        <div>

                                                            <img alt="Businesses" class="listar-image-icon"
                                                                 src="https://image.flaticon.com/icons/svg/1899/1899582.svg">

                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="listar-feature-content-wrapper" style="padding-top: 0px;">


                                                    <div class="listar-feature-item-title listar-feature-counter-added">

                  <span><span>01</span>

                    General Public </span>

                                                    </div>


                                                    <div class="listar-feature-item-excerpt">

                                                        Start publish listings to show everyone the details and goodies
                                                        of your establishment.
                                                    </div>


                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div
                                        class="listar-feature-fix-bottom-padding listar-fix-feature-arrow-button-height"></div>

                                </div>


                                <div
                                    class="col-xs-12 col-sm-6 col-md-3 listar-feature-item-wrapper listar-feature-with-image listar-height-changed"
                                    data-aos="fade-zoom-in" data-aos-group="features" data-line-height="25.2px">

                                    <div class="listar-feature-item listar-feature-has-link">


                                        <a href="<?php echo e(url('groupleader-register')); ?>"></a>


                                        <div class="listar-feature-item-inner">


                                            <div class="listar-feature-right-border"></div>


                                            <div class="listar-feature-block-content-wrapper">

                                                <div class="listar-feature-icon-wrapper">

                                                    <div class="listar-feature-icon-inner">

                                                        <div>

                                                            <img alt="Customers" class="listar-image-icon"
                                                                 src="https://image.flaticon.com/icons/svg/1899/1899506.svg">

                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="listar-feature-content-wrapper" style="padding-top: 0px;">


                                                    <div class="listar-feature-item-title listar-feature-counter-added">

                  <span><span>02</span>

                   Group Leader </span>

                                                    </div>


                                                    <div class="listar-feature-item-excerpt">

                                                        Easily find interesting places by local experts, save time by
                                                        checking listing features.
                                                    </div>


                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div
                                        class="listar-feature-fix-bottom-padding listar-fix-feature-arrow-button-height"></div>

                                </div>


                                <div
                                    class="col-xs-12 col-sm-6 col-md-3 listar-feature-item-wrapper listar-feature-with-image listar-height-changed"
                                    data-aos="fade-zoom-in" data-aos-group="features" data-line-height="25.2px">

                                    <div class="listar-feature-item listar-feature-has-link">


                                        <a href="<?php echo e(url('trade-register')); ?>"></a>


                                        <div class="listar-feature-item-inner">


                                            <div class="listar-feature-right-border"></div>


                                            <div class="listar-feature-block-content-wrapper">

                                                <div class="listar-feature-icon-wrapper">

                                                    <div class="listar-feature-icon-inner">

                                                        <div>

                                                            <img alt="Feedback" class="listar-image-icon"
                                                                 src="https://image.flaticon.com/icons/svg/1899/1899604.svg">

                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="listar-feature-content-wrapper" style="padding-top: 0px;">


                                                    <div class="listar-feature-item-title listar-feature-counter-added">

                  <span><span>03</span>

                    Trade Visitor </span>

                                                    </div>


                                                    <div class="listar-feature-item-excerpt">

                                                        Visitors discuss listings to share experiences, so businesses
                                                        get reputation consolidated.
                                                    </div>


                                                </div>

                                            </div>


                                        </div>

                                    </div>

                                    <div
                                        class="listar-feature-fix-bottom-padding listar-fix-feature-arrow-button-height"></div>

                                </div>


                                <div
                                    class="col-xs-12 col-sm-6 col-md-3 listar-feature-item-wrapper listar-feature-with-image listar-height-changed"
                                    data-aos="fade-zoom-in" data-aos-group="features" data-line-height="25.2px">

                                    <div class="listar-feature-item listar-feature-has-link">


                                        <a href="<?php echo e(url('register-exhibitor')); ?>"></a>


                                        <div class="listar-feature-item-inner">


                                            <div class="listar-feature-right-border"></div>


                                            <div class="listar-feature-block-content-wrapper">

                                                <div class="listar-feature-icon-wrapper">

                                                    <div class="listar-feature-icon-inner">

                                                        <div>

                                                            <img alt="Feedback" class="listar-image-icon"
                                                                 src="https://image.flaticon.com/icons/svg/1899/1899506.svg">

                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="listar-feature-content-wrapper" style="padding-top: 0px;">


                                                    <div class="listar-feature-item-title listar-feature-counter-added">

                  <span><span>04</span>

                    Exhabitur </span>

                                                    </div>


                                                    <div class="listar-feature-item-excerpt">

                                                        Visitors discuss listings to share experiences, so businesses
                                                        get reputation consolidated.
                                                    </div>


                                                </div>

                                            </div>


                                        </div>

                                    </div>

                                    <div
                                        class="listar-feature-fix-bottom-padding listar-fix-feature-arrow-button-height"></div>

                                </div>

                            </div>

                        </div>

                    </div>


                </div>


            </div>

        </div>

    </div>

</section>


<!-- /Parent Section Ended -->


<!-- jQuery 2.2.0-->

</div>


<script src="js/jquery.js"></script>


<!-- Google Map Api -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"
        type="text/javascript"></script>

<script src="<?php echo e(asset('Frontend/js/map.js')); ?>" type="text/javascript"></script>


<!-- REVOLUTION JS FILES -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>


<!-- Bootstrap Core JavaScript -->

<script src="js/bootstrap.min.js"></script>


<!-- Owl Carousel 2 Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>


<!-- Fancy Box Javacript -->

<script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>


<!-- Wow Js -->

<script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>


<!-- Appear Js-->

<script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>


<!-- Countdown Js -->

<script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>


<!-- Parallax Js -->

<script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>


<!-- Particles Core Js -->

<script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>


<!-- Cube Portfolio Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>


<!-- Circliful Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>


<!-- Swiper Slider Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>


<!-- Crypto Currency Core Javascript -->

<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>


<!-- Custom JavaScript -->

<script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>


</body>
</html>
<?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/createHere.blade.php ENDPATH**/ ?>