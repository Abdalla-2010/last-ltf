<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.edit')); ?> <?php echo e(trans('global.sliders.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.sliders.update", [$slider->id])); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
                <label for="group"><?php echo e(trans('global.sliders.fields.image')); ?></label>
                <input type="file"   name="image" class="form-control">
                <?php if($errors->has('image')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('image')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.sliders.fields.image_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                <label for="title"><?php echo e(trans('global.sliders.fields.title')); ?>*</label>
                <input type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($slider) ? $slider->title : '')); ?>">
                <?php if($errors->has('title')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('title')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.sliders.fields.title_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('content') ? 'has-error' : ''); ?>">
                <label for="content"><?php echo e(trans('global.sliders.fields.content')); ?></label>
                <textarea id="content" name="content" class="form-control "><?php echo e(old('content', isset($slider) ? $slider->content : '')); ?></textarea>
                <?php if($errors->has('content')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('content')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.sliders.fields.content_helper')); ?>

                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/sliders/edit.blade.php ENDPATH**/ ?>