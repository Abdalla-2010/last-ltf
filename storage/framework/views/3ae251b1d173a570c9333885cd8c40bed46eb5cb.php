

<?php $__env->startSection('content'); ?>



<div class="card">

    <div class="card-header">

        <?php echo e(trans('global.show')); ?> <?php echo e(trans('global.contactus.title')); ?>



    </div>



    <div class="card-body">

        <table class="table table-bordered table-striped">

            <tbody>

                <tr>

                    <th>

                        <?php echo e(trans('global.contactus.fields.Fname')); ?>



                    </th>

                    <td>

                        <?php echo e($contactus->Fname); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.contactus.fields.Lname')); ?>



                    </th>

                    <td>

                        <?php echo e($contactus->Lname); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.contactus.fields.email')); ?>



                    </th>

                    <td>

                        <?php echo e($contactus->email); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.contactus.fields.phone')); ?>



                    </th>

                    <td>

                        <?php echo e($contactus->phone); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.contactus.fields.msg')); ?>



                    </th>

                    <td>

                        <?php echo e($contactus->massage); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.contactus.fields.created_at')); ?>



                    </th>

                    <td>

                         <?php echo e($contactus->created_at ?? ''); ?>



                    </td>

                </tr>

            </tbody>

        </table>

    </div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/contactus/showProfile.blade.php ENDPATH**/ ?>
