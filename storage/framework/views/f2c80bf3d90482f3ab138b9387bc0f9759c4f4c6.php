
<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.myoffers.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route('admin.myoffers.store')); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                <label for="name"><?php echo e(trans('global.myoffers.fields.name')); ?>*</label>
                <input type="text" id="name" name="name" class="form-control" value="<?php echo e(old('name', isset($myoffer) ? $myoffer->name : '')); ?>">
                <?php if($errors->has('name')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('name')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.myoffers.fields.name_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('img') ? 'has-error' : ''); ?>">
                <label for="img"><?php echo e(trans('global.myoffers.fields.img')); ?>*</label>
                <input type="file" id="img" name="img" class="form-control" value="<?php echo e(old('img', isset($myoffer) ? $myoffer->img : '')); ?>">
                <?php if($errors->has('email')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('email')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.myoffers.fields.img_helper')); ?>

                </p>
            </div>

            <div class="form-group <?php echo e($errors->has('duration') ? 'has-error' : ''); ?>">
                <label for="duration"><?php echo e(trans('global.myoffers.fields.duration')); ?></label>
                <input type="text" id="duration" name="duration" class="form-control" value="<?php echo e(old('duration', isset($myoffer) ? $myoffer->duration : '')); ?>">
                <?php if($errors->has('duration')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('duration')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.myoffers.fields.duration_helper')); ?>

                </p>
            </div>
       

            <div class="form-group <?php echo e($errors->has('destination') ? 'has-error' : ''); ?>">
                <label for="destination"><?php echo e(trans('global.myoffers.fields.destination')); ?></label>
                <input type="text" id="destination" name="destination" class="form-control" value="<?php echo e(old('destination', isset($myoffer) ? $myoffer->destination : '')); ?>">
                <?php if($errors->has('destination')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('destination')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.myoffers.fields.destination_helper')); ?>

                </p>
            </div>



            <div class="form-group <?php echo e($errors->has('price') ? 'has-error' : ''); ?>">
                <label for="price"><?php echo e(trans('global.myoffers.fields.price')); ?></label>
                <input type="text" id="price" name="price" class="form-control" value="<?php echo e(old('price', isset($myoffer) ? $myoffer->price : '')); ?>">
                <?php if($errors->has('price')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('price')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.myoffers.fields.price_helper')); ?>

                </p>
            </div>


            <div class="form-group <?php echo e($errors->has('url') ? 'has-error' : ''); ?>">
                <label for="url"><?php echo e(trans('global.myoffers.fields.url')); ?></label>
                <input type="text" id="url" name="url" class="form-control" value="<?php echo e(old('url', isset($myoffer) ? $myoffer->url : '')); ?>">
                <?php if($errors->has('url')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('url')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.myoffers.fields.url_helper')); ?>

                </p>
            </div>
            
            <div>
                <input class="btn btn-danger" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/myoffers/create.blade.php ENDPATH**/ ?>