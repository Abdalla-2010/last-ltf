<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome!</title>
</head>
<body>

    <h3>    Dear <?php echo e($user->name .' '. $user->last_name); ?> , </h3>
        <p>Thank you for registering for LTF Online Travel Fair.  <br>
        We are pleased to confirm receipt of your registration details.  You registered with this email: <?php echo e($user->email); ?>. If you forgot your password, simply hit “Forgot Password“ and you'll be promoted to reset it.
        </p>
        <p>If you have any questions leading up to the event, kindly feel free to contact us anytime. <br>
             We are looking forward to seeing you on our online event!</p>

            <h3>Best Regards, 
           LTF Registration Team</h3>
               <h4>Support@cvc.vacation <br>
+ 2 22 68 24 28</h4>

</body>
</html>
 <?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/email/welcome.blade.php ENDPATH**/ ?>