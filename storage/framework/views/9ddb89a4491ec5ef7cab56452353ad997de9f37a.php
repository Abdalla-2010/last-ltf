<html lang="en">
<head><style>.dismissButton{background-color:#fff;border:1px solid #dadce0;color:#1a73e8;border-radius:4px;font-family:Roboto,sans-serif;font-size:14px;height:36px;cursor:pointer;padding:0 24px}.dismissButton:hover{background-color:rgba(66,133,244,0.04);border:1px solid #d2e3fc}.dismissButton:focus{background-color:rgba(66,133,244,0.12);border:1px solid #d2e3fc;outline:0}.dismissButton:hover:focus{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd}.dismissButton:active{background-color:rgba(66,133,244,0.16);border:1px solid #d2e2fd;box-shadow:0 1px 2px 0 rgba(60,64,67,0.3),0 1px 3px 1px rgba(60,64,67,0.15)}.dismissButton:disabled{background-color:#fff;border:1px solid #f1f3f4;color:#3c4043}
</style><style>.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}
</style><style>.gm-control-active>img{box-sizing:content-box;display:none;left:50%;pointer-events:none;position:absolute;top:50%;transform:translate(-50%,-50%)}.gm-control-active>img:nth-child(1){display:block}.gm-control-active:hover>img:nth-child(1),.gm-control-active:active>img:nth-child(1){display:none}.gm-control-active:hover>img:nth-child(2),.gm-control-active:active>img:nth-child(3){display:block}
</style><link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700"><style>.gm-ui-hover-effect{opacity:.6}.gm-ui-hover-effect:hover{opacity:1}
</style><style>.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px;box-sizing:border-box}
</style><style>@media  print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media  screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style>.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
</style><style>.gm-style img{max-width: none;}.gm-style {font: 400 11px Roboto, Arial, sans-serif; text-decoration: none;}</style>
<style>
body {
    background-image:url(Frontend/images/Mercator_Blank_Map_World.png);
    background-size:cover !important;
  
}
.navbar .navbar-nav li a {
    color: #191919 !important;
}
.swiper-slider-heading {
    font-size:35px !important;
}
.boxed {
  border: 1px solid #eee ;
  padding:20px;
  margin-bottom: 20px;
}
.boxed li {
    list-style:none;
}
.navbar-fixed-top {

            background: white;

            margin: auto;


        }
 
</style>

    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

     <meta name="description" content="">
    
  <meta name="keywords" content="">

    <!-- Insert Favicon Here -->
    <link href="<?php echo e(asset('Frontend/images/favicon.png')); ?>" rel="icon">

    <!-- Page Title(Name)-->
    <title>LTF</title>

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">

    <!-- Font-Awesome CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">

    <!-- Slider Revolution CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">

    <!--  Fancy Box CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">

    <!-- Circleful CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">

    <!-- Animate CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">

    <!-- Cube Portfolio CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">

    <!-- Owl Carousel CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.theme.default.min.css')); ?>">

    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">

    <!-- Custom Style CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">

    <!-- Crypto Currency CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">

    <!-- Color StyleSheet CSS File -->
    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!--<link rel="stylesheet" href="css/intlTelInput.css">-->
<!--  <link rel="stylesheet" href="css/demo.css">-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



<script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script><style type="text/css">@-webkit-keyframes _gm7906 {
0% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }
50% { -webkit-transform: translate3d(0px,-20px,0); -webkit-animation-timing-function: ease-in; }
100% { -webkit-transform: translate3d(0px,0px,0); -webkit-animation-timing-function: ease-out; }
}
</style><script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script></head>


<body>




<!-- Loader -->

<!-- Loader -->

<div class="loader" style="display: none;">

    <div class="loader-inner">

        <div class="spinner">

            <div class="dot1"></div>

            <div class="dot2"></div>

        </div>

    </div>

</div>
<!-- Parent Section -->

<section class="page_content_parent_section">


    <!-- Header Section -->

    <header>

        <!-- Navbar Section -->

    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <!-- /Navbar Section -->
</header>

   

      
</section>

<section class="contact_form_section" id="contact-form">

        <div class="container">







<form action="<?php echo e(route('register3')); ?>" method="post" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                    
            <div class="row">

                <div class="contact_form_inner big_padding clearfix">

                    <div class="col-md-5  " style="visibility: visible; ">
<h3 style="margin-bottom: 25px;
    margin-left: 20px;">Contact Information</h3>
                        <div class="contact_form_detail  center_on_mobile">

                            <p class="default_small_heading raleway blue_color font_200"></p>
                            
                            
<input type="hidden"  value="<?php echo e($role_id); ?>" name="role_id">

<input type="hidden"  value="<?php echo e($password); ?>" name="password">

<input type="hidden"  value="<?php echo e($email); ?>" name="email">

<input type="hidden"  value="<?php echo e($company_name); ?>" name="company_name">

<input type="hidden"  value="<?php echo e($phone); ?>" name="phone">

<input type="hidden"  value="<?php echo e($radio); ?>" name="radio">

<input type="hidden"  value="<?php echo e($status); ?>" name="status">




    <div class="form-group">
    <label for="">Title </label>
    <select name="title" class="form-control">
	<option  value="Mr"> Mr</option>
	<option value="Mrs">Mrs</option>
    </select>
  </div>
   <div class="form-group">
    <label for=""> First Name</label>
    <input type="" class="form-control" id="" aria-describedby="emailHelp" placeholder="Enter Name" value="<?php echo e($name); ?>" name="name" required>
   
  </div>
   <div class="form-group">
    <label for=""> Last Name</label>
    <input type="" class="form-control" value="<?php echo e($last); ?>" aria-describedby="emailHelp" placeholder="Enter Name" name="last" required>
   
  </div>
   <div class="form-group">
    <label for="">Designation</label>
    <input type="" class="form-control" id="" aria-describedby="emailHelp" placeholder="" name="designation" required>
   
  </div>
  
  <!--  <div class="form-group">-->
  <!--     <label for=""> Cell Phone</label>-->
  <!--     <br>-->
  <!--     <input type="tel" id="phone" name="phone" aria-describedby="" placeholder="" style="width: 200%;">-->
  <!--</div>-->
  
<div class="form-group">
    <?php if($status == 3): ?>
    <label for="exampleInputPassword1"> Organization Email</label>
    
    <?php else: ?>
        <label for="exampleInputPassword1"> Company Email</label>

    <?php endif; ?>
    
    <input type="" class="form-control" id="exampleInputPassword1" placeholder="Email" required name="company_email" required>
  </div>
  
    <div class="form-group">
    <label for=""> Company Type </label>
    
    
    <?php if($status == 3): ?>
        <h3 style="color: #337ab7;">My Organization</h3>
    <?php else: ?>
        <h3 style="color: #337ab7;display:none">My Company</h3>
    <?php endif; ?>
    
    <select name="company_type" class="form-control" required>
            				<option  value="InBound"> InBound</option>
            				<option value="OutBound">OutBound</option>
            				<option value="InBound / OutBound">InBound / OutBound</option>

            			</select>
  </div>
  
  <div class="form-group">
    <label for="exampleInputPassword1">Website</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="URL"  value="<?php echo e($website); ?>" name="url">
  </div>

 <div class="form-group">
    <label for="imgPersonal">Select Personal Image : </label>
  <input type="file" class="form-control"  id="imgPersonal"  name="imgPersonal" accept="image/*" style="height:40px">
  </div>
 
 

<a  class="btn btn-primary" style="width: 91px;margin-top:20px;color:#fff;" href="<?php echo e(url('register')); ?>"> previous </a>
                </div>

            </div>
            <div class="col-md-7">
                <div class="" >
                    
    <?php if($status == 3): ?>
    <label for="exampleInputPassword1">My Organization </label>
    
    <?php else: ?>
                <h3 style="color: #337ab7;">My Company</h3>

    <?php endif; ?>
    
    
                <hr>
                
                
                <p> 
    <?php if($status == 3): ?>
    Organization

    <?php else: ?>
   Company Name
    <?php endif; ?>
                
                :<span> <?php echo e($company_name); ?> </span> </p>
              
              <hr>
              <div class="boxed">
             <h5> Address </h5>
             <h6 style="float:right"> Edit </h6>
             <li>zip Code : <div class="form-group">
    <input type="" class="form-control" id="" aria-describedby="emailHelp" placeholder=" " style="width: 66%;" value="<?php echo e($zip_code); ?>" name="zip_code">
  
  </div></li>
             <li>Country : <div class="form-group">
    <input type="" class="form-control" id="" aria-describedby="emailHelp" placeholder=" " style="width: 66%;" name="country"  value="<?php echo e($city); ?>">
  
  </div></li>
         
               <div class="form-group">
                   
    <?php if($status == 3): ?>
        <label for="img">Select Organization Logo Image:</label>

    <?php else: ?>
    <label for="imgCompany">Select Company Logo Image:</label>

    <?php endif; ?>
    
    
  <input type="file" id="imgCompany" name="imgCompany"  class="form-control" accept="image/*" style="height: 40px;">
  </div>
</div>
<div class="row">
    <div class="col-md-6">
 <div class="form-group">
    <label for=""> Facebook</label>
    <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Facebook link" name="facebook" required>
  
  </div>
  </div>
  <div class="col-md-6">
   <div class="form-group">
    <label for=""> Twitter </label>
    <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Twitter link" name="twitter">
   
  </div>
  </div>
  </div>
  <div class="row">
      <div class="col-md-6">
   <div class="form-group">
    <label for=""> LinkedIn</label>
    <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Linkedin link" name="linkedin">
   </div>
  </div>
  <div class="col-md-6">
   <div class="form-group">
    <label for="">Instagram</label>
    <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Instagram link" name="instagram">
   
  </div>
  </div>
  </div>
    
    </div>
     <button type="submit" class="btn btn-primary" style="margin-top: 90px;width: 91px;float:right" >Next</button>


    </form>
    </section>

    <!-- /Parent Section Ended -->

<!-- jQuery 2.2.0-->
 <!--<script src="js/intlTelInput.js"></script>-->
 <!-- <script>-->
 <!--   var input = document.querySelector("#phone");-->
 <!--   window.intlTelInput(input, {-->
     
 <!--     utilsScript: "build/js/utils.js",-->
 <!--   });-->
 <!-- </script>-->

<script src="<?php echo e(asset('Frontend/js/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/build/js/intlTelInput.min.js')); ?>"></script> 
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="<?php echo e(asset('Frontend/build/js/intlTelInput-jquery.min.js')); ?>"></script> 
<!-- Google Map Api -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U" type="text/javascript"></script>
<script src="<?php echo e(asset('Frontend/js/map.js')); ?>" type="text/javascript"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/bootstrap.min.js')); ?>"></script>

<!-- Owl Carousel 2 Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>

<!-- Fancy Box Javacript -->
<script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>

<!-- Wow Js -->
<script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>

<!-- Appear Js-->
<script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>

<!-- Countdown Js -->
<script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>

<!-- Parallax Js -->
<script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

<!-- Particles Core Js -->
<script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>

<!-- Cube Portfolio Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>

<!-- Circliful Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>

<!-- Swiper Slider Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>

<!-- Crypto Currency Core Javascript -->
<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>

<!-- Custom JavaScript -->
<script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>



</body></html>

<?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/company-info.blade.php ENDPATH**/ ?>