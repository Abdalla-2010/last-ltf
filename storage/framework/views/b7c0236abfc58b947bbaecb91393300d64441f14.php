

<?php $__env->startSection('content'); ?>



<div class="card">

    <div class="card-header">

        <?php echo e(trans('global.show')); ?> <?php echo e(trans('global.librarys.title')); ?>



    </div>



    <div class="card-body">

        <table class="table table-bordered table-striped">

            <tbody>

                <tr>

                    <th>

                        <?php echo e(trans('global.librarys.fields.name')); ?>



                    </th>

                    <td>

                        <?php echo e($library->name); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.librarys.fields.video')); ?>



                    </th>

                    <td>

                        <?php echo e($library->video); ?>



                    </td>

                </tr>

            </tbody>

        </table>

    </div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/librarys/showProfile.blade.php ENDPATH**/ ?>
