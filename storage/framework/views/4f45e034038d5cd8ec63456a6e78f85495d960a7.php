<html lang="en">
<head>
    <style>.dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }

        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }

        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }

        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }

        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }

        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }

    </style>
    <style>.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }

    </style>
    <style>.gm-control-active > img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active > img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover > img:nth-child(1), .gm-control-active:active > img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover > img:nth-child(2), .gm-control-active:active > img:nth-child(3) {
            display: block
        }

    </style>
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700">
    <style>.gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

    </style>
    <style>.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }

    </style>
    <style>@media  print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media  screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>

    <style>

        body {

            background-image: url(Frontend/images/Mercator_Blank_Map_World.png);

            background-size: cover !important;


        }

        .navbar .navbar-nav li a {

            color: #191919 !important;

        }

        .swiper-slider-heading {

            font-size: 35px !important;

        }


        .flex-container {

            display: flex;

            flex-wrap: nowrap;


        }


        .flex-container > div {

            background-color: #f1f1f1;

            width: 300px;

            margin: 10px;

            text-align: center;

            line-height: 75px;

            font-size: 17px;

            font-weight: 700;

            height: 106px;

            padding-top: 16px;

            margin-left: 58px;

        }

        .partners img {

            width: 100%;

            padding: 30px;

        }

    </style>

    <!-- Meta Tags For Seo + Page Optimization -->

    <meta charset="utf-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="title" content="<?php echo e(!empty($meta->title) ? $meta->title : ''); ?>">
    <meta name="description" content="<?php echo e(!empty($meta->body) ? $meta->body : ''); ?>">
    <meta name="keywords" content="<?php echo e(!empty($meta->key_words) ? $meta->key_words : ''); ?>">


    <!-- Insert Favicon Here -->

    <link href="<?php echo e(asset('Frontend/images/favicon.png')); ?>" rel="icon">


    <!-- Page Title(Name)-->

    <title>LTF</title>


    <!-- Bootstrap CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">


    <!-- Font-Awesome CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">


    <!-- Slider Revolution CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">


    <!--  Fancy Box CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">


    <!-- Circleful CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">


    <!-- Animate CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">


    <!-- Cube Portfolio CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">


    <!-- Owl Carousel CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.theme.default.min.css')); ?>">


    <!-- Swiper CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">


    <!-- Custom Style CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">


    <!-- Crypto Currency CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">


    <!-- Color StyleSheet CSS File -->

    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">


    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('Frontend/fonts/line-icons.css')); ?>">


    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script>
    <style type="text/css">@-webkit-keyframes _gm7906 {

                               0% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                               50% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }

                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                           }

    </style>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script>
</head>


<body>


<!-- Loader -->


<!-- Loader -->


<div class="loader" style="display: none;">


    <div class="loader-inner">


        <div class="spinner">


            <div class="dot1"></div>


            <div class="dot2"></div>


        </div>


    </div>


</div>

<!-- Parent Section -->


<section class="page_content_parent_section">


    <!-- Header Section -->


    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</section>


<section class="contact_form_section" id="contact-form">


    <div class="container">


        <div class="row">


            <div class="contact_form_inner big_padding clearfix">


                <div class="col-md-12  " style="visibility: visible; ">

                    <div class="partners">

                        <div class="container">

                            <div class="row">

                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/egyptair.png')); ?>">

                                </div>


                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/flotours.png')); ?>">

                                </div>


                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/cadence.png')); ?>">


                                </div>


                            </div>

                            <div class="row">

                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/croconile.png')); ?>">

                                </div>


                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/aircairo.png')); ?>">

                                </div>


                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/avista.png')); ?>">


                                </div>


                            </div>


                            <div class="row">

                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/nile air.png')); ?>">

                                </div>


                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/righttravel.png')); ?>"
                                         style="width: 70%;margin-left: 43px;">

                                </div>


                                <div class="col-md-4">

                                    <img src="<?php echo e(asset('Frontend/images/tareek.png')); ?>">


                                </div>


                            </div>


                        </div>

                    </div>

                </div>

</section>


<!-- /Parent Section Ended -->


<!-- jQuery 2.2.0-->


<script src="<?php echo e(asset('Frontend/js/jquery.js')); ?>"></script>


<!-- Google Map Api -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"
        type="text/javascript"></script>

<script src="<?php echo e(asset('Frontend/js/map.js')); ?>" type="text/javascript"></script>


<!-- REVOLUTION JS FILES -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>


<!-- Bootstrap Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/bootstrap.min.js')); ?>"></script>


<!-- Owl Carousel 2 Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>


<!-- Fancy Box Javacript -->

<script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>


<!-- Wow Js -->

<script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>


<!-- Appear Js-->

<script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>


<!-- Countdown Js -->

<script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>


<!-- Parallax Js -->

<script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>


<!-- Particles Core Js -->

<script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>


<!-- Cube Portfolio Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>


<!-- Circliful Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>


<!-- Swiper Slider Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>


<!-- Crypto Currency Core Javascript -->

<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>


<!-- Custom JavaScript -->

<script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>


</body>
</html>
<?php /**PATH C:\xampp\htdocs\LTF\resources\views/sponsored.blade.php ENDPATH**/ ?>