<?php $__env->startSection('content'); ?>

    <div class="card">
        <div class="card-header">
            <?php echo e(trans('global.show')); ?> <?php echo e(trans('global.visitor.title_singular')); ?>

        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.type')); ?>

                    </th>
                    <td>
                        <?php echo e($user->type); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.name')); ?>

                    </th>
                    <td>
                        <?php echo e($user->name); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.last-name')); ?>

                    </th>
                    <td>
                        <?php echo e($user->last_name); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.email')); ?>

                    </th>
                    <td>
                        <?php echo e($user->email); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.phone')); ?>

                    </th>
                    <td>
                        <?php echo e($user->phone); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.imgPersonal')); ?>

                    </th>
                    <td>
                        <img src="<?php echo e(asset($user->imgPersonal)); ?>" style="height: 50px; width: 100px">
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.company')); ?>

                    </th>
                    <td>
                        <?php echo e($user->company); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.company-email')); ?>

                    </th>
                    <td>
                        <?php echo e($user->company_email); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.imgCompany')); ?>

                    </th>
                    <td>
                        <img src="<?php echo e(asset($user->imgCompany)); ?>" style="height: 50px; width: 100px">
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.website')); ?>

                    </th>
                    <td>
                        <a href="<?php echo e($user->website); ?>" target="_blank"><?php echo e($user->website); ?></a>
                    </td>
                </tr>
                <?php if($user->facebook != null): ?>
                    <tr>
                        <th>
                            <?php echo e(trans('global.user.fields.facebook')); ?>

                        </th>
                        <td>
                            <a href="<?php echo e($user->facebook); ?>" target="_blank">Facebook</a>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if($user->linkedin != null): ?>
                    <tr>
                        <th>
                            <?php echo e(trans('global.user.fields.linkedin')); ?>

                        </th>
                        <td>
                            <a href="<?php echo e($user->linkedin); ?>" target="_blank">LinkedIn</a>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if($user->twitter != null): ?>
                    <tr>
                        <th>
                            <?php echo e(trans('global.user.fields.twitter')); ?>

                        </th>
                        <td>
                            <a href="<?php echo e($user->twitter); ?>" target="_blank">Twitter</a>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if($user->instgram != null): ?>
                    <tr>
                        <th>
                            <?php echo e(trans('global.user.fields.instgram')); ?>

                        </th>
                        <td>
                            <a href="<?php echo e($user->instgram); ?>" target="_blank">Instagram</a>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.designation')); ?>

                    </th>
                    <td>
                        <?php echo e($user->designation); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.zip-code')); ?>

                    </th>
                    <td>
                        <?php echo e($user->zip_code); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.country')); ?>

                    </th>
                    <td>
                        <?php echo e($user->country); ?>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.travel')); ?>

                    </th>
                    <td>
                        <ul>
                            <?php $__currentLoopData = $user->travel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $travel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($travel); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.languages')); ?>

                    </th>
                    <td>
                        <ul>
                            <?php $__currentLoopData = $user->languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($language); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php echo e(trans('global.user.fields.destination')); ?>

                    </th>
                    <td>
                        <ul>
                            <?php $__currentLoopData = $user->destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($destination); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/visitors/showProfile.blade.php ENDPATH**/ ?>
