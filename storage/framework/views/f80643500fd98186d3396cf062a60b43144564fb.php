<html lang="en">
<head>
    <style>.dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }

        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }

        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }

        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }

        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }

        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }
    </style>
    <style>.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }
    </style>
    <style>/*.gm-control-active>img{box-sizing:content-box;display:none;left:50%;pointer-events:none;position:absolute;top:50%;transform:translate(-50%,-50%)}.gm-control-active>img:nth-child(1){display:block}.gm-control-active:hover>img:nth-child(1),.gm-control-active:active>img:nth-child(1){display:none}.gm-control-active:hover>img:nth-child(2),.gm-control-active:active>img:nth-child(3){display:block}*/
        *

        /</style>
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700">
    <style>.gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

        *

        /
    </style>
    <style>.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }
    </style>
    <style>@media  print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media  screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }
    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>
    <style>
        body {
            background-image: url(assets/img//Mercator_Blank_Map_World.png);
            background-size: cover !important;

        }

        .body {
            background-image: url(Frontend/images/Mercator_Blank_Map_World.png);
            background-size: cover !important;
            opacity: 0.8;
        }

        .background {
            background: #fff;
            opacity: 0.9;
        }

        .navbar-fixed-top {
            background: white;
            margin: auto;

        }

        .navbar .navbar-nav li a {
            color: #191919 !important;
        }

        .swiper-slider-heading {
            font-size: 35px !important;
        }

        .menu ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        .menu li {
            padding: 8px;
            margin-bottom: 7px;
            background-color: #33b5e5;
            color: #ffffff;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        }

        .menu li:hover {
            background-color: #0099cc;
        }

        p {
            font-size: 15px !important;
            color: #777;
        }

        .counter {
            background-color: #f5f5f5;
            padding: 20px 0;
            border-radius: 5px;
        }

        .count-title {
            font-size: 40px;
            font-weight: normal;
            margin-top: 10px;
            margin-bottom: 0;
            text-align: center;
        }

        .count-text {
            font-size: 13px;
            font-weight: normal;
            margin-top: 10px;
            margin-bottom: 0;
            text-align: center;
        }

        .fa-2x {
            margin: 0 auto;
            float: none;
            display: table;
            color: #4ad1e5;
        }

        .countup {
            padding: 30px;
        }
    </style>
    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="title" content="<?php echo e(!empty($meta->title) ? $meta->title : ''); ?>">
    <meta name="description" content="<?php echo e(!empty($meta->body) ? $meta->body : ''); ?>">
    <meta name="keywords" content="<?php echo e(!empty($meta->key_words) ? $meta->key_words : ''); ?>">

    <!-- Page Title(Name)-->
    <title>LTF</title>

    <!-- Insert Favicon Here -->
    <link href="images/favicon.png" rel="icon">

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">

    <!-- Font-Awesome CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">

    <!-- Slider Revolution CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">

    <!--  Fancy Box CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">

    <!-- Circleful CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">

    <!-- Animate CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">

    <!-- Cube Portfolio CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">

    <!-- Owl Carousel CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.theme.default.min.css')); ?>">

    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">

    <!-- Custom Style CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">

    <!-- Crypto Currency CSS File -->
    <link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">

    <!-- Color StyleSheet CSS File -->
    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">

    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('Frontend/fonts/line-icons.css')); ?>">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('Frontend/css/main1.css')); ?>">


    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script>
    <style type="text/css">@-webkit-keyframes _gm7906 {
                               0% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }
                               50% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }
                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }
                           }
    </style>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script>
</head>
<body>


<!--<div class="body">-->
<!--    <div class="background">-->
<div class="loader" style="display: none;">

    <div class="loader-inner">

        <div class="spinner">

            <div class="dot1"></div>

            <div class="dot2"></div>

        </div>

    </div>

</div>

<section class="page_content_parent_section">


    <!-- Header Section -->

    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</section>

<!-- About Section Start -->
<section id="about" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title-header text-center" style="margin-top:50px">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s"><?php echo $about[0]->name; ?> </h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s"><?php echo $about[0]->body; ?></p>
                </div>
            </div>
        </div>
        <p style="margin-left:10px;"> <?php echo $about[1]->body; ?> </p> <br>

        <p style="margin-left:10px;"> <?php echo $about[2]->body; ?> </p>
    </div>
</section>
<!-- About Section End -->

<!-- countup Section start -->
<div class="countup">
    <div class="container">
        <div class="row text-center">

            <div class="col-md-3">
                <div class="counter">
                    <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                    <h2 class="timer count-title count-number" data-to="100" data-speed="1500"></h2>
                    <p class="count-text ">Number of Visitor</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="counter">
                    <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                    <h2 class="timer count-title count-number" data-to="1700" data-speed="1500"></h2>
                    <p class="count-text ">Number of Exhibitor</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="counter">
                    <i class="fa fa-globe fa-2x" aria-hidden="true"></i>
                    <h2 class="timer count-title count-number" data-to="11900" data-speed="1500"></h2>
                    <p class="count-text ">countries</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="counter">
                    <i class="fa fa-user fa-2x" aria-hidden="true"></i>

                    <h2 class="timer count-title count-number" data-to="157" data-speed="1500"></h2>
                    <p class="count-text "> Group Leaders</p>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- countup Section End -->

<!-- Services Section Start -->
<section id="services" class="services section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title-header text-center">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s"><?php echo !empty($about[9]->name) ? $about[9]->name :''; ?></h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s"><?php echo !empty($about[9]->body) ? $about[9]->body : ''; ?></p>
                </div>
            </div>
        </div>
        <div class="row services-wrapper">
            <!-- Services item -->
            <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                <div class="services-item wow fadeInDown" data-wow-delay="0.2s">
                    <div class="icon">
                        <i class="lni-heart"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#"><?php echo !empty($about[3]->name) ? $about[3]->name : ''; ?></a></h3>
                        <p><?php echo !empty($about[3]->body) ? $about[3]->body : ''; ?></p>
                    </div>
                </div>
            </div>
            <!-- Services item -->
            <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                <div class="services-item wow fadeInDown" data-wow-delay="0.4s">
                    <div class="icon">
                        <i class="lni-gallery"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#"><?php echo !empty($about[4]->name) ? $about[4]->name : ''; ?></a></h3>
                        <p><?php echo !empty($about[4]->body) ? $about[4]->body : ''; ?></p>
                    </div>
                </div>
            </div>
            <!-- Services item -->
            <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                <div class="services-item wow fadeInDown" data-wow-delay="0.6s">
                    <div class="icon">
                        <i class="lni-envelope"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#"><?php echo !empty($about[5]->name) ? $about[5]->name : ''; ?></a></h3>
                        <p><?php echo !empty($about[5]->body) ? $about[5]->body : ''; ?></p>
                    </div>
                </div>
            </div>
            <!-- Services item -->
            <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                <div class="services-item wow fadeInDown" data-wow-delay="0.8s">
                    <div class="icon">
                        <i class="lni-cup"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#"><?php echo !empty($about[6]->name) ? $about[6]->name : ''; ?></a></h3>
                        <p><?php echo !empty($about[6]->body) ? $about[6]->body : ''; ?></p>
                    </div>
                </div>
            </div>
            <!-- Services item -->
            <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                <div class="services-item wow fadeInDown" data-wow-delay="1s">
                    <div class="icon">
                        <i class="lni-user"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#"><?php echo !empty($about[7]->name) ? $about[7]->name : ''; ?></a></h3>
                        <p><?php echo !empty($about[7]->body) ? $about[7]->body : ''; ?></p>
                    </div>
                </div>
            </div>
            <!-- Services item -->
            <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                <div class="services-item wow fadeInDown" data-wow-delay="1.2s">
                    <div class="icon">
                        <i class="lni-bubble"></i>
                    </div>
                    <div class="services-content">
                        <h3><a href="#"><?php echo !empty($about[8]->name) ? $about[8]->name : ''; ?></a></h3>
                        <p><?php echo !empty($about[8]->body) ? $about[8]->body : ''; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->


<!-- Go to Top Link -->
<!--<a href="#" class="back-to-top">-->
<!--	<i class="lni-chevron-up"></i>-->
<!--</a>-->

<!--</div>-->
<!--</div>-->


<script>
    (function ($) {
        $.fn.countTo = function (options) {
            options = options || {};

            return $(this).each(function () {
                // set options for current element
                var settings = $.extend({}, $.fn.countTo.defaults, {
                    from: $(this).data('from'),
                    to: $(this).data('to'),
                    speed: $(this).data('speed'),
                    refreshInterval: $(this).data('refresh-interval'),
                    decimals: $(this).data('decimals')
                }, options);

                // how many times to update the value, and how much to increment the value on each update
                var loops = Math.ceil(settings.speed / settings.refreshInterval),
                    increment = (settings.to - settings.from) / loops;

                // references & variables that will change with each update
                var self = this,
                    $self = $(this),
                    loopCount = 0,
                    value = settings.from,
                    data = $self.data('countTo') || {};

                $self.data('countTo', data);

                // if an existing interval can be found, clear it first
                if (data.interval) {
                    clearInterval(data.interval);
                }
                data.interval = setInterval(updateTimer, settings.refreshInterval);

                // initialize the element with the starting value
                render(value);

                function updateTimer() {
                    value += increment;
                    loopCount++;

                    render(value);

                    if (typeof (settings.onUpdate) == 'function') {
                        settings.onUpdate.call(self, value);
                    }

                    if (loopCount >= loops) {
                        // remove the interval
                        $self.removeData('countTo');
                        clearInterval(data.interval);
                        value = settings.to;

                        if (typeof (settings.onComplete) == 'function') {
                            settings.onComplete.call(self, value);
                        }
                    }
                }

                function render(value) {
                    var formattedValue = settings.formatter.call(self, value, settings);
                    $self.html(formattedValue);
                }
            });
        };

        $.fn.countTo.defaults = {
            from: 0,               // the number the element should start at
            to: 0,                 // the number the element should end at
            speed: 1000,           // how long it should take to count between the target numbers
            refreshInterval: 100,  // how often the element should be updated
            decimals: 0,           // the number of decimal places to show
            formatter: formatter,  // handler for formatting the value before rendering
            onUpdate: null,        // callback method for every time the element is updated
            onComplete: null       // callback method for when the element finishes updating
        };

        function formatter(value, settings) {
            return value.toFixed(settings.decimals);
        }
    }(jQuery));

    jQuery(function ($) {
        // custom formatting example
        $('.count-number').data('countToOptions', {
            formatter: function (value, options) {
                return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
            }
        });

        // start all the timers
        $('.timer').each(count);

        function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
            $this.countTo(options);
        }
    });
</script>
<script src="js/jquery.js"></script>

<!-- Google Map Api -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"
        type="text/javascript"></script>
<script src="<?php echo e(asset('Frontend/js/map.js')); ?>" type="text/javascript"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>

<!-- Contact Form JavaScript File -->
<!--<script src="<?php echo e(asset('Frontend/js/contactform.js')); ?>"></script>-->

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>


<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/bootstrap.min.js')); ?>"></script>

<!-- Owl Carousel 2 Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>
<script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>

<!-- Fancy Box Javacript -->
<script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>

<!-- Wow Js -->
<script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>

<!-- Appear Js-->
<script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>

<!-- Countdown Js -->
<script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>

<!-- Parallax Js -->
<script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

<!-- Particles Core Js -->
<script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>

<!-- Cube Portfolio Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>

<!-- Circliful Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>

<!-- Swiper Slider Core JavaScript -->
<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>

<!-- Crypto Currency Core Javascript -->
<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>

<!-- Custom JavaScript -->
<script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\LTF\resources\views/about.blade.php ENDPATH**/ ?>