<html lang="en">
<head>
    <style>.dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }

        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }

        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }

        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }

        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }

        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }

    </style>
    <style>.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }

    </style>
    <style>.gm-control-active > img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active > img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover > img:nth-child(1), .gm-control-active:active > img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover > img:nth-child(2), .gm-control-active:active > img:nth-child(3) {
            display: block
        }

    </style>
    <link type="text/css" rel="stylesheet" href="">
    <style>.gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

    </style>
    <style>.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }

    </style>
    <style>@media  print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media  screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


    <!-- Meta Tags For Seo + Page Optimization -->

    <meta charset="utf-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="title" content="<?php echo e(!empty($meta->title) ? $meta->title : ''); ?>">
    <meta name="description" content="<?php echo e(!empty($meta->body) ? $meta->body : ''); ?>">
    <meta name="keywords" content="<?php echo e(!empty($meta->key_words) ? $meta->key_words : ''); ?>">


    <!-- Insert Favicon Here -->

<!--<link href="<?php echo e(asset('Frontend/images/favicon.png')); ?>" rel="icon">-->


    <!-- Page Title(Name)-->

    <title>LTF</title>


    <!-- Bootstrap CSS File -->

<!--<link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">-->


    <!-- Font-Awesome CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">

    <!--<link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free.min.css" media="all">-->

    <!--<link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-font-face.min.css" media="all">-->

    <!--<link rel="stylesheet" href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css" media="all">-->


    <!-- Slider Revolution CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">


    <!--  Fancy Box CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">


    <!-- Circleful CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">


    <!-- Animate CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">


    <!-- Cube Portfolio CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">


    <!-- Owl Carousel CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.theme.default.min.css')); ?>">


    <!-- Swiper CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">


    <!-- Custom Style CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/counters.css')); ?>">


    <!-- Crypto Currency CSS File -->

<!--<link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">-->


    <!-- Color StyleSheet CSS File -->

    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">


    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('Frontend/fonts/line-icons.css')); ?>">


<!--<link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">-->

    <!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->

    <!--<link rel="stylesheet" href="https://unpkg.com/flickity@2.0/dist/flickity.min.css">-->

    <!--<script src="https://unpkg.com/flickity@2.0/dist/flickity.pkgd.min.js"></script>-->

    <style>

        .carousel-wrapper {

            position: relative;

            width: 70%;

            height: 70%;

            top: 50%;

            left: 50%;

            transform: translate(-50%, -50%);


            overflow: hidden;

        }

        .carousel-wrapper .carousel {

            position: absolute;

            top: 50%;

            transform: translateY(-50%);

            width: 100%;

            height: auto;

        }

        .navbar {

            margin: 20px !important;

        }

        .carousel-wrapper .carousel .carousel-cell {

            padding: 10px;

            background-color: #FFFFFF;

            opacity: 0.7;

            width: 20%;

            height: auto;

            min-width: 120px;

            margin: 0 20px;

            transition: transform 500ms ease;

        }

        .carousel-wrapper .carousel .carousel-cell .more {

            display: block;

            opacity: 0;

            margin: 5px 0 15px 0;

            text-align: center;

            font-size: 10px;

            color: #CFCFCF;

            text-decoration: none;

            transition: opacity 300ms ease;

        }

        .carousel-wrapper .carousel .carousel-cell .more:hover, .carousel-wrapper .carousel .carousel-cell .more:active, .carousel-wrapper .carousel .carousel-cell .more:visited, .carousel-wrapper .carousel .carousel-cell .more:focus {

            color: #CFCFCF;

            text-decoration: none;

        }

        .carousel-wrapper .carousel .carousel-cell .line {

            position: absolute;

            width: 2px;

            height: 0;

            background-color: black;

            left: 50%;

            margin: 5px 0 0 -1px;

            transition: height 300ms ease;

            display: block;

        }

        .carousel-wrapper .carousel .carousel-cell .price {

            position: absolute;

            font-weight: 700;

            margin: 45px auto 0 auto;

            left: 50%;

            transform: translate(-50%);

            opacity: 0;

            transition: opacity 300ms ease 300ms;

        }

        .carousel-wrapper .carousel .carousel-cell .price sup {

            top: 2px;

            position: absolute;

        }

        .carousel-wrapper .carousel .carousel-cell.is-selected {

            transform: scale(1.2);

        }

        .carousel-wrapper .carousel .carousel-cell.is-selected .line {

            height: 35px;

        }

        .carousel-wrapper .carousel .carousel-cell.is-selected .price, .carousel-wrapper .carousel .carousel-cell.is-selected .more {

            opacity: 1;

        }

        .carousel-wrapper .flickity-page-dots {

            display: none;

        }

        .carousel-wrapper .flickity-viewport, .carousel-wrapper .flickity-slider {

            overflow: visible;

        }

        .counter {

            background-color: #f5f5f5;

            padding: 20px 0;

            border-radius: 5px;

            margin-bottom: 21px;

        }


        .count-title {

            font-size: 40px;

            font-weight: normal;

            margin-top: 10px;

            margin-bottom: 0;

            text-align: center;

        }


        .count-text {

            font-size: 13px;

            font-weight: normal;

            margin-top: 10px;

            margin-bottom: 0;

            text-align: center;

        }


        .fa-2x {

            margin: 0 auto;

            float: none;

            display: table;

            color: #4ad1e5;

        }

        @media  only screen and (max-width: 768px) {

            body {

                overflow-y: scroll !important;

            }

        }

        .navbar .navbar-nav li a {

            color: #fff;

        }

    </style>

    <script>

        const second = 1000,

            minute = second * 60,

            hour = minute * 60,

            day = hour * 24;


        let countDown = new Date('Sep 30, 2020 00:00:00').getTime(),

            x = setInterval(function () {


                let now = new Date().getTime(),

                    distance = countDown - now;


                document.getElementById('days').innerText = Math.floor(distance / (day)),

                    document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),

                    document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),

                    document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);


                document.getElementById('day').innerText = Math.floor(distance / (day)),

                    document.getElementById('hour').innerText = Math.floor((distance % (day)) / (hour)),

                    document.getElementById('minute').innerText = Math.floor((distance % (hour)) / (minute)),

                    document.getElementById('second').innerText = Math.floor((distance % (minute)) / second);


                //do something later when date is reached

                //if (distance < 0) {

                //  clearInterval(x);

                //  'IT'S MY BIRTHDAY!;

                //}


            }, second)

    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script>
    <style type="text/css">@-webkit-keyframes _gm7906 {

                               0% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                               50% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }

                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                           }

    </style>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script>
</head>

<!--<body  data-spy="scroll" data-target=".navbar" data-offset="50" class=" particles_special_id crypto_currency_version" style="overflow-y: hidden;"><div class="parallax-mirror" style="visibility: hidden; z-index: -100; position: fixed; top: 0px; left: 0px; overflow-y: hidden; height: 942px; width: 1519px;"><img class="parallax-slider" src="images/stock_exchange.jpg" style="position: absolute;overflow-y: hidden; backg; width: 1519px; max-width: none;"></div><div class="parallax-mirror" style="visibility: visible; z-index: -100;overflow-y: hidden; position: fixed; top: 0px; left: 0px; overflow: hidden; transform: translate3d(0px, 0px, 0px);  width: 1519px;"><img class="parallax-slider" src="images/slider_slide_16.jpg" style="transform: translate3d(0px, -279px, 0px); position: absolute; height: 854px; width: 1519px; max-width: none; background-size:cover;background-repeat:no-repeat;overflow-y: hidden;"></div>-->


<body data-spy="scroll" data-target=".navbar" data-offset="50" class=" particles_special_id crypto_currency_version"
      style="overflow-y: hidden;">

<div class="parallax-mirror"
     style="visibility: hidden; z-index: -100; position: fixed; top: 0px; left: 0px; overflow-y: hidden; height: 942px; width: 1519px;">

    <img class="parallax-slider" src=""
         style="position: absolute;overflow-y: hidden; backg; width: 1519px; max-width: none;"></div>

<div class="parallax-mirror"
     style="visibility: visible; z-index: -100;overflow-y: hidden; position: fixed; top: 0px; left: 0px; overflow: hidden; transform: translate3d(0px, 0px, 0px);  width: 1519px;">
    <img class="parallax-slider"></div>


<!-- Loader -->


<!-- Loader -->


<div class="loader" style="display: none;">


    <div class="loader-inner">


        <div class="spinner">


            <div class="dot1"></div>


            <div class="dot2"></div>


        </div>


    </div>


</div>

<!-- Parent Section -->


<section class="page_content_parent_section">


    <!-- Header Section -->


    <header>


        <!-- Navbar Section -->


    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



    <!-- /Navbar Section -->


        <!-- Main Slider Section -->


        <section class="page_content_main_slider" id="home">

            <!--<div class="crypto_slider_main swiper-container parallax-window swiper-container-horizontal" data-parallax="scroll" data-image-src="images/slider_slide_16.jpg" style="background-size: cover; ">-->


            <div class="crypto_slider_wrapper" style=" background: rgba(0,0,0,.6);">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->

                    <ol class="carousel-indicators">

                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

                        <li data-target="#myCarousel" data-slide-to="1"></li>

                        <li data-target="#myCarousel" data-slide-to="2"></li>

                        <li data-target="#myCarousel" data-slide-to="3"></li>

                    </ol>


                    <!-- Wrapper for slides -->

                    <div class="carousel-inner">
                        <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($loop->first): ?>
                                <div class="item active">


                                    <img src="<?php echo e($slider->image); ?>" alt="LTF"
                                         style="background-size: cover;">

                                    <div class="center-content" style="margin-bottom:300px;">

                                        <div class="intro-content text-center">

                                            <h1 class="intro-title">We are Comming Soon<span class="obak">!</span></h1>

                                            <i class="fa fa-clock-o fa-spin fa-5x fa-clock-spin"
                                               style="color:#fff;"></i>

                                            <ul class="countdown">

                                                <li>

                                                    <span id="day" class="days">00</span>

                                                    <p class="days-ref">days</p>

                                                </li>

                                                <li class="seperator"><i class="fa fa-clock-o"></i><br><br><i
                                                        class="fa fa-clock-o"></i></li>

                                                <li>

                                                    <span id="hour" class="hours">00</span>

                                                    <p class="hours-ref">hours</p>

                                                </li>

                                                <li class="seperator"><i class="fa fa-clock-o"></i><br><br><i
                                                        class="fa fa-clock-o"></i></li>

                                                <li>

                                                    <span id="minute" class="minutes">00</span>

                                                    <p class="minutes-ref">minutes</p>

                                                </li>

                                                <li class="seperator"><i class="fa fa-clock-o"></i><br><br><i
                                                        class="fa fa-clock-o"></i></li>

                                                <li>

                                                    <span id="second" class="seconds">00</span>

                                                    <p class="seconds-ref">seconds</p>

                                                </li>

                                            </ul>

                                            <div class="intro-subtitle">

                                                <p>We are currently Creating Something Awesome.</p>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            <?php else: ?>
                                <?php if(!empty($slider->image)): ?>
                                    <div class="item">
                                        <img src="<?php echo e($slider->image); ?>" alt="Chicago" style="width:100%;">
                                        
                                        
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>


                    <!-- Left and right controls -->

                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">

                        <span class="glyphicon glyphicon-chevron-left"></span>

                        <span class="sr-only">Previous</span>

                    </a>

                    <a class="right carousel-control" href="#myCarousel" data-slide="next">

                        <span class="glyphicon glyphicon-chevron-right"></span>

                        <span class="sr-only">Next</span>

                    </a>

                </div>


        </section>

        <div class="co" style="background: #eee;">

            <div class="container" style=" color: #333;

  margin: 0 auto;

  padding: 0.5rem;

  text-align: center;">

                <h1 style="font-weight: 800;

    margin-top: 58px;

    color: #1e213f; font-family: latobold;">Countdown To Event</h1>


                <ul style="display: flex;;

  font-size: 1.5em;

  list-style-type: none;

  padding: 1em;

  text-transform: uppercase;">

                    <li><span id="days" style="display: block;

  font-size: 2.5rem; margin-left:10px;"></span>Days
                    </li>&nbsp;&nbsp;&nbsp;


                    <li><span id="hours" style="display: block;

  font-size: 2.5rem;"></span>Hours
                    </li>&nbsp;&nbsp;&nbsp;


                    <li><span id="minutes" style="display: block;

  font-size: 2.5rem;"></span>Min
                    </li>&nbsp;&nbsp;&nbsp;


                    <li><span id="seconds" style="display: block;

  font-size: 2.5rem;"></span>Seconds
                    </li>

                </ul>

            </div>

        </div>

        <div class="container">


            <div class="row text-center">

                <div class="col-xs-6">

                    <div class="counter">

                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>

                        <h2 class="timer count-title count-number" data-to="100" data-speed="1500"></h2>

                        <p class="count-text ">Number of Visitors</p>

                    </div>

                </div>

                <div class="col-xs-6">

                    <div class="counter">

                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>

                        <h2 class="timer count-title count-number" data-to="1700" data-speed="1500"></h2>

                        <p class="count-text ">Number of Exhibitor</p>

                    </div>

                </div>

                <div class="col-xs-6">

                    <div class="counter">

                        <i class="fa fa-globe fa-2x" aria-hidden="true"></i>

                        <h2 class="timer count-title count-number" data-to="11900" data-speed="1500"></h2>

                        <p class="count-text ">Countries</p>

                    </div>
                </div>

                <div class="col-xs-6">

                    <div class="counter">

                        <i class="fa fa-user fa-2x" aria-hidden="true"></i>

                        <h2 class="timer count-title count-number" data-to="157" data-speed="1500"></h2>

                        <p class="count-text ">Group Leaders</p>

                    </div>

                </div>

            </div>

        </div>


        <!-- /Parent Section Ended -->


        <script>

            (function ($) {

                $.fn.countTo = function (options) {

                    options = options || {};


                    return $(this).each(function () {

                        // set options for current element

                        var settings = $.extend({}, $.fn.countTo.defaults, {

                            from: $(this).data('from'),

                            to: $(this).data('to'),

                            speed: $(this).data('speed'),

                            refreshInterval: $(this).data('refresh-interval'),

                            decimals: $(this).data('decimals')

                        }, options);


                        // how many times to update the value, and how much to increment the value on each update

                        var loops = Math.ceil(settings.speed / settings.refreshInterval),

                            increment = (settings.to - settings.from) / loops;


                        // references & variables that will change with each update

                        var self = this,

                            $self = $(this),

                            loopCount = 0,

                            value = settings.from,

                            data = $self.data('countTo') || {};


                        $self.data('countTo', data);


                        // if an existing interval can be found, clear it first

                        if (data.interval) {

                            clearInterval(data.interval);

                        }

                        data.interval = setInterval(updateTimer, settings.refreshInterval);


                        // initialize the element with the starting value

                        render(value);


                        function updateTimer() {

                            value += increment;

                            loopCount++;


                            render(value);


                            if (typeof (settings.onUpdate) == 'function') {

                                settings.onUpdate.call(self, value);

                            }


                            if (loopCount >= loops) {

                                // remove the interval

                                $self.removeData('countTo');

                                clearInterval(data.interval);

                                value = settings.to;


                                if (typeof (settings.onComplete) == 'function') {

                                    settings.onComplete.call(self, value);

                                }

                            }

                        }


                        function render(value) {

                            var formattedValue = settings.formatter.call(self, value, settings);

                            $self.html(formattedValue);

                        }

                    });

                };


                $.fn.countTo.defaults = {

                    from: 0,               // the number the element should start at

                    to: 0,                 // the number the element should end at

                    speed: 1000,           // how long it should take to count between the target numbers

                    refreshInterval: 100,  // how often the element should be updated

                    decimals: 0,           // the number of decimal places to show

                    formatter: formatter,  // handler for formatting the value before rendering

                    onUpdate: null,        // callback method for every time the element is updated

                    onComplete: null       // callback method for when the element finishes updating

                };


                function formatter(value, settings) {

                    return value.toFixed(settings.decimals);

                }

            }(jQuery));


            jQuery(function ($) {

                // custom formatting example

                $('.count-number').data('countToOptions', {

                    formatter: function (value, options) {

                        return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');

                    }

                });


                // start all the timers

                $('.timer').each(count);


                function count(options) {

                    var $this = $(this);

                    options = $.extend({}, options || {}, $this.data('countToOptions') || {});

                    $this.countTo(options);

                }

            });

        </script>

        <script>

            /* ==========================================================================

                   countdown timer

                   ========================================================================== */

            jQuery('#clock').countdown('2020/06/21', function (event) {

                var $this = jQuery(this).html(event.strftime(''

                    + '<div class="time-entry days"><span>%-D</span> Days</div> '

                    + '<div class="time-entry hours"><span>%H</span> Hours</div> '

                    + '<div class="time-entry minutes"><span>%M</span> Minutes</div> '

                    + '<div class="time-entry seconds"><span>%S</span> Seconds</div> '));

            });


        </script>

        <!-- jQuery 2.2.0-->

        <script src="<?php echo e(asset('Frontend/js/jquery.js')); ?>"></script>


        <!-- Google Map Api -->

        <script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"
                type="text/javascript"></script>

    <!--<script src="<?php echo e(asset('Frontend/js/map.js')); ?>" type="text/javascript"></script>-->


        <!-- REVOLUTION JS FILES -->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>-->


        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>-->

        <script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>-->

    <!--<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>-->

        <script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.downCount.js')); ?>"></script>


        <!-- Bootstrap Core JavaScript -->

    <!--<script src="<?php echo e(asset('Frontend/js/bootstrap.min.js')); ?>"></script>-->


        <!-- Owl Carousel 2 Core JavaScript -->

        <script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>

        <script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>


        <!-- Fancy Box Javacript -->

        <script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>


        <!-- Wow Js -->

        <script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>


        <!-- Appear Js-->

        <script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>


        <!-- Countdown Js -->

        <script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>


        <!-- Parallax Js -->

        <script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>


        <!-- Particles Core Js -->

        <script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>


        <!-- Cube Portfolio Core JavaScript -->

        <script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>


        <!-- Circliful Core JavaScript -->

    <!--<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>-->


        <!-- Swiper Slider Core JavaScript -->

    <!--<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>-->


        <!-- Crypto Currency Core Javascript -->

    <!--<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>-->


        <!-- Custom JavaScript -->

        <script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>


</body>
</html>
<?php /**PATH C:\xampp\htdocs\LTF\resources\views/main.blade.php ENDPATH**/ ?>