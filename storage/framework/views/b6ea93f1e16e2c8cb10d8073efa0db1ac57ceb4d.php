<?php $__env->startSection('content'); ?>
   
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.contactus.title_singular')); ?> <?php echo e(trans('global.list')); ?>

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            <?php echo e(trans('global.contactus.fields.Fname')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.contactus.fields.Lname')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.contactus.fields.email')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.contactus.fields.phone')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.contactus.fields.msg')); ?>

                        </th>
                        <th>
                        <?php echo e(trans('global.contactus.fields.created_at')); ?>

                        </th>
                        <th>
                             Control<!--&nbsp;-->
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($contact->id); ?>">
                            <td>

                            </td>
                            <td>
                                <?php echo e($contact->Fname ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($contact->Lname ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($contact->email ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($contact->phone ?? ''); ?>

                            </td>
                            <td> 
                                <?php if(strlen($contact->massage) >= 50 ): ?>
                                
                                <?php echo e(substr(strip_tags($contact->massage),0,50) ?? ''); ?>

                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('contactus_show')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('admin.contactus.show', $contact->id)); ?>">
                                        Read more..
                                    </a>
                                <?php endif; ?>
                            
                            <?php else: ?>
                                <?php echo e($contact->massage ?? ''); ?>

                            <?php endif; ?>
                            
                            </td>
                            <td>
                                <?php echo e($contact->created_at ?? ''); ?>

                            </td>
                            <td>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('contactus_show')): ?>
                                    <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.contactus.show', $contact->id)); ?>">
                                        <?php echo e(trans('global.view')); ?>

                                    </a>
                                <?php endif; ?>
                                <a class="btn btn-xs btn-success" href="#">
                                        Reply
                                    </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "<?php echo e(route('admin.contactus.massDestroy')); ?>",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

        return
      }

      if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
 let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('contactus_delete')): ?>
  dtButtons.push(deleteButton)
<?php endif; ?>
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/contactus/index.blade.php ENDPATH**/ ?>