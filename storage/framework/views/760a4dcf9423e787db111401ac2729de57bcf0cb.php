<?php $__env->startSection('content'); ?>
    <style>
        .page-content {
            display: grid;
            grid-gap: 1rem;
            padding: 1rem;
            max-width: 1024px;
            margin: 0 auto;
            font-family: var(--font-sans);
        }

        .content li {
            list-style-type: none !important;
        }

        @media (min-width: 600px) {
            .page-content {
                grid-template-columns: repeat(2, 1fr);
            }
        }

        @media (min-width: 800px) {
            .page-content {
                grid-template-columns: repeat(4, 1fr);
            }
        }

        .card-image {
            position: relative;
            display: -webkit-box;
            display: flex;
            -webkit-box-align: end;
            align-items: flex-end;
            overflow: hidden;
            padding: 1rem;
            width: 100%;
            text-align: center;
            color: whitesmoke;
            background-color: whitesmoke;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1), 0 2px 2px rgba(0, 0, 0, 0.1), 0 4px 4px rgba(0, 0, 0, 0.1), 0 8px 8px rgba(0, 0, 0, 0.1), 0 16px 16px rgba(0, 0, 0, 0.1);
        }

        @media (min-width: 600px) {
            .card-image {
                height: 350px;
            }
        }

        .card-image:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 110%;
            background-size: cover;
            background-position: 0 0;
            -webkit-transition: -webkit-transform calc(var(--d) * 1.5) var(--e);
            transition: -webkit-transform calc(var(--d) * 1.5) var(--e);
            transition: transform calc(var(--d) * 1.5) var(--e);
            transition: transform calc(var(--d) * 1.5) var(--e), -webkit-transform calc(var(--d) * 1.5) var(--e);
            pointer-events: none;
        }

        .card-image:after {
            content: '';
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 200%;
            pointer-events: none;
            background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0)), color-stop(11.7%, rgba(0, 0, 0, 0.009)), color-stop(22.1%, rgba(0, 0, 0, 0.034)), color-stop(31.2%, rgba(0, 0, 0, 0.072)), color-stop(39.4%, rgba(0, 0, 0, 0.123)), color-stop(46.6%, rgba(0, 0, 0, 0.182)), color-stop(53.1%, rgba(0, 0, 0, 0.249)), color-stop(58.9%, rgba(0, 0, 0, 0.32)), color-stop(64.3%, rgba(0, 0, 0, 0.394)), color-stop(69.3%, rgba(0, 0, 0, 0.468)), color-stop(74.1%, rgba(0, 0, 0, 0.54)), color-stop(78.8%, rgba(0, 0, 0, 0.607)), color-stop(83.6%, rgba(0, 0, 0, 0.668)), color-stop(88.7%, rgba(0, 0, 0, 0.721)), color-stop(94.1%, rgba(0, 0, 0, 0.762)), to(rgba(0, 0, 0, 0.79)));
            background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.009) 11.7%, rgba(0, 0, 0, 0.034) 22.1%, rgba(0, 0, 0, 0.072) 31.2%, rgba(0, 0, 0, 0.123) 39.4%, rgba(0, 0, 0, 0.182) 46.6%, rgba(0, 0, 0, 0.249) 53.1%, rgba(0, 0, 0, 0.32) 58.9%, rgba(0, 0, 0, 0.394) 64.3%, rgba(0, 0, 0, 0.468) 69.3%, rgba(0, 0, 0, 0.54) 74.1%, rgba(0, 0, 0, 0.607) 78.8%, rgba(0, 0, 0, 0.668) 83.6%, rgba(0, 0, 0, 0.721) 88.7%, rgba(0, 0, 0, 0.762) 94.1%, rgba(0, 0, 0, 0.79) 100%);
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: -webkit-transform calc(var(--d) * 2) var(--e);
            transition: -webkit-transform calc(var(--d) * 2) var(--e);
            transition: transform calc(var(--d) * 2) var(--e);
            transition: transform calc(var(--d) * 2) var(--e), -webkit-transform calc(var(--d) * 2) var(--e);
        }


        .content {
            position: relative;
            display: -webkit-box;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            flex-direction: column;
            -webkit-box-align: center;
            align-items: center;
            width: 100%;
            padding: 1rem;
            -webkit-transition: -webkit-transform var(--d) var(--e);
            transition: -webkit-transform var(--d) var(--e);
            transition: transform var(--d) var(--e);
            transition: transform var(--d) var(--e), -webkit-transform var(--d) var(--e);
            z-index: 1;
        }

        .content > * + * {
            margin-top: 1rem;
        }

        .title {
            font-size: 1.3rem;
            font-weight: bold;
            line-height: 1.2;
        }

        .copy {
            font-family: var(--font-serif);
            font-size: 1.125rem;
            font-style: italic;
            line-height: 1.35;
        }

        .copy li {
            list-style-type: none !important;
        }

        .btn {
            cursor: pointer;
            margin-top: 1.5rem;
            padding: 0.75rem 1.5rem;
            font-size: 0.65rem;
            font-weight: bold;
            letter-spacing: 0.025rem;
            text-transform: uppercase;
            color: white;
            background-color: black;
            border: none;
        }

        .btn:hover {
            background-color: #0d0d0d;
        }

        .btn:focus {
            outline: 1px dashed yellow;
            outline-offset: 3px;
        }

        @media (hover: hover) and (min-width: 600px) {
            .card-image:after {
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }

            .content {
                -webkit-transform: translateY(calc(100% - 4.5rem));
                transform: translateY(calc(100% - 4.5rem));
            }

            .content > *:not(.title) {
                opacity: 0;
                -webkit-transform: translateY(1rem);
                transform: translateY(1rem);
                -webkit-transition: opacity var(--d) var(--e), -webkit-transform var(--d) var(--e);
                transition: opacity var(--d) var(--e), -webkit-transform var(--d) var(--e);
                transition: transform var(--d) var(--e), opacity var(--d) var(--e);
                transition: transform var(--d) var(--e), opacity var(--d) var(--e), -webkit-transform var(--d) var(--e);
            }

            .card-image:hover,
            .card-image:focus-within {
                -webkit-box-align: center;
                align-items: center;
            }

            .card-image:hover:before,
            .card-image:focus-within:before {
                -webkit-transform: translateY(-4%);
                transform: translateY(-4%);
            }

            .card-image:hover:after,
            .card-image:focus-within:after {
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
            }

            .card-image:hover .content,
            .card-image:focus-within .content {
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }

            .card-image:hover .content > *:not(.title),
            .card-image:focus-within .content > *:not(.title) {
                opacity: 1;
                -webkit-transform: translateY(0);
                transform: translateY(0);
                -webkit-transition-delay: calc(var(--d) / 8);
                transition-delay: calc(var(--d) / 8);
            }

            .card-image:focus-within:before, .card-image:focus-within:after,
            .card-image:focus-within .content,
            .card-image:focus-within .content > *:not(.title) {
                -webkit-transition-duration: 0s;
                transition-duration: 0s;
            }
        }

        .destination {

        }

        .destination label {
            text-align: left;
            margin-top: 10px;
        }

        .submit button {
            margin-bottom: 20px;
            background-color: #17a2b8;
        }

        .submit button:hover {
            background-color: #17a2b8;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <?php echo e(trans('global.offers.title_singular')); ?> <?php echo e(trans('global.list')); ?>

        </div>
        <br>
        <div class="container">


            <form class="destination" method="get" action="<?php echo e(route('admin.offers.index')); ?>">
                <div class="row">
                    <div class="col-md-2">
                        <label>Search</label>
                    </div>
                    <div class="col-md-5">
                        <select class=" form-control" name="company" id="company">
                            <option id="company" disabled selected>Select Company</option>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($user->company); ?>"><?php echo e($user->company); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>

                    <div class="col-md-5">
                        <select class=" form-control" name="destination" id="destination">
                            <option id="destination" disabled selected>Select Destination</option>
                            <?php $__currentLoopData = $destinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $destination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($destination); ?>"><?php echo e($destination); ?></option>
                                <!--<option>Aswan</option>-->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-10">

                    </div>
                    <div class="col-md-">
                        <div class="col-md-2 submit">
                            <button type="submit" class="btn btn-info" style="margin-bottom:20px">Search Now</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <br>
        <br>
        <h6 style="text-align:center; font-size: 27px;">Lastest Offers</h6>
        <main class="page-content">
            <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card-image" style="background-image: url('<?php echo e(asset('uploads/offers/'.$offer->img)); ?> ')">
                    <div class="content">
                        <h2 class="title"><?php echo e($offer->name); ?> </h2>
                        <p class="copy">
                        <li>Destination : <?php echo e($offer->destination); ?></li>
                        <li>Duration : <?php echo e($offer->duration); ?></li>
                        <li>Offer Price : $ <?php echo e($offer->price); ?></li>

                        </p>
                        <a class="btn btn-info" href="<?php echo e($offer->url); ?>">View Trips</a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </main>

    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/offers/index.blade.php ENDPATH**/ ?>