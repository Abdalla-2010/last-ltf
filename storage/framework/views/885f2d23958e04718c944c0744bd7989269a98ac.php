<?php $__env->startSection('content'); ?>
<style>
    .col-form-label {
        padding-left: 35px;
    }
    @media  only screen and (max-width: 768px) {
       .text h5,h6 {
           font-size:12px;
           margin-top:20px;
       }
       .check {
           margin-left:20px;
       }
    }
</style>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.search.title_singular')); ?> <?php echo e(trans('global.list')); ?>

    </div>
    <div class="back" style="background-color:#eee;">
        <form action="<?php echo e(route('admin.results')); ?>" method="post">
            <?php echo csrf_field(); ?>
  <div class="form-group row" style="margin-top: 20px;">
    <label for="staticEmail" class="col-sm-2 col-form-label">Last Name Start With:</label>
   <div class="col-sm-4">
      <input type="text" class="form-control" id="inputPassword" placeholder="Write Last Name" name="last_name">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">First Name Start With:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="inputPassword" placeholder="Write First Name" name="name">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Company Start With:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="inputPassword" placeholder="Write Company Start With" name="company">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">City:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="inputPassword" placeholder="Write Your City" name="city">
    </div>
    <div class="form-check col-sm-6 text">
  <input class="form-check-input check" type="checkbox" name="limit" value="limit" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
   <h5>Limit Search Parameters To Only Include LTF Verified Travel Advisors</h5>
   <h6 style="text-align:center;">See All Of The Verified Travel Advisors On TravelSense.org</h6>
  </label>
</div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Country:</label>
    <div class="col-sm-4">
    <select name="country" required class="form-control">

						<option value="AF">Afghanistan</option>
						<option value="AL">Albania</option>
						<option value="DZ">Algeria</option>
						<option value="AS">American Samoa</option>
						<option value="AD">Andorrra</option>
						<option value="AO">Angola</option>
						<option value="AI">Anguilla</option>
						<option value="AQ">Antarctica</option>
						<option value="AG">Antigua and Barbuda</option>
						<option value="AR">Argentina</option>
						<option value="AM">Armenia</option>
						<option value="AW">Aruba</option>
						<option value="AU">Australia</option>
						<option value="AT">Austria</option>
						<option value="AY">Ayunga</option>
						<option value="AZ">Azerbaijan</option>
						<option value="BS">Bahamas</option>
						<option value="BH">Bahrain</option>
						<option value="BD">Bangladesh</option>
						<option value="BB">Barbados</option>
						<option value="BY">Belarus</option>
						<option value="BE">Belgium</option>
						<option value="BZ">Belize</option>
						<option value="BJ">Benin</option>
						<option value="BM">Bermuda</option>
						<option value="BT">Bhutan</option>
						<option value="BO">Bolivia</option>
						<option value="BA">Bosnia and Herzegovina</option>
						<option value="BW">Botswana</option>
						<option value="BV">Bouvet Island</option>
						<option value="BR">Brazil</option>
						<option value="IO">British Indian Ocean Territory</option>
						<option value="BN">Brunei Darussalam</option>
						<option value="BG">Bulgaria</option>
						<option value="BF">Burkina Faso</option>
						<option value="BI">Burundi</option>
						<option value="KH">Cambodia</option>
						<option value="CM">Cameroon</option>
						<option value="CA">Canada</option>
						<option value="CV">Cape Verde</option>
						<option value="KY">Cayman Islands</option>
						<option value="CF">Central African Republic</option>
						<option value="TD">Chad</option>
						<option value="CL">Chile</option>
						<option value="CN">China</option>
						<option value="CX">Christmas Island</option>
						<option value="CC">Cocos (Keeling) Islands</option>
						<option value="CO">Colombia</option>
						<option value="KM">Comoros</option>
						<option value="CG">Congo</option>
						<option value="CD">Congo, The Democratic Republic of the</option>
						<option value="CK">Cook Islands</option>
						<option value="CR">Costa Rica</option>
						<option value="HR">Croatia</option>
						<option value="CI">Cte D'Ivoire</option>
						<option value="CU">Cuba</option>
						<option value="CY">Cyprus</option>
						<option value="CZ">Czech Republic</option>
						<option value="DK">Denmark</option>
						<option value="DJ">Djibouti</option>
						<option value="DM">Dominica</option>
						<option value="DO">Dominican Republic</option>
						<option value="EC">Ecuador</option>
						<option value="EG">Egypt</option>
						<option value="SV">El Salvador</option>
						<option value="GQ">Equatorial Guinea</option>
						<option value="ER">Eritrea</option>
						<option value="EE">Estonia</option>
						<option value="ET">Ethiopia</option>
						<option value="FK">Falkland Islands (Malvinas)</option>
						<option value="FO">Faroe Islands</option>
						<option value="FJ">Fiji</option>
						<option value="FI">Finland</option>
						<option value="FR">France</option>
						<option value="GF">French Guiana</option>
						<option value="PF">French Polynesia</option>
						<option value="TF">French Southern Territories</option>
						<option value="GA">Gabon</option>
						<option value="GM">Gambia</option>
						<option value="GE">Georgia</option>
						<option value="DE">Germany</option>
						<option value="GH">Ghana</option>
						<option value="GI">Gibraltar</option>
						<option value="GR">Greece</option>
						<option value="GL">Greenland</option>
						<option value="GD">Grenada</option>
						<option value="GP">Guadeloupe</option>
						<option value="GU">Guam</option>
						<option value="GT">Guatemala</option>
						<option value="GN">Guinea</option>
						<option value="GW">Guinea-Bissau</option>
						<option value="GY">Guyana</option>
						<option value="HT">Haiti</option>
						<option value="HM">Heard Island and McDonald Islands</option>
						<option value="VA">Holy See (Vatican City State)</option>
						<option value="HN">Honduras</option>
						<option value="HK">Hong Kong</option>
						<option value="HU">Hungary</option>
						<option value="IS">Iceland</option>
						<option value="IN">India</option>
						<option value="ID">Indonesia</option>
						<option value="IR">Iran, Islamic Republic of</option>
						<option value="IQ">Iraq</option>
						<option value="IE">Ireland</option>
						<option value="IL">Israel</option>
						<option value="IT">Italy</option>
						<option value="JM">Jamaica</option>
						<option value="JP">Japan</option>
						<option value="JO">Jordan</option>
						<option value="KZ">Kazakhstan</option>
						<option value="KE">Kenya</option>
						<option value="KI">Kiribati</option>
						<option value="KP">Korea, Democratic People's Republic of</option>
						<option value="KR">Korea, Republic of</option>
						<option value="KW">Kuwait</option>
						<option value="KG">Kyrgyzstan</option>
						<option value="AX">land Islands</option>
						<option value="LA">Lao People's Democratic Republic</option>
						<option value="LV">Latvia</option>
						<option value="LB">Lebanon</option>
						<option value="LS">Lesotho</option>
						<option value="LR">Liberia</option>
						<option value="LY">Libyan Arab Jamahiriya</option>
						<option value="LI">Liechtenstein</option>
						<option value="LT">Lithuania</option>
						<option value="LU">Luxembourg</option>
						<option value="MO">Macao</option>
						<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
						<option value="MG">Madagascar</option>
						<option value="MW">Malawi</option>
						<option value="MY">Malaysia</option>
						<option value="MV">Maldives</option>
						<option value="ML">Mali</option>
						<option value="MT">Malta</option>
						<option value="MH">Marshall Islands</option>
						<option value="MQ">Martinique</option>
						<option value="MR">Mauritania</option>
						<option value="MU">Mauritius</option>
						<option value="YT">Mayotte</option>
						<option value="MX">Mexico</option>
						<option value="FM">Micronesia, Federated States of</option>
						<option value="MD">Moldova, Republic of</option>
						<option value="MC">Monaco</option>
						<option value="MN">Mongolia</option>
						<option value="MS">Montserrat</option>
						<option value="MA">Morocco</option>
						<option value="MZ">Mozambique</option>
						<option value="MM">Myanmar</option>
						<option value="NA">Namibia</option>
						<option value="NR">Nauru</option>
						<option value="NP">Nepal</option>
						<option value="NL">Netherlands</option>
						<option value="AN">Netherlands Antilles</option>
						<option value="NC">New Caledonia</option>
						<option value="NZ">New Zealand</option>
						<option value="NI">Nicaragua</option>
						<option value="NE">Niger</option>
						<option value="NG">Nigeria</option>
						<option value="NU">Niue</option>
						<option value="NF">Norfolk Island</option>
						<option value="MP">Northern Mariana Islands</option>
						<option value="NO">Norway</option>
						<option value="OM">Oman</option>
						<option value="PK">Pakistan</option>
						<option value="PW">Palau</option>
						<option value="PS">Palestinian Territory, Occupied</option>
						<option value="PA">Panama</option>
						<option value="PG">Papua New Guinea</option>
						<option value="PY">Paraguay</option>
						<option value="PE">Peru</option>
						<option value="PH">Philippines</option>
						<option value="PN">Pitcairn</option>
						<option value="PL">Poland</option>
						<option value="PT">Portugal</option>
						<option value="PR">Puerto Rico</option>
						<option value="QA">Qatar</option>
						<option value="RO">Romania</option>
						<option value="RE">Runion</option>
						<option value="RU">Russian Federation</option>
						<option value="RW">Rwanda</option>
						<option value="SH">Saint Helena</option>
						<option value="KN">Saint Kitts and Nevis</option>
						<option value="LC">Saint Lucia</option>
						<option value="PM">Saint Pierre and Miquelon</option>
						<option value="VC">Saint Vincent and The Grenadines</option>
						<option value="WS">Samoa</option>
						<option value="SM">San Marino</option>
						<option value="ST">Sao Tome and Principe</option>
						<option value="SA">Saudi Arabia</option>
						<option value="SN">Senegal</option>
						<option value="CS">Serbia and Montenegro</option>
						<option value="SC">Seychelles</option>
						<option value="SL">Sierra Leone</option>
						<option value="SG">Singapore</option>
						<option value="SK">Slovakia</option>
						<option value="SI">Slovenia</option>
						<option value="SB">Solomon Islands</option>
						<option value="SO">Somalia</option>
						<option value="ZA">South Africa</option>
						<option value="GS">South Georgia and The South Sandwich Islands</option>
						<option value="ES">Spain</option>
						<option value="LK">Sri Lanka</option>
						<option value="SD">Sudan</option>
						<option value="SR">Suriname</option>
						<option value="SJ">Svalbard and Jan Mayen</option>
						<option value="SZ">Swaziland</option>
						<option value="SE">Sweden</option>
						<option value="CH">Switzerland</option>
						<option value="SY">Syrian Arab Republic</option>
						<option value="TW">Taiwan, Province of China</option>
						<option value="TJ">Tajikistan</option>
						<option value="TZ">Tanzania, United Republic of</option>
						<option value="TH">Thailand</option>
						<option value="TL">Timor-Leste</option>
						<option value="TG">Togo</option>
						<option value="TK">Tokelau</option>
						<option value="TO">Tonga</option>
						<option value="TT">Trinidad and Tobago</option>
						<option value="TN">Tunisia</option>
						<option value="TR">Turkey</option>
						<option value="TM">Turkmenistan</option>
						<option value="TC">Turks and Caicos Islands</option>
						<option value="TV">Tuvalu</option>
						<option value="UG">Uganda</option>
						<option value="UA">Ukraine</option>
						<option value="AE">United Arab Emirates</option>
						<option value="GB">United Kingdom</option>
						<option value="US">United States</option>
						<option value="UM">United States Minor Outlying Islands</option>
						<option value="UY">Uruguay</option>
						<option value="UZ">Uzbekistan</option>
						<option value="VU">Vanuatu</option>
						<option value="VE">Venezuela</option>
						<option value="VN">Viet Nam</option>
						<option value="VG">Virgin Islands, British</option>
						<option value="VI">Virgin Islands, U.S.</option>
						<option value="WF">Wallis and Futuna</option>
						<option value="EH">Western Sahara</option>
						<option value="XZ">Xenozandlia</option>
						<option value="YE">Yemen</option>
						<option value="ZM">Zambia</option>
						<option value="ZW">Zimbabwe</option>

					</select>
</div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">State:</label>
    <div class="col-sm-4">
   <select name="" class="form-control">
						<option value="AA">Armed Forces Americas</option>
						<option value="AE">Armed Forces Africa, Canada, Europe, Middle East</option>
						<option value="AK">Alaska</option>
						<option value="AL">Alabama</option>
						<option value="AR">Arkansas</option>
						<option value="AZ">Arizona</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="CT">Connecticut</option>
						<option value="CZ">Canal Zone</option>
						<option value="DC">District of Columbia</option>
						<option value="DE">Delaware</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="GU">Guam</option>
						<option value="HI">Hawaii</option>
						<option value="IA">Iowa</option>
						<option value="ID">Idaho</option>
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
						<option value="MA">Massachusetts</option>
						<option value="MD">Maryland</option>
						<option value="ME">Maine</option>
						<option value="MI">Michigan</option>
						<option value="MN">Minnesota</option>
						<option value="MO">Missouri</option>
						<option value="MS">Mississippi</option>
						<option value="MT">Montana</option>
						<option value="NC">North Carolina</option>
						<option value="ND">North Dakota</option>
						<option value="NE">Nebraska</option>
						<option value="NH">New Hampshire</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NV">Nevada</option>
						<option value="NY">New York</option>
						<option value="OH">Ohio</option>
						<option value="OK">Oklahoma</option>
						<option value="OR">Oregon</option>
						<option value="PA">Pennsylvania</option>
						<option value="PR">Puerto Rico</option>
						<option value="RI">Rhode Island</option>
						<option value="SC">South Carolina</option>
						<option value="SD">South Dakota</option>
						<option value="TN">Tennessee</option>
						<option value="TX">Texas</option>
						<option value="UT">Utah</option>
						<option value="VA">Virginia</option>
						<option value="VT">Vermont</option>
						<option value="WA">Washington</option>
						<option value="WI">Wisconsin</option>
						<option value="WV">West Virginia</option>
						<option value="WY">Wyoming</option>

					</select>
</div>
  </div>
   <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Chapter:</label>
    <div class="col-sm-4">
    <select name="" class="form-control">
						<option value="AFGHANISTAN">AFGHANISTAN</option>
						<option value="ALABAMA">ALABAMA</option>
						<option value="ALBANIA">ALBANIA</option>
						<option value="ALGERIA">ALGERIA</option>
						<option value="ANDORRA">ANDORRA</option>
						<option value="ANGUILLA">ANGUILLA</option>
						<option value="ANTIGUA">ANTIGUA</option>
						<option value="ARGENTINA">ARGENTINA</option>
						<option value="ARIZONA">ARIZONA</option>
						<option value="AZSBN">AZSBN</option>
						<option value="ARMENIA">ARMENIA</option>
						<option value="ARUBA">ARUBA</option>
						<option value="ASTAHQ">ASTAHQ</option>
						<option value="AUSTRALIA">AUSTRALIA</option>
						<option value="AUSTRIA">AUSTRIA</option>
						<option value="AZERBAIJAN">AZERBAIJAN</option>
						<option value="BAHAMAS">BAHAMAS</option>
						<option value="BAHRAIN">BAHRAIN</option>
						<option value="BANGLADESH">BANGLADESH</option>
						<option value="BARBADOS">BARBADOS</option>
						<option value="BELARUS">BELARUS</option>
						<option value="BELGIUM">BELGIUM</option>
						<option value="BELIZE">BELIZE</option>
						<option value="BENIN">BENIN</option>
						<option value="BERMUDA">BERMUDA</option>
						<option value="BEST WESTERN">BEST WESTERN</option>
						<option value="BHUTAN">BHUTAN</option>
						<option value="BOLIVIA">BOLIVIA</option>
						<option value="BONAIRE">BONAIRE</option>
						<option value="BOTSWANA">BOTSWANA</option>
						<option value="BRAZIL">BRAZIL</option>
						<option value="BRITISH VI">BRITISH VI</option>
						<option value="BRUNEI">BRUNEI</option>
						<option value="BULGARIA">BULGARIA</option>
						<option value="CAMBODIA">CAMBODIA</option>
						<option value="CAMEROON">CAMEROON</option>
						<option value="CANADA">CANADA</option>
						<option value="CAROLINAS">CAROLINAS</option>
						<option value="CAYMAN ISLANDS">CAYMAN ISLANDS</option>
						<option value="CTL &amp; N FLORIDA">CTL &amp; N FLORIDA</option>
						<option value="CTL ATLANTIC">CTL ATLANTIC</option>
						<option value="CCA">CCA</option>
						<option value="CTL CALIFORNIA">CTL CALIFORNIA</option>
						<option value="CLT FLORIDA">CLT FLORIDA</option>
						<option value="COHSBN">COHSBN</option>
						<option value="CLTSBN">CLTSBN</option>
						<option value="CHILE">CHILE</option>
						<option value="CHINA">CHINA</option>
						<option value="CHINESE TAIPEI">CHINESE TAIPEI</option>
						<option value="COLOMBIA">COLOMBIA</option>
						<option value="CTSBN">CTSBN</option>
						<option value="COSTA RICA">COSTA RICA</option>
						<option value="COTE D'IVOIRE">COTE D'IVOIRE</option>
						<option value="CROATIA">CROATIA</option>
						<option value="CURACAO">CURACAO</option>
						<option value="CYPRUS">CYPRUS</option>
						<option value="CZECH REPUBLIC">CZECH REPUBLIC</option>
						<option value="DALLAS FT WORTH">DALLAS FT WORTH</option>
						<option value="DEL VALLEY">DEL VALLEY</option>
						<option value="DENMARK">DENMARK</option>
						<option value="DEN">DEN</option>
						<option value="DJIBOUTI">DJIBOUTI</option>
						<option value="DOMINICA">DOMINICA</option>
						<option value="DOMINICAN REPUB">DOMINICAN REPUB</option>
						<option value="E CAROLINE ISL">E CAROLINE ISL</option>
						<option value="EMDSBN">EMDSBN</option>
						<option value="ECUADOR">ECUADOR</option>
						<option value="EGYPT">EGYPT</option>
						<option value="EL SALVADOR">EL SALVADOR</option>
						<option value="ERITREA">ERITREA</option>
						<option value="ESTONIA">ESTONIA</option>
						<option value="ETHIOPIA">ETHIOPIA</option>
						<option value="FIJI">FIJI</option>
						<option value="FINLAND">FINLAND</option>
						<option value="FRANCE">FRANCE</option>
						<option value="FR POLYNESIA">FR POLYNESIA</option>
						<option value="GEORGIA">GEORGIA</option>
						<option value="GERMANY">GERMANY</option>
						<option value="GHANA">GHANA</option>
						<option value="GIBRALTAR">GIBRALTAR</option>
						<option value="GREAT LAKES">GREAT LAKES</option>
						<option value="ATL">ATL</option>
						<option value="AUSTIN">AUSTIN</option>
						<option value="PORTLAND">PORTLAND</option>
						<option value="SEATTLE">SEATTLE</option>
						<option value="GREECE">GREECE</option>
						<option value="GRENADA">GRENADA</option>
						<option value="GUADELOUPE">GUADELOUPE</option>
						<option value="GUATEMALA">GUATEMALA</option>
						<option value="HAITI">HAITI</option>
						<option value="HISBN">HISBN</option>
						<option value="HAWAII">HAWAII</option>
						<option value="HONDURAS">HONDURAS</option>
						<option value="HONG KONG CHINA">HONG KONG CHINA</option>
						<option value="HOU">HOU</option>
						<option value="HUNGARY">HUNGARY</option>
						<option value="ILSBN">ILSBN</option>
						<option value="INDIA">INDIA</option>
						<option value="INDONESIA">INDONESIA</option>
						<option value="IRAN">IRAN</option>
						<option value="IRAQ">IRAQ</option>
						<option value="IRELAND">IRELAND</option>
						<option value="ISEMPIRE">ISEMPIRE</option>
						<option value="ISRAEL">ISRAEL</option>
						<option value="ITALY">ITALY</option>
						<option value="JAMAICA">JAMAICA</option>
						<option value="JAPAN">JAPAN</option>
						<option value="JORDAN">JORDAN</option>
						<option value="KAZAKSTAN">KAZAKSTAN</option>
						<option value="KENYA">KENYA</option>
						<option value="KOREA">KOREA</option>
						<option value="KUWAIT">KUWAIT</option>
						<option value="kYRGYZSTAN">kYRGYZSTAN</option>
						<option value="LATVIA">LATVIA</option>
						<option value="LEBANON">LEBANON</option>
						<option value="LIBYA">LIBYA</option>
						<option value="LIECHTENSTEIN">LIECHTENSTEIN</option>
						<option value="LITHUANIA">LITHUANIA</option>
						<option value="LONG ISLAND">LONG ISLAND</option>
						<option value="LONGSBN">LONGSBN</option>
						<option value="LASBN">LASBN</option>
						<option value="LUXEMBOURG">LUXEMBOURG</option>
						<option value="MACAU CHINA">MACAU CHINA</option>
						<option value="Macedonia">Macedonia</option>
						<option value="MAINE">MAINE</option>
						<option value="MALAWI">MALAWI</option>
						<option value="MALAYSIA">MALAYSIA</option>
						<option value="MALDIVES">MALDIVES</option>
						<option value="MALI">MALI</option>
						<option value="MALTA">MALTA</option>
						<option value="MARTINIQUE">MARTINIQUE</option>
						<option value="MAURITIUS">MAURITIUS</option>
						<option value="MEXICO">MEXICO</option>
						<option value="MID AMERICA">MID AMERICA</option>
						<option value="MIDMISBN">MIDMISBN</option>
						<option value="MIDWEST">MIDWEST</option>
						<option value="MISSOURI VALLEY">MISSOURI VALLEY</option>
						<option value="MOKS">MOKS</option>
						<option value="MONACO">MONACO</option>
						<option value="MONGOLIA">MONGOLIA</option>
						<option value="MONTSERRAT">MONTSERRAT</option>
						<option value="MOROCCO">MOROCCO</option>
						<option value="MYANMAR">MYANMAR</option>
						<option value="NAMIBIA">NAMIBIA</option>
						<option value="NEPAL">NEPAL</option>
						<option value="NETHERLANDS">NETHERLANDS</option>
						<option value="NETHERLANDS ANTILLES">NETHERLANDS ANTILLES</option>
						<option value="NEW ENGLAND">NEW ENGLAND</option>
						<option value="NESBN">NESBN</option>
						<option value="NEW JERSEY">NEW JERSEY</option>
						<option value="NEW YORK CITY">NEW YORK CITY</option>
						<option value="NYSBN">NYSBN</option>
						<option value="NEW ZEALAND">NEW ZEALAND</option>
						<option value="NICARAGUA">NICARAGUA</option>
						<option value="NIGERIA">NIGERIA</option>
						<option value="N CAROLINA">N CAROLINA</option>
						<option value="N FLORIDA">N FLORIDA</option>
						<option value="NTXSBN">NTXSBN</option>
						<option value="N CALIFORNIA">N CALIFORNIA</option>
						<option value="N NEW ENGLAND">N NEW ENGLAND</option>
						<option value="NVSBN">NVSBN</option>
						<option value="PACIFIC NW">PACIFIC NW</option>
						<option value="NORWAY">NORWAY</option>
						<option value="OHIO">OHIO</option>
						<option value="OMAN">OMAN</option>
						<option value="ORANGE COUNTY">ORANGE COUNTY</option>
						<option value="ORANGESBN">ORANGESBN</option>
						<option value="PAKISTAN">PAKISTAN</option>
						<option value="PALAU">PALAU</option>
						<option value="PANAMA">PANAMA</option>
						<option value="PAPUA N GUINEA">PAPUA N GUINEA</option>
						<option value="PARAGUAY">PARAGUAY</option>
						<option value="PERU">PERU</option>
						<option value="PHILIPPINES">PHILIPPINES</option>
						<option value="POLAND">POLAND</option>
						<option value="PORTUGAL">PORTUGAL</option>
						<option value="PUERTO RICO/VI">PUERTO RICO/VI</option>
						<option value="QATAR">QATAR</option>
						<option value="RAL">RAL</option>
						<option value="RICSBN">RICSBN</option>
						<option value="ROCKY MTN">ROCKY MTN</option>
						<option value="ROMANIA">ROMANIA</option>
						<option value="RUSSIA">RUSSIA</option>
						<option value="RWANDA">RWANDA</option>
						<option value="SMFSBN">SMFSBN</option>
						<option value="SAINT EUSTATIUS">SAINT EUSTATIUS</option>
						<option value="SAINT KITTS &amp; NEVIS">SAINT KITTS &amp; NEVIS</option>
						<option value="SAINT KITTS &amp;amp; NEVIS">SAINT KITTS &amp; NEVIS</option>
						<option value="SAINT KITTS &amp;amp;amp; NEVIS">SAINT KITTS &amp;amp; NEVIS</option>
						<option value="SAINT LUCIA">SAINT LUCIA</option>
						<option value="SAINT MARTIN">SAINT MARTIN</option>
						<option value="SAINT VINCENT">SAINT VINCENT</option>
						<option value="SAN DIEGO">SAN DIEGO</option>
						<option value="SFOSBN">SFOSBN</option>
						<option value="San Marino">San Marino</option>
						<option value="SAUDI ARABIA">SAUDI ARABIA</option>
						<option value="SERBIA">SERBIA</option>
						<option value="SINGAPORE">SINGAPORE</option>
						<option value="SINT MAARTEN">SINT MAARTEN</option>
						<option value="SLOVAK REPUBLIC">SLOVAK REPUBLIC</option>
						<option value="SLOVENIA">SLOVENIA</option>
						<option value="SOUTH AFRICA">SOUTH AFRICA</option>
						<option value="S CAROLINA">S CAROLINA</option>
						<option value="SOUTH FLORIDA">SOUTH FLORIDA</option>
						<option value="SFLSBN">SFLSBN</option>
						<option value="SOUTHEAST">SOUTHEAST</option>
						<option value="SEMISBN">SEMISBN</option>
						<option value="S CALIFORNIA">S CALIFORNIA</option>
						<option value="S NEW ENGLAND">S NEW ENGLAND</option>
						<option value="SOUTHWEST">SOUTHWEST</option>
						<option value="SWFLSBN">SWFLSBN</option>
						<option value="SPAIN">SPAIN</option>
						<option value="SRI LANKA">SRI LANKA</option>
						<option value="ST LOUIS">ST LOUIS</option>
						<option value="STATENSBN">STATENSBN</option>
						<option value="SUDAN">SUDAN</option>
						<option value="SUFFOLKSBN">SUFFOLKSBN</option>
						<option value="SURINAM">SURINAM</option>
						<option value="SWEDEN">SWEDEN</option>
						<option value="SWITZERLAND">SWITZERLAND</option>
						<option value="SYRIA">SYRIA</option>
						<option value="TBSBN">TBSBN</option>
						<option value="TANZANIA">TANZANIA</option>
						<option value="THAILAND">THAILAND</option>
						<option value="TRINIDAD &amp; TOB">TRINIDAD &amp; TOB</option>
						<option value="TUNISIA">TUNISIA</option>
						<option value="TURKEY">TURKEY</option>
						<option value="TURKS &amp; CAICOS">TURKS &amp; CAICOS</option>
						<option value="UGANDA">UGANDA</option>
						<option value="UKRAINE">UKRAINE</option>
						<option value="UNITED ARAB EMIRATES">UNITED ARAB EMIRATES</option>
						<option value="UNITED KINGDOM">UNITED KINGDOM</option>
						<option value="UPPER MIDWEST">UPPER MIDWEST</option>
						<option value="UPSTATE NY">UPSTATE NY</option>
						<option value="URUGUAY">URUGUAY</option>
						<option value="Uzbekistan">Uzbekistan</option>
						<option value="VENEZUELA">VENEZUELA</option>
						<option value="VIETNAM">VIETNAM</option>
						<option value="WESTERN SAMOA">WESTERN SAMOA</option>
						<option value="YEMEN">YEMEN</option>
						<option value="YUGOSLAVIA">YUGOSLAVIA</option>
						<option value="ZAMBIA">ZAMBIA</option>
						<option value="ZIMBABWE">ZIMBABWE</option>

					</select>
</div>
  </div>

    <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Destination Specially:</label>
    <div class="col-sm-4">
   <select class="form-control" name="destination[]" multiple="multiple">
						<option value="(Any)">(Any)</option>
						<option value="AFRI">Africa</option>
						<option value="ANTA">Antarctica/Arctic Region</option>
						<option value="ASCE">Asia - Central Asia</option>
						<option value="ASCH">Asia - China, Japan, Korea Mongolia</option>
						<option value="ASSE">Asia - Southeast Asia</option>
						<option value="AUST">Australia/New Zealand</option>
						<option value="CANA">Canada</option>
						<option value="CARI">Caribbean</option>
						<option value="CTLA">Central America</option>
						<option value="CROA">Croatia</option>
						<option value="CUBA">Cuba</option>
						<option value="EGYP">Egypt</option>
						<option value="UKIN">England - United Kingdom</option>
						<option value="EURE">Europe - Eastern</option>
						<option value="EURN">Europe - Northern</option>
						<option value="EURW">Europe - Western</option>
						<option value="FRAN">France</option>
						<option value="GERM">Germany</option>
						<option value="GREE">Greece</option>
						<option value="INDI">India</option>
						<option value="ISRA">Israel</option>
						<option value="ITAL">Italy</option>
						<option value="LATN">Latin America &amp; Mexico</option>
						<option value="ASME">Middle East</option>
						<option value="PACI">Pacific Islands - Tahiti, Fiji, Bali, etc.</option>
						<option value="RUSS">Russia</option>
						<option value="SAME">South America</option>
						<option value="SPAI">Spain</option>
						<option value="USAK">U.S. - Alaska</option>
						<option value="USFL">U.S. - Florida</option>
						<option value="USHI">U.S. - Hawaii</option>
						<option value="USLV">U.S. - Las Vegas</option>
						<option value="USMW">U.S. - Midwest</option>
						<option value="USNE">U.S. - Northeast</option>
						<option value="USSE">U.S. - Southeast</option>
						<option value="USWS">U.S. - West</option>

					</select>
</div>
 <label for="" class="col-sm-2 col-form-label">Business Type:</label>
    <div class="col-sm-3">
   <select class="form-control" multiple="multiple">
						<option value="(Any)">(Any)</option>
						<option value="ACC">ACC Accessible Travel/Handicap</option>
						<option value="ACT">ACT Accounting/Consulting</option>
						<option value="AIR">AIR Airline</option>
						<option value="APL">APL Airplane Leasing/Exec Jets/Charter</option>
						<option value="ATT">ATT Attractions &amp; Restaurants</option>
						<option value="CAR">CAR Car Rental Firm</option>
						<option value="CNS">CNS Consortium/Franchise - Agency</option>
						<option value="CON">CON Consolidator/Wholesaler</option>
						<option value="CRU">CRU Cruise Line/Boating</option>
						<option value="DMC">DMC Destination Management Company</option>
						<option value="FER">FER Ferry/Riverboat</option>
						<option value="FTP">FTP Future Travel Professional/Student</option>
						<option value="GDS">GDS Global Distribution System</option>
						<option value="HBA">HBA Home Based Agent</option>
						<option value="HOS">HOS Host Agency</option>
						<option value="HUM">HUM Human Resources</option>
						<option value="IND">IND Independent Contractor</option>
						<option value="INS">INS Insurance - Traveler &amp; Business</option>
						<option value="LAW">LAW Consultants/Lawyers</option>
						<option value="LBB">LBB Lodging - Bed &amp; Breakfast/Boutique</option>
						<option value="LCC">LCC Lodging - Global Hotel Company</option>
						<option value="LIM">LIM Limousine/Van Service</option>
						<option value="LIP">LIP Lodging - Individual Property</option>
						<option value="LRE">LRE Lodging - Resort</option>
						<option value="LST">LST Lodging - Short Term Rental</option>
						<option value="MOT">MOT Motorcoach/Bus</option>
						<option value="OFF">OFF Office Services/Travel Supplies</option>
						<option value="OTH">OTH Other</option>
						<option value="PAY">PAY Payment Services/Credit Cards</option>
						<option value="PUB">PUB Public Relations/Marketing/Publication</option>
						<option value="REC">REC Receptive Agency/Ground Operator</option>
						<option value="RET">RET Retail Travel Agency</option>
						<option value="RRL">RRL Rail Line/Railroad</option>
						<option value="SRI">SRI Senior/Retired</option>
						<option value="TBD">TBD Tourist Board/CVB/Government Agency</option>
						<option value="TEK">TEK Technology Solutions</option>
						<option value="TMC">TMC Travel Management Company</option>
						<option value="TOP">TOP ASTA's Tour Operator Program</option>
						<option value="TOU">TOU Tour Operator</option>
						<option value="TRD">TRD Global Trade Association</option>
						<option value="TSC">TSC School/Education</option>
						<option value="TSI">TSI Traveler Services &amp; Information</option>

					</select>
</div>
  </div>

  <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Travel Specially:</label>
    <div class="col-sm-4">
<select  multiple="multiple" name="travel[]" class="form-control">
						<option value="(Any)">(Any)</option>
						<option value="DISAB">Accessible/Special Needs</option>
						<option value="ADOPT">Adoption</option>
						<option value="ADVEN">Adventure Travel</option>
						<option value="AIR">Airline</option>
						<option value="ALLIN">All Inclusive</option>
						<option value="AMUSE">Amusement/Theme Parks</option>
						<option value="ARCHE">Archeology</option>
						<option value="ARTSA">Art &amp; Antiques</option>
						<option value="ARTSC">Art &amp; Culture/Music</option>
						<option value="AVORS">ASTA Volunteer Responders</option>
						<option value="BARGE">Barge/Canal/River Cruises</option>
						<option value="BEACH">Beach Vacations</option>
						<option value="BICYC">Bicycle</option>
						<option value="BOATS">Boating/Yacht/Sailing</option>
						<option value="BUDGT">Budget Travel</option>
						<option value="BUSIN">Business Travel</option>
						<option value="CAMPI">Camping/Hiking</option>
						<option value="CASTL">Castles/Villas</option>
						<option value="CORPG">Corporate/Government</option>
						<option value="CRURIV">Cruising - River</option>
						<option value="CRUIS">Cruising/Cruise Lines</option>
						<option value="CULIN">Culinary/Cooking/Gastronomy</option>
						<option value="CUSTO">Customized Travel</option>
						<option value="DESWD">Destination Weddings</option>
						<option value="DISNE">Disney</option>
						<option value="ECOTO">Eco-Tourism</option>
						<option value="EDUCA">Educational</option>
						<option value="EQUES">Equestrian</option>
						<option value="FAFUN">Family Fun</option>
						<option value="FAMIL">Family/Multi-generational</option>
						<option value="FERRY">Ferry/Riverboat</option>
						<option value="FISHI">Fishing/Hunting</option>
						<option value="GENEA">Genealogy</option>
						<option value="GOLFT">Golf &amp; Tennis</option>
						<option value="GOVER">Government Travel</option>
						<option value="GRTOU">Great Outdoors</option>
						<option value="GROUP">Group Vacation</option>
						<option value="HISTO">Historical</option>
						<option value="HONEY">Honeymoon</option>
						<option value="HOTEL">Hotel</option>
						<option value="INCEN">Incentive Travel</option>
						<option value="LEISU">Leisure Travel</option>
						<option value="GAYLE">LGBT</option>
						<option value="LIFES">Lifestyle/Family/Specialty</option>
						<option value="LUXUR">Luxury Travel</option>
						<option value="MEDIC">Medical Travel</option>
						<option value="MPLAN">Meeting/Events</option>
						<option value="MAFRI">Minority - African American</option>
						<option value="MASIA">Minority - Asian</option>
						<option value="MHISP">Minority - Hispanic</option>
						<option value="MOTOR">Motor coach/Bus</option>
						<option value="MUSIC">Music &amp; Performing Arts</option>
						<option value="NPARK">National Parks</option>
						<option value="NATUR">Nature</option>
						<option value="OTHER">Other</option>
						<option value="RAFTI">Rafting</option>
						<option value="RAILT">Rail</option>
						<option value="RELIG">Religious/Faith/Spiritual</option>
						<option value="RESOR">Resorts</option>
						<option value="REUNI">Reunions</option>
						<option value="SAFAR">Safari &amp; Wildlife</option>
						<option value="SCUBA">Scuba Diving/Water Sports</option>
						<option value="SENIO">Senior/Mature Adult</option>
						<option value="SINGL">Singles</option>
						<option value="SKIWA">Ski/Winter Sports</option>
						<option value="SOLO">Solo Travel</option>
						<option value="SPECL">Speciality</option>
						<option value="SPORT">Sports/Exercise</option>
						<option value="STUDE">Student/Youth</option>
						<option value="TOURS">Tours</option>
						<option value="VTOUR">Voluntourism</option>
						<option value="SPAFI">Wellness/Spa/Fitness</option>
						<option value="WOMEN">Women's Travel</option>

					</select>
					</div>
					<label for="" class="col-sm-2 col-form-label">Certification:</label>
    <div class="col-sm-3">
   <select multiple="multiple" class="form-control">
						<option value="(Any)">(Any)</option>
						<option value="Botswana">Botswana Destination</option>
						<option value="Bulgaria">Bulgaria</option>
						<option value="Cambodia_Laos_Myanmar">Cambodia, Laos and Myanmar</option>
						<option value="China">China</option>
						<option value="Cuba">Cuba</option>
						<option value="Czech Republic">Czech Republic</option>
						<option value="Dubai">Dubai Destination Specialist</option>
						<option value="Rail - European">European Rail Travel</option>
						<option value="Family">Family</option>
						<option value="France - Beaujolais &amp; Rhone-Alps">France - Beaujolais &amp; Rhone-Alps</option>
						<option value="France">France Destination Specialist</option>
						<option value="France Soft Adventure">France Soft Adventure</option>
						<option value="FUNDISouthAfrica">FUNDI South Africa</option>
						<option value="Germany">Germany</option>
						<option value="Greece">Greece</option>
						<option value="Green">Green</option>
						<option value="Hungary">Hungary</option>
						<option value="Incheon_Daegu_Korea">Incheon, Daegu and Korea</option>
						<option value="India">India</option>
						<option value="Jeju_Korea">Jeju, Korea</option>
						<option value="Jordan">Jordan</option>
						<option value="Lyon_Beaujolais_Rhone-Alpes">Lyon, Beaujolais Countryside and Rhone-Alpes</option>
						<option value="Malaysia_Indonesia_Taiwan">Malaysia, Indonesia and Taiwan</option>
						<option value="Mature Adult">Mature Adult</option>
						<option value="Medit_Cruising">Mediterranean Cruising</option>
						<option value="Merida_Mexico">Merida Mexico Destination Specialist</option>
						<option value="Middle_East_Religious">Middle East Religious Travel</option>
						<option value="Morocco">Morocco Destination Specialist</option>
						<option value="Namibia">Namibia Destination</option>
						<option value="Niche">Niche</option>
						<option value="Peru">Peru Destination Specialist</option>
						<option value="Poland">Poland</option>
						<option value="Puerto_Rico">Puerto Rico Destination Specialist</option>
						<option value="Rail - North American">Rail - North American</option>
						<option value="Romania_Macedonia_Albania">Romania, Macedonia and Albania</option>
						<option value="Seville">Seville Spain</option>
						<option value="Slovakia">Slovakia</option>
						<option value="Slovenia_Croatia_Bosnia_Serbia">Slovenia, Croatia, Bosnia and Serbia</option>
						<option value="South_Korea">South Korea</option>
						<option value="SouthernAfricanSpa">Southern African Spa</option>
						<option value="SouthernAfricanWinelands">Southern African Winelands</option>
						<option value="Thailand">Thailand</option>
						<option value="Travel Marketing">Travel Marketing</option>
						<option value="Treasures_S_Europe">Treasures of Southern Europe</option>
						<option value="Turkey">Turkey Destination</option>
						<option value="Turkish_Culinary">Turkish Culinary</option>
						<option value="USA">USA</option>
						<option value="VTA">Verified Travel Advisor</option>
						<option value="France River Barges &amp; Cruises">Western European Cruise and River Barging</option>
						<option value="France Culinary">Western European Culinary Travel</option>
						<option value="European Honeymoon">Western European Honeymoons</option>
						<option value="France Mature Adult">Western European Mature Adult Travel</option>

					</select>
</div>
  </div>

   <div class="form-group row">
    <label for="" class="col-sm-2 col-form-label">Language:</label>
    <div class="col-sm-4">
  <select class="form-control" name="languages[]" multiple="multiple">
						<option value="ARA">Arabic</option>
						<option value="BEN">Bengali</option>
						<option value="BUL">Bulgarian</option>
						<option value="CAT">Catalan</option>
						<option value="YUE">Chinese (Cantonese)</option>
						<option value="CMN">Chinese (Mandarin)</option>
						<option value="HRV">Croatian</option>
						<option value="CES">Czech</option>
						<option value="DAN">Danish</option>
						<option value="NLD">Dutch</option>
						<option value="ENG">English</option>
						<option value="EST">Estonian</option>
						<option value="FRA">French</option>
						<option value="DEU">German</option>
						<option value="ELL">Greek</option>
						<option value="HEB">Hebrew</option>
						<option value="HIN">Hindi</option>
						<option value="HUN">Hungarian</option>
						<option value="ISL">Icelandic</option>
						<option value="IND">Indonesian</option>
						<option value="ITA">Italian</option>
						<option value="JPN">Japanese</option>
						<option value="KOR">Korean</option>
						<option value="LAV">Latvian</option>
						<option value="LIT">Lithuanian</option>
						<option value="NOR">Norwegian</option>
						<option value="POL">Polish</option>
						<option value="POR">Portuguese</option>
						<option value="PAN">Punjabi</option>
						<option value="RUS">Russian</option>
						<option value="SRP">Serbian</option>
						<option value="SLK">Slovak</option>
						<option value="SLV">Slovenian</option>
						<option value="SPA">Spanish</option>
						<option value="SWE">Swedish</option>
						<option value="TGL">Tagalog</option>
						<option value="TUR">Turkish</option>
						<option value="URD">Urdu</option>
						<option value="VIE">Vietnamese</option>

					</select>
  </div>
     </div>
   <div class="form-group row">
   <div class="col-sm-4">
    <button class="btn btn-danger"  type="submit" style="margin-left: 135px;
    margin-bottom: 20px;width:30%">Find</button>
    </div>
    </form>

    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/searchs/index.blade.php ENDPATH**/ ?>