

<?php $__env->startSection('content'); ?>



<div class="card">

    <div class="card-header">

        <?php echo e(trans('global.show')); ?> <?php echo e(trans('global.fans.title')); ?>



    </div>



    <div class="card-body">

        <table class="table table-bordered table-striped">

            <tbody>

                <tr>

                    <th>

                        <?php echo e(trans('global.fans.fields.name')); ?>



                    </th>

                    <td>

                        <?php echo e($fan->name); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Designation

                    </th>

                    <td>

                        <?php echo $fan->designation; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Organization

                    </th>

                    <td>

                        <?php echo $fan->organization; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Title

                    </th>

                    <td>

                        <?php echo $fan->title; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Email

                    </th>

                    <td>

                        <?php echo $fan->email; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Address_Line

                    </th>

                    <td>

                        <?php echo $fan->address_Line; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        City

                    </th>

                    <td>

                        <?php echo $fan->city; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        State

                    </th>

                    <td>

                        <?php echo $fan->state; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Zip_Code

                    </th>

                    <td>

                        <?php echo $fan->zip_code; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Province

                    </th>

                    <td>

                        <?php echo $fan->province; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Country

                    </th>

                    <td>

                        <?php echo $fan->country; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Phone

                    </th>

                    <td>

                        <?php echo $fan->phone; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Fax

                    </th>

                    <td>

                        <?php echo $fan->fax; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Website

                    </th>

                    <td>

                        <?php echo $fan->website; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        How many meetings does your company plan annually?

                    </th>

                    <td>

                        <?php echo $fan->q1; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Name of meeting you could bring to Albuquerque*

                    </th>

                    <td>

                        <?php echo $fan->q2; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Month and year of meeting or conference*

                    </th>

                    <td>

                        <?php echo $fan->q3; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Number of rooms on peak*

                    </th>

                    <td>

                        <?php echo $fan->q4; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Total room nights or estimate*

                    </th>

                    <td>

                        <?php echo $fan->q5; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Net sq.ft. of exhibit space needed

                    </th>

                    <td>

                        <?php echo $fan->q6; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        Other comments or requests

                    </th>

                    <td>

                        <?php echo $fan->q7; ?>



                    </td>

                </tr>

                 <tr>

                    <th>

                        <?php echo e(trans('global.fans.fields.created_at')); ?>



                    </th>

                    <td>

                         <?php echo e($fan->created_at ?? ''); ?>



                    </td>

                </tr>

            </tbody>

        </table>

    </div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/fans/showProfile.blade.php ENDPATH**/ ?>
