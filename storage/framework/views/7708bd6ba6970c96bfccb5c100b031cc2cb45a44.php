<header>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Navbar Section -->


    <nav class="navbar navbar-fixed-top " style="margin: auto;">

        <div class="container-fluid">

            <!--second nav button -->

            <div id="menu_bars" class="right menu_bars">

                <span class="t1"></span>

                <span class="t2"></span>

                <span class="t3"></span>

            </div>

            <!-- Brand and toggle get grouped for better mobile display -->


            <div class="container">

                <div class="navbar-header">

                    <div class="crypto_logo_main">


                        <p class="swiper-slider-heading raleway font_600 " style="color: #007bff;">LTF </p>


                    </div>

                    <!--<a class="navbar-brand yellow_logo_crypto" href="#"><img src="images/logo_crypto.png" alt="logo"></a>-->

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->

                <div class="collapse navbar-collapse navbar-ex1-collapse  ">

                    <ul class="nav navbar-nav navbar-right">

                        <li class="<?php echo e((request()->is('/'))  ? 'active' : ''); ?>"><a href="<?php echo e(url('/')); ?>">Home</a></li>

                        <li class="<?php echo e((request()->is('tripfam')) ? 'active' : ''); ?>"><a href="<?php echo e(url('tripfam')); ?>">FAM
                                Trip</a></li>

                        <li class="<?php echo e((request()->is('visitor')) ? 'active' : ''); ?>"><a href="<?php echo e(url('visitor')); ?>">Visitors</a>
                        </li>

                        <li class="<?php echo e((request()->is('Exhaibitur')) ? 'active' : ''); ?>"><a href="<?php echo e(url('Exhaibitur')); ?>">Exhibitors</a>
                        </li>

                        <li class="<?php echo e((request()->is('sponsored')) ? 'active' : ''); ?>"><a href="<?php echo e(url('sponsored')); ?>">Sponserd&Partners</a>
                        </li>

                        <?php if(auth()->guard()->check()): ?>
                            <li class="<?php echo e((request()->is('admin/dashboard')) ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.dashboard.index')); ?>">My Profile</a>
                            </li>
                        <?php else: ?>
                            <li class="<?php echo e((request()->is('login')) ? 'active' : ''); ?>"><a href="<?php echo e(route('login')); ?>">Login</a>
                            </li>
                        <?php endif; ?>
                    </ul>


                </div>

                <!-- /.navbar-collapse -->

            </div>

            <div class="sidebar_menu">

                <nav class="pushmenu pushmenu-right">

                    <p class="swiper-slider-heading raleway font_600 " style="color:#007bff;">LTF </p>

                    <ul class="push_nav centered">

                        <li class="clearfix">

                            <a href="<?php echo e(url('/')); ?>"><span>01.</span>Home</a>


                        </li>

                        <li class="clearfix">

                            <a href="<?php echo e(url('about')); ?>"> <span>02.</span>About</a>


                        </li>

                        <li class="clearfix">

                            <a href="<?php echo e(url('visitor')); ?>"> <span>03.</span>Visitors</a>


                        </li>


                        <li class="clearfix">

                            <a href="<?php echo e(url('Exhaibitur')); ?>"> <span>04.</span>Exhibitors</a>


                        </li>

                        <li class="clearfix">

                            <a href="<?php echo e(url('outer-news')); ?>"> <span>05.</span>News</a>


                        </li>


                        <li class="clearfix">

                            <a href="<?php echo e(url('library')); ?>"> <span>06.</span>library</a>


                        </li>


                        <li class="clearfix">

                            <a href="<?php echo e(url('contact')); ?>"> <span>07.</span>Contact</a>


                        </li>
                        <?php if(auth()->guard()->check()): ?>
                            <li class="clearfix">

                                <a href="<?php echo e(route('admin.dashboard.index')); ?>"> <span>08.</span>My Profile</a>

                            </li>
                        <?php else: ?>

                            <li class="clearfix">

                                <a href="<?php echo e(route('login')); ?>"> <span>08.</span>login</a>

                            </li>

                        <?php endif; ?>

                        <li class="clearfix">

                            <a href="<?php echo e(url('tripfam')); ?>"> <span>09.</span>Trip FAM</a>


                        </li>

                        </li>

                        <li class="clearfix">

                            <a href="<?php echo e(url('sponsored')); ?>"> <span>10.</span>sponsored&Partners</a>


                        </li>


                    </ul>

                    <div class="clearfix"></div>

                    <ul class="social_icon black top25 bottom20 list-inline">


                        <li><a href="#"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a></li>

                        <li><a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a></li>

                        <li><a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a></li>

                        <li><a href="#"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i>

                            </a></li>


                    </ul>

                </nav>

            </div>

        </div>

    </nav>


    <!-- /Navbar Section -->

</header>
<?php /**PATH C:\xampp\htdocs\LTF\resources\views/nav.blade.php ENDPATH**/ ?>