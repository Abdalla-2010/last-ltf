
<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.news.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.articles.store")); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                <label for="title"><?php echo e(trans('global.news.fields.title')); ?>*</label>
                <input type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($article) ? $article->title : '')); ?>">
                <?php if($errors->has('title')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('title')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.news.fields.title_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('description') ? 'has-error' : ''); ?>">
                <label for="description"><?php echo e(trans('global.news.fields.description')); ?></label>
                <textarea id="description" name="description" class="form-control "><?php echo e(old('description', isset($article) ? $article->description : '')); ?></textarea>
                <?php if($errors->has('description')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('description')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.news.fields.description_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
                <label for="image"><?php echo e(trans('global.news.fields.images')); ?></label>
                <input type="file" id="image" name="image" class="form-control" value="<?php echo e(old('images', isset($article) ? $article->images : '')); ?>">
                <?php if($errors->has('image')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('image')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.news.fields.images_helper')); ?>

                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/articles/create.blade.php ENDPATH**/ ?>