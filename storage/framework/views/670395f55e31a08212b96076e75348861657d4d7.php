<?php $__env->startSection('content'); ?>

    <div class="card">
        <div class="card-header">
            <?php echo e(trans('global.results.title_singular')); ?> <?php echo e(trans('global.list')); ?>

        </div>
        <div class="container">
            <div class="result">
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th></th>
                        <th scope="col">Full Name</th>
                        <th scope="col">Company</th>
                        <th scope="col">Title</th>
                        <th scope="col">Country</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $i = 1 ?>
                    <?php $__empty_1 = true; $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <tr>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($company->name . ' '. $company->last_name); ?></td>
                            <td><?php echo e($company->company); ?></td>
                            <td><?php echo e($company->title); ?></td>
                            <td><?php echo e($company->country); ?></td>
                            <?php echo e($i++); ?>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <th>
                            No Results
                        </th>
                    <?php endif; ?>
                    </tbody>
                </table>

            </div>

        </div>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/results/index.blade.php ENDPATH**/ ?>