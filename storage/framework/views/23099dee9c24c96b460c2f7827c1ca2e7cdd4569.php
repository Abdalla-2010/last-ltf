
<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.edit')); ?> <?php echo e(trans('global.about.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.abouts.update", [$about->id])); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                <label for="name"><?php echo e(trans('global.about.fields.name')); ?>*</label>
                <input type="text" id="name" name="name" class="form-control" value="<?php echo e(old('name', isset($about) ? $about->name : '')); ?>">
                <?php if($errors->has('name')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('name')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.about.fields.name_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('body') ? 'has-error' : ''); ?>">
                <label for="body"><?php echo e(trans('global.about.fields.body')); ?>*</label>
                <textarea id="body" cols="15" rows="10" name="body" class="form-control">"<?php echo e(old('body', isset($about) ? $about->body : '')); ?>"</textarea >
                <?php if($errors->has('body')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('body')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.about.fields.body_helper')); ?>

                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/abouts/edit.blade.php ENDPATH**/ ?>