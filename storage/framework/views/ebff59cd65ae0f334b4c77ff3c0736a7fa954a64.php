<style>

    @media  only screen and (max-width: 768px) {

        .card {

            width: 100% !important;

            overflow-x: scroll !important;

        }


    }

</style>



<?php $__env->startSection('content'); ?>



    <div class="card">

        <div class="card-header">

            <?php echo e(trans('global.results.title_singular')); ?> <?php echo e(trans('global.list')); ?>


        </div>

        <div class="container">

            <div class="result">

                <table class="table table-striped ">

                    <thead>

                    <tr>

                        <th></th>

                        <th scope="col">Company</th>

                        <th scope="col">Full Name</th>

                        <th scope="col">Title</th>

                        <th scope="col">Country</th>

                        <th scope="col">View Profile</th>

                        <th scope="col">Make Favourite</th>

                        <th scope="col">Appointment Booking</th>

                        1

                    </tr>

                    </thead>

                    <tbody>


                    <?php $i = 1 ?>

                    <?php $__empty_1 = true; $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

                        <tr>

                            <td><?php echo e($i); ?></td>

                            <td><?php echo e($company->company); ?></td>

                            <td><?php echo e($company->name . ' '. $company->last_name); ?></td>

                            <td><?php echo e($company->title); ?></td>

                            <td><?php echo e($company->country); ?></td>

                            <td><a href="<?php echo e(route('admin.results.show.profile', $company->id)); ?>">Profile</a></td>

                            <td>

                                <?php $exist = false ?>

                                <?php $__currentLoopData = Auth::user()->favourites; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $favourite): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php if($company->id == $favourite->id): ?>

                                        <?php $exist = true ?>

                                        <?php break; ?>

                                    <?php else: ?>

                                        <?php $exist = false ?>

                                    <?php endif; ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php if($exist): ?>

                                    <button type="submit" data-id="<?php echo e($company->id); ?>"

                                            title="remove from favourites list"

                                            class="remove heart btn btn-border">

                                        <i style="color: red" class="fas fa-heart"></i></button>

                                <?php else: ?>

                                    <button type="submit" data-id="<?php echo e($company->id); ?>" title="add to favourite list"

                                            class="add heart btn btn-border">

                                        <i style="color: red" class="far fa-heart"></i></button>

                                <?php endif; ?>

                            </td>


                            <?php echo e($i++); ?>



                            <td>
                                <a href="#" class="btn btn-success">Book an appointment</a>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                        <th>

                            No Results

                        </th>

                    <?php endif; ?>

                    </tbody>

                </table>


            </div>


        </div>

    </div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>



    <script>

        $(function () {


            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $('.heart').click(function () {

                if ($(this).hasClass("add")) {

                    var company_id = $(this).data('id');

                    var btn = $(this);

                    $.ajax({

                        data: {company_id: company_id, _token: '<?php echo e(csrf_token()); ?>'},

                        url: "<?php echo e(route('admin.favourites.add')); ?>",

                        type: "POST",

                        success: function (data) {

                            btn.removeClass('add');

                            btn.addClass('remove');

                            btn.attr('title', 'remove from favourites list');

                            btn.children().removeClass('far').addClass('fas');

                        },

                    });

                } else if ($(this).hasClass("remove")) {

                    var company_id = $(this).data('id');

                    var btn = $(this);

                    $.ajax({

                        type: "DELETE",

                        url: "<?php echo e(route('admin.favourites.add')); ?>" + '/destroy/' + company_id,

                        success: function (data) {

                            btn.removeClass('remove');

                            btn.attr('title', 'add to favourites list');

                            btn.addClass('add');

                            btn.children().removeClass('fas').addClass('far');

                        }

                    });

                }


            });

        });


    </script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\laaast-ltf\resources\views/admin/results/index.blade.php ENDPATH**/ ?>