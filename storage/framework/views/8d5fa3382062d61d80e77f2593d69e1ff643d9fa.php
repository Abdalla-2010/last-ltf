
<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.librarys.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.librarys.store")); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                <label for="name"><?php echo e(trans('global.librarys.fields.name')); ?>*</label>
                <input type="text" id="name" name="name" class="form-control" value="<?php echo e(old('name', isset($library) ? $library->name : '')); ?>">
                <?php if($errors->has('name')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('name')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.librarys.fields.name_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('video') ? 'has-error' : ''); ?>">
                <label for="video"><?php echo e(trans('global.librarys.fields.video')); ?></label>
                <input type="file" id="video" name="video" class="form-control" value="<?php echo e(old('video', isset($library) ? $library->video : '')); ?>">
                <?php if($errors->has('video')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('video')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.librarys.fields.video_helper')); ?>

                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/librarys/create.blade.php ENDPATH**/ ?>