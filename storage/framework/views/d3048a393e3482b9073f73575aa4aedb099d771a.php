

<?php $__env->startSection('content'); ?>



<div class="card">

    <div class="card-header">

        <?php echo e(trans('global.show')); ?> <?php echo e(trans('global.news.title')); ?>



    </div>



    <div class="card-body">

        <table class="table table-bordered table-striped">

            <tbody>

                <tr>

                    <th>

                        <?php echo e(trans('global.news.fields.title')); ?>



                    </th>

                    <td>

                        <?php echo e($article->title); ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.news.fields.description')); ?>



                    </th>

                    <td>

                        <?php echo $article->description; ?>



                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.news.fields.images')); ?>



                    </th>

                    <td>

                         <img src="<?php echo e(asset ('uploads/articles/'. $article->images)); ?>" width="100" alt=" <?php echo e(trans('global.news.title_singular')); ?>">

                    </td>

                </tr>

                <tr>

                    <th>

                        <?php echo e(trans('global.news.fields.created_at')); ?>



                    </th>

                    <td>

                         <?php echo e($article->created_at ?? ''); ?>



                    </td>

                </tr>

            </tbody>

        </table>

    </div>

</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/admin/articles/showProfile.blade.php ENDPATH**/ ?>
