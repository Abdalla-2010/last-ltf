<html lang="en">
<head>
    <style>.dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }

        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }

        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }

        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }

        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }

        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }

    </style>
    <style>.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }

    </style>
    <style>.gm-control-active > img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active > img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover > img:nth-child(1), .gm-control-active:active > img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover > img:nth-child(2), .gm-control-active:active > img:nth-child(3) {
            display: block
        }

    </style>
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700">
    <style>.gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

    </style>
    <style>.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }

    </style>
    <style>@media  print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media  screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>

    <style>

        body {

            background-image: url(Frontend/images/Mercator_Blank_Map_World.png);

            background-size: cover !important;


        }

        .navbar .navbar-nav li a {

            color: #191919 !important;

        }

        .swiper-slider-heading {

            font-size: 35px !important;

        }

        label {

        }

        label:hover {

            cursor: pointer;

        }
        .navbar-fixed-top {

            background: white;

            margin: auto;


        }


        @media  only screen and (max-width: 768px) {

            .phone {

                width: 150% !important;


            }

            .check {

                text-align: left;

            }

        }


    </style>

    <!-- Meta Tags For Seo + Page Optimization -->

    <meta charset="utf-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="description" content="">


    <meta name="keywords" content="">


    <!-- Insert Favicon Here -->

    <link href="<?php echo e(asset('Frontend/images/favicon.png')); ?>" rel="icon">


    <!-- Page Title(Name)-->

    <title>LTF</title>


    <!-- Bootstrap CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">


    <!-- Font-Awesome CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">


    <!-- Slider Revolution CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">


    <!--  Fancy Box CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">


    <!-- Circleful CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">


    <!-- Animate CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">


    <!-- Cube Portfolio CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">


    <!-- Owl Carousel CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.theme.default.min.css')); ?>">


    <!-- Swiper CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">


    <!-- Custom Style CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">


    <!-- Crypto Currency CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">


    <!-- Color StyleSheet CSS File -->

    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/intlTelInput.css')); ?>">

    <!--<link rel="stylesheet" href="css/demo.css">-->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script>
    <style type="text/css">@-webkit-keyframes _gm7906 {

                               0% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                               50% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }

                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                           }

    </style>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script>
</head>


<body>


<!-- Loader -->


<!-- Loader -->


<div class="loader" style="display: none;">


    <div class="loader-inner">


        <div class="spinner">


            <div class="dot1"></div>


            <div class="dot2"></div>


        </div>


    </div>


</div>

<!-- Parent Section -->


<section class="page_content_parent_section">


    <input name="" type="hidden" value="">


    <!-- Header Section -->


    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</section>


<section class="contact_form_section" id="contact-form">


    <div class="container">


        <div class="row">


            <div class="contact_form_inner big_padding clearfix">


                <div class="col-md-12 " style="visibility: visible; ">


                    <div class="contact_form_detail  center_on_mobile">


                        <p class="default_small_heading raleway blue_color font_200"></p>


                        <h3 class="default_section_heading raleway navy_blue"><span
                                class="font_200">Join LTF Membership</span></h3>


                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">


                        <p class="default_text_light default_text open_sans">


                            Become a member of our Online Live Travel Fair & meet your international business partners,
                            travel industry experts and top decision-makers. Expand your business opportunities and more
                            …


                        </p>

                        <form method="POST" action="<?php echo e(route('register2')); ?>">

                            <?php echo e(csrf_field()); ?>


                            <div class="row">

                                <div class="contact_form_extra_inner clearfix center_on_mobile">

                                    <div class="col-md-11 col-sm-12 check">

                                        <input type="hidden" name="role_id" value="6">
                                        <input type="hidden" name="status" value="4">

                                        <label>

                                            <input type="radio" class="paymethod" name="radio" value="square"
                                                   style="margin-right:5px">

                                            Travel Agency


                                            <div class="square_fields"
                                                 style="margin-left: 12px;font-size: 14px;font-weight: 500;"></div>

                                        </label>

                                        <br>

                                        <label>

                                            <input type="radio" class="paymethod" name="radio" value="stripe"
                                                   style="margin-right:5px">Consortium

                                            <div class="stripe_fields"
                                                 style="margin-left: 12px;font-size: 14px;font-weight: 500;"></div>

                                        </label>

                                        <br>

                                        <label>

                                            <input type="radio" class="paymethod" name="radio" value="authorize.net"
                                                   style="margin-right:5px">Travel Tourism Organization

                                            <div class="authorize_net_fields"
                                                 style="margin-left: 12px;font-size: 14px;font-weight: 500;"></div>

                                        </label>

                                        <br>

                                        <label>

                                            <input type="radio" class="paymethod" name="radio" value="paypal"
                                                   style="margin-right:5px">Travel Supplier/Tour Operator

                                            <div class="paypal_fields"
                                                 style="margin-left: 12px;font-size: 14px;font-weight: 500;"></div>

                                        </label>

                                    </div>
                                </div>

                                <div class="check">
                                    <div class="container">
                                        <div class="row" style="margin-top:20px">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="first">First Name</label>

                                                    <input type="text" class="form-control" placeholder="" id="name"
                                                           name="name" value="<?php echo e(session()->get('register.name')); ?>"
                                                           required>

                                                </div>

                                            </div>

                                            <!--  col-md-6   -->


                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="last">Last Name</label>

                                                    <input type="text" class="form-control" placeholder="" id="last"
                                                           name="last"  required>

                                                </div>

                                            </div>

                                            <!--  col-md-6   -->

                                        </div>


                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="company">Company Name</label>

                                                    <input type="text" class="form-control" placeholder=" Company Name"
                                                           id="company" name="company_name"  required>

                                                </div>


                                            </div>

                                            <!--  col-md-6   -->


                                            <div class="col-md-6">


                                                <div class="form-group">

                                                    <label for="phone"> Cell Phone </label>

                                                    <br>

                                                    <input type="tel" id="phone" name="phone" class="form-control phone"
                                                           aria-describedby="" placeholder="" style="width:241%"
                                                            required>

                                                </div>

                                            </div>

                                            <!--  col-md-6   -->

                                        </div>

                                        <!--  row   -->


                                        <div class="row">

                                            <div class="col-md-6">

                                                <label for="email">Email address</label>

                                                <input type="email" class="form-control" id="email" name="email"
                                                       value="<?php echo e(session()->get('register.email')); ?>"
                                                       placeholder="company email">

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label for="url">Your Website <small>Please include http:// or
                                                            https://</small></label>

                                                    <input type="url" class="form-control" id="url" placeholder="url"
                                                           name="website">

                                                </div>

                                            </div>

                                            <!--  col-md-6   -->

                                            <div class="col-md-6">

                                                <label for=""> Country </label>

                                                <select name="city" class="form-control" value="Countr" required>

                                                    <option selected="selected" value="United States">United States
                                                    </option>

                                                    <option value="Canada">Canada</option>

                                                    <option value="Afghanistan">Afghanistan</option>

                                                    <option value="Aland Islands">Aland Islands</option>

                                                    <option value="Albania">Albania</option>

                                                    <option value="Algeria">Algeria</option>

                                                    <option value="American Samoa">American Samoa</option>

                                                    <option value="Andorra">Andorra</option>

                                                    <option value="Angola">Angola</option>

                                                    <option value="Anguilla">Anguilla</option>

                                                    <option value="Antarctica">Antarctica</option>

                                                    <option value="Antigua">Antigua</option>

                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>

                                                    <option value="Argentina">Argentina</option>

                                                    <option value="Armenia">Armenia</option>

                                                    <option value="Aruba">Aruba</option>

                                                    <option value="Australia">Australia</option>

                                                    <option value="Austria">Austria</option>

                                                    <option value="Azerbaijan">Azerbaijan</option>

                                                    <option value="Bahamas">Bahamas</option>

                                                    <option value="Bahrain">Bahrain</option>

                                                    <option value="Bangladesh">Bangladesh</option>

                                                    <option value="Barbados">Barbados</option>

                                                    <option value="Belarus">Belarus</option>

                                                    <option value="Belgium">Belgium</option>

                                                    <option value="Belize">Belize</option>

                                                    <option value="Benin">Benin</option>

                                                    <option value="Bermuda">Bermuda</option>

                                                    <option value="Bharain">Bharain</option>

                                                    <option value="Bhutan">Bhutan</option>

                                                    <option value="Bolivia">Bolivia</option>

                                                    <option value="Bonaire">Bonaire</option>

                                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina
                                                    </option>

                                                    <option value="Botswana">Botswana</option>

                                                    <option value="Bouvet Island">Bouvet Island</option>

                                                    <option value="Brazil">Brazil</option>

                                                    <option value="British Indian Ocean Terr">British Indian Ocean
                                                        Terr
                                                    </option>

                                                    <option value="British Virgin Islands">British Virgin Islands
                                                    </option>

                                                    <option value="Brunei">Brunei</option>

                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>

                                                    <option value="Bulgaria">Bulgaria</option>

                                                    <option value="Burkina Faso">Burkina Faso</option>

                                                    <option value="Burma">Burma</option>

                                                    <option value="Burundi">Burundi</option>

                                                    <option value="Cambodia">Cambodia</option>

                                                    <option value="Cameroon">Cameroon</option>

                                                    <option value="Canada">Canada</option>

                                                    <option value="Canary Islands">Canary Islands</option>

                                                    <option value="Cape Verde">Cape Verde</option>

                                                    <option value="Cayman Islands">Cayman Islands</option>

                                                    <option value="Central African Republic">Central African Republic
                                                    </option>

                                                    <option value="Chad">Chad</option>

                                                    <option value="Chile">Chile</option>

                                                    <option value="China">China</option>

                                                    <option value="Chinese Taipei">Chinese Taipei</option>

                                                    <option value="Christmas Island">Christmas Island</option>

                                                    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands
                                                    </option>

                                                    <option value="Colombia">Colombia</option>

                                                    <option value="Comoros">Comoros</option>

                                                    <option value="Congo">Congo</option>

                                                    <option value="Congo, The Dem Rep Of The">Congo, The Dem Rep Of
                                                        The
                                                    </option>

                                                    <option value="Cook Islands">Cook Islands</option>

                                                    <option value="Costa Rica">Costa Rica</option>

                                                    <option value="Cote D'Ivoire">Cote D'Ivoire</option>

                                                    <option value="Croatia">Croatia</option>

                                                    <option value="Cuba">Cuba</option>

                                                    <option value="Curacao">Curacao</option>

                                                    <option value="Cyprus">Cyprus</option>

                                                    <option value="Czech Republic">Czech Republic</option>

                                                    <option value="Denmark">Denmark</option>

                                                    <option value="Djibouti">Djibouti</option>

                                                    <option value="Dominica">Dominica</option>

                                                    <option value="Dominican Republic">Dominican Republic</option>

                                                    <option value="Ecuador">Ecuador</option>

                                                    <option value="Egypt">Egypt</option>

                                                    <option value="El Salvador">El Salvador</option>

                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>

                                                    <option value="Eritrea">Eritrea</option>

                                                    <option value="Estonia">Estonia</option>

                                                    <option value="Ethiopia">Ethiopia</option>

                                                    <option value="Falkland Is (Malvinas)">Falkland Is (Malvinas)
                                                    </option>

                                                    <option value="Faroe Islands">Faroe Islands</option>

                                                    <option value="Fiji">Fiji</option>

                                                    <option value="Finland">Finland</option>

                                                    <option value="France">France</option>

                                                    <option value="French Guiana">French Guiana</option>

                                                    <option value="French Polynesia">French Polynesia</option>

                                                    <option value="French Southern Terr">French Southern Terr</option>

                                                    <option value="Gabon">Gabon</option>

                                                    <option value="Gambia">Gambia</option>

                                                    <option value="Georgia">Georgia</option>

                                                    <option value="Germany">Germany</option>

                                                    <option value="Ghana">Ghana</option>

                                                    <option value="Gibraltar">Gibraltar</option>

                                                    <option value="Greece">Greece</option>

                                                    <option value="Greenland">Greenland</option>

                                                    <option value="Grenada">Grenada</option>

                                                    <option value="Guadeloupe">Guadeloupe</option>

                                                    <option value="Guam">Guam</option>

                                                    <option value="Guatemala">Guatemala</option>

                                                    <option value="Guinea">Guinea</option>

                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>

                                                    <option value="Guyana">Guyana</option>

                                                    <option value="Haiti">Haiti</option>

                                                    <option value="Heard Is and McDonald Is">Heard Is and McDonald Is
                                                    </option>

                                                    <option value="Holy See (Vatican City)">Holy See (Vatican City)
                                                    </option>

                                                    <option value="Honduras">Honduras</option>

                                                    <option value="Hong Kong">Hong Kong</option>

                                                    <option value="Hong Kong, China">Hong Kong, China</option>

                                                    <option value="Hungary">Hungary</option>

                                                    <option value="Iceland">Iceland</option>

                                                    <option value="India">India</option>

                                                    <option value="Indonesia">Indonesia</option>

                                                    <option value="Iran">Iran</option>

                                                    <option value="Iran, Islamic Republic of">Iran, Islamic Republic
                                                        of
                                                    </option>

                                                    <option value="Iraq">Iraq</option>

                                                    <option value="Ireland">Ireland</option>

                                                    <option value="Israel">Israel</option>

                                                    <option value="Italy">Italy</option>

                                                    <option value="Jamaica">Jamaica</option>

                                                    <option value="Japan">Japan</option>

                                                    <option value="Jordan">Jordan</option>

                                                    <option value="Kazakhstan">Kazakhstan</option>

                                                    <option value="Kazakstan">Kazakstan</option>

                                                    <option value="Kenya">Kenya</option>

                                                    <option value="Kiribati">Kiribati</option>

                                                    <option value="Korea, Dem People's Rep">Korea, Dem People's Rep
                                                    </option>

                                                    <option value="Korea, Rebublic Of">Korea, Rebublic Of</option>

                                                    <option value="Korea, Republic of">Korea, Republic of</option>

                                                    <option value="Kosovo">Kosovo</option>

                                                    <option value="Kuwait">Kuwait</option>

                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>

                                                    <option value="Lao People's Dem Republic">Lao People's Dem
                                                        Republic
                                                    </option>

                                                    <option value="Laos">Laos</option>

                                                    <option value="Latvia">Latvia</option>

                                                    <option value="Lebanon">Lebanon</option>

                                                    <option value="Lesotho">Lesotho</option>

                                                    <option value="Liberia">Liberia</option>

                                                    <option value="Libya">Libya</option>

                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya
                                                    </option>

                                                    <option value="Liechtenstein">Liechtenstein</option>

                                                    <option value="Lithuania">Lithuania</option>

                                                    <option value="Luxembourg">Luxembourg</option>

                                                    <option value="Macao">Macao</option>

                                                    <option value="Macau, China">Macau, China</option>

                                                    <option value="Macedonia">Macedonia</option>

                                                    <option value="Madagascar">Madagascar</option>

                                                    <option value="Malawi">Malawi</option>

                                                    <option value="Malaysia">Malaysia</option>

                                                    <option value="Maldives">Maldives</option>

                                                    <option value="Mali">Mali</option>

                                                    <option value="Malta">Malta</option>

                                                    <option value="Marshall Islands">Marshall Islands</option>

                                                    <option value="Martinique">Martinique</option>

                                                    <option value="Mauritania">Mauritania</option>

                                                    <option value="Mauritius">Mauritius</option>

                                                    <option value="Mayotte">Mayotte</option>

                                                    <option value="Mexico">Mexico</option>

                                                    <option value="Micronesia, Fed States Of">Micronesia, Fed States
                                                        Of
                                                    </option>

                                                    <option value="Moldova">Moldova</option>

                                                    <option value="Moldova, Republic Of">Moldova, Republic Of</option>

                                                    <option value="Monaco">Monaco</option>

                                                    <option value="Mongolia">Mongolia</option>

                                                    <option value="Montenegro">Montenegro</option>

                                                    <option value="Montserrat">Montserrat</option>

                                                    <option value="Morocco">Morocco</option>

                                                    <option value="Mozambique">Mozambique</option>

                                                    <option value="Myanmar">Myanmar</option>

                                                    <option value="Namibia">Namibia</option>

                                                    <option value="Nauru">Nauru</option>

                                                    <option value="Nepal">Nepal</option>

                                                    <option value="Netherlands">Netherlands</option>

                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>

                                                    <option value="New Caledonia">New Caledonia</option>

                                                    <option value="New Guinea">New Guinea</option>

                                                    <option value="New Zealand">New Zealand</option>

                                                    <option value="Nicaragua">Nicaragua</option>

                                                    <option value="Niger">Niger</option>

                                                    <option value="Nigeria">Nigeria</option>

                                                    <option value="Niue">Niue</option>

                                                    <option value="Norfolk Island">Norfolk Island</option>

                                                    <option value="North Cyprus">North Cyprus</option>

                                                    <option value="Northern Mariana Islands">Northern Mariana Islands
                                                    </option>

                                                    <option value="Norway">Norway</option>

                                                    <option value="Oman">Oman</option>

                                                    <option value="Pakistan">Pakistan</option>

                                                    <option value="Palau">Palau</option>

                                                    <option value="Palestine">Palestine</option>

                                                    <option value="Palestinian Territory">Palestinian Territory</option>

                                                    <option value="Panama">Panama</option>

                                                    <option value="Papua New Guinea">Papua New Guinea</option>

                                                    <option value="Paraguay">Paraguay</option>

                                                    <option value="Peoples Republic of Congo">Peoples Republic of
                                                        Congo
                                                    </option>

                                                    <option value="Peru">Peru</option>

                                                    <option value="Philippines">Philippines</option>

                                                    <option value="Pitcairn">Pitcairn</option>

                                                    <option value="Poland">Poland</option>

                                                    <option value="Portugal">Portugal</option>

                                                    <option value="Puerto Rico">Puerto Rico</option>

                                                    <option value="Qatar">Qatar</option>

                                                    <option value="Reunion">Reunion</option>

                                                    <option value="Romania">Romania</option>

                                                    <option value="Russia">Russia</option>

                                                    <option value="Russian Federation">Russian Federation</option>

                                                    <option value="Rwanda">Rwanda</option>

                                                    <option value="Saint Barthelemy">Saint Barthelemy</option>

                                                    <option value="Saint Eustatius">Saint Eustatius</option>

                                                    <option value="Saint Helena">Saint Helena</option>

                                                    <option value="Saint Kitts &amp; Nevis">Saint Kitts &amp; Nevis
                                                    </option>

                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>

                                                    <option value="Saint Lucia">Saint Lucia</option>

                                                    <option value="Saint Martin">Saint Martin</option>

                                                    <option value="Saint Pierre and Miquelon">Saint Pierre and
                                                        Miquelon
                                                    </option>

                                                    <option value="Saint Vincent">Saint Vincent</option>

                                                    <option value="Samoa">Samoa</option>

                                                    <option value="San Marino">San Marino</option>

                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>

                                                    <option value="Saudi Arabia">Saudi Arabia</option>

                                                    <option value="Senegal">Senegal</option>

                                                    <option value="Serbia">Serbia</option>

                                                    <option value="Serbia and Montenegro">Serbia and Montenegro</option>

                                                    <option value="Seychelles">Seychelles</option>

                                                    <option value="Sierra Leone">Sierra Leone</option>

                                                    <option value="Singapore">Singapore</option>

                                                    <option value="Sint Maarten">Sint Maarten</option>

                                                    <option value="Slovak Republic">Slovak Republic</option>

                                                    <option value="Slovakia">Slovakia</option>

                                                    <option value="Slovenia">Slovenia</option>

                                                    <option value="Solomon Islands">Solomon Islands</option>

                                                    <option value="Somalia">Somalia</option>

                                                    <option value="South Africa">South Africa</option>

                                                    <option value="South Georgia and the SSI">South Georgia and the
                                                        SSI
                                                    </option>

                                                    <option value="Spain">Spain</option>

                                                    <option value="Sri Lanka">Sri Lanka</option>

                                                    <option value="St Vincent and Grenadines">St Vincent and
                                                        Grenadines
                                                    </option>

                                                    <option value="Sudan">Sudan</option>

                                                    <option value="Suriname">Suriname</option>

                                                    <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen
                                                    </option>

                                                    <option value="Swaziland">Swaziland</option>

                                                    <option value="Sweden">Sweden</option>

                                                    <option value="Switzerland">Switzerland</option>

                                                    <option value="Syria">Syria</option>

                                                    <option value="Syrian Arab Republic">Syrian Arab Republic</option>

                                                    <option value="Tahiti">Tahiti</option>

                                                    <option value="Taiwan">Taiwan</option>

                                                    <option value="Taiwan, Province of China">Taiwan, Province of
                                                        China
                                                    </option>

                                                    <option value="Tajikistan">Tajikistan</option>

                                                    <option value="Tanzania">Tanzania</option>

                                                    <option value="Tanzania, United Republic">Tanzania, United
                                                        Republic
                                                    </option>

                                                    <option value="Thailand">Thailand</option>

                                                    <option value="Timor-Leste">Timor-Leste</option>

                                                    <option value="Togo">Togo</option>

                                                    <option value="Tokelau">Tokelau</option>

                                                    <option value="Tonga">Tonga</option>

                                                    <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>

                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>

                                                    <option value="Tunisia">Tunisia</option>

                                                    <option value="Turkey">Turkey</option>

                                                    <option value="Turkmenistan">Turkmenistan</option>

                                                    <option value="Turks &amp; Caicos">Turks &amp; Caicos</option>

                                                    <option value="Turks and Caicos Islands">Turks and Caicos Islands
                                                    </option>

                                                    <option value="Tuvalu">Tuvalu</option>

                                                    <option value="UAE">UAE</option>

                                                    <option value="Uganda">Uganda</option>

                                                    <option value="Ukraine">Ukraine</option>

                                                    <option value="United Arab Emirates">United Arab Emirates</option>

                                                    <option value="United Kingdom">United Kingdom</option>

                                                    <option value="United States">United States</option>

                                                    <option value="Uruguay">Uruguay</option>

                                                    <option value="US Minor Outlying Islands">US Minor Outlying
                                                        Islands
                                                    </option>

                                                    <option value="Uzbekistan">Uzbekistan</option>

                                                    <option value="Vanuatu">Vanuatu</option>

                                                    <option value="Venezuela">Venezuela</option>

                                                    <option value="Viet Nam">Viet Nam</option>

                                                    <option value="Vietnam">Vietnam</option>

                                                    <option value="Virgin Islands, British">Virgin Islands, British
                                                    </option>

                                                    <option value="Virgin Islands, US">Virgin Islands, US</option>

                                                    <option value="Wallis and Futuna">Wallis and Futuna</option>

                                                    <option value="Western Sahara">Western Sahara</option>

                                                    <option value="Yemen">Yemen</option>

                                                    <option value="Yugoslavia">Yugoslavia</option>

                                                    <option value="Zambia">Zambia</option>

                                                    <option value="Zimbabwe">Zimbabwe</option>


                                                </select>

                                            </div>

                                            <div class="col-md-6">


                                                <label for="email">Zip code</label>

                                                <input type="" class="form-control" placeholder="code" name="zip_code"
                                                       value="">

                                            </div>

                                        </div>


                                        <!--  col-md-6   -->

                                        <div class="row">

                                            <div class="col-md-6">


                                                <label for=""> Password</label>

                                                <input type="password" class="form-control" id="txtNewPassword"
                                                       name="password" placeholder="Password"
                                                       value="<?php echo e(session()->get('register.password')); ?>" required>

                                                <h6 style="color:red">Password must be at least 8 characters </h6>

                                            </div>

                                            <div class="col-md-6">

                                                <label for=""> Confirmed Password</label>

                                                <input type="password" class="form-control" id="txtConfirmPassword"
                                                       onChange="checkPasswordMatch();" placeholder="Confirmed"
                                                       name="Confirmed" value="" required>

                                            </div>

                                            <div class="registrationFormAlert" id="divCheckPasswordMatch" style="text-align:center;">

                                            </div>


                                            <button type="submit" class="btn btn-primary" style="margin-top: 25px;

    float: right;

    margin-right: 72px;

    width: 91px;float:right;"> Next
                                            </button>

                                            <!--<a href="https://cvctravel.tech/OTS/company-info.html" style="color:#fff;"> Next</a></button>-->

                                        </div>


                                        <div class="contact_form_extra_inner clearfix center_on_mobile">


                                            <div class="col-md-11 col-sm-12">


                                            </div>


                                        </div>


                                        <div class="contact_form_extra_inner clearfix center_on_mobile">


                                            <div class="col-md-11 col-sm-12">


                                            </div>


                                        </div>


                                    </div>

                                </div>

                            </div>

                        </form>

                        <div class="col-md-6 " style="visibility: visible;">


                            <!--<form onsubmit="return false" class="form_class">-->


                            <!--    <div class="row">-->


                            <!--        <div class="mew_form clearfix">-->

                            <!--            <div class="col-sm-12" id="result"></div>-->


                            <!--            <div class="col-sm-6">-->


                            <!--                <input placeholder="Your Name" class="form_inputs" id="name" name="name" required="required">-->


                            <!--            </div>-->


                            <!--            <div class="col-sm-6">-->


                            <!--                <input placeholder="Email" class="form_inputs" id="email" name="email" required="required">-->


                            <!--            </div>-->


                            <!--            <div class="col-sm-12">-->


                            <!--                <textarea placeholder="Your Message" class="form_inputs form_textarea" id="message" name="message" required="required"></textarea>-->


                            <!--            </div>-->


                            <!--            <div class="col-xs-12">-->


                            <!--                <div class="button_div  center_on_mobile">-->

                            <!--                    <input id="submit_handle" type="submit" style="display: none">-->

                            <!--                    <a href="javascript:" id="submit_btn" class="bg_pink button button_default_style open_sans bg_before_navy"> <i class="fa fa-envelope" aria-hidden="true"></i> Send Message</a>-->


                            <!--                </div>-->


                            <!--            </div>-->


                            <!--        </div>-->


                            <!--    </div>-->


                        </div>


                    </div>


                </div>


            </div>
        </div>
    </div>
</section><!-- /Parent Section Ended -->


<!-- jQuery 2.2.0-->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>

<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="<?php echo e(asset('Frontend/js/PassRequirements.js')); ?>"></script>

<script>

    function checkPasswordMatch() {

        var password = $("#txtNewPassword").val();

        var confirmPassword = $("#txtConfirmPassword").val();


        if (password != confirmPassword)

            $("#divCheckPasswordMatch").html("Password do not match!").css('color', 'red');

        else

            $("#divCheckPasswordMatch").html("Password match.").css('color', 'green');

    }


    $(document).ready(function () {

        $("#txtConfirmPassword").keyup(checkPasswordMatch);

    });

</script>

<script>

    $('#test1').PassRequirements({

        rules: {

            containSpecialChars: {

                text: "Your input should contain at least minLength special character(s)",

                minLength: 1,

                regex: new RegExp('([^!,%,&,@,#,$,^,*,?,_,~])', 'g')

            },

            containNumbers: {

                text: "Your input should contain at least minLength number(s)",

                minLength: 2,

                regex: new RegExp('[^0-9]', 'g')

            }

        }

    });

</script>

<script type="text/javascript">


    var _gaq = _gaq || [];

    _gaq.push(['_setAccount', 'UA-36251023-1']);

    _gaq.push(['_setDomainName', 'jqueryscript.net']);

    _gaq.push(['_trackPageview']);


    (function () {

        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);

    })();


</script>

<script src="<?php echo e(asset('Frontend/js/intlTelInput.js')); ?>"></script>

<script>

    var input = document.querySelector("#phone");

    window.intlTelInput(input, {

        utilsScript: "build/js/utils.js",

    });

</script>


<script>

    $("#myform").validate({

        rules: {

            password: "required",

            password_again: {

                equalTo: "#password"

            }

        }

    });

</script>


<script>

    $(document).ready(function () {

        $(".paymethod").change(function () {

            paymentMethod = $(this).val();

            $('.val:checked').remove();

            $(this).parent().siblings('label').children('div').html('');

            switch (paymentMethod) {

                case "stripe":

                    $(".stripe_fields").append(`<input type="radio" id="" name="radiobutton" value="Representative of a Consortium">Representative of a Consortium <br><input type="radio" id="" name="radiobutton" value="Employee of an existing LTF Travel Agency member">Employee of an existing LTF Travel Agency member `);

                    break;

                case "square":

                    $(".square_fields").append(`<input type="radio" id="" name="radiobutton" value="Independent Advisor or Contractor with annual sales less than $5 Million">Independent Advisor or Contractor with annual sales less than $5 Million <br> <input type="radio" id="" name="radiobutton" value="Representative of a Travel Agency with annual sales greater than $5 Million">Representative of a Travel Agency with annual sales greater than $5 Million <br><input type="radio" id="" name="radiobutton" value="Employee of an existing LTF Travel Agency member">Employee of an existing LTF Travel Agency member <br><input type="radio" id="" name="radiobutton" value="Representative of a Host Agency">Representative of a Host Agency`);

                    break;

                case "authorize.net":

                    $(".authorize_net_fields").append(`<input type="radio" id="" name="radiobutton" value="Representative of a Travel Tourism Organization">Representative of a Travel Tourism Organization <br><input type="radio" id="" name="radiobutton" value="Employee of an existing Travel Tourism Organization member">Employee of an existing Travel Tourism Organization member `);

                    break;

                case "paypal":

                    $(".paypal_fields").append(`<input type="radio" id="" name="radiobutton" value="Representative of a travel supplier/tour operator">Representative of a travel supplier/tour operator <br><input type="radio" id="" name="radiobutton" value="Employee of an existing LTF travel supplier/tour operator member">Employee of an existing LTF travel supplier/tour operator member `);

                    break;

                default:

                    alert("select a payment method");

            }

        });

    });

</script>


<script src="<?php echo e(asset('Frontend/js/jquery.js')); ?>"></script>


<!-- Google Map Api -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"
        type="text/javascript"></script>

<script src="<?php echo e(asset('Frontend/js/map.js" type="text/javascript')); ?>"></script>


<!-- REVOLUTION JS FILES -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>


<!-- Bootstrap Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/bootstrap.min.js')); ?>"></script>


<!-- Owl Carousel 2 Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>


<!-- Fancy Box Javacript -->

<script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>


<!-- Wow Js -->

<script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>


<!-- Appear Js-->

<script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>


<!-- Countdown Js -->

<script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>


<!-- Parallax Js -->

<script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>


<!-- Particles Core Js -->

<script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>


<!-- Cube Portfolio Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>


<!-- Circliful Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>


<!-- Swiper Slider Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>


<!-- Crypto Currency Core Javascript -->

<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>


<!-- Custom JavaScript -->

<script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>


</body>
</html>
<?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/register-exhibitor.blade.php ENDPATH**/ ?>