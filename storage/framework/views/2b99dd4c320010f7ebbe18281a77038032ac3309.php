<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.edit')); ?> <?php echo e(trans('global.metas.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.metas.update", [$meta->id])); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="form-group <?php echo e($errors->has('group') ? 'has-error' : ''); ?>">
                <label for="group"><?php echo e(trans('global.metas.fields.group')); ?></label>
                <input type="text" disabled id="group" name="group" class="form-control" value="<?php echo e(old('group', isset($meta) ? $meta->group : '')); ?>">
                <?php if($errors->has('group')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('group')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.metas.fields.group_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                <label for="title"><?php echo e(trans('global.metas.fields.name')); ?>*</label>
                <input type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($meta) ? $meta->title : '')); ?>">
                <?php if($errors->has('title')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('title')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.metas.fields.name_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('key_words') ? 'has-error' : ''); ?>">
                <label for="body"><?php echo e(trans('global.metas.fields.keywords')); ?></label>
                <input type="text" id="title" name="key_words" class="form-control" value="<?php echo e(old('key_words', isset($meta) ? $meta->key_words : '')); ?>">                <?php if($errors->has('key_words')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('key_words')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.metas.fields.keywords_helper')); ?>

                </p>
            </div>
            <div class="form-group <?php echo e($errors->has('body') ? 'has-error' : ''); ?>">
                <label for="body"><?php echo e(trans('global.metas.fields.description')); ?></label>
                <textarea id="body" name="body" class="form-control "><?php echo e(old('body', isset($meta) ? $meta->body : '')); ?></textarea>
                <?php if($errors->has('body')): ?>
                    <em class="invalid-feedback">
                        <?php echo e($errors->first('body')); ?>

                    </em>
                <?php endif; ?>
                <p class="helper-block">
                    <?php echo e(trans('global.metas.fields.description_helper')); ?>

                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\LTF\resources\views/admin/metas/edit.blade.php ENDPATH**/ ?>