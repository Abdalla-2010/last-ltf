<?php $__env->startSection('content'); ?>
    <div class="card">
        <div class="card-header">
            <?php echo e(trans('global.favourites.title')); ?> <?php echo e(trans('global.list')); ?>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable">
                    <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            <?php echo e(trans('global.exhibitor.fields.company')); ?>

                        </th>

                        <th>
                            <?php echo e(trans('global.user.fields.name')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.user.fields.email')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.user.fields.phone')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.user.fields.country')); ?>

                        </th>
                        <th>
                            Control &nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $favourites; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $favourite): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($favourite->id); ?>">
                            <td>

                            </td>
                            <td>
                                <?php echo e($favourite->name.' '.$favourite->last_name ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($favourite->company ?? ''); ?>

                            </td>

                            <td>
                                <?php echo e($favourite->email ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($favourite->phone ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($favourite->country ?? ''); ?>

                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary"
                                   href="<?php echo e(route('admin.favourites.show', $favourite->id)); ?>">
                                    <?php echo e(trans('global.view')); ?>

                                </a>
                                <form action="<?php echo e(route('admin.favourites.delete', ['id'=>$favourite->id])); ?>" method="post"
                                      onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');"
                                      style="display: inline-block;">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('delete'); ?>
                                    <input type="submit" class="btn btn-xs btn-danger"
                                           value="<?php echo e(trans('global.delete')); ?>">
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php $__env->startSection('scripts'); ?>
    ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
    <script>
        $(function () {
            let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "<?php echo e(route('admin.favourites.massDestroy')); ?>",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({selected: true}).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

                        return
                    }

                    if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: {ids: ids, _method: 'DELETE'}
                        })
                            .done(function () {
                                location.reload()
                            })
                    }
                }
            }
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            dtButtons.push(deleteButton)
            $('.datatable:not(.ajaxTable)').DataTable({buttons: dtButtons})
        })

    </script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\laaast-ltf\resources\views/admin/favourites/index.blade.php ENDPATH**/ ?>