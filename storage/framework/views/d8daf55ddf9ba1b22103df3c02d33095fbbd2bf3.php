<html lang="en">
<head>
    <style>.dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }

        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }

        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }

        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }

        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }

        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }

    </style>
    <style>.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }

    </style>
    <style>.gm-control-active > img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active > img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover > img:nth-child(1), .gm-control-active:active > img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover > img:nth-child(2), .gm-control-active:active > img:nth-child(3) {
            display: block
        }

    </style>
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700">
    <style>.gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }

    </style>
    <style>.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }

    </style>
    <style>@media  print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media  screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>

    <style>

        body {

            background-image: url(images/Mercator_Blank_Map_World.png);

            background-size: cover !important;


        }

        .navbar .navbar-nav li a {

            color: #191919 !important;

        }

        .swiper-slider-heading {

            font-size: 35px !important;

        }

        .menu ul {

            list-style-type: none;

            margin: 0;

            padding: 0;

        }

        .navbar-fixed-top {

            background: white;

            margin: auto;


        }


        .menu li {

            padding: 8px;

            margin-bottom: 7px;

            background-color: #33b5e5;

            color: #ffffff;

            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);

        }


        .menu li:hover {

            background-color: #0099cc;

        }

        .flex-container {

            display: flex;

            flex-wrap: nowrap;


        }


        .flex-container > div {

            background-color: #f1f1f1;

            width: 254px;

            margin: 10px;

            text-align: center;

            line-height: 75px;

            font-size: 17px;

            font-weight: 700;

            height: 106px;

            padding-top: 16px;

        }

        .content-blog h1 {

            color: #007bff;

            font-size: 28px;

            font-weight: 700;

            margin-top: 0;

            padding-left: 20px;

        }

        .title {


            margin-bottom: 50px;

            text-transform: uppercase;

        }


        .card-block {

            font-size: 1em;

            position: relative;

            margin: 0;

            padding: 1em;

            border: none;

            border-top: 1px solid rgba(34, 36, 38, .1);

            box-shadow: none;


        }

        .card {

            font-size: 1em;

            overflow: hidden;

            padding: 5;

            border: none;

            border-radius: .28571429rem;

            box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;

            margin-top: 20px;

        }


        .carousel-indicators li {

            border-radius: 12px;

            width: 12px;

            height: 12px;

            background-color: #404040;

        }

        .carousel-indicators li {

            border-radius: 12px;

            width: 12px;

            height: 12px;

            background-color: #404040;

        }

        .carousel-indicators .active {

            background-color: white;

            max-width: 12px;

            margin: 0 3px;

            height: 12px;

        }

        .carousel-control-prev-icon {

            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E") !important;

        }


        .carousel-control-next-icon {

            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E") !important;

        }

        lex-direction: column

        ;

        }


        .btn {

            margin-top: auto;

        }

        .card-title {

            font-size: 24px;

            font-weight: 800;

            text-align: center;

        }

        .card-text {

            font-size: large;

        }

    </style>

    <!-- Meta Tags For Seo + Page Optimization -->

    <meta charset="utf-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="title" content="<?php echo e(!empty($meta->title) ? $meta->title : ''); ?>">
    <meta name="description" content="<?php echo e(!empty($meta->body) ? $meta->body : ''); ?>">
    <meta name="keywords" content="<?php echo e(!empty($meta->key_words) ? $meta->key_words : ''); ?>">


    <!-- Insert Favicon Here -->

    <link href="<?php echo e(asset('images/favicon.png')); ?>" rel="icon">


    <!-- Page Title(Name)-->

    <title>LTF</title>


    <!-- Bootstrap CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/bootstrap.css')); ?>">


    <!-- Font-Awesome CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-awesome.css')); ?>">


    <!-- Slider Revolution CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/settings.css')); ?>">


    <!--  Fancy Box CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.fancybox.css')); ?>">


    <!-- Circleful CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/jquery.circliful.css')); ?>">


    <!-- Animate CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/animate.css')); ?>">


    <!-- Cube Portfolio CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/cubeportfolio.min.css')); ?>">


    <!-- Owl Carousel CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/owl.carousel.min.css')); ?>">

    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <!-- Swiper CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/swiper.min.css')); ?>">


    <!-- Custom Style CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/style.css')); ?>">


    <!-- Crypto Currency CSS File -->

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/cryptocurrency-assets/css/style.css')); ?>">


    <!-- Color StyleSheet CSS File -->

    <link href="<?php echo e(asset('Frontend/css/yellow.css')); ?>" rel="stylesheet" id="color" type="text/css">

    <link rel="stylesheet" href="<?php echo e(asset('Frontend/media/itb/itb_layout/itb_layout_styles/project_itb.css')); ?>"
          media="screen">


    <link rel="stylesheet" href="<?php echo e(asset('Frontend/css/font-six.css')); ?>">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="http://maps.google.com/maps-api-v3/api/js/40/7/map.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/marker.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/infowindow.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/stats.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/onion.js"></script>
    <style type="text/css">@-webkit-keyframes _gm7906 {

                               0% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                               50% {
                                   -webkit-transform: translate3d(0px, -20px, 0);
                                   -webkit-animation-timing-function: ease-in;
                               }

                               100% {
                                   -webkit-transform: translate3d(0px, 0px, 0);
                                   -webkit-animation-timing-function: ease-out;
                               }

                           }

    </style>
    <script type="text/javascript" charset="UTF-8"
            src="http://maps.google.com/maps-api-v3/api/js/40/7/controls.js"></script>
</head>


<body>


<!-- Loader -->


<!-- Loader -->


<div class="loader" style="display: none;">


    <div class="loader-inner">


        <div class="spinner">


            <div class="dot1"></div>


            <div class="dot2"></div>


        </div>


    </div>


</div>

<!-- Parent Section -->


<section class="page_content_parent_section">


    <!-- Header Section -->


    <?php echo $__env->make('nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</section>


<section class="contact_form_section" id="contact-form">


    <div class="container">


        <div class="row">


            <div class="contact_form_inner big_padding clearfix">

                <div class="content-blog">

                    <!--<h1>Posts</h1>-->


                </div>

                <div class="container py-3">

                    <div class="title h1 text-center"></div>

                <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <!-- Card Start -->

                        <div class="card">

                            <div class="row ">


                                <div class="col-md-7 px-3">

                                    <div class="card-block px-6">

                                        <h4 class="card-title wow fadeInDown"
                                            data-wow-delay="0.2s"><?php echo $article->title; ?></h4>

                                        <p class="card-text wow fadeInUp"
                                           data-wow-delay="0.2s"><?php echo substr(strip_tags($article->description),0,800); ?></p>

                                        <br>

                                        <a href="<?php echo e(url('news/'.$article->id )); ?>" class="mt-auto btn btn-primary">Read
                                            More</a>

                                    </div>

                                </div>

                                <!-- img start -->

                                <div class="col-md-5">

                                    <img class="d-block wow fadeInDown" data-wow-delay="0.3s"
                                         src="<?php echo e(asset('uploads/articles/' .$article->images)); ?>" alt="" width="500">

                                </div>

                                <!-- End of img -->

                            </div>

                        </div>

                        <!-- End of card -->

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    <div class="col-md-12 col-sm-6 col-xs-12">

                        <ul class="pagination pagination-lg">

                            <li> <?php echo e($articles->render()); ?> </li>

                        </ul>

                    </div>


                    <!--<div class="card">-->

                    <!--  <div class="row ">-->


                    <!--    <div class="col-md-7 px-3">-->

                    <!--      <div class="card-block px-6">-->

                    <!--        <h4 class="card-title">Eight things the hotel industry can do to aid revenue recovery</h4>-->

                    <!--        <p class="card-text">Hotels have been attempting to mitigate lowering the rates and putting new measures in place with an aim to revive the hotel industry and contribute to revenue recovery amid the coronavirus outbrea Hotels have been attempting to mitigate lowering the rates and putting new measures in place with an aim to revive the hotel industry and contribute to revenue recovery amid the coronavirus outbrea...</p>-->


                <!--        <a href="<?php echo e(url('news')); ?>" class="mt-auto btn btn-primary  ">Read More</a>-->

                    <!--      </div>-->

                    <!--    </div>-->

                    <!-- Carousel start -->

                    <!--    <div class="col-md-5">-->

                    <!--      <div id="CarouselTest" class="carousel slide" data-ride="carousel">-->

                    <!--        <ol class="carousel-indicators">-->

                    <!--          <li data-target="#CarouselTest" data-slide-to="0" class=""></li>-->

                    <!--          <li data-target="#CarouselTest" data-slide-to="1"></li>-->

                    <!--          <li data-target="#CarouselTest" data-slide-to="2"></li>-->


                    <!--        </ol>-->

                    <!--        <div class="carousel-inner">-->

                    <!--          <div class="carousel-item active">-->

                    <!--            <img class="d-block" src="https://picsum.photos/450/300?image=1072" alt="">-->

                    <!--          </div>-->


                    <!--          <a class="carousel-control-prev" href="#CarouselTest" role="button" data-slide="prev">-->

                    <!--  <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->

                    <!--  <span class="sr-only">Previous</span>-->

                    <!--</a>-->

                    <!--          <a class="carousel-control-next" href="#CarouselTest" role="button" data-slide="next">-->

                    <!--  <span class="carousel-control-next-icon" aria-hidden="true"></span>-->

                    <!--  <span class="sr-only">Next</span>-->

                    <!--</a>-->

                    <!--        </div>-->

                    <!--      </div>-->

                    <!--    </div>-->

                    <!-- End of carousel -->

                    <!--  </div>-->

                    <!--</div>-->

                    <!-- End of card -->


                </div>


                <div class="container">


                </div>


                <br>

                <br>


            </div>

        </div>

    </div>

</section>


<!-- /Parent Section Ended -->


<!-- jQuery 2.2.0-->


<script src="js/jquery.js"></script>


<!-- Google Map Api -->

<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"
        type="text/javascript"></script>

<script src="<?php echo e(asset('Frontend/js/map.js')); ?>" type="text/javascript"></script>


<!-- REVOLUTION JS FILES -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.tools.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/jquery.themepunch.revolution.min.js')); ?>"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/actions.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/carousel.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/kenburn.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/layeranimation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/migration.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/navigation.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/slideanims.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('Frontend/js/video.min.js')); ?>"></script>


<!-- Bootstrap Core JavaScript -->

<script src="js/bootstrap.min.js"></script>


<!-- Owl Carousel 2 Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/owl.carousel.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.animate.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoheight.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autoplay.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.autorefresh.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.hash.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.lazyload.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.navigation.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.support.js')); ?>"></script>

<script src="<?php echo e(asset('Frontend/js/owl.video.js')); ?>"></script>


<!-- Fancy Box Javacript -->

<script src="<?php echo e(asset('Frontend/js/jquery.fancybox.js')); ?>"></script>


<!-- Wow Js -->

<script src="<?php echo e(asset('Frontend/js/wow.min.js')); ?>"></script>


<!-- Appear Js-->

<script src="<?php echo e(asset('Frontend/js/jquery.appear.js')); ?>"></script>


<!-- Countdown Js -->

<script src="<?php echo e(asset('Frontend/js/jquery.countdown.js')); ?>"></script>


<!-- Parallax Js -->

<script src="<?php echo e(asset('Frontend/js/parallax.min.js')); ?>"></script>


<!-- Particles Core Js -->

<script src="<?php echo e(asset('Frontend/js/particles.js')); ?>"></script>


<!-- Cube Portfolio Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.cubeportfolio.min.js')); ?>"></script>


<!-- Circliful Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/jquery.circliful.min.js')); ?>"></script>


<!-- Swiper Slider Core JavaScript -->

<script src="<?php echo e(asset('Frontend/js/swiper.min.js')); ?>"></script>


<!-- Crypto Currency Core Javascript -->

<script src="<?php echo e(asset('Frontend/cryptocurrency-assets/js/script.js')); ?>"></script>


<!-- Custom JavaScript -->

<script src="<?php echo e(asset('Frontend/js/script.js')); ?>"></script>


</body>
</html>
<?php /**PATH C:\xampp\htdocs\LTF\resources\views/outer-news.blade.php ENDPATH**/ ?>