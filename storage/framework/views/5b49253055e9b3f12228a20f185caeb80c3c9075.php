<?php echo strip_tags($header); ?>


<?php echo strip_tags($slot); ?>

<?php if(isset($subcopy)): ?>

<?php echo strip_tags($subcopy); ?>

<?php endif; ?>

<?php echo strip_tags($footer); ?>

<?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/vendor/mail/text/layout.blade.php ENDPATH**/ ?>