<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(trans('global.site_title')); ?></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/@coreui/coreui@2.1.16/dist/css/coreui.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
    <link href="<?php echo e(asset('css/custom.css')); ?>" rel="stylesheet" />
    <?php echo $__env->yieldContent('styles'); ?>
</head>
<style>
    .notification {
  position: fixed;
  top: 0.5rem;
  right: 1rem;
  /* align the content to the rigth */
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  color: hsl(0, 0%, 20%);
  /* hide the overflow to conceal the message when translated to the right of the container */
  overflow-x: hidden;
  /* padding to avoid cropping the box-shadow on the message */
  padding: 0.25rem;
}

/* reduce the size of the icon and give a white fill to the elements of the graphic */
.notification__bell {
  display: block;
  width: 25px;
  height: auto;
  fill: hsl(0, 0%, 100%);
}
/* when the .notification container is given a class of .received animate the body and clapper of the bell to swing
! animate the clapper with a slight delay as to follow through the animation of the main body
*/
.notification.received .bell__body {
  animation: swingBody 0.7s 0.02s cubic-bezier(0.455, 0.03, 0.515, 0.955);
}
.notification.received .bell__clapper {
  animation: swingClapper 0.7s 0.04s cubic-bezier(0.455, 0.03, 0.515, 0.955);
}
/* ! for both elements the rotation occurs using the parent group element as a hinge, allowing for the pendulum-like swing */
@keyframes  swingBody {
  25% {
    transform: rotate(-5deg);
  }
  75% {
    transform: rotate(5deg);
  }
}
/* animation for the clapper, following the main body and with a larger rotation */
@keyframes  swingClapper {
  5% {
    transform: rotate(0deg);
  }
  30% {
    transform: rotate(-8deg);
  }
  80% {
    transform: rotate(8deg);
  }
}

/* style the content of the message to show a grid with the dismiss button in the top right corner
|h1  |  button |
|p   |p        |
*/
.notification__message {
  display: grid;
  grid-gap: 0.2rem;
  grid-template-columns: 1fr auto;
  padding: 0.5rem 1rem;
  margin: 1rem 0;
  /* style the div as a rounded rectangle with a border on the left segment */
  background: hsl(0, 0%, 100%);
  border-radius: 10px;
  box-shadow: 0 0 5px hsla(0, 0%, 0%, 0.1), 0 2px 3px hsla(0, 0%, 0%, 0.1);
  border-left: 0.5rem solid hsl(0, 0%, 100%);
  /* by default hide the element from sight and include a transition for the chosen properties */
  transform: translateX(100%);
  opacity: 0;
  visibility: hidden;
  transition-property: transform, opacity, visibility;
  transition-duration: 0.7s;
  transition-timing-function: cubic-bezier(0.445, 0.05, 0.55, 0.95);
}
.notification__message h1 {
  font-size: 1.1rem;
  font-weight: 500;
  text-transform: capitalize;
}
.notification__message p {
  max-width: 320px;
  font-size: 0.8rem;
  grid-column: 1/-1;
}
.notification__message button {
  background: none;
  border: none;
  color: inherit;
  width: 20px;
  height: 20px;
  grid-column: 2/3;
  grid-row: 1/2;
  align-self: center;
}
.notification__message button svg {
  display: block;
  width: 100%;
  height: 100%;
}

/* when the .notification container has a class of .received transition the message into view */
.notification.received .notification__message {
  transform: translateX(0%);
  opacity: 1;
  visibility: visible;
}
/* change the border color according to the different modifiers
! for each modifier specify also an svg icon in the background, to highlight the message
*/
.message--info {
  border-left-color: #90dee9;
  background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><circle stroke="none" fill="%2390dee9" cx="50" cy="22" r="8"></circle><path fill="none" stroke="%2390dee9" stroke-width="12" stroke-linejoin="round" stroke-linecap="round" d="M 45 40 h 5 v 40 h -5 h 10"></path></svg>'),
    hsl(0, 0%, 100%);
  background-repeat: no-repeat;
  background-size: 35px;
  background-position: 100% 100%;
}
.message--success {
  border-left-color: hsl(120, 67%, 74%);
  background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path fill="none" stroke="%2390e990" stroke-width="12" stroke-linejoin="round" stroke-linecap="round" d="M 20 52 l 25 25 l 30 -50"></path></svg>'),
    hsl(0, 0%, 100%);
  background-repeat: no-repeat;
  background-size: 35px;
  background-position: 100% 100%;
}
.message--warning {
  border-left-color: hsl(54, 67%, 74%);
  background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path fill="none" stroke="%23e9e090" stroke-width="12" stroke-linejoin="round" stroke-linecap="round" d="M 50 18 v 40"></path><circle stroke="none" fill="%23e9e090" cx="50" cy="78" r="8"></circle></svg>'),
    hsl(0, 0%, 100%);
  background-repeat: no-repeat;
  background-size: 35px;
  background-position: 100% 100%;
}
.message--danger {
  border-left-color: hsl(0, 67%, 74%);
  background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><g transform="translate(50 50) rotate(45)"><path fill="none" stroke="%23e99090" stroke-width="12" stroke-linejoin="round" stroke-linecap="round" d="M 0 -30 v 60 z M -30 0 h 60"></path></g></svg>'),
    hsl(0, 0%, 100%);
  background-repeat: no-repeat;
  background-size: 35px;
  background-position: 100% 100%;
}

</style>
<body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done sidebar-lg-show">
    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <span class="navbar-brand-full">LTF</span>
            <span class="navbar-brand-minimized">P</span>
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="nav navbar-nav ml-auto">
            <?php if(count(config('panel.available_languages', [])) > 1): ?>
                <li class="nav-item dropdown d-md-down-none">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <?php echo e(strtoupper(app()->getLocale())); ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <?php $__currentLoopData = config('panel.available_languages'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $langLocale => $langName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a class="dropdown-item" href="<?php echo e(url()->current()); ?>?change_language=<?php echo e($langLocale); ?>"><?php echo e(strtoupper($langLocale)); ?> (<?php echo e($langName); ?>)</a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </li>
            <?php endif; ?>
        </ul>
        <div class="notification">
    <!-- the viewBox is 79x79 due to
        - the size of the graphic, 65x75
        - the stroke width, 4x4
        - the empty space included on either side of the graphic to avoid cropping when rotating the bell's shape 10x0
    -->
    <svg class="notification__bell" viewBox="0 0 79 79" width="79" height="79">
        <!-- group to apply default styles -->
        <g
            stroke="currentColor"
            stroke-width="4">
            <!-- group to translate the graphic enough to avoid cropping of the stroke -->
            <g transform="translate(2 2)">
                <!-- group to center the elements in the 75x75 box allocated to the bell
                ! the group also modifies the transform-origin for the nested elements, which will be rotated from the center of the graphic
                -->
                <g transform="translate(37.5 0)">
                    <circle
                        cx="0"
                        cy="8"
                        r="8">
                    </circle>
                    <!-- circle animated alongside the bell
                    ! the rotation occurs from the point described by the parent group
                    -->
                    <circle
                        class="bell__clapper"
                        cx="0"
                        cy="63"
                        r="12">
                    </circle>
                    <!-- bell shape
                    ! the rotation occurs from the point described by the parent group
                    -->
                    <path
                        class="bell__body"
                        stroke-linejoin="round"
                        d="M 0 8 a 25 25 0 0 1 25 25 v 17 l 5 6 q 3 7 -6 7 h -48 q -9 0 -6 -7 l 5 -6 v -17 a 25 25 0 0 1 25 -25">
                    </path>
                </g>
            </g>
        </g>
    </svg>

    <!-- message displaying the notification
    include modifiers to change the style of the message
    message--info
    message--success
    message--warning
    message--danger

    ! add a button to dismiss the message to the side
    -->
    <div class="notification__message message--info">
        <h1>Info</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing.</p>

        <!-- x icon through a path element -->
        <button aria-labelledby="button-dismiss">
            <span id="button-dismiss" hidden>Dismiss</span>
            <svg viewBox="0 0 100 100" width="10" height="10">
                <!-- group to style the path -->
                <g
                    stroke="currentColor"
                    stroke-width="6"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    fill="none">
                    <!-- group to center and rotate the + sign to show an x sign instead -->
                    <g transform="translate(50 50) rotate(45)">
                        <!-- path describing two perpendicular lines -->
                        <path
                            d="M 0 -30 v 60 z M -30 0 h 60">
                        </path>
                    </g>
                </g>
            </svg>
        </button>
    </div>
</div>
    </header>

    <div class="app-body">
        <?php echo $__env->make('partials.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <main class="main">


            <div style="padding-top: 20px" class="container-fluid">

                <?php echo $__env->yieldContent('content'); ?>

            </div>


        </main>
        <form id="logoutform" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo e(csrf_field()); ?>
        </form>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://unpkg.com/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script src="<?php echo e(asset('js/main.js')); ?>"></script>
    <script>
        $(function() {
  let copyButtonTrans = '<?php echo e(trans('global.datatables.copy')); ?>'
  let csvButtonTrans = '<?php echo e(trans('global.datatables.csv')); ?>'
  let excelButtonTrans = '<?php echo e(trans('global.datatables.excel')); ?>'
  let pdfButtonTrans = '<?php echo e(trans('global.datatables.pdf')); ?>'
  let printButtonTrans = '<?php echo e(trans('global.datatables.print')); ?>'
  let colvisButtonTrans = '<?php echo e(trans('global.datatables.colvis')); ?>'

  let languages = {
    'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
  };

  $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })
  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      url: languages.<?php echo e(app()->getLocale()); ?>
    },
    columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
    }, {
        orderable: false,
        searchable: false,
        targets: -1
    }],
    select: {
      style:    'multi+shift',
      selector: 'td:first-child'
    },
    order: [],
    scrollX: true,
    pageLength: 100,
    dom: 'lBfrtip<"actions">',
    buttons: [
      {
        extend: 'copy',
        className: 'btn-default',
        text: copyButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'csv',
        className: 'btn-default',
        text: csvButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        className: 'btn-default',
        text: excelButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        className: 'btn-default',
        text: pdfButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        className: 'btn-default',
        text: printButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'colvis',
        className: 'btn-default',
        text: colvisButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      }
    ]
  });

  $.fn.dataTable.ext.classes.sPageButton = '';
});

    </script>
    <script>
        // utility function returning a random item from the input array
const randomItem = arr => arr[Math.floor(Math.random() * arr.length)];

// possible values for the message title and modifier
const messageTitle = [
  'info',
  'success',
  'warning',
  'danger',
];

// possible values for the body of the message
// end result of the emmet shortcut p*10>lorem10
const messageText = [
  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem, quaerat.',
  'Ab asperiores inventore tempora maiores, est et magni harum maxime?',
  'Laboriosam, vel maxime. Doloremque saepe aut quis mollitia corporis illo?',
  'Cum eum magnam facere commodi quae voluptate suscipit doloribus architecto?',
  'Ipsa veniam tempora necessitatibus corporis voluptate nobis, ut quam magni.',
  'Veritatis obcaecati non dolorum vero? Ipsam aperiam optio sint dicta.',
  'Itaque quod amet a. Voluptate nostrum temporibus ipsa explicabo exercitationem.',
  'Quasi veritatis inventore mollitia ipsum, aut voluptatibus suscipit a labore.',
  'Iusto alias eius quae ducimus quibusdam veniam sint soluta nam!',
  'Corrupti temporibus sequi laboriosam alias magni? Nam consectetur amet odit!'
];

/* logic
- create a message
- show the message
- allow to dismiss the message through the dismiss button

once the message is dismissed the idea is to go through the loop one more time, with a different title and text values
*/
const notification = document.querySelector('.notification');

// function called when the button to dismiss the message is clicked
function dismissMessage() {
  // remove the .received class from the .notification widget
  notification.classList.remove('received');

  // call the generateMessage function to show another message after a brief delay
  generateMessage();
}

// function showing the message
function showMessage() {
  // add a class of .received to the .notification container
  notification.classList.add('received');

  // attach an event listener on the button to dismiss the message
  // include the once flag to have the button register the click only one time
  const button = document.querySelector('.notification__message button');
  button.addEventListener('click', dismissMessage, { once: true });
}

// function generating a message with a random title and text
function generateMessage() {
  // after an arbitrary and brief delay create the message and call the function to show the element
  const delay = Math.floor(Math.random() * 1000) + 1500;
  const timeoutID = setTimeout(() => {
    // retrieve a random value from the two arrays
    const title = randomItem(messageTitle);
    const text = randomItem(messageText);

    // update the message with the random values and changing the class name to the title's option
    const message = document.querySelector('.notification__message');

    message.querySelector('h1').textContent = title;
    message.querySelector('p').textContent = text;
    message.className = `notification__message message--${title}`;

    // call the function to show the message
    showMessage();
    clearTimeout(timeoutID);
  }, delay);
}

// immediately call the generateMessage function to kickstart the loop
generateMessage();

    </script>
    <?php echo $__env->yieldContent('scripts'); ?>
</body>

</html>
<?php /**PATH D:\laaast-ltf\resources\views/layouts/admin.blade.php ENDPATH**/ ?>