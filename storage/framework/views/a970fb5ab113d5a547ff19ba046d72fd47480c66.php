<div class="sidebar">
    <nav class="sidebar-nav ps ps--active-y">


        <ul class="nav">
            <li class="nav-item">
                <a href="<?php echo e(route("admin.dashboard.index")); ?>" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    <?php echo e(trans('global.dashboard')); ?>

                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo e(route("admin.edit.profile")); ?>" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    <?php echo e('Edit Profile'); ?>

                </a>
            </li>

            <?php if(Auth::user()->status == 0): ?>


                <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle">
                        <i class="fas fa-users nav-icon">

                        </i>
                        <?php echo e(trans('global.userManagement.title')); ?>

                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.permissions.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : ''); ?>">
                                <i class="fas fa-unlock-alt nav-icon">

                                </i>
                                <?php echo e(trans('global.permission.title')); ?>

                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.roles.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : ''); ?>">
                                <i class="fas fa-briefcase nav-icon">

                                </i>
                                <?php echo e(trans('global.role.title')); ?>

                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.users.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : ''); ?>">
                                <i class="fas fa-user nav-icon">

                                </i>
                                <?php echo e(trans('global.user.title')); ?>

                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.leaders.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/leaders') || request()->is('admin/leaders/*') ? 'active' : ''); ?>">
                                <i class="fas fa-user-friends nav-icon">

                                </i>
                                <?php echo e(trans('global.leaders.title')); ?>

                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.tourguides.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/tourguides') || request()->is('admin/tourguides/*') ? 'active' : ''); ?>">
                                <i class="fas fa-user-friends nav-icon">

                                </i>
                                <?php echo e(trans('global.tourguides.title')); ?>

                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.visitors.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/visitors') || request()->is('admin/visitors/*') ? 'active' : ''); ?>">
                                <i class="fas fa-users nav-icon">

                                </i>
                                <?php echo e(trans('global.visitor.title')); ?>

                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#"
                               class="nav-link ">
                                <i class="fas fa-users nav-icon">

                                </i>
                                <?php echo e(trans('global.trad_visitor.title')); ?>

                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.exhibitors.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/exhibitors') || request()->is('admin/exhibitors/*') ? 'active' : ''); ?>">
                                <i class="fas fa-home nav-icon">

                                </i>
                                <?php echo e(trans('global.exhibitor.title')); ?>

                            </a>
                        </li>


                    </ul>
                </li>







             <li class="nav-item">
                <a href="<?php echo e(route('admin.searchs.index')); ?>" class="nav-link"
                   >
                    <i class="nav-icon fas fa-search">

                    </i>
                    <?php echo e(trans('global.search.title')); ?>

                </a>
            </li>



                <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle">
                        <i class="fas fa-user-cog nav-icon">

                        </i>
                        <?php echo e(trans('global.setting.title')); ?>

                    </a>
                    <ul class="nav-dropdown-items">

                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.abouts.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/abouts') || request()->is('admin/abouts/*') ? 'active' : ''); ?>">
                                <i class="fas fa-address-card nav-icon">

                                </i>
                                <?php echo e(trans('global.about.title')); ?>

                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.articles.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/articles') || request()->is('admin/articles/*') ? 'active' : ''); ?>">
                                <i class="fas fa-edit nav-icon">

                                </i>
                                <?php echo e(trans('global.news.title')); ?>

                            </a>
                        </li>


                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.metas.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/metas') || request()->is('admin/metas/*') ? 'active' : ''); ?>">
                                <i class="fas fa-smile-wink nav-icon">

                                </i>
                                Meta
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo e(route("admin.sliders.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/sliders') || request()->is('admin/sliders/*') ? 'active' : ''); ?>">
                                <i class="fas fa-smile-wink nav-icon">
                                </i>
                                <?php echo app('translator')->get('global.sliders.title'); ?>
                            </a>
                        </li>
                        
                        
                        
                <li class="nav-item">
                    <a href="<?php echo e(route("admin.librarys.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/librarys') || request()->is('admin/librarys/*') ? 'active' : ''); ?>">
                        <i class="fas fa-book-open nav-icon">

                        </i>
                        <?php echo e(trans('global.librarys.title')); ?>

                    </a>
                </li>
                
                    </ul>
                </li>
                
                            
            <li class="nav-item">
                <a href="<?php echo e(route('admin.favourites.index')); ?>" class="nav-link"
                >
                    <i class="nav-icon fas fa-heart">

                    </i>
                    <?php echo e(trans('global.favourites.title')); ?>

                </a>
            </li>

                <li class="nav-item">
                    <a href="<?php echo e(route("admin.contactus.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/contactus') || request()->is('admin/contactus/*') ? 'active' : ''); ?>">
                        <i class="fas fa-phone nav-icon">

                        </i>
                        <?php echo e(trans('global.contactus.title')); ?>

                    </a>
                </li>




                <li class="nav-item">
                    <a href="<?php echo e(route("admin.offers.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/offers') || request()->is('admin/offers/*') ? 'active' : ''); ?>">
                        <i class="fas fa-box nav-icon">

                        </i>
                        <?php echo e(trans('global.offers.title')); ?>

                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo e(route("admin.myoffers.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : ''); ?>">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>


       <li class="nav-item">
                <a href="<?php echo e(route("admin.fans.index")); ?>"
                   class="nav-link <?php echo e(request()->is('admin/fans') || request()->is('admin/fans/*') ? 'active' : ''); ?>">
                    <i class="fas fa-bank nav-icon">

                    </i>
                    Trip Fam
                </a>
            </li>
            
            
             <li class="nav-item">
                            <a href="<?php echo e(route("admin.rates.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : ''); ?>">
                                <i class="fas fa-star nav-icon">

                                </i>
                                <?php echo e(trans('global.rates.title')); ?>

                            </a>
                        </li>
                        
                        
            
            <?php endif; ?>


            <!--<li class="nav-item">-->
            <!--    <a href="#" class="nav-link"-->
            <!--      >-->
            <!--        <i class="nav-icon fas fa-sign-out-alt">-->

            <!--        </i>-->
            <!--        Appointment-->
            <!--    </a>-->
            <!--</li>-->


            <!--<li class="nav-item nav-dropdown">-->
            <!--    <a class="nav-link  nav-dropdown-toggle">-->
            <!--        <i class="fas fa-star nav-icon">-->

            <!--        </i>-->
            <!--        <?php echo e(trans('global.rates.title')); ?>-->
            <!--    </a>-->
            <!--    <ul class="nav-dropdown-items">-->
            <!--        <li class="nav-item">-->
            <!--            <a href="<?php echo e(route("admin.rates.index")); ?>"-->
            <!--               class="nav-link <?php echo e(request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : ''); ?>">-->
            <!--                <i class="fas fa-star nav-icon">-->

            <!--                </i>-->
            <!--                <?php echo e(trans('global.rates.title')); ?>-->
            <!--            </a>-->
            <!--        </li>-->
            <!--        <li class="nav-item">-->
            <!--            <a href="<?php echo e(route("admin.forms.index")); ?>"-->
            <!--               class="nav-link <?php echo e(request()->is('admin/forms') || request()->is('admin/forms/*') ? 'active' : ''); ?>">-->
            <!--                <i class="fas fa-book nav-icon">-->

            <!--                </i>-->
            <!--                <?php echo e(trans('global.forms.title')); ?>-->
            <!--            </a>-->
            <!--        </li>-->
            <!--    </ul>-->
            <!--</li>-->


            <!-- <li class="nav-item">
                <a href="<?php echo e(route("admin.media.index")); ?>"
                   class="nav-link <?php echo e(request()->is('admin/media') || request()->is('admin/media/*') ? 'active' : ''); ?>">
                    <i class="fas fa-square nav-icon">

                    </i>
                    <?php echo e(trans('global.media.title')); ?>

                </a>
            </li> -->

     


            <?php if(Auth::user()->status == 1 ): ?>
     
     
     
     
                <li class="nav-item">
                    <a href="#" class="nav-link"
                      >
                        <i class="nav-icon fas fa-sign-out-alt">

                        </i>
                        Appointment
                    </a>
                </li>
                
                
             <li class="nav-item">
                <a href="<?php echo e(route('admin.searchs.index')); ?>" class="nav-link"
                   >
                    <i class="nav-icon fas fa-search">

                    </i>
                    <?php echo e(trans('global.search.title')); ?>

                </a>
            </li>


            
                <li class="nav-item">
                    <a href="<?php echo e(route("admin.myoffers.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : ''); ?>">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>
 
 
 
            <li class="nav-item">
                <a href="<?php echo e(route('admin.favourites.index')); ?>" class="nav-link"
                >
                    <i class="nav-icon fas fa-heart">

                    </i>
                    <?php echo e(trans('global.favourites.title')); ?>

                </a>
            </li>



             <li class="nav-item">
                            <a href="<?php echo e(route("admin.rates.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : ''); ?>">
                                <i class="fas fa-star nav-icon">

                                </i>
                                <?php echo e(trans('global.rates.title')); ?>

                            </a>
                        </li>
                        
                        
            <?php endif; ?>



  <?php if(Auth::user()->status == 2): ?>

                <li class="nav-item">
                    <a href="<?php echo e(route("admin.offers.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/offers') || request()->is('admin/offers/*') ? 'active' : ''); ?>">
                        <i class="fas fa-box nav-icon">

                        </i>
                        <?php echo e(trans('global.offers.title')); ?>

                    </a>
                </li>

<?php endif; ?>

  <?php if(Auth::user()->status == 3 || Auth::user()->status == 5): ?>
     
     
     
     
                <li class="nav-item">
                    <a href="#" class="nav-link"
                       >
                        <i class="nav-icon fas fa-sign-out-alt">

                        </i>
                        Appointment
                    </a>
                </li>
                
                
             <li class="nav-item">
                <a href="<?php echo e(route('admin.searchs.index')); ?>" class="nav-link"
                   >
                    <i class="nav-icon fas fa-search">

                    </i>
                    <?php echo e(trans('global.search.title')); ?>

                </a>
            </li>


            
                <li class="nav-item">
                    <a href="<?php echo e(route("admin.myoffers.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : ''); ?>">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>
 
 
 
            <li class="nav-item">
                <a href="<?php echo e(route('admin.favourites.index')); ?>" class="nav-link"
                >
                    <i class="nav-icon fas fa-heart">

                    </i>
                    <?php echo e(trans('global.favourites.title')); ?>

                </a>
            </li>



             <li class="nav-item">
                            <a href="<?php echo e(route("admin.rates.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : ''); ?>">
                                <i class="fas fa-star nav-icon">

                                </i>
                                <?php echo e(trans('global.rates.title')); ?>

                            </a>
                        </li>
                        
                        
            <?php endif; ?>




     <?php if(Auth::user()->status == 4 ): ?>
     
     
     
     
     
                <li class="nav-item">
                    <a href="#" class="nav-link"
                      >
                        <i class="nav-icon fas fa-sign-out-alt">

                        </i>
                        Appointment
                    </a>
                </li>
                
                
             <li class="nav-item">
                <a href="<?php echo e(route('admin.searchs.index')); ?>" class="nav-link"
                   >
                    <i class="nav-icon fas fa-search">

                    </i>
                    <?php echo e(trans('global.search.title')); ?>

                </a>
            </li>
                
                   
                   
                   
                <li class="nav-item">
                    <a href="<?php echo e(route("admin.myoffers.index")); ?>"
                       class="nav-link <?php echo e(request()->is('admin/myoffers') || request()->is('admin/myoffers/*') ? 'active' : ''); ?>">
                        <i class="fas fa-grin-stars nav-icon">

                        </i>
                        My Offer
                    </a>
                </li>

         
            <li class="nav-item">
                <a href="<?php echo e(route('admin.favourites.index')); ?>" class="nav-link"
                >
                    <i class="nav-icon fas fa-heart">

                    </i>
                    <?php echo e(trans('global.favourites.title')); ?>

                </a>
            </li>


             <li class="nav-item">
                            <a href="<?php echo e(route("admin.rates.index")); ?>"
                               class="nav-link <?php echo e(request()->is('admin/rates') || request()->is('admin/rates/*') ? 'active' : ''); ?>">
                                <i class="fas fa-star nav-icon">

                                </i>
                                <?php echo e(trans('global.rates.title')); ?>

                            </a>
                        </li>

     
                
                          

                
                
     <?php endif; ?>

  

            <li class="nav-item">
                <a href="#" class="nav-link"
                   onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    <?php echo e(trans('global.logout')); ?>

                </a>
            </li>


        </ul>

        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
<?php /**PATH /home/yassers1/public_html/last-ltf/resources/views/partials/menu.blade.php ENDPATH**/ ?>