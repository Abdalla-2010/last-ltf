<?php

//facebook login
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::group(['middleware' => 'guest'], function () {
    Route::get('register', 'FrontendController@register'); //don't know what is this
    Route::post('register', 'FrontendController@StoreRegister');// don't know what is this

    Route::get('createHere', 'FrontendController@createHere'); // step one for all types

    Route::get('registerfront', 'FrontendController@registerfront'); //for public user register by facebook

    Route::get('trade-register', 'FrontendController@trade_register');          //step 1 for trade visitor register
    Route::get('groupleader-register', 'FrontendController@groupleader_register'); //step 1 for groupleader register
    Route::get('register-exhibitor', 'FrontendController@register_exhibitor');  //step 1 for exhibitor register
    Route::get('register-tourguide', 'FrontendController@register_tourguide');  //step 1 for tourguide register

    Route::post('company-info', 'FrontendController@register2')->name('register2'); // step 2 for all types
    Route::post('individual-profile', 'FrontendController@register3')->name('register3');// step 3 for all types
    Route::post('users_login', 'FrontendController@individual_profile')->name('store.user'); // store step

});

Auth::routes(['register' => false]);


Route::get('/', 'FrontendController@index');
Route::get('exhibitor', 'FrontendController@exhibitor');
Route::get('visitor', 'FrontendController@visitor');
Route::get('library', 'FrontendController@library');
Route::get('privacy', 'FrontendController@privacy');
Route::get('tourguide', 'FrontendController@tourguide');

Route::get('termsandcondition', 'FrontendController@termsandcondition');

//Route::post('company-info', 'FrontendController@company_info');

Route::get('tripfam', 'FrontendController@tripfams');
Route::post('tripfam', 'FrontendController@tripfam');


Route::get('news/{id}', 'FrontendController@news');
Route::get('outer-news', 'FrontendController@outer_news');
Route::get('about', 'FrontendController@About');
Route::get('contact', 'FrontendController@contact');
Route::post('mail', 'FrontendController@contacts')->name('mail');
Route::get('sponsored', 'FrontendController@sponsored');
Route::get('destinationpartner', 'FrontendController@destinationpartner');


Route::redirect('/home', 'admin');


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {

    Route::get('/', 'DashboardController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');

    Route::delete('visitors/destroy', 'VisitorsController@massDestroy')->name('visitors.massDestroy');
    Route::resource('visitors', 'VisitorsController');

    Route::delete('exhibitors/destroy', 'ExhibitorsController@massDestroy')->name('exhibitors.massDestroy');
    Route::resource('exhibitors', 'ExhibitorsController')->only('index', 'show', 'destroy');

    Route::delete('tourguides/destroy', 'TourGuidesController@massDestroy')->name('tourguides.massDestroy');
    Route::resource('tourguides', 'TourGuidesController')->only('index', 'show', 'destroy');


    Route::delete('searchs/destroy', 'SearchController@massDestroy')->name('searchs.massDestroy');

    Route::resource('searchs', 'SearchController');

    Route::delete('rates/destroy', 'RatesController@massDestroy')->name('rates.massDestroy');

    Route::resource('rates', 'RatesController');

    Route::delete('forms/destroy', 'FormsController@massDestroy')->name('forms.massDestroy');

    Route::resource('forms', 'FormsController');

    Route::delete('offers/destroy', 'OffersController@massDestroy')->name('offers.massDestroy');
    Route::resource('offers', 'OffersController');


    Route::post('results', 'ResultsController@result')->name('results');
    Route::get('results/show/profile/{id}', 'ResultsController@showProfile')->name('results.show.profile');

    Route::delete('abouts/destroy', 'AboutController@massDestroy')->name('abouts.massDestroy');

    Route::resource('abouts', 'AboutController');

    Route::delete('slides/destroy', 'SlideController@massDestroy')->name('slides.massDestroy');

    Route::resource('slides', 'SlideController');

    Route::delete('leaders/destroy', 'LeaderController@massDestroy')->name('leaders.massDestroy');
    Route::resource('leaders', 'LeaderController')->only('index', 'show', 'destroy');

    Route::delete('librarys/destroy', 'LibraryController@massDestroy')->name('librarys.massDestroy');

    Route::resource('librarys', 'LibraryController');

    Route::delete('contactus/destroy', 'ContactUsController@massDestroy')->name('contactus.massDestroy');

    Route::resource('contactus', 'ContactUsController');

    Route::delete('media/destroy', 'MediaController@massDestroy')->name('media.massDestroy');

    Route::resource('media', 'MediaController');

    Route::delete('dashboard/destroy', 'DashboardController@massDestroy')->name('dashboard.massDestroy');

    Route::resource('dashboard', 'DashboardController');

    Route::delete('articles/destroy', 'ArticleController@massDestroy')->name('articles.massDestroy');

    Route::resource('articles', 'ArticleController');

    Route::delete('fans/destroy', 'FamController@massDestroy')->name('fans.massDestroy');

    Route::resource('fans', 'FamController');

    Route::delete('metas/destroy', 'MetaController@massDestroy')->name('metas.massDestroy');
    Route::resource('metas', 'MetaController');

    Route::resource('sliders', 'SlidersController')->only(['index', 'edit', 'update']);

    Route::delete('myoffers/destroy', 'MyOffersController@massDestroy')->name('myoffers.massDestroy');
    Route::resource('myoffers', 'MyOffersController');


    Route::delete('favourites', 'FavouritesController@massDestroy')->name('favourites.massDestroy');
    Route::get('favourites', 'FavouritesController@index')->name('favourites.index');
    Route::get('favourites/{id}', 'FavouritesController@show')->name('favourites.show');
    Route::post('favourites', 'FavouritesController@addFavourite')->name('favourites.add');
    Route::delete('favourites/destroy/{id}', 'FavouritesController@deleteFavourite')->name('favourites.delete');

    Route::get('edit/profile', 'ProfileController@edit')->name('edit.profile');
    Route::patch('edit/profile', 'ProfileController@update')->name('update.profile');
    Route::get('show/profile', 'ProfileController@show')->name('show.profile');
    Route::post('download/{id}', 'ProfileController@Download')->name('companyProfile.download');


});

