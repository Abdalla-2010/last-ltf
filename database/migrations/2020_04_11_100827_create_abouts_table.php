<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->longText('body')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        DB::table('abouts')->insert([
            'name' => 'about 0 name',
            'body' => 'about 0 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 1 name',
            'body' => 'about 1 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 2 name',
            'body' => 'about 2 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 3 name',
            'body' => 'about 3 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 4 name',
            'body' => 'about 4 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 5 name',
            'body' => 'about 5 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 6 name',
            'body' => 'about 6 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 7 name',
            'body' => 'about 7 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 8 name',
            'body' => 'about 8 body',
        ]);

        DB::table('abouts')->insert([
            'name' => 'about 9 name',
            'body' => 'about 9 body',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
