<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create1555355612782UsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->nullable();
            $table->string('title')->nullable();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('designation')->nullable();
            $table->string('password');
            $table->string('type')->nullable();



            $table->string('company')->nullable();
            $table->string('website')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instgram')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('country')->nullable();
            $table->string('company_email')->nullable();
            $table->text('company_type')->nullable();

            $table->text('imgPersonal')->nullable();
            $table->text('imgCompany')->nullable();

            $table->longText('travel')->nullable();
            $table->longText('languages')->nullable();
            $table->longText('destination')->nullable();

            $table->datetime('email_verified_at')->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('remember_token')->nullable();

            $table->tinyInteger('payment_status')->default(0);
            $table->text('youtubeLink')->nullable();
            $table->text('company_profile')->nullable();
            $table->text('brochure')->nullable();


            $table->timestamps();
            $table->softDeletes();
        });



    }



    public function down()
    {
        Schema::dropIfExists('users');
    }
}
