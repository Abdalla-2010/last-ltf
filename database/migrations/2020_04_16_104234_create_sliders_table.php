<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Text('image');
            $table->string('title');
            $table->longText('content');
            $table->timestamps();
        });

        DB::table('sliders')->insert([
            'image' => 'https://cvctravel.tech/system/public/Frontend/images/t82.webp',
            'title' => 'advfvea',
            'content' => 'advfavfv',
        ]);

        DB::table('sliders')->insert([
            'image' => 'https://cvctravel.tech/system/public/Frontend/images/t2.webp',
            'title' => 'advfvea',
            'content' => 'advfavfv',
        ]);

        DB::table('sliders')->insert([
            'image' => 'https://cvctravel.tech/system/public/Frontend/images/t5.webp',
            'title' => 'advfvea',
            'content' => 'advfavfv',
        ]);

        DB::table('sliders')->insert([
            'image' => 'https://cvctravel.tech/system/public/Frontend/images/t15.webp',
            'title' => 'advfvea',
            'content' => 'advfavfv',
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
