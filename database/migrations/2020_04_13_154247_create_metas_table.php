<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('group')->nullable();
            $table->string('title');
            $table->string('body')->nullable();
            $table->string('key_words');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('metas')->insert([
            'group' => 'Home Page',
            'title' => 'Home',
            'body' => 'Home',
            'key_words' => 'Home'
        ]);

        DB::table('metas')->insert([
            'group' => 'News Page',
            'title' => 'News',
            'body' => 'News',
            'key_words' => 'News'
        ]);

        DB::table('metas')->insert([
            'group' => 'E-Libaray Page',
            'title' => 'E-Libaray',
            'body' => 'E-Libaray',
            'key_words' => 'E-Libaray'
        ]);

        DB::table('metas')->insert([
            'group' => 'About Page',
            'title' => 'About',
            'body' => 'About',
            'key_words' => 'About'
        ]);

        DB::table('metas')->insert([
            'group' => 'Partners Page',
            'title' => 'Partners',
            'body' => 'Partners',
            'key_words' => 'Partners'
        ]);

        DB::table('metas')->insert([
            'group' => 'Contact us Page',
            'title' => 'Contact',
            'body' => 'Contact',
            'key_words' => 'Contact'
        ]);

        DB::table('metas')->insert([
            'group' => 'Login Page',
            'title' => 'Login',
            'body' => 'Login',
            'key_words' => 'Login'
        ]);

        DB::table('metas')->insert([
            'group' => 'Visitors Page',
            'title' => 'Visitors',
            'body' => 'Visitors',
            'key_words' => 'Visitors'
        ]);

        DB::table('metas')->insert([
            'group' => 'Exhibitors Page',
            'title' => 'Exhibitors',
            'body' => 'Exhibitors',
            'key_words' => 'Exhibitors'
        ]);

        DB::table('metas')->insert([
            'group' => 'Account Types Page',
            'title' => 'Account',
            'body' => 'Account',
            'key_words' => 'Account'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metas');
    }
}
