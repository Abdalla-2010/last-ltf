jQuery(function($) {
  init();

  var lang;
  var decimalSeparator;
  var thousandsSeparator;

  function init() {
    lang = document.querySelector('html').getAttribute('lang');
    if (lang === 'de') {
      decimalSeparator = ',';
      thousandsSeparator = '.';
    } else {
      decimalSeparator = '.';
      thousandsSeparator = ',';
    }

    setupOdometers();
  }

  function setupOdometers() {
    var animationTriggerThreshold = 10;

    var getNumberElements = function() {
      return $('.fact h4');
    };

    var numberElements = getNumberElements();
    for (var i = 0; i < numberElements.length; i++) {
      var numberElement = numberElements[i];
      var isDecimal = numberElement.textContent.match(/,/);
      numberElement.textContent = numberElement.textContent.trim().replace(/[,.]/, '');
      var stringComps = numberElement.textContent.match(/^([^0-9]*)([0-9]+)([^0-9]*)$/);
      if (!stringComps) { // element doesn't contain any number, skip it
        continue;
      }

      // Clear h3 content
      numberElement.innerHTML = '';

      var odometerElement;
      for (var j = 1; j < stringComps.length; j++) {
        var comp = stringComps[j];
        if (!comp) { // string component is empty, skip it
          continue;
        }

        if (isNaN(comp)) {
          var nonNumberComp = document.createElement('span');
          nonNumberComp.innerHTML = comp;
          numberElement.appendChild(nonNumberComp);

        } else {
          var numberComp = document.createElement('span');
          numberComp.classList.add("odometer");
          numberComp.innerHTML = comp;
          numberElement.appendChild(numberComp);
          odometerElement = numberComp;
        }
      }

      // Get starting and ending number values for the odometer animation
      var endingValue = odometerElement.textContent;
      // var startingValue = Math.pow(10, endingValue.length-1);

      // Setup Odometer
      var odometerOptions = {
        el: odometerElement,
        value: 0,
        format: '(' + thousandsSeparator + 'ddd)',
        theme: 'minimal'
      };

      if (isDecimal) {
        odometerOptions.format += decimalSeparator + 'd';
        endingValue /= 10;
      }

      var odometer = new Odometer(odometerOptions);

      $(numberElement).data('odometer-value', endingValue);
      $(numberElement).data('odometer', odometer);
    }

    var getNumbersInVisibleRect = function() {
      var nums = [];
      var numberElements = getNumberElements();
      for (var i = 0; i < numberElements.length; i++) {
        var $n = $(numberElements[i]);

        // Skip elements where the animation has already been run once
        if ($n.data('odometer-state') === 'finished') {
          continue;
        }

        var posBottom = $n.offset().top - window.pageYOffset - window.innerHeight + $n.height() + animationTriggerThreshold;
        var posTop = $n.offset().top - window.pageYOffset - animationTriggerThreshold;
        if (posBottom <= 0 && posTop >= 0) {
          nums.push($n);
        }
      }
      return nums;
    };

    var animateNumbersIfVisible = function() {
      var $numbers = getNumbersInVisibleRect();
      if ($numbers.length) {
        window.setTimeout(function() {
          var $numbers = getNumbersInVisibleRect();
          for (var i = 0; i < $numbers.length; i++) {
            var $number = $numbers[i];
            $number.data('odometer').update($number.data('odometer-value'));
            $number.data('odometer-state', 'finished');
            $number.parent().children().css('opacity', '1.0');
          }
        }, 300);
      }
    };

    // Add scroll listener
    window.addEventListener('scroll', animateNumbersIfVisible);
    animateNumbersIfVisible();
  }

});
