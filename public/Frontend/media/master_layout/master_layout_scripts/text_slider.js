var mobileBreak = 767;
var screenBreak = 1107;
var wideBreak = 1779;
var w, d, e, g, x, y;
var sliders = [];

function getClientValues() {
	w = window;
	d = document;
	e = d.documentElement;
	g = d.getElementsByTagName('body')[0];
	x = w.innerWidth || e.clientWidth || g.clientWidth;
	y = w.innerHeight || e.clientHeight|| g.clientHeight;
}

function registerSlider(id, params){
	params.rootId = id;
	sliders[id] = params;
	textSlider(params);
}

function textSlider(params) {
	// Text-Slider konfigurieren
	var newsVisible = 1;
	var maxNewsItems = 1;
	var newsVertical = false;

	if(params.maxVisible){
		newsVisible = params.maxVisible;
		maxNewsItems = params.maxVisible;
	}

	var hideSlider = false;

	if (x <= mobileBreak) { 
		if (params && params.hideMobile){
			hideSlider = true;
		}
	}	
	if (x > mobileBreak && x <= screenBreak) {
		if(!params.maxVisible){ 
			newsVertical = true; 
			maxNewsItems = 3;
		}
		if (params && params.hideTablet){
			hideSlider = true;
		}
	}
	if (x > screenBreak && x <= wideBreak) {
		if(!params.maxVisible){
			newsVisible = 3;
			maxNewsItems = 3;
		}
		if (params && params.hideScreen){
			hideSlider = true;
		}
	}
	if (x > wideBreak) {
		if(!params.maxVisible){
			newsVisible = 5;
			maxNewsItems = 5;
		}
		if (params && params.hideWide){
			hideSlider = true;
		}
	}
	
	if (hideSlider){
		// Text-Slider ausblenden
		$('#' + params.rootId).hide();
	} else {
		// Text-Slider einblenden
		$('#' + params.rootId).show();
		var jCaraouselParams = {
			wrap: 'circular'			
		};
		if(newsVertical){
			jCaraouselParams.vertical = newsVertical;
		}
		
		var jcarousel = $('#' + params.rootId + params.sliderSelector);
		jcarousel.on('jcarousel:reload jcarousel:create', function () {
				var width = jcarousel.innerWidth();
				jcarousel.jcarousel('items').css('width', (width/newsVisible)+'px');
			}).jcarousel(jCaraouselParams);
		if(params && params['autoplay'] === true){
			var interval = 500;
			if(params['interval']){
				interval = params['interval'];
			}
			jcarousel.jcarouselAutoscroll({
				interval: interval,
			    target: '+=1',
			    autostart: true
			});
		}
		if(params && params['randomStart'] === true){
			var itemCount =$('#' + params.rootId + params.itemSelector).length;
			var startVal = Math.floor(Math.random()* itemCount);

			jcarousel.jcarousel('scroll', startVal);
		}
	
		$('#' + params.rootId + params.prevButtonSelector).jcarouselControl({
			target: '-=1'
		});
	
		$('#' + params.rootId + params.nextButtonSelector).jcarouselControl({
			target: '+=1'
		});
			
		var countItems = $('#' + params.rootId + params.itemSelector).length;
		if (countItems <= maxNewsItems) {
			$('#' + params.rootId + params.prevButtonSelector).addClass(params.classButtonInactive);
			$('#' + params.rootId + params.nextButtonSelector).addClass(params.classButtonInactive);
		} else {
			$('#' + params.rootId + params.prevButtonSelector).removeClass(params.classButtonInactive);
			$('#' + params.rootId + params.nextButtonSelector).removeClass(params.classButtonInactive);
		}
	}
}

$(document).ready(function(){
    try {
		getClientValues();
		for(key in sliders){
			var params = sliders[key];
			textSlider(params);
		}
    } catch(e) {
        console.log(e);
    }
});

$(window).resize(function(){
    try {
		getClientValues();
        for(key in sliders){
			var params = sliders[key];
			textSlider(params);
		}
    } catch(e) {
        console.log(e);
    }
});