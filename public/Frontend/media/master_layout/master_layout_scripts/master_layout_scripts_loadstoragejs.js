var loadData = {
	/**
	 * Sets configuration parameters
	 */
    config: {
		widgetType: 'tab',
		widgetObject: $('.tab-navigation'),
		keyPrefix: '',
		loading: $('.widget-navigation-loading'),
		loadingStatus: $('.widget-loading-status'),
		loadingAnimation: $('.widget-navigation-loading .icon-loader-animation'),
		loadingHtml: 'Loading ...',
		loadingAnimationHtml: '<div class="icon-loader-animation-wrap"><div class="icon-loader-animation">Lädt ...</div></div>',
		errorHtml: 'no content available',
		singleView: false
	},

	/**
	 * Loads jQuery UI widgets, depending on configuration parameters
	 * @param configuration literal
	 */
    init: function(config) {
		// extends the configuration literal
		if (config && typeof(config) == 'object') $.extend(loadData.config, config);
		
		// hides widget container, activates loading text or animation
		loadData.config.widgetObject.hide();
		loadData.setLoadingContent();
		// tab navigation
		if (loadData.config.widgetType == 'tab') {
			// calls jQuery UI widget
			loadData.config.widgetObject.tabs({ 
				heightStyle: 'content',
				create: function(event, ui) {
					// shows loading text or animation
					if ($.trim($(this).html()) == '') {
						// displayes error message if no content is available
						loadData.config.loadingAnimation.hide();
						loadData.config.loading.empty().html(loadData.config.errorHtml);
					} else {
						// checks url for hash to find the initial tab
						var re = new RegExp('-tab1$');
						var tabNr = $(document.location.hash).selector.split("-tab")[1];
						if (isInt(tabNr)) re = new RegExp('-tab'+tabNr+'$');
						// loads data from web storage
						if (ui.panel.selector.match(re) != null) {
							loadData.loadContent(event, ui);
						}
					}
				},
				beforeActivate: function(event, ui) {
					// hides loading text or animation
					loadData.config.loadingStatus.hide();
					// loads data form web storage, except external content (which is loaded via AJAX)
					if (ui.newTab.context.className.indexOf('tab-externalpage') == -1) {
						loadData.loadContent(event, ui);
					}
				},
				beforeLoad: function(event, ui) {
					// shows loading text or animation
					loadData.setLoadingContent(loadData.config.loadingStatus);
					loadData.config.loadingStatus.show();
				},
				load: function(event, ui) {
					// calls external content via AJAX
					// uses the jQuery plugin for responsive pictures
					$('picture').picture();
					// uses the jQuery plugin to detect when images have been loaded
					$(this).imagesLoaded(
						function() {
							loadData.config.loadingStatus.delay(200).fadeOut(600);
							fixFooter();
						}
					);
				}
			});
		}

		// accordion navigation
		if (loadData.config.widgetType == 'accordion') {
			// calls jQuery UI widget
			loadData.config.widgetObject.accordion({ 
				header: '.accordion-navigation-link', 
				active: false, 
				collapsible: true, 
				heightStyle: 'content', 
				create: function(event, ui) {
					// hides loading text or animation, shows widget container
					loadData.config.loading.hide();
					$(this).show();
				},
				beforeActivate: function(event, ui) {
					// gets the HTML after user interaction
					if (ui.newHeader.length > 0) {
						if ($(ui.newPanel).hasClass('accordion-content-external')) {
							// loads external content via AJAX, uses method form master script
							loadAccordionAjaxContent($(ui.newPanel)); // from master_script.js
						} else {
							// loads data from web storage
							loadData.loadContent(event, ui);
						}
					}
				},
				activate: function(event, ui) {
					// adds behaviour after user interaction (scrolling, footer)
					if (ui.newHeader && ui.newHeader.attr('id')) {
						scrollToElement('#'+ui.newHeader.attr('id'));
					}
					fixFooter();
				} 
			});
			// adds initial scrolling behaviour 
			scrollToElement(document.location.hash);
		}
    },

	/**
	 * Sets loading text oder animation (css3)
	 * @param loadingObject html container for loading information
	 */
	setLoadingContent: function(loadingObject) {
		// fallback if parameter is missing
		if (loadingObject === undefined) loadingObject = loadData.config.loading;
		// set text as content
		var loadingContent = loadData.config.loadingHtml;
		// replaces text to animation if client is not an old Internet Explorer
		var ie = getInternetExplorerVersion();
		if (ie > -1) {
			if (ie > 9.0) loadingContent = loadData.config.loadingAnimationHtml;
		} else {
			loadingContent = loadData.config.loadingAnimationHtml;
		}
		loadingObject.empty().html(loadingContent);
	},
	
	/**
	 * Loads and fades in content from web storage
	 * @param event jQuery UI event
	 * @param event jQuery UI objects
	 */
	loadContent: function(event, ui) {
		// finds the new tab
		var newTabSelector = null;
		if (typeof ui.newPanel != 'undefined') {
			newTabSelector = '#'+ui.newPanel['0'].id;
		} else if (typeof ui.panel != 'undefined') {
			newTabSelector = '#'+ui.panel['0'].id;
		}

		// processes the content
		if (newTabSelector != null) {
			// gets the widget type (standard is tabs)
			var prefix = 'tab';
			if (event.type == 'accordionbeforeactivate') prefix = 'accordion';
			// gets the tab is (without hash)
			var newTabId = newTabSelector.substr(1);
			// gets the html elements (content container, status indication, progress indication)
			var contentElement = newTabSelector+' .'+prefix+'-navigation-content';
			var contentStatus = newTabSelector+' .'+prefix+'-navigation-status';
			var contentProgress = newTabSelector+' .'+prefix+'-navigation-progress';

			// fades in the content
			function fadeInContent() {
				// hides loading text or animation, shows widget container
				loadData.config.loading.hide();
				loadData.config.widgetObject.show();
				$(contentStatus).hide();
				$(contentElement).fadeIn(600, function() {

					fixFooter();
				});
			}

			// if there are no errors, image preloading (if necessary) leads to method fadeInContent()
			try {
				if ($.trim($(contentElement).html()) == '') {
					// gets the data from web storage
					var localStorageValue = localStorage.getItem(loadData.config.keyPrefix+newTabId);
					if (localStorageValue != '') {
						// escaped the html entities (opening and closing brackets)
						var localStorageHtml = localStorageValue.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
						// adds HTML to DOM, fades in the content after loading is completed
						$(contentElement).empty().html(localStorageHtml).promise().done(function(){

							// updates the progress bar, fades in when all images are loaded
							function imageLoaded() {
								progressBarValue += 1;
								addProgressBarValue = progressBar.val(progressBarValue);
								counter--;
								if (counter === 0) fadeInContent();
							}

							// gets all images and progress bar elements
							var images = $(contentElement+' img');
							var total = images.length, counter = images.length;
							var progressBar = $(contentProgress), progressBarMax = progressBar.attr('max'), progressBarValue = progressBar.val();

							// checks need for image reloading
							if (total > 0) {
								// starts an image preload and progress bar if loaded content contains images
								images.each(function() {
									if (this.complete) {
										imageLoaded.call(this);
									} else {
										$(this).one('load', imageLoaded);
										$(this).one('error', imageLoaded);
									}
								});
							} else {
								// fades in content without image preloading and progress bar
								$('picture').picture();
								fadeInContent();
							}
							domChanged(contentElement);
								var mi24Elements = $(contentElement + ' .mi24');
								if(mi24Elements.length > 0){
									for(var i = 0; i < mi24Elements.length; i++){
										var id = mi24Elements[i].id;
										var scripts = $(mi24Elements[i]).children("script");
										if(scripts.length > 0){
											var scriptSrc = $(scripts[0]).attr("src");
											var url = scriptSrc + "&jsdiv=" + id;
											$.getScript( url )
	  											.done(function( script, textStatus ) {
													console.log( textStatus );
	  											})
	  											.fail(function( jqxhr, settings, exception ) {
													$( "div.log" ).text( "Triggered ajaxError handler." );
												});
										}
									}
								}
							
								window.trackURLs(contentElement + ' ');
							
						});
					} else {
						// displays error message if there is no content available
						$(contentElement).empty().html('<div class="row"><div class="col-12"><p class="text-error icon-message">'+loadData.config.errorHtml+'</p></div></div>').show();
					} 
				} else {
					if ($.trim($(contentElement).html()) != '') {
						// show static content if available
						fadeInContent();
					} else {
						// displays error message if there is no content available
						$(contentElement).empty().html('<div class="row"><div class="col-12"><p class="text-error icon-message">'+loadData.config.errorHtml+'</p></div></div>').show();
					}
				} 
			} catch (e) {
				// displays error message if there is a problem caused by web storage
				$(contentStatus).empty().html('<div class="row"><div class="col-12"><p class="text-error icon-message">'+loadData.config.errorHtml+' [Web Storage Loading]</p></div></div>');
			} 
		} 
	} 
};