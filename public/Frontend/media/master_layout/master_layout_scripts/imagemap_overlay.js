function changeImagemapOverlay(image, map){
    document.getElementById(map).src = image;
}

$(document).ready(function(){
	if(navigator.appName.indexOf("Internet Explorer") > 0){
		var areas = document.getElementsByTagName('area');
		for(var i = 0; i < areas.length; i++){
			areas[i].removeAttribute('alt');
		}
	}
});