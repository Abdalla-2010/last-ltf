/* ===========================================
   master_script.js, Version 0.60
   =========================================== */

var isLayoutSwitch = document.body.classList.contains('design2019');

// True, wenn aktuelle Seite Startseite ist
var isHome = false;
if (document.getElementById('content-home') != null) isHome = true;
// True, wenn aktuelle Seite Splashpage ist
var isSplash = false;
if (document.getElementById('content-splashpage') != null) isSplash = true;

// Breiten für unterschiedliche Ausgabegrößen
var mobileBreak = 767;
if(isLayoutSwitch) {
	var desktopBreak = 1249;
	var useBreakpoint = desktopBreak;
} else {
	var screenBreak = 1107;
	var wideBreak = 1779;
	var useBreakpoint = screenBreak;
}

// Client values, siehe function getClientValues()
var w, d, e, g, x, y;

// True, wenn Suchfeld ausgewählt ist
var searchFieldOnFocus = false;

// Parameter für Fancybox
var fbParams = {
    fitToView: false,
    width: '80%',
    height: 'auto',
    padding: 0,
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none',
    helpers: { title: null },
    beforeLoad: function() {
      if (typeof dataLayer !== "undefined") dataLayer.push({'event': 'gtm.fancybox'});
    }
  };

// movingimage
var isFullscreenVideo = false;

/**
 * Ermittelt Höhe, Breite, Document, Window, Body und speichert die Werte global
 */
function getClientValues() {
  w = window;
  d = document;
  e = d.documentElement;
  g = d.getElementsByTagName('body')[0];
  x = w.innerWidth || e.clientWidth || g.clientWidth; // Breite, mit Fallback für verschiedene Browser
  y = w.innerHeight || e.clientHeight|| g.clientHeight; // Höhe, mit Fallback für verschiedene Browser
}

/**
 * Hook für Funktionen, die nach dem Nachladen neuer Inhalte ausgeführt werden sollen
 */
var onDomChangedFunctions = [];
function domChanged(selector) {
	var s = '';
  	if(selector !== undefined && selector !== null) {
		s = selector + ' ';
  	}
	for(var i = 0; i < onDomChangedFunctions.length; i++) {
		onDomChangedFunctions[i](s);
	}
}

/**
 * Öffnet bzw. schließt Dropdown in Mobilansicht
 */
function toggleMobileNav() {
  var htmlNode = document.documentElement;
  var pageWrap = document.getElementById('top');
  var adBannerTop = document.getElementById('ad-banner-top');
  var adBannerTopHeight = 0;
  // Anpassung der Höhe, wenn Banner angezeigt wird
  if (adBannerTop != null) adBannerTopHeight = adBannerTop.offsetHeight;

  var headerHeight = 10 + adBannerTopHeight;
  var headerIcon = document.querySelector('.mobile-nav-toggle');
  if (headerIcon) {
	headerHeight = headerHeight + headerIcon.offsetHeight;
  }
  var mobileNav = document.getElementById('nav-mobile');
  var mobileNavMarginTop = headerHeight;
  //if (x >= y) mobileNavMarginTop = 0;
  if (pageWrap.style.overflow == '') {
    htmlNode.style.height = '100%';
    pageWrap.style.overflow = 'hidden';
    pageWrap.style.height = (y-10)+'px';
    pageWrap.style.paddingBottom = '0';
    mobileNav.style.marginTop = mobileNavMarginTop+'px';
    mobileNav.style.height = (y-mobileNavMarginTop)+'px';
  } else {
    htmlNode.style.height = '101%';
    pageWrap.style.overflow = '';
    pageWrap.style.height = 'auto';
    pageWrap.style.paddingBottom = '8px';
    mobileNav.style.height = 'auto';
  }
  mobileNav.classList.toggle('show-nav-mobile');
  $('.mobile-nav-toggle').toggleClass('mobile-nav-toggle-active');
}

/**
 * Schließt Dropdown in Mobilansicht
 */
function closeMobileNav() {
  var htmlNode = document.documentElement;
  var pageWrap = document.getElementById('top');
  var mobileNav = document.getElementById('nav-mobile');
  if (pageWrap.style.overflow == 'hidden') {
    htmlNode.style.height = '101%';
    pageWrap.style.overflow = '';
    pageWrap.style.height = 'auto';
    pageWrap.style.paddingBottom = '8px';
    mobileNav.style.height = 'auto';
    mobileNav.classList.toggle('show-nav-mobile');
  }
  $('.mobile-nav-toggle').removeClass('mobile-nav-toggle-active');
}

/**
 * Verkleinert den Header und blendet den Werbebanner aus, wenn nach unten gescrollt wird
 * Setzt den Ausgangszustand zurück, wenn ganz nach oben gescrollt wurde
 *
 * @param {scrollTop} Scroll-Offset (von oben)
 */
function setOnScrolling(scrollTop){
  // Advertisment Banner (top)
  var adBannerTop = document.getElementById('ad-banner-top');
  if (adBannerTop != null) {
    (scrollTop > 0) ? adBannerTop.style.display = 'none' : adBannerTop.style.display = 'block';
  }
  // Header
  var fixedArea = $('#screen-fixed');
  var mainNavActive = $('#nav-main');
  // Footer
  var footer = document.getElementById('footer');
  if (footer != null) {
    var footerHeight = $('footer').height();
    var footerOffsetTop = footer.offsetTop;
    } else {
    var footerHeight = 0;
    var footerOffsetTop = 0;
  }
  // Set Classes
  if (scrollTop > 0 && y < (+footerOffsetTop+footerHeight)) {
    $('body').addClass('on-scrolling');
    fixedArea.addClass('on-scrolling');
    mainNavActive.addClass('on-scrolling');
  } else {
    $('body').removeClass('on-scrolling');
    fixedArea.removeClass('on-scrolling');
    mainNavActive.removeClass('on-scrolling');
  }
}

/**
 * Ermittelt Scroll-Offset und führt Aktionen aus
 */
function onScrolling() {
  var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
  setOnScrolling(scrollTop);
}

/**
 * Fixiert Footer bei großen Auflösungen am unteren Bildrand
 */
function fixFooter() {
  if (x > useBreakpoint && !isLayoutSwitch) {
    var page = document.getElementById('page');
    if (y > (page.offsetTop + page.offsetHeight)) {
      $('body').addClass('fixed-footer');
      $('#footer').addClass('fixed');
    } else {
      $('body').removeClass('fixed-footer');
      $('#footer').removeClass('fixed');
    }
  }
}

/**
 * Scrollt zu einem bestimmten Element, auch in Tabs oder Akkordeons
 *
 * @param {hash} CSS-Selektor für das Element
 */
function scrollToElement(hash){
  var isScroll = false;
  if($(hash).length){
    if (x > useBreakpoint && !isLayoutSwitch) {
      setOnScrolling(1);
    }

    var elementPosition = 0;
    var headerHeight = 0;
    if (x > useBreakpoint) {
      if (document.getElementById('screen-fixed') != null){
        headerHeight = document.getElementById('screen-fixed').offsetHeight;
      } else if (isLayoutSwitch && document.getElementById('header') != null) {
        headerHeight = document.getElementById('header').offsetHeight;
      }
      headerHeight = headerHeight + 20;
      isScroll = true;
    }

    if ($(hash).selector.indexOf('-tab') >= 0 && $(hash).parent('.tab-navigation')) {
      // Tab-Navigation --> select tab
      var tabNr = $(hash).selector.split("-tab")[1];
      // if tabs are not numbered, find tab
      if(!isInt(tabNr)){
        tabNr = $(hash).parent('.tab-navigation').children('.ui-tabs-panel').index($(hash)) + 1;
      }
      elementPosition = $(hash).parent('.tab-navigation').offset().top;
      $(hash).parent('.tab-navigation').tabs({ active: (tabNr-1)});
      isScroll = true;
    } else if ($(hash).selector.indexOf('-acc') >= 0 && $(hash).parent('.accordion-navigation')) {
      // Accordion-Navigation --> open accordion
      var accNr = $(hash).selector.split("-acc")[1];
      elementPosition = $(hash).prev().offset().top;
      $(hash).parent('.accordion-navigation').accordion({ active: (accNr-1)});
      isScroll = true;
    } else if ($(hash).hasClass('fancybox-content')) {
      // Fancybox --> open
      $.fancybox.open($(hash),fbParams);
    } else if ($(hash).hasClass('filter-group') || $(hash).hasClass('filter-group-mobile')) {
      // Filter
	  if (x > useBreakpoint || $(hash).hasClass('filter-group-mobile')) {
		var filterGroup = $(hash).data('filter-group');
	  	$('.filter-group, .filter-group-mobile').each(function(){
			if ($(this).data('filter-group') == filterGroup) {
				$(this).hide();
			}
		});
	  	$(hash).show();
	  }
	  elementPosition = $(hash).offset().top;
	} else if ($(hash).hasClass('mi24')) {
		isScroll = false;
	} else if ($(hash).closest('.image-accordion-header').length) {
		isScroll = false;
    } else if($(hash).offset()) {
      var tab = $(hash).parents('.ui-tabs-panel');
      if(tab.length > 0){
        // Anchor within Tab-Navigation
        var tabNr = tab.prevAll('.ui-tabs-panel').length;
        tab.closest('.ui-tabs').tabs({ active: tabNr});
      }
      var acc = $(hash).parents('.ui-accordion-content');
      if(acc.length > 0) {
        // Anchor within Accordion-Navigation
        var accNr = acc.prevAll('.ui-accordion-content').length;
        acc.closest('.accordion-navigation').accordion({ active: accNr});
        isScroll = false;
        setTimeout(function() {
          elementPosition = $(hash).offset().top;
          $('html,body').animate({scrollTop: (elementPosition - headerHeight)}, 1000);
        },450);
      } else {
        isScroll = true;
      }
      elementPosition = $(hash).offset().top;
    }
    if (isScroll) {
      $('html,body').animate({scrollTop: (elementPosition - headerHeight)}, 1000);
    }
  }
}

/**
 * Setzt Scroll-Funktion auf Links mit der Klasse 'al'
 *
 */
function adjustAnchors() {
  anchorLinks = document.getElementsByClassName('al');
  for (var i = 0; i < anchorLinks.length; i++) {
    // nur Links innerhalb der Seite anpassen
    if (anchorLinks[i].getAttribute('href').split('#')[0].toString().trim().length == 0) {
      anchorLinks[i].onclick = function() {
        scrollToElement('#' + this.getAttribute('href').split('#')[1].toString()); return false;
      };
    }
  }
}

// Setzt Scroll-Funktion auf Links mit der Klasse 'ScrollTop'
var topLinks = document.getElementsByClassName('link-top');
for (var i = 0; i < topLinks.length; i++) {
  topLinks[i].onclick = function () { window.scrollTo(0,0); return false; };
}

/**
 * Passt Kachelhöhe auf Startseite und Splashpage an mobile Bildschirmgröße an
 * (so dass Kacheln quadratisch, bzw. doppelt so breit wie hoch bleiben)
 *
 * @param {resizeBig} wenn true, wird die große quadratische Kachel auf die halbe Höhe verkleinert
 */
function mobileTileHeight(resizeBig) {

  var borderWidth = 2;
  var tileSectionWidth = (document.getElementById('tiles-section') || document.getElementById('tiles-section-left')).offsetWidth;
  var tileMargin = 0;
  var tileWidth = 0;

  if(document.getElementById('tile-2') !== null) {
    tileMargin = window.getComputedStyle(document.getElementById('tile-2')).getPropertyValue('margin-right');
    tileWidth = (tileSectionWidth-parseInt(tileMargin))/2 - 1;
  }

    var tilesText = document.getElementsByClassName('tile-single-text');
    for (var i = 0; i < tilesText.length; i++) {
      tilesText[i].style.width = (tileWidth-borderWidth)+'px';
      tilesText[i].style.maxWidth = (tileWidth-borderWidth)+'px';
      tilesText[i].style.height = (tileWidth-borderWidth)+'px';
      tilesText[i].style.maxHeight = (tileWidth-borderWidth)+'px';
    }

    var tilesBig = $('.tile-big');
    for (var i = 0; i < tilesBig.length; i++) {
      if (resizeBig) {
        tilesBig[i].style.height = tileWidth +'px';
        tilesBig[i].style.maxHeight = tileWidth+'px';
      } else {
        tilesBig[i].style.height = tilesBig[i].offsetWidth+'px';
        tilesBig[i].style.maxHeight = tilesBig[i].offsetWidth+'px';
      }
    }

  $('.stage').each(function (index, element) {
	var bgUrl = $(element).data('image-narrow');
    $(element).css('background-image', 'url(' + bgUrl + ')');
  });
}

/**
 * Fügt Meta-Navigation und Sprachauswahl im Footer ein
 */
function moveMobileNav() {
  $('#nav-meta').insertBefore($('#nav-footer')).addClass('show-mobile');
  $('#nav-lang').insertBefore($('#footer-copyright')).addClass('show-mobile');
  if($('#header').has($('#nav-social-div'))) {
    $('#nav-social').insertBefore($('#footer-copyright')).addClass('show-mobile');
  }
  $('#nav-mblogo').insertBefore($('#footer-copyright')).addClass('show-mobile');
}

/**
 * Entfernt Meta-Navigation und Sprachauswahl aus dem Footer
 */
function moveMobileNavBack() {
  $('#nav-meta').insertAfter($('#nav-meta-div')).removeClass('show-mobile');
  $('#nav-lang').insertAfter($('#nav-lang-div')).removeClass('show-mobile');
  if($('#header').has($('#nav-social-div'))) {
    $('#nav-social').insertAfter($('#nav-social-div')).removeClass('show-mobile');
  }
  $('#nav-mblogo').insertAfter($('#sr-navigation')).removeClass('show-mobile');
}

/**
 * Passt Kachelhöhe auf Startseite und Splashpage an normale Bildschirmgröße an
 * (so dass Kacheln quadratisch, bzw. doppelt so breit wie hoch bleiben)
 *
 * @param {tileSize} Höhe der kleinen Kacheln
 * @param {tileSize} Höhe der großen Kachel
 */
function resetTileHeight(tileSize, tileSizeBig) {
  var borderWidth = 2;
  var tilesText = document.getElementsByClassName('tile-single-text');
  for (var i = 0; i < tilesText.length; i++) {
    tilesText[i].style.width = (tileSize-borderWidth)+'px';
    tilesText[i].style.maxWidth = (tileSize-borderWidth)+'px';
    tilesText[i].style.height = (tileSize-borderWidth)+'px';
    tilesText[i].style.maxHeight = (tileSize-borderWidth)+'px';
  }

  var tilesBig = $('.tile-big');
  for (var i = 0; i < tilesBig.length; i++) {
    tilesBig[i].style.height = tileSizeBig+'px';
    tilesBig[i].style.maxHeight = tileSizeBig+'px';
  }

//  if (document.getElementById('tile-big')) {
//    document.getElementById('tile-big').style.height = tileSizeBig+'px';
//    document.getElementById('tile-big').style.maxHeight = tileSizeBig+'px';
//  }

  $('.stage').each(function (index, element) {
	var bgUrl = $(element).data('image-wide');
    $(element).css('background-image', 'url(' + bgUrl + ')');
  });
}

/**
 * Passt Größe der Suchfeld-Kachel auf der Homepage an und blendet Bereich für Vorschläge ein und aus
 *
 * @param {animate} wenn true, wird die Größenänderunge animiert
 */
function resetHomeSearchTile(animate) {
    var tile_search = $('#tile-search');
    var tile_width = tile_search.width();
    var tile_height = tile_search.height();
    var finalWidth;
    var finalHeight;
    var finalOpacity;

    $('.tile-search-content').css('top', (tile_height-1)+'px');

    if (searchFieldOnFocus) {
        var tile_next = tile_search.next('div');
        var tile_margin = tile_next.offset().left - tile_search.offset().left - tile_width;
        finalWidth = tile_margin + 2*tile_width;
        finalHeight = tile_height + $('.tile-search-content').height() + 60;
        finalOpacity = '0.95';
    } else {
        finalWidth = tile_width;
        finalHeight = tile_height;
        finalOpacity = '1';
    }

    $('.search-field-and-icon').css({'height': tile_height+'px'});
    if (animate) {
        $('.tile-search-inner').animate({'width' : finalWidth+'px', 'height': finalHeight+'px', 'opacity': finalOpacity}, 'slow');
    } else {
        $('.tile-search-inner').css({'width': finalWidth+'px', 'height': finalHeight+'px', 'opacity': finalOpacity});
    }
}

/**
 * Passt Größe der Suchfeld-Kachel auf der Splashpage an und blendet Bereich für Vorschläge ein und aus
 *
 * @param {animate} wenn true, wird die Größenänderunge animiert
 */
function resetSplashSearchTile(animate) {
    var tile_search = $('#tile-search');
    var tile_width = tile_search.width();
    var tile_height = tile_search.height();
	var tile_left;
    var finalWidth;
    var finalHeight;
    var finalOpacity;

    $('.tile-search-content').css('top', (tile_height-1)+'px');

   if (searchFieldOnFocus) {
        var tile_prev = tile_search.prev('div');
      var tile_margin = tile_search.offset().left - tile_prev.offset().left - tile_prev.width();
      finalWidth = tile_margin + 2*tile_width;
        finalHeight = tile_height + $('.tile-search-content').height() + 60;
        finalOpacity = '0.95';
    tile_left = 0 - tile_margin - tile_width - 1;
    } else {
        finalWidth = tile_width;
        finalHeight = tile_height;
        finalOpacity = '1';
    tile_left = -1;
    }

    $('.search-field-and-icon').css({'height': tile_height+'px'});
    if (animate) {
        $('.tile-search-inner').animate({'left' : tile_left+'px', 'width' : finalWidth+'px', 'height': finalHeight+'px', 'opacity': finalOpacity}, 'slow');
    } else {
        $('.tile-search-inner').css({'left' : tile_left+'px', 'width': finalWidth+'px', 'height': finalHeight+'px', 'opacity': finalOpacity});
    }
}

/**
 * Passt Größe der Suchfeld-Kachel auf Content-Seiten an und blendet Bereich für Vorschläge ein und aus
 *
 * @param {animate} wenn true, wird die Größenänderunge animiert
 */
function resizeSearchTile(animate) {
  var searchTile = $('.nav-level2-search');
  var sibling = searchTile.next('li');
  var tileMargin = parseFloat(searchTile.css('margin-right'));
  var tileHeight = searchTile.height();
  var finalWidth;
  var finalHeight;
  var finalOpacity;

  if (searchFieldOnFocus) {
    if(sibling.length > 0){
      finalWidth = searchTile.width() + 2*(sibling.width()) + 2*tileMargin + 4;
    }else{
      var tilePadding = parseFloat(searchTile.css('padding-right'))+parseFloat(searchTile.css('padding-left'));
      finalWidth = 3*searchTile.width() + 2*(tileMargin+tilePadding) + 4;
    }
    finalHeight = tileHeight + $('.tile-search-content').height() +40;
    finalOpacity    = '.95';
  } else {
    finalWidth = searchTile.width();
    finalHeight = tileHeight;
    finalOpacity = '1';
  }

  $('.search-field-and-icon').css('height', tileHeight+'px');
  $('.tile-search-icon').css('margin-top', (tileHeight * 0.10)+'px');
  $('.tile-search-icon').css('margin-bottom', (tileHeight * 0.095) + 'px');

  if (animate) {
    $('.tile-search-inner').animate({'width': finalWidth+'px', 'height': finalHeight+'px', 'opacity': finalOpacity}, 'slow');
  } else {
    $('.tile-search-inner').css({'width': finalWidth+'px', 'height': finalHeight+'px', 'opacity': finalOpacity});
  }
}

/**
 * Initialisiert Masterscript-Funktionen
 *
 * @param {resize} zur Zeit nicht berücksichtigt
 */
function init(resize) {
  if(typeof resize == 'undefined') resize = false;

  getClientValues();

  // Fancybox
  $('.fancybox').fancybox({
    helpers:  {
      title : {
        type : 'inside'
      }
    },
    beforeLoad: function() {
      if (typeof dataLayer !== "undefined") dataLayer.push({'event': 'gtm.fancybox'});
    }
  });

  $('.tablet-col-12').parent().addClass('col-tablet-12');
  $('.tablet-col-6').parent().addClass('col-tablet-6');
  $('.no-padding-bottom').parent().addClass('no-padding-bottom');
  $('.no-padding-top').parent().addClass('no-padding-top');

  // Fancybox (pageflip)
  $('.fancybox-pageflip').fancybox({
    helpers: {
      title: null,
      buttons : {
        position: 'bottom',
        tpl: '<div id="fancybox-buttons"><ul><li><a class="btnToggle" title="Seitengröße / Originalgröße" href="javascript:;"></a></li></ul></div>'
      }
    }
  });

  // Fancybox (Inline Content)
  var fbcMaxWidth = 612;
  var fbcMaxHeight = 500;
  if (x > useBreakpoint) {
    fbcMaxWidth = 1000;
    fbcMaxHeight = 900;
  } else if (x > mobileBreak) {
    fbcMaxWidth = 736;
    fbcMaxHeight = 520;
  }

  fbParams.maxWidth = fbcMaxWidth;
  fbParams.maxHeight = fbcMaxHeight;

  $('.link-fancybox-content').fancybox(fbParams);

  // Tooltip
  onDomChangedFunctions.push(function(selector){
    $(selector  + '.tooltip').tooltip({
      track: true
    });
  });

  // adjust Links to hidden videos on each dom change
  onDomChangedFunctions.push(function(selector){
	$(selector + 'a').each(function() {
	  var href = $(this).attr('href');
	  if(href && href.indexOf('#') >= 0){
		var hash = href.substr(href.indexOf('#'));
		if(!hash.lastIndexOf('#') && $(hash) && $(hash).hasClass('mi24')) { // skip invalid links
          var videoId = hash.substr(1);
		  $(this).click(function(event){
			event.preventDefault();
			isFullscreenVideo = true;
			var player = VideoPlayer.Collection.getPlayerById(videoId);
			player.requestFullscreen();
			$('#' + videoId).show();
			player.play();
		  });
		}
	  }
    });
  });

  // responsive Images
  $('picture').picture();
  $('body').imagesLoaded(
    function() {
      if (document.body.classList.contains('body--has-htmlcontent')) {
        setTimeout(fixFooter, 500);
      } else {
        fixFooter();
      }
    }
  );

  // Homepage
  if (isHome) {
    // Kacheln
	if (isLayoutSwitch) {
		if (x > useBreakpoint) {
			resetTileHeight(196, 399);
	    } else if (x < useBreakpoint && x > mobileBreak) {
			resetTileHeight(170, 345);
    	} else {
      		mobileTileHeight();
    	}
	} else {
		if (x > wideBreak) {
			resetTileHeight(242, 494);
		} else if (x > screenBreak) {
			resetTileHeight(166, 337);
		} else if (x < screenBreak && x > mobileBreak) {
			resetTileHeight(170, 345);
		} else {
			mobileTileHeight();
    	}
	}
    window.setTimeout(function(){resetHomeSearchTile(true);}, 400);
    // Suche
    $('.tile-search-content').show();

    // video
    if ($("#bgvideo source")) {
      if (x > mobileBreak) {
        $("#bgvideo source").each(function(){
          if (!$(this).attr("src")){
            if ($(this).data("src")) {
              $(this).attr("src",$(this).data("src"));
              $(this).parent().load();
            }
          }
        });
      }
    }
  }

  // Splashpage
  if (isSplash) {
    // Kacheln
    if (x > useBreakpoint) {
      resetTileHeight(196, 399);
    } else if (x < useBreakpoint && x > mobileBreak) {
      resetTileHeight(170, 345);
    } else {
      mobileTileHeight(true);
    }
    resetSplashSearchTile(false);
    // Suche
    $('.tile-search-content').show();
  }

  // Contentpage
  if (!isHome && !isSplash) {
    // Suche
    resizeSearchTile();
    $('.tile-search-content').show();
    // Listen
    $("li:has(ul)").addClass("nested");
    $("#content li.nested").each(function(index) {
      var firstListChild = $(this).get(0).firstChild.nodeName;
      if($(this).get(0).firstChild.nodeType == 3 || firstListChild == "B" || firstListChild == "I") {
        $(this).removeClass("nested");
      }
    });
  }

  // Mobile
  if (x < mobileBreak && !isLayoutSwitch) {
    moveMobileNav();
    closeMobileNav();
  }

  if (x >= mobileBreak && !isLayoutSwitch) {
    moveMobileNavBack();
  }

  // Silbentrennung
  if (!resize && (isHome || isSplash || x > mobileBreak)) {
    hyphenate();
  }

  adjustAnchors();

  // Screen
  if (x > useBreakpoint && !isLayoutSwitch) {
    if (!isHome && !isSplash) window.onscroll = onScrolling;
  }
}

/**
 * Initialisiert Funktionen für Klicks in Suchfelder
 */
function initClickFunctions() {
  if (isHome || isSplash) {
    if (isHome) {
      $('#gsa-search-input').click(function(){
        searchFieldOnFocus = true;
        resetHomeSearchTile(true);
      });
    } else {
      $('#gsa-search-input').click(function(){
        searchFieldOnFocus = true;
        resetSplashSearchTile(true);
      });
    }
    $('html').click(function(){
      searchFieldOnFocus = false;
      if (isHome) {
        resetHomeSearchTile(true);
      } else {
        resetSplashSearchTile(true);
      }
    });
    $('#tile-search').click(function(event){
      event.stopPropagation();
    });
  } else {
    $(window).resize(function(){
      resizeSearchTile(false);
    });

    $('#gsa-search-input').click(function(){
      searchFieldOnFocus = true;
      resizeSearchTile(true);
    });

    $('html').click(function(){
      searchFieldOnFocus = false;
      resizeSearchTile(true);
    });

    $('.nav-level2-search').click(function(event){
      event.stopPropagation();
    });
  }
}

/**
 * Lädt Inhalte von Akkordeons per AJAX nach
 * @param {uiNewPanel} Akkordeon-Panel, welches geladen werden soll
 */
function loadAccordionAjaxContent(uiNewPanel) {
  try {
    if (typeof jQuery.ui != 'undefined') {
      var accUrl = uiNewPanel.find('a.accordion-navigation-external-link').attr('href');
      var accContent = uiNewPanel.find('.accordion-navigation-external-content');
      if (accUrl != "" && accContent.html() == "") {
        $.ajax({
          url: accUrl,
          success: function(html) {
            accContent.html(html).promise().done(function(){
              $('picture').picture();
              $('.tooltip').tooltip({track: true});
              $(this).show();
            });
          },
          error: function(xhr, status, error) {
            accContent.html(xhr.responseText);
          }
        });
      }
    }
  } catch(e) {

  }
}

// Helper
jQuery.fn.resize_delayed = function (func, duration){
  this.resize(function() {
    clearTimeout(window._resize_delayed);
    window._resize_delayed = setTimeout(func, duration);
  });
};

/**
 * Überprüft, ob sich ein Eingabewert als Integer darstellen lässt
 * @param {x} Eingabewert
 * @return true, wenn es ein Integer-Wert ist, false, wenn nicht
 */
function isInt(x) {
   var y = parseInt(x, 10);
   return !isNaN(y) && x == y && x.toString() == y.toString();
}

/**
 * Ermittelt die Version des verwendeten Internet Explorers
 *
 * @author webfox
 * @since 0.30
 * @return float mit Version, wenn ein Internet Explorer genutzt wird, sonst -1
 */
function getInternetExplorerVersion() {
  var rv = -1;
  if (navigator.appName == 'Microsoft Internet Explorer') {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null) rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

/**
 * Setzt bei Bestätigung den Cookie und blendet den Cookie-Hinweis aus
 *
 * @author webfox
 * @since 0.44
 * @return sets cookie and hides cookie notice if confirmed
 */
function handleCookie() {
	var cookie = document.cookie;
	if (!cookie.match(/mb_accept_cookies\s*\=\s*true/g)) {
		$('#cookie-overlay').show();
		$('#cookie-overlay .cookie-confirm').click(function() {
			var expires = new Date((new Date()).getTime() + 31536000000).toGMTString(); // 1 year
			document.cookie = "mb_accept_cookies=true;expires=" + expires + ";path=/";
			$('#cookie-overlay').hide();
		});
	}
}

$(document).ready(function() {
  // Init
  try {
    init();
    initClickFunctions();
	domChanged();
  } catch(e) {

  }

  // Resize
  $(window).resize_delayed(function(){
    try {
      init(true);
    } catch(e) {

    }
  }, 250);

  // handle cookie
  handleCookie();
});

$(window).load(function(){
  try{
    // Contentpage
    // if (!isHome && !isSplash) {
      // jQuery UI
      if (typeof jQuery.ui != 'undefined') {

        $('.tab-navigation:not(.tab-navigation-load)').tabs({
          heightStyle: 'content',
          create: function(event, ui) {
            $('.widget-navigation-loading').hide();
            $(this).show();
          },
          activate: function(event, ui) {
            if(ui.newPanel && ui.newPanel.hasClass("activateTab")){
              $.fn.activateTab(ui.newPanel.selector);
            }
            fixFooter();
          },
          load: function(event, ui) {
            $('picture').picture();
          }
        });
        $('.accordion-navigation:not(.accordion-navigation-load)').accordion({
          header: '.accordion-navigation-link',
          active: false,
          collapsible: true,
          heightStyle: 'content',
          create: function(event, ui) {
            $('.widget-navigation-loading').hide();
            $(this).show();
          },
          activate: function(event, ui) {
            if(ui.newHeader && ui.newHeader.attr('id')){
              scrollToElement('#'+ui.newHeader.attr('id'));
            }
            fixFooter();
          },
          beforeActivate: function(event, ui) {
            if (ui.newHeader.length > 0) {
              if ($(ui.newPanel).hasClass('accordion-content-external')) {
                loadAccordionAjaxContent($(ui.newPanel));
              }
            }
          }
        });
        scrollToElement(document.location.hash);
      //}
    }
  } catch(e) {

  }
});

/* === Sidepanel-Plugin ============================================= */
jQuery(function ($) {

    sidepanels();

    function sidepanels() {

        var $sidepanels = $('.sidepanel__content');

        $sidepanels.each(function () {
            sidepanel.apply(this);
        });


        function sidepanel() {
            var $this = $(this);
            var $sidepanelLink = $this.prev();
            var $sidepanelClose = $this.find('.sidepanel__close');

            var sidepanelId = $this.data('id');

            var autoOpen = $this.data('autoopen');
            var autoOpenOnlyOnce = $this.data('autoopenonlyonce');
            var openOnHover = $this.data('openonhover');
            var closeOthers = $this.data('closeothers');

            var sidepanelClosedCookieName = 'mb_sidepanel_' + sidepanelId + '_closed';

            if (isAutoOpen()) {
                $this.addClass('sidepanel--open');
            }

            $sidepanelLink.unbind('click');
            if (openOnHover) {
                $sidepanelLink.mouseover(openSidepanel);
            } else {
                $sidepanelLink.click(toggleSidepanel);
            }
            $sidepanelClose.click(closeSidepanel);

            function toggleSidepanel(e) {
                e.preventDefault();
                if ($this.hasClass('sidepanel--open')) {
                    closeSidepanel(e);
                } else {
                    openSidepanel(e);
                }
            }

            function openSidepanel(e) {
                e.preventDefault();

                if ($this.hasClass('sidepanel--open')) {
                    return;
                }

                if (closeOthers) {
                    $sidepanels.removeClass('sidepanel--open');
                }
                $this.addClass('sidepanel--open');
            }

            function closeSidepanel(e) {
                e.preventDefault();
                $this.removeClass('sidepanel--open');

                if (autoOpen && autoOpenOnlyOnce && getCookie(sidepanelClosedCookieName) !== 'true') {
                    setCookie(sidepanelClosedCookieName, 'true');
                }
            }

            function isAutoOpen() {
                return autoOpen && !(autoOpenOnlyOnce && getCookie(sidepanelClosedCookieName) === 'true');
            }

            function setCookie(key, value) {
                var expires = new Date((new Date()).getTime() + 31536000000).toGMTString(); // 1 year
                document.cookie = key + '=' + value + ';expires=' + expires + ';path=/';
            }

            function getCookie(name) {
                var value = '; ' + document.cookie;
                var parts = value.split('; ' + name + '=');
                if (parts.length === 2) {
                    return parts.pop().split(';').shift();
                }

                return null;
            }
        }
    }
});