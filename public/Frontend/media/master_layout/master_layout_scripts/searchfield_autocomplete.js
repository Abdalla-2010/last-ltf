function searchfieldAutocomplete(form, inputElement, suggestionElement, isMobile) {
	if (form != undefined && inputElement != undefined) {
		var lang = $("input[name='hl']", form).val();

		var searchTips = new Array();
		searchTips['de'] = new Array();
		searchTips['en'] = new Array();
		searchTips['es'] = new Array();
		searchTips['zh'] = new Array();
		searchTips['de'][0] = 'Bitte Suchbegriff eingeben ...';
		searchTips['en'][0] = 'Please enter search term ... ';
		searchTips['es'][0] = 'Por favor introduzca un término de búsqueda ...';
		searchTips['zh'][0] = '请输入搜索关键词';
		searchTips['de'][1] = 'Bitte betätigen Sie nach Eingabe des Suchbegriffs die Enter-Taste.';
		searchTips['en'][1] = 'After entering the search term, please press the enter key.';
		searchTips['es'][1] = 'Introduzca un término de búsqueda y pulse el botón ENTER.';
		searchTips['zh'][1] = '输入搜索关键词后按回车键确认';

		if (form.length > 0) {
			
		    var client = $("input[name='client']", form).val();
		    if (client == null || client == "") {
			    client = "MB-GSA_Search-" + lang.toUpperCase();
		    }
		    var site = $("input[name='site']", form).val();
		    if (site == null || site == "") {
			    site = "MB-" + lang.toUpperCase();;
		    }

		    var $input = $(inputElement, form).autocomplete({
			select: function( event, ui ) {
			    form.submit();
			    return true;
			},
			close: function( event, ui ) {
			},
			appendTo: ( isMobile != undefined && isMobile ? '#search-mobile-suggestions' : '.tile-search-content' ),
			source: function(request, response) {
			    $.ajax({
					url: "https://www.messe-berlin.de/suggest",
					dataType: "jsonp",

					data: {
						q: request.term,
						site: site,
						client: client,
						cmd: "suggest",
						max: 10,
						access: "p",
						format: "os"
					},

					success: function(data) {
						convertUnicode(data[1]);

				    	if (suggestionElement != undefined) {
					
							var $list;
							if (isMobile != undefined && isMobile) {
								$list = $('.search-mobile-suggestions-ul');
								$list.addClass('hidden');
							} else {
								$list = $(suggestionElement);
								searchFieldOnFocus = true;
							}
							$list.empty();

							response(data[1]);
							if (data[1] == '') {
								$list.empty().append($('<li>'+searchTips[lang][1]+'</li>')).removeClass('hidden');
								$list.prev().hide();
							} else {
								$list.prev().show();
							}

							$('.ui-autocomplete li.ui-menu-item').click(function(){
								$(inputElement).val($(this).text());
								form.submit();
							});

							if (isHome) { 
								resetHomeSearchTile(true);
							} else if (isSplash) {
								resetSplashSearchTile(true);
							} else {
								resizeSearchTile(true);
							}
					
						} else {
							response(data[1]);
						}

						function convertUnicode(data) {
							var re = /(\\u[a-fA-F0-9]+)/g;
							for (var i = data.length - 1; i >= 0; i--) {
								data[i] = data[i].replace(re, replacer);
							}
							function replacer(match, p1, p2, p3, offset, string) {
								return String.fromCodePoint('0x' + match.substring(2));
							}
						}

					}
			    }); 
			}, 
			minLength: 1
		    });

		    $input.autocomplete("instance").close = function(e) {};
	    
 		}
		
		$(inputElement).on('change keyup copy paste cut', function() {
			
    		if(this.value.length == 0) {
				if(isMobile != undefined && isMobile){
					$(suggestionElement).empty();
					$(suggestionElement).prev().hide();
				}else{
					$(suggestionElement).empty().append($('<li>'+searchTips[lang][0]+'</li>'));
					$(suggestionElement).prev().show();
				}
				if (isHome) { 
					resetHomeSearchTile(true);
				} else if (isSplash) {
					resetSplashSearchTile(true);
				} else {
					resizeSearchTile(true);
				}
			}
		});
	} 
}