// **********
// Download center
// **********

$(function () {

$("#downloadcenter-category-form").on("submit", function (ev) {
	ev.preventDefault();
	var selected = $("#downloadcenter-search-category > option:selected").val();
	if (selected === "downloadcenter_show_all") {
		show_all_categories();
	} else {
		filter_categories(selected);
	}	
})

/*
$("#downloadcenter-search-form").on("submit", function (ev) {
	ev.preventDefault();
	var search_term = $("#downloadcenter-search-keyword").val().trim();
	search_categories(search_term);
})
*/

function filter_categories(name) {
	//show all first
	show_all_categories();
	name = name.trim();
	$(".downloadcenter-category").each(function () {
		var cat = $("h3", this).text().trim();
		//console.log(cat);
		//console.log(name);
		if (cat !== name) {
			$(this).hide();
		}
	});
}

function show_all_categories() {
	$(".downloadcenter-category").each(function () {
		$(this).show();
	});
}

function search_categories(name) {
	$(".downloadcenter-item").each(function () {
		var title = $("a", this).text().trim();
		if (title.toLowerCase().indexOf(name.toLowerCase()) == -1) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});
}

}); //END on document load