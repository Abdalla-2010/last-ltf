jQuery(function ($) {

	var lang = $('html').attr('lang');

    var type = null;
    if ($('body.fachbesucher').length) {
        type = 'fachbesucher';
    }
    if ($('body.privatbesucher').length) {
        type = 'privatbesucher';
    }
	if ($('body.fachbesucher--newsletter').length || $('body.privatbesucher--newsletter').length) {
		type = null;
	}

    if (type === null) {
        $('.link-aside.icon-side-mail').css('display', 'none');
    } else {
        newsletterSidepanel();
    }

    function newsletterSidepanel() {
        var title = lang === 'de' ? 'Newsletter-Anmeldung' : 'Newsletter registration';
        var intro = type === 'privatbesucher' ? 
			'Fernweh gefällig? Tipps für Urlaubsmomente in Berlin, Brandenburg und versteckten Ecken der Welt sowie zum ITB Wochenende gibt’s in unserem Privatbesucher-Newsletter! Hier geht’s zur Anmeldung:' :
			lang === 'de' ?
            	'Programm-Highlights, Branchen-News, Tipps & Tricks rund um die ITB Berlin... Alles drin in den Fachbesucher-News! Hier geht’s zur Anmeldung:' : 
	            'Show highlights, industry news, hints & tricks about ITB Berlin... All covered in the Trade Visitor News! This way to registration:';
        var disclaimer = lang === 'de' ? 
            'Eine Abmeldung ist jederzeit möglich. Den Abmeldelink finden Sie am Ende eines jeden Newsletters.' : 
            'You can unsubscribe at any time. You will find the corresponding link at the bottom of each newsletter.';

        var widget = type === 'fachbesucher' ?
            '<iframe src="https://mb-itb.widgets.attention-ngn.com/'+lang+'/widget-1093-html" style="width: 100%; border: none;" scrolling="no"></iframe>\n' +
            '<script src="https://services.attention-ngn.com/js/v4/jsmin/responsive-frame.min.js"></script>\n' :

            '<div id="pinuts-widgets-newsletter-54ca4-loader" class="pinuts-widgets-loading">Der Vorgang wird gestartet ...</div>\n' +
            '<script id="pinuts-widgets-newsletter-54ca4">\n' +
            '   (function() {\n' +
            '        var errorHtml = \'Momentan wird an unserem System gearbeitet. Der Vorgang kann deshalb nicht durchgeführt werden. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal..\';\n' +
            '        var locale = \'de\';\n' +
            '        var containerId = \'pinuts-widgets-newsletter-54ca4\';\n' +
            '        window.setTimeout(function(){var d = document.getElementById(containerId + \'-loader\');if (!d) return;d.setAttribute("class","pinuts-widgets-loading failed");d.innerHTML = errorHtml;}, 10000);\n' +
            '        var script = document.createElement(\'script\'); script.src = \'https://www.messe-berlin.de/cmsbs-restproxy/de.pinuts.cmsbs.newsletter.Widget/index?name=54ca4896-2857-42e4-b3f8-783e083d3814&containerId=\'+containerId+\'&locale=\'+locale;\n' +
            '        document.getElementsByTagName(\'script\')[0].parentNode.appendChild(script);\n' +
            '    })();\n' +
            '</script>\n';

        var $newsletterContainer;
        var $newsletterLink;
        var $newsletterClose;

        init();

        function init() {
            $newsletterLink = $('.link-aside.icon-side-mail');
            $newsletterLink.unbind('click');
            $newsletterLink.addClass('newsletter-sidepanel-toggle');
            $newsletterLink.parent().addClass('link-aside--newsletter');

            createElement();

			if (!checkCookie()) {
				window.setTimeout(function(){
					$newsletterContainer.addClass('newsletter-sidepanel--open');
				}, 1);
			}

			addTracking();

            $newsletterLink.click(toggleNewsletter);
			$newsletterClose.click(closeNewsletter);
        }

		function addTracking() {
			var $checkWidgetButton = setInterval(addDataLayer, 200);

			function addDataLayer() {
				var $submitButton = document.getElementById('btnSubmit-1048618001');
				if ($submitButton !== null) {
					$submitButton.addEventListener('click', function(){
						var $mailInput = document.getElementById('pi-newsletter-widget-308821103-1048618001');
						if ($mailInput.value.trim().length > 0 && typeof dataLayer !== 'undefined') {
							dataLayer.push({'event':'gtm.umnlsuccess'});
						}
					});
					clearInterval($checkWidgetButton);
				}
			}
		}

        function createElement() {
            $newsletterContainer = $('<div class="newsletter-sidepanel">');

			$newsletterClose = $('<a title="Schließen" class="fancybox-close newsletter-sidepanel__close" href="#"></a>');

            $newsletterContainer.html(
                '<h4 class="newsletter-sidepanel__title">'+title+'</h4>\n' +
                '<div class="newsletter-sidepanel__intro">' + intro + '</div>\n' +
                widget +
                (type === 'privatbesucher' ? ('<div class="newsletter-sidepanel__disclaimer">' + disclaimer + '</div>') : '')
            );
			$newsletterContainer.prepend($newsletterClose);
            $newsletterContainer.insertAfter($newsletterLink);
        }

        function toggleNewsletter(e) {
            e.preventDefault();
            $newsletterContainer.toggleClass('newsletter-sidepanel--open');
			if (!checkCookie()) {
				setCookie();
			}
        }

        function openNewsletter(e) {
            e.preventDefault();
            $newsletterContainer.addClass('newsletter-sidepanel--open');
        }

        function closeNewsletter(e) {
            e.preventDefault();
            $newsletterContainer.removeClass('newsletter-sidepanel--open');
			if (!checkCookie()) {
				setCookie();
			}
        }

        function checkCookie() {
            return type === 'fachbesucher' ? !!document.cookie.match(/itb_newsletter_registration_fachbesucher\s*\=\s*true/g) :
											 !!document.cookie.match(/itb_newsletter_registration_privatbesucher\s*\=\s*true/g);
        }

        function setCookie() {
            var expires = new Date((new Date()).getTime() + 31536000000).toGMTString(); // 1 year
            document.cookie = "itb_newsletter_registration_"+type+"=true;expires=" + expires + ";path=/";
        }
    }

});