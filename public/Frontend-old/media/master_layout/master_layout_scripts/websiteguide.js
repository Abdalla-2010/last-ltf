function websiteguide(guideData, guideTexts) {

  // component container
  var $websiteGuide = $('.websiteguide');

  // toggle fallback behaviour
  var $websiteGuideList = $('.websiteguide--list');
  var $websiteGuideSelection = $('.websiteguide--selection');

  $websiteGuideList.css('display', 'none');
  $websiteGuideSelection.css('display', 'flex');

  // frontend elements
  var $targetGroupSelection = $('#websiteguide-targetgroup');
  var $landingPageSelection = $('#websiteguide-landingpage');
  var $websiteGuideButton = $('.websiteguide__button');

  if (!jQuery.isEmptyObject(guideData) && !jQuery.isEmptyObject(guideTexts)) {

    // populate target group selection
    for (var targetGroupIndex in guideData) {
      if (!guideData.hasOwnProperty(targetGroupIndex)) {
        continue;
      }
      var targetGroup = guideData[targetGroupIndex];
      var $option = $('<option>');
      $option.attr('value', targetGroupIndex);
      $option.text(targetGroup.name);
      $targetGroupSelection.append($option);
    }

    $targetGroupSelection.change(populateLandingPageSelection);
    $landingPageSelection.change(function() {
      $websiteGuideButton.removeClass('disabled');
    });

    $websiteGuideButton.click(function() {
      if (!$(this).hasClass('disabled')) {
        var val = $landingPageSelection.val();
        var newTab = val.charAt(0) === '1';
        var url = val.substr(1);
		(newTab) ? window.open(url, '_blank') : window.location.href = url;
      } else {
        if ($landingPageSelection.parent().hasClass('websiteguide__select--disabled')) {
          highlightSelection.apply($targetGroupSelection);
        } else {
          highlightSelection.apply($landingPageSelection);
        }
      }
    });

    function populateLandingPageSelection() {
      var currentTarget = parseInt($targetGroupSelection.val());
      var landingPages = guideData[currentTarget].pages;

      $('.websiteguide__select--disabled').removeClass('websiteguide__select--disabled');
      $landingPageSelection.html('<option value="hide">'+guideTexts.labelSelect+'</option>');

      for (var landingPageIndex in landingPages) {
        if (!landingPages.hasOwnProperty(landingPageIndex)) {
          continue;
        }
        var landingPage = landingPages[landingPageIndex];
        var $option = $('<option>');
        $option.attr('value', (landingPage.isNewTab ? '1' : '0') + landingPage.url);
        $option.text(landingPage.name);
        $landingPageSelection.append($option);

        populateStyledSelection.apply($landingPageSelection);
      }
    }

    function buildWrappers() {
      var $this = $(this);

      $this.addClass('websiteguide__select--hidden');
      $this.wrap('<div class="websiteguide__select"></div>');
      $this.after('<div class="websiteguide__select--styled"></div>');

      if (this.id === 'websiteguide-landingpage') {
        $this.parent().addClass('websiteguide__select--disabled');
      }

      var $styledSelect = $this.next('div.websiteguide__select--styled');
      $styledSelect.text($this.children('option').eq(0).text());

      var $list = $('<ul />', {
        'class': 'websiteguide__select--options'
      }).insertAfter($styledSelect);

      $styledSelect.click(function(e) {
        e.stopPropagation();

        if (!$(this).parent().hasClass('websiteguide__select--disabled')) {
          $('div.websiteguide__select--styled.active').not(this).each(function () {
            $(this).removeClass('active').next('ul.websiteguide__select--options').hide();
          });
          $(this).toggleClass('active').next('ul.websiteguide__select--options').toggle();
        } else {
          highlightSelection.apply($targetGroupSelection);
        }
      });

      $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
      });
    }

    function populateStyledSelection() {
      var $this = $(this);
      var numberOfOptions = $this.children('option').length;

      var $list = $this.next().next();
      $list.html('');

      $list.prev().text(guideTexts.labelSelect);

      $websiteGuideButton.addClass('disabled');

      for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
          text: $this.children('option').eq(i).text(),
          rel: $this.children('option').eq(i).val()
        }).appendTo($list);
      }

      var $listItems = $list.children('li');

      $listItems.click(function(e) {
        e.stopPropagation();
        $list.prev().text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $this.change();
        $list.hide();
      });
    }

    function highlightSelection() {
      var $this = $(this);
      var $styledSelection = $this.next();
      $styledSelection.addClass('websiteguide__select--highlighted');

      window.setTimeout(function() {
        $styledSelection.removeClass('websiteguide__select--highlighted');
      }, 200);
    }

    $('.websiteguide__select').each(function() {
      buildWrappers.apply(this);
      populateStyledSelection.apply(this);
    });

  } else {

    var $websiteNoscript = $('.websiteguide noscript');

    // display fallback content (noscript)
    $websiteGuideSelection.css('display', 'none');
    $websiteGuide.html($websiteNoscript.html());
    $websiteGuideList.css('display', 'block');

  }

}