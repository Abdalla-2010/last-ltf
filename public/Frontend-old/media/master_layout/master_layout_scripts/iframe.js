/* ===========================================
   iframe.js, Version 0.1
   =========================================== */

function resizeIframe(iframe, widthSmartphone, heightSmartphone, urlSmartphone,
		widthTablet, heightTablet, urlTablet,
		widthScreen, heightScreen, urlScreen,
		widthWidescreen, heightWidescreen, urlWidescreen){
		getClientValues();
		var hide, iframeWidth, iframeHeight, iframeUrl;
		if(urlSmartphone){
			hide = false;
			iframeWidth = widthSmartphone;
			iframeHeight = heightSmartphone;
			iframeUrl = urlSmartphone;
		}else{
			hide = true;
		}
		if (x > wideBreak) {
		 	if(urlWidescreen){
				hide = false;
		 		iframeWidth = widthWidescreen;
			 	iframeHeight = heightWidescreen;
			 	iframeUrl = urlWidescreen;
			}else{
				hide = true;
			}
		} else if (x > screenBreak) {
			if(urlScreen){
				hide = false;
		 		iframeWidth = widthScreen;
			 	iframeHeight = heightScreen;
			 	iframeUrl = urlScreen;
			}else{
				hide = true;
			}
		} else if (x > mobileBreak) {
			if(urlTablet){
		 		hide = false;
				iframeWidth = widthTablet;
			 	iframeHeight = heightTablet;
			 	iframeUrl = urlTablet;
			}else{
				hide = true;
			}
		}
		if(hide){
			iframe.src = "";
			iframe.style.display = "none";
		}else{
			iframe.style.display = "block";
			iframe.width = iframeWidth;
			iframe.height = iframeHeight;
			iframe.src = iframeUrl;
		}
	}