// **********
// Atmosfair
// **********
$(function () {

function validate( eid, validateFunc ) {
	var f = validateFunc || function( v, e ) { return ( v && v.length > 0 ); },
		$e = $( '#' + eid );

	if ( !f( $e.val(), $e ) ) {
		$e.css( { 'border-color': '#c00' } );
		$( 'label[for=' + eid + ']' ).css( { 'font-weight': 'bold', 'color': '#c00' } );
		
		return false;
	}

	return true;
}
	
function array2String( a, sep, n ) {
	var s = '',
		l = n || a.length;
	for ( var i = 0; i < l; ++i ) {
		if ( i > 0 ) {
			s += sep;
		}

		s += a[ i ];
	}

	return s;
}

function number_format( number, decimals, dec_point, thousands_sep ) {
  /* https://raw.github.com/kvz/phpjs/master/functions/strings/number_format.js */
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

if (typeof atmosfair === 'undefined') {
	atmosfair = {};
}
if (!atmosfair.globalPreview) {
	$( '#atmosfair-result-pane' ).hide();
	$( '#atmosfair-success-pane' ).hide();
}
$( '#atmosfair-result' ).click( function( e ) {
	e.preventDefault();
	var vr1 = validate( 'from' ),
		vr2 = validate( 'to' ),
		vr3 = ( $( '#from' ).val().toUpperCase() != $( '#to' ).val().toUpperCase() );

	if ( !vr3 ) {
		alert( atmosfair.errorText );
	}

	if ( vr1 && vr2 && vr3 ) {
		$.post( './form.atmosfair', $( '#atmosfair-calculation' ).serialize(), function( data ) {
			if ( 'error' in data ) {
				var t = '';
				if ( data.stacktrace && data.stacktrace.length > 0 ) {
					t = "\n\n" + array2String( data.stacktrace, "\n", 12 );
				}
				alert( data.error + "\n\n" + data.errorMsg + t );
			} else {
				$( '#atmosfair-result-emission' ).text( number_format( data.emission, 2, ',', '.' ) + ' kg' );
				$( '#atmosfair-result-offset' ).text( number_format( data.offset, 2, ',', '.' ) + ' Euro ' );
				$( '#offset' ).val( data.offset );
				$( '#atmosfair-result-pane' ).fadeIn();
			}
		}, 'json' );
	}
	return false;
} );

$( '#atmosfair-compensate' ).click( function( e ) {
	e.preventDefault();

	$( this ).attr( 'disabled', 'disabled' );

	var vr1 = validate( 'compensate', function( v, e ) { return e.is( ':checked' ); } ),
		vr2 = validate( 'name' ),
		vr3 = validate( 'email' );

	if ( vr1 && vr2 && vr3 ) {
		$.post( './form.atmosfair', $( '#atmosfair-compensation' ).serialize(), function( data ) {
			if ( data.success ) {
				$( '#atmosfair-calculation-pane' ).fadeOut();
				$( '#atmosfair-result-pane' ).fadeOut();
				$( '#atmosfair-success-pane' ).fadeIn();
			} else {
				var t = '';
				if ( 'stacktrace' in data && data.stacktrace.length > 0 ) {
					t = "\n\n" + array2String( data.stacktrace, "\n", 12 );
				} else if ( 'resultStatusValue' in data ) {
					t = "\n\n" + data.resultStatusValue;
				}
				alert( data.errorMsg + t );
			}
			$( this ).attr( 'disabled', null );
		}, 'json' );
	} else {
		$( this ).attr( 'disabled', null );
	}

	return false;
} );
function buildQuery( term ) {
	return "select * from json where url = 'http://airportcode.riobard.com/search?fmt=JSON&q=" + term + "'";
}
function acSrc( req, resp ) {
	$.ajax( {
		url: 'http://query.yahooapis.com/v1/public/yql',
		dataType: 'jsonp',
		data: { q: buildQuery( req.term ), format: 'json' },
		success: function( data ) {
			var airports = [];
            if ( data && data.query && data.query.results && data.query.results.json && data.query.results.json.json ) {
                airports = data.query.results.json.json;
            } else if ( data && data.query && data.query.results && data.query.results.json ) {
                airports = { 'json': data.query.results.json };
            }
			resp( $.map( airports, function( item ) {
                return {
                    label: item.code + ( item.name ? ", " + item.location : "" ) + ", " + item.location,
                    value: item.code
                };
            } ) );
		},
		error: function() {
			resp( [] );
		}
    } );
}
$( '#from' ).autocomplete( {
	source: acSrc,
	minLength: 2
} );
$( '#to' ).autocomplete( {
	source: acSrc,
	minLength: 2
} );

});
