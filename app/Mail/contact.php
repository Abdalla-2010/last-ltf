<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contact extends Mailable
{
use Queueable, SerializesModels;
 public $Fname;
 public $Lname;
 public $Email;
 public $phone;
 public $massage;
 

    public function __construct($Fname,$Lname,$Email,$phone,$massage)
    {
        //
        
        $this->Fname=$Fname;
        $this->Lname=$Lname;
        $this->Email=$Email;
        $this->phone=$phone;
        $this->massage= $massage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.email');
    }
}
