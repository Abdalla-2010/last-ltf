<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactUs extends Model
{
    protected $table = "contactus";
    
     use SoftDeletes;
     
      protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
     protected $fillable = [
        'Fname',
        'Lname',
        'email',
        'phone',
        'massage',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
