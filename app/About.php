<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class About extends Model
{
     use SoftDeletes;
     protected $table = 'abouts';

      protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

     protected $fillable = [
        'name',
        'body',
    ];
}
