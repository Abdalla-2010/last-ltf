<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
 
class Article extends Model
{
    
     use SoftDeletes;
     
    
      protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
     protected $fillable = [
        'title',
        'description',
        'images',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
    // protected $castas = [
    //     'images' => 'array'
    // ];
}
