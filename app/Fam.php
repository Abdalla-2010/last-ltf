<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fam extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'Designation',
        'organization',
        'title',
        'email',
        'address_Line',
        'city',
        'state',
        'zip_code',
        'province',
        'country',
        'phone',
        'fax',
        'website',
        'q1',
        'q2',
        'q3',
        'q4',
        'q5',
        'q6',
        'q7',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
    ];
}
