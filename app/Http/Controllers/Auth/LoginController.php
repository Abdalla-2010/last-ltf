<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Meta;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Redirect the user to the FACEBOOK authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from FACEBOOK.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {


        $userSocial = Socialite::driver('facebook')->user();

        $user = User::where('email', $userSocial->email)->first();
        if (!$user) {
            $user = new User;
            $user->name = $userSocial->name;
            $user->email = $userSocial->email;
            $user->status = 2;
            $user->type = 'trade visitor';
            $user->password = bcrypt(random_bytes(10));
            $user->save();
            $user->roles()->sync([4]);
        }

        Auth::login($user);

        return  redirect()->route('admin.home');
    }

    public function showLoginForm()
    {
        $meta = Meta::where('group', 'Login Page ')->first();
        return view('loginfront', compact('meta'));
    }



}
