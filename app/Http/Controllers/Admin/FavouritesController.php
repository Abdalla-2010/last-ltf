<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouritesController extends Controller
{
    public function index()
    {
        $favourites = Auth::user()->favourites()->get();
        return view('admin.favourites.index', compact('favourites'));
    }

    public function show($id)
    {
        if ($user = Auth::user()->favourites()->where('favourite_id', $id)->first()){

            return view('admin.favourites.show', compact('user'));
        }
        return redirect()->back();
    }

    public function addFavourite()
    {
        if (Auth::check()) {
            if (Auth::user()->favourites()->where('favourite_id', \request()->company_id)->first()) {
                return response()->json('exist', 200);
            } else {
                Auth::user()->favourites()->attach(\request()->company_id);
                return response()->json('success', 200);
            }
        }
    }

    public function deleteFavourite($id)
    {

        if (Auth::check()) {
            if (\request()->ajax()) {
                if (Auth::user()->favourites()->where('favourite_id', $id)->first()) {
                    Auth::user()->favourites()->detach($id);
                    return response()->json('deleted', 200);
                } else {
                    return response()->json('Not exist', 200);
                }
            } else {
                if (Auth::user()->favourites()->where('favourite_id', $id)->first()) {
                    Auth::user()->favourites()->detach($id);
                }

                return redirect()->route('admin.favourites.index');
            }
        }
    }

    public function massDestroy()
    {
        Auth::user()->favourites()->detach(\request('ids'));
        return response(null, 204);

    }
}
