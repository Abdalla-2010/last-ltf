<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RatesController extends Controller
{
   
    public function index(){
        
        abort_unless(\Gate::allows('rates_access'), 403);
        
        return view('admin.rates.index');
    }
    
    
   
}
