<?php

namespace App\Http\Controllers\Admin;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ResultsController extends Controller
{
    public function result(Request $request)
    {

        abort_unless(\Gate::allows('results_access'), 403);
//        $user = DB::table('users')
//            ->whereJsonContains('travel', ['Adoption'])
//            ->get();
//        dd($user);
//        dd(DB::table('users')
//            ->whereJsonContains('languages', ['Arabic', 'Bengali'])
//            ->get());
        $input = collect($request->all())->filter(function ($value) {
            return null !== $value;
        })->toArray();
        $companies = DB::table('users')
            ->where('status', 4)
            ->where(function ($query) use ($input) {
                if (array_key_exists('country', $input)) {
                    $query->where('country', $input['country']);
                }
            })->where(function ($query) use ($input) {
                if (array_key_exists('name', $input)) {
                    $query->where('name','like', '%'. $input['name'] . '%');
                }
            })->where(function ($query) use ($input) {
                if (array_key_exists('last_name', $input)) {
                    $query->where('last_name', 'like', '%'. $input['last_name'] . '%');
                }
            })->where(function ($query) use ($input, $request) {
                if (array_key_exists('company', $input)) {
                    $query->where('company', 'like', '%' . $input['company'] . '%');
                }
            })->where(function ($query) use ($input, $request) {
                if (array_key_exists('destination', $input)) {
                    $query->whereJsonContains('destination', $input['destination']);
                }
            })->where(function ($query) use ($input, $request) {
                if (array_key_exists('languages', $input)) {
                    $query->whereJsonContains('languages', $input['languages']);
                }
            })->where(function ($query) use ($input, $request) {
                if (array_key_exists('travel', $input)) {
                    $query->whereJsonContains('travel', $input['travel']);
                }
            })->get();

        return view('admin.results.index', compact('companies'));
    }

    public function showProfile($id){

        return view('admin.results.showProfile', [
           'user' => User::findOrFail($id),
        ]);

    }
}
