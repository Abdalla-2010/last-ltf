<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MassDestroyMyOfferRequest;
use App\MyOffer;
use App\User;

class MyOffersController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('myoffer_access'), 403);

        $myoffers = MyOffer::all();
        
        // $users  = User::all()->where('id', request['userId'])->get();

        return view('admin.myoffers.index', compact('myoffers','users'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('myoffer_create'), 403);

        return view('admin.myoffers.create');
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('myoffer_create'), 403);
        $user = auth()->user();
        $user_id=auth()->user()->id;
        $img = $request->file('img');
        $image_new_name = time().$img->getClientOriginalName();
        $img->move(public_path().'/uploads/offers/',$image_new_name);
        
        $myoffer = MyOffer::create([
            'user_id'=>$user_id,
            'name'=>$request->name,
            'duration'=>$request->duration,
            'destination'=>$request->destination,
            'price'=>$request->price,
            'url'=>$request->url,
            'img'=>$image_new_name,
            
            ]);


        return redirect()->route('admin.myoffers.index');
    }

    public function edit(MyOffer $myoffer)
    {
        abort_unless(\Gate::allows('myoffer_edit'), 403);

        return view('admin.myoffers.edit', compact('myoffer'));
    }

    public function update(Request $request, MyOffer $myoffer)
    {
        abort_unless(\Gate::allows('myoffer_edit'), 403);

        if($request->hasFile('img')){
        $img = $request->file('img');
        $image_new_name = time().$img->getClientOriginalName();
        $img->move(public_path().'/uploads/offers/',$image_new_name);
        $myoffer->img = $image_new_name;
        }
        
        $myoffer->update([
            $myoffer->name=$request->name,
            $myoffer->duration=$request->duration,
            $myoffer->destination=$request->destination,
            $myoffer->price=$request->price,
            $myoffer->url=$request->url,
            
        ]);
        

        return redirect()->route('admin.myoffers.index');
    }

    public function show(MyOffer $myoffer)
    {
        abort_unless(\Gate::allows('myoffer_show'), 403);
        
        return view('admin.myoffers.show', compact('myoffer'));
    }

    public function destroy(MyOffer $myoffer)
    {
        abort_unless(\Gate::allows('user_delete'), 403);

        $myoffer->delete();

        return back();
    }

    public function massDestroy(MassDestroyMyOfferRequest $request)
    {
        MyOffer::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
