<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function index()
    {
        
        abort_unless(\Gate::allows('search_access'), 403);

        return view('admin.searchs.index');
    }
}
