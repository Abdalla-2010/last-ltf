<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\MyOffer;
use Illuminate\Support\Facades\DB;

class OffersController extends Controller
{
    public function index(Request $request)
    {

        abort_unless(\Gate::allows('offers_access'), 403);
        $users = User::all()->where('status', 4);
        $destinations = MyOffer::all()->pluck('destination')->unique();
        $offers = MyOffer::all();

        $users = User::all()->where('status', 4);
        $destinations = MyOffer::all()->pluck('destination')->unique();
        $offers = MyOffer::all();

        if ($request->has('destination') && $request->has('company')) {
            $offers = User::where('company', $request->company)->first()
                ->offers()->where('destination', $request->destination)->get();
        } elseif ($request->has('destination') && empty($request->company)) {
            $offers = MyOffer::where('destination', $request->destination)->get();
        } elseif ($request->has('company') && empty($request->destination)) {
            $offers = User::where('company', $request->company)->first()->offers;
        }

        return view('admin.offers.index', compact('users', 'offers', 'destinations'));
    }


// public function show(Request $request){

//     abort_unless(\Gate::allows('offers_show'), 403);

//         $Company = $request->Company;
//         $destination = $request->destination;
//         // dd($destination);

//         $filteredOffers = MyOffer::where('name', 'like', '%' . $destination . '%')->get();
//                             // ->orWhere('email', 'like', '%' . $q . '%');

//          if ($filteredOffers->count()) {

//         return view('admin.offers.index')->with([
//             'users' =>  $filteredOffers
//         ]);
//     } else {

//         return redirect('/offers')->with([
//             'status' => 'search failed ,, please try again'
//         ]);
//     }
// }


// $destination = DB::table('myoffers')->orderBy('name')->get();

// if(empty($Company) && empty($destination)){
//     $offers = MyOffer::orderBy('id')->get();
// }elseif(!empty($Company) && !empty($destination)){
//   $offers = DB::table('kids')->where('destination', $destination)->where('Company', $Company)->orderby('id')->get();
// }elseif($Company != true){
//     $offers = DB::table('kids')->where('destination', $destination)->orderby('id')->get();
// }elseif($destination != true){
//     $offers = DB::table('kids')->where('gender', $Company)->orderby('id')->get();
// }else{
//     $offers = MyOffer::orderBy('id')->get();
// }
//   return view('admin.offers.show',compact('offers','destination'));


}
