<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyLibraryRequest;
use App\Http\Requests\StoreLibraryRequest;
use App\Http\Requests\UpdateLibraryRequest;
use App\Library;

class LibraryController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('library_access'), 403);

        $librarys = Library::all();

        return view('admin.librarys.index',compact('librarys'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('library_create'), 403);
        
        return view('admin.librarys.create');
    }

    public function store(StoreLibraryRequest $request)
    {
        abort_unless(\Gate::allows('library_create'), 403);

        $video = $request->file('video');
        $video_new_name = '/uploads/librarys/' . time().$video->getClientOriginalName();
        $video->move('uploads/librarys',$video_new_name);
        
        $library = Library::create([
               'name'=>$request->name,
               'video'=>$video_new_name,
            
            ]);

        return redirect()->route('admin.librarys.index');
    }

    public function edit(Library $library)
    {
        abort_unless(\Gate::allows('library_edit'), 403);

        return view('admin.librarys.edit', compact('library'));
    }

    public function update(UpdateLibraryRequest $request, Library $library)
    {
        abort_unless(\Gate::allows('library_edit'), 403);
        $input = $request->except('video');
       if($request->hasFile('video') && !empty($request->video)){
        //delete video
        unlink(public_path() . $library->video);
        //add new video
        $video = $request->file('video');
        $video_new_name = '/uploads/librarys/' . time().$video->getClientOriginalName();
        $video->move('uploads/librarys',$video_new_name);
        
        $input['video'] = $video_new_name;
        }
        
        $library->update($input);
        
        return redirect()->route('admin.librarys.index');
    }

    public function show(Library $library)
    {
        abort_unless(\Gate::allows('library_show'), 403);

        return view('admin.librarys.show', compact('library'));
    }

    public function destroy(Library $library)
    {
        abort_unless(\Gate::allows('library_delete'), 403);
        
        //delete video
        unlink(public_path() . $library->video);
        //delete library
        $library->delete();

        return back();
    }

    public function massDestroy(MassDestroyLibraryRequest $request)
    {
        $libraries = Library::whereIn('id', request('ids'))->get();
        foreach($libraries as $library){
            //delete video
            unlink(public_path() . $library->video);
            //delete library
            $library->delete();
        }
        
        return response(null, 204);
    }
}

