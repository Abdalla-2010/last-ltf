<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Request\UpdateVisitorRequest;
use Illuminate\Http\Request\MassDestroyVisitorRequest;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Storage;

class VisitorsController extends Controller
{


    public function index()
    {
        abort_unless(\Gate::allows('visitor_access'), 403);

        $users = User::all()->where('status', 1);

        return view('admin.visitors.index', compact('users'));
    }

//    public function create()
//    {
//        abort_unless(\Gate::allows('visitor_create'), 403);
//
//        $roles = Role::all()->pluck('title', 'id');
//
//        return view('admin.users.create', compact('roles'));
//    }
//
//    public function store(StoreUserRequest $request)
//    {
//        abort_unless(\Gate::allows('user_create'), 403);
//
//        $user = User::create($request->all());
//        $user->roles()->sync($request->input('roles', []));
//
//        return redirect()->route('admin.users.index');
//    }
//
//    public function edit(User $user)
//    {
//        abort_unless(\Gate::allows('user_edit'), 403);
//
//        $roles = Role::all()->pluck('title', 'id');
//
//        $user->load('roles');
//
//        return view('admin.users.edit', compact('roles', 'user'));
//    }
//
//    public function update(UpdateUserRequest $request, User $user)
//    {
//        abort_unless(\Gate::allows('user_edit'), 403);
//
//        $user->update($request->all());
//        $user->roles()->sync($request->input('roles', []));
//
//        return redirect()->route('admin.users.index');
//    }

    public function show($id)
    {
        abort_unless(\Gate::allows('visitor_show'), 403);

        $user = User::findOrFail($id);
        if ($user->status == 1) {
            return view('admin.visitors.show', compact('user'));
        }

        return redirect()->back();

    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('user_delete'), 403);
        $user = User::findorFail($id);
        if ($user->status == 1) {

           if($user->imgCompany != '/noimage.png'){
                $path = str_replace('/storage/', 'public/', $user->imgCompany);
                Storage::delete($path);
            }
            if($user->imgPersonal != '/noimage.png'){
                $path = str_replace('/storage/', 'public/', $user->imgPersonal);
                Storage::delete($path);
            }
            $user->delete();
        }
        return back();
    }

    public function massDestroy()
    {
        $users = User::whereIn('id', request('ids'))->get();

        foreach ($users as $user) {
            if($user->imgCompany != '/noimage.png'){
                $path = str_replace('/storage/', 'public/', $user->imgCompany);
                Storage::delete($path);
            }
            if($user->imgPersonal != '/noimage.png'){
                $path = str_replace('/storage/', 'public/', $user->imgPersonal);
                Storage::delete($path);
            }
            $user->delete();
        }
        return response(null, 204);
    }


}
