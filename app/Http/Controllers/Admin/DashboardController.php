<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DashboardController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('dashboard_access'), 403);



        return view('admin.dashboard.index',
        [
            'visitorsCount' => User::where('status', 1)->count(),
            'exhibitorsCount' => User::where('status', 4)->count(),
            'paymentaccounts' => User::Where('status', '!=', 0)->where('payment_status', 1)->count(),
        ]);
    }
}
