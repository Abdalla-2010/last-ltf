<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function edit()
    {

        return view('admin.profiles.edit', [
            'user' => Auth::user(),
        ]);

    }

    public function update(UpdateProfileRequest $request)
    {
        $input = $request->except(['imgPersonal', 'imgCompany', 'company_profile', 'brochure']);

        if ($request->has('imgPersonal') && !empty($request->imgPersonal)) {

            if (auth()->user()->imgPersonal != '/noimage.png') {
                $path = str_replace('/storage/', 'public/', \auth()->user()->imgPersonal);
                Storage::delete($path);
            }


            $file = request()->file('imgPersonal');
            $image1 = time() . $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/images/personal', $file, $image1);

            $input['imgPersonal'] = Storage::url('public/uploads/images/personal/' . $image1);
        }

        if ($request->has('imgCompany') && !empty($request->imgCompany)) {

            if (auth()->user()->imgCompany != '/noimage.png') {
                $path = str_replace('/storage/', 'public/', \auth()->user()->imgCompany);
                Storage::delete($path);
            }

            $file = request()->file('imgCompany');
            $image2 = time() . $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/images/compLogo', $file, $image2);

            $input['imgCompany'] = Storage::url('public/uploads/images/compLogo/' . $image2);
        }

        if ($request->has('company_profile') && !empty($request->company_profile)) {
            if (\auth()->user()->company_profile != null) {
                $path = str_replace('/storage/', 'public/', \auth()->user()->company_profile);
                Storage::delete($path);
            }

            $file = \request()->file('company_profile');
            $pdf = time() . $file->getClientOriginalName();
            $path = 'public/uploads/companies-profiles/' . $pdf;
            Storage::putFileAs('public/uploads/companies-profiles', $file, $pdf);
            $input['company_profile'] = $path;
        }
        if ($request->has('brochure') && !empty($request->brochure)) {
            if (\auth()->user()->brochure != null) {
                $path = str_replace('/storage/', 'public/', \auth()->user()->brochure);
                Storage::delete($path);
            }

            $file = \request()->file('brochure');
            $image = time() . $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/companies-Brochure', $file, $image);

            $input['brochure'] = Storage::url('public/uploads/companies-Brochure/' . $image);

            \auth()->user()->brochure = $input['brochure'];
            \auth()->user()->save();

        }

        \auth()->user()->update($input);

        return redirect()->route('admin.home');
    }

    public function show()
    {


        return view('admin.profiles.show',[
            'user' => auth()->user(),
        ]);
    }

    public function download($id)
    {
        $user = User::where('id', $id)->first();
        return Storage::download($user->company_profile);
    }
}
