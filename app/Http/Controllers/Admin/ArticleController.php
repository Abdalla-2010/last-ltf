<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyArticleRequest;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('article_access'), 403);

        $articles = Article::all();

        return view('admin.articles.index', compact('articles'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('article_create'), 403);

        return view('admin.articles.create');
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('article_create'), 403);

        $images = $request->file('image');
        $image_new_name = time().$images->getClientOriginalName();
        $images->move(public_path() . '/uploads/articles/',$image_new_name);
        
        $article = Article::create([
               'title'=>$request->title,
               'description'=>$request->description,
               'images'=>$image_new_name,
            
            ]);
            
        return redirect()->route('admin.articles.index');
    }

    public function edit(Article $article)
    {
        abort_unless(\Gate::allows('article_edit'), 403);

        return view('admin.articles.edit', compact('article'));
    }

    public function update(Request $request, Article $article)
    {
        abort_unless(\Gate::allows('article_edit'), 403);

        if($request->hasFile('images')){
        $images = $request->file('images');
        $image_new_name = time().$images->getClientOriginalName();
        $images->move(public_path() . '/uploads/articles/',$image_new_name);
        $article->images = $image_new_name;
        }
        
        $article->update([
            $article->title=$request->title,
            $article->description=$request->description,
            
        ]);
        
        $article->save();
        return redirect()->route('admin.articles.index');
    }

    public function show(Article $article)
    {
        abort_unless(\Gate::allows('article_show'), 403);

        return view('admin.articles.show', compact('article'));
    }

    public function destroy(Article $article)
    {
        abort_unless(\Gate::allows('article_delete'), 403);

        $article->delete();

        return back();
    }

    public function massDestroy(MassDestroyArticleRequest $request)
    {
        Article::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}


