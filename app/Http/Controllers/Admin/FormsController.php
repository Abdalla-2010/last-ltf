<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormsController extends Controller
{
    public function index(){
        
        abort_unless(\Gate::allows('forms_access'), 403);
        
        return view('admin.forms.index');
    }
}
