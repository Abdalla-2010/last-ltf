<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MediaController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('media_access'), 403);



        return view('admin.media.index');
    }
}
