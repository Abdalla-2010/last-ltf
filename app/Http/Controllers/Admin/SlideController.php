<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('slide_access'), 403);


        return view('admin.slides.index');
    }
}
