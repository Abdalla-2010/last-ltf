<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFanRequest;
use App\Fam;

class FamController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('fan_access'), 403);

        $fans = Fam::all();

        return view('admin.fans.index', compact('fans'));
    }
    
    // public function edit(Fan $fan)
    // {
    //     abort_unless(\Gate::allows('fan_edit'), 403);

    //     return view('admin.fans.edit', compact('fan'));
    // }

    // public function update(Request $request, Fan $fan)
    // {
    //     abort_unless(\Gate::allows('fan_edit'), 403);

    //     $fan->update($request->all());

    //     return redirect()->route('admin.fans.index');
    // }

    public function show(Fam $fan)
    {
        abort_unless(\Gate::allows('fan_show'), 403);

        return view('admin.fans.show', compact('fan'));
    }

    public function destroy(Fam $fan)
    {
        abort_unless(\Gate::allows('fan_delete'), 403);

        $fan->delete();

        return back();
    }

    public function massDestroy(MassDestroyFanRequest $request)
    {
        Fan::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
