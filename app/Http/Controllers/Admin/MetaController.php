<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MassDestroyMetaRequest;
use App\Http\Controllers\Controller;
use App\Meta;

class MetaController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('meta_access'), 403);
        $metas = Meta::all();
        return view('admin.metas.index',compact('metas'));
    }

    public function edit(Meta $meta)
    {
        abort_unless(\Gate::allows('meta_edit'), 403);

        return view('admin.metas.edit', compact('meta'));
    }

    public function update(Request $request, Meta $meta)
    {
        abort_unless(\Gate::allows('meta_edit'), 403);

        $meta->update($request->all());

        return redirect()->route('admin.metas.index');
    }

    public function show(Meta $meta)
    {
        abort_unless(\Gate::allows('meta_show'), 403);

        return view('admin.metas.show', compact('meta'));
    }

    public function destroy(Meta $meta)
    {
        abort_unless(\Gate::allows('meta_delete'), 403);

        $meta->delete();

        return back();
    }

    public function massDestroy(MassDestroyMetaRequest $request)
    {
        Meta::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
