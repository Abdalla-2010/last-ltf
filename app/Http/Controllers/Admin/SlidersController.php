<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAboutRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SlidersController extends Controller
{
    public function index()
    {

        abort_unless(\Gate::allows('slider_access'), 403);

        $sliders = Slider::all();

        return view('admin.sliders.index', compact('sliders'));
    }


    public function edit(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_edit'), 403);

        return view('admin.sliders.edit', compact('slider'));
    }

    public function update(Request $request, Slider $slider)
    {
        abort_unless(\Gate::allows('slider_edit'), 403);
        $input = $request->except('image');

        if ($request->has('image')) {
            if ($slider->image != 'https://cvctravel.tech/system/public/Frontend/images/t2.webp' &&
                $slider->image != 'https://cvctravel.tech/system/public/Frontend/images/t5.webp' &&
                $slider->image != 'https://cvctravel.tech/system/public/Frontend/images/t2.webp' &&
                $slider->image != 'https://cvctravel.tech/system/public/Frontend/images/t82.webp') {
                $path = str_replace('/storage/', 'public/', $slider->image);
                Storage::delete($path);
            }

            $file = request()->file('image');
            $image = time() . $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/images/sliders', $file, $image);
            $input['image'] = Storage::url('public/uploads/images/sliders/' . $image);
        }

        $slider->update($input);

        return redirect()->route('admin.sliders.index');
    }
}

