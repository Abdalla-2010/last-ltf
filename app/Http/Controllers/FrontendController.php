<?php

namespace App\Http\Controllers;

use App\Mail\welcomeMail;
use App\Meta;
use App\Slider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Mail\contact;
use Illuminate\Http\Request;
use App\User;
use App\ContactUs;
use App\About;
use App\Article;
use App\Fam;
use App\Library;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class FrontendController extends Controller
{
    public function index()
    {
        $meta = Meta::where('group', 'Home Page')->first();
        $sliders = Slider::all();
        return view('main', compact('meta', 'sliders'));
    }

    public function exhibitor()
    {

        $meta = Meta::where('group', 'Exhibitors Page ')->first();
        return view('exhibitor', compact('meta'));
    }


    public function registerfront()
    {

        return view('registerfront');
    }

    public function termsandcondition()
    {

        return view('termsandcondition');
    }

    public function privacy()
    {

        return view('privacy');
    }


    public function register()
    {

        return view('register');
    }


     public function tourguide()
    {

        return view('tourguide');
    }


    public function tripfams()
    {

        return view('tripfam');
    }

    public function tripfam(Request $request)
    {

        $fam = Fam::create($request->all());
        if ($fam == true) {
            $notification = array(
                'message' => 'Your Message reached Successfully!',
                'alert-type' => 'success');
        } else {
            echo "error";
            $notification = array(
                'message' => 'Error! input is empty !',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);

    }


    public function groupleader_register()
    {

        return view('groupleader-register');
    }
    public function destinationpartner()
    {

        return view('destinationpartner');
    }


    public function register_exhibitor()
    {

        return view('register-exhibitor');
    }


    public function trade_register()
    {

        return view('trade-register');
    }

    public function register_tourguide()
    {
        return view('register-tourGuide');
    }


    public function register2()
    {
        $name = request('name');
        $email = request('email');
        $password = request('password');
        $last = request('last');
        $company_name = request('company_name');
        $phone = request('phone');
        $website = request('website');
        $city = request('city');
        $zip_code = request('zip_code');
        $radio = request('radiobutton');
        $status = request('status');
        $role_id = request('role_id');

        return view('company-info', compact('name', 'email', 'password', 'last', 'company_name', 'phone', 'website', 'city', 'zip_code', 'radio', 'status', 'role_id'));
    }


    public function register3()
    {
        $name = request('name');
        $email = request('email');
        $password = request('password');
        $company_name = request('company_name');
        $phone = request('phone');
        $radio = request('radio');
        $title = request('title');
        $last = request('last');
        $designation = request('designation');
        $company_email = request('company_email');
        $company_type = request('company_type');
        $url = request('url');
        $role_id = request('role_id');


        //save personal photos to storage directory and get the path
        if(request()->file('imgPersonal') == null){

            $imgPersonal = '/noimage.png';
        }else{
            $file = request()->file('imgPersonal');
            $image1 = time() . $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/images/personal', $file, $image1);

            $imgPersonal = Storage::url('public/uploads/images/personal/' . $image1);
        }

        //save companies' photos to storage directory and get the path
        if(request()->file('imgCompany') == null){
            $imgCompany = '/noimage.png';
        }else{
            $file = request()->file('imgCompany');
            $image2 = time() . $file->getClientOriginalName();
            Storage::putFileAs('public/uploads/images/compLogo', $file, $image2);

            $imgCompany = Storage::url('public/uploads/images/compLogo/' . $image2);
        }

        $zip_code = request('zip_code');
        $country = request('country');
        $facebook = request('facebook');
        $twitter = request('twitter');
        $linkedin = request('linkedin');
        $instagram = request('instagram');
        $status = request('status');

        return view('individual-profile', compact('name', 'email', 'password',
            'company_name', 'phone', 'radio', 'title', 'last', 'designation', 'company_email',
            'company_type', 'url', 'zip_code', 'country', 'facebook', 'twitter', 'linkedin', 'instagram', 'status', 'imgPersonal', 'imgCompany', 'role_id'));
    }

    public function individual_profile(Request $request)
    {


        // dd(request()->all());

        $name = request('name');
        $email = request('email');
        $password = request('password');
        $company_name = request('company_name');
        $phone = request('phone');
        $radio = request('radio');
        $title = request('title');
        $last = request('last');
        $designation = request('designation');
        $company_email = request('company_email');
        $company_type = request('company_type');
        $url = request('url');
        $imgPersonal = request('imgPersonal');
        $imgCompany = request('imgCompany');
        $zip_code = request('zip_code');
        $country = request('country');
        $facebook = request('facebook');
        $twitter = request('twitter');
        $linkedin = request('linkedin');
        $instagram = request('instagram');
        $travel = request('travel');
        $destination = request('destination');
        $languages = request('languages');
        $role_id = request('role_id');

        $status = request('status');
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'last_name' => $last,
            'type' => $radio,
            'company' => $company_name,
            'phone' => $phone,
            'website' => $url,
            'linkedin' => $linkedin,
            'twitter' => $twitter,
            'facebook' => $facebook,
            'instgram' => $instagram,
            'zip_code' => $zip_code,
            'title' => $title,
            'country' => $country,
            'company_email' => $company_email,
            'designation' => $designation,
            'travel' => $travel,
            'destination' => $destination,
            'languages' => $languages,
            'status' => $status,
            'imgPersonal' => $imgPersonal,
            'imgCompany' => $imgCompany,
            'company_type' => $company_type,

        ]);
        if ($user){
            $user->roles()->sync($role_id);
            Mail::to($user->email)
                ->send(new welcomeMail($user));
        }


        Auth::login($user);

        return  redirect()->route('admin.home');

    }

    public function contact()
    {

        $meta = Meta::where('group', 'Contact us Page')->first();

        return view('contact', compact('meta'));
    }

    public function contacts(Request $request)
    {

        $Contactus = new ContactUs();
        $Fname = $Contactus->Fname = $request->input("Fname");
        $Lname = $Contactus->Lname = $request->input("Lname");
        $Email = $Contactus->email = $request->input("email");
        $phone = $Contactus->phone = $request->input("phone");
        $massage = $Contactus->massage = $request->input("message");

        $Contactus->save();
        // mail::to('info@cvctravel.tech')->send(new contact($Fname,$Lname,$Email,$phone,$massage));
        if ($Contactus == true) {
            $notification = array(
                'message' => 'Your Message reached Successfully!',
                'alert-type' => 'success');
        } else {
            echo "error";
            $notification = array(
                'message' => 'Error! input is empty !',
                'alert-type' => 'error'
            );
        }
        return back()->with($notification);


    }

    public function library()
    {

        $libraries = Library::latest()->get();
        $meta = Meta::where('group', 'E-Libaray Page')->first();
        return view('library', compact('libraries', 'meta'));
    }


    public function visitor()
    {

        $meta = Meta::where('group', 'Visitors Page ')->first();

        return view('visitor', compact('meta'));
    }

    public function news($id)
    {

        $article = Article::find($id);
        $articles = Article::all();
        $about = About::all();
        return view('news', compact('article', 'about', 'articles'));
    }

    public function About()
    {
        $about = About::all();
        $meta = Meta::where('group', 'About Page')->first();
        return view('about', compact('about', 'meta'));
    }

    public function outer_news()
    {
        $articles = Article::paginate(3);
        $meta = Meta::where('group', 'News Page')->first();
        return view('outer-news', compact('articles', 'meta'));
    }


    public function sponsored()
    {
        $meta = Meta::where('group', 'Partners Page ')->first();
        return view('sponsored', compact('meta'));
    }

    public function createHere()
    {

        $meta = Meta::where('group', 'Account Types Page ')->first();
        return view('createHere', compact('meta'));
    }
}
