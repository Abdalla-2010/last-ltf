<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Library;

class StoreLibraryRequest extends FormRequest
{
     public function authorize()
    {
        return \Gate::allows('library_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
        ];
    }
}
