<?php

namespace App\Http\Requests;

use App\Exhibitor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyExhibitorRequest extends FormRequest
{
    public function authorize()
    {
        return abort_if(Gate::denies('exhibitor_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:exhibitors,id',
        ];
    }
}