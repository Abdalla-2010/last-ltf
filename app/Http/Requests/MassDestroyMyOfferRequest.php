<?php

namespace App\Http\Requests;

use App\MyOffer;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyMyOfferRequest extends FormRequest
{
    public function authorize()
    {
        return abort_if(Gate::denies('myoffer_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:myoffers,id',
        ];
    }
}
