<?php

namespace App\Http\Requests;

use App\Visitor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyVisitorRequest extends FormRequest
{
    public function authorize()
    {
        return abort_if(Gate::denies('visitor_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:visitors,id',
        ];
    }
}
