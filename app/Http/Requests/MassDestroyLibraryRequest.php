<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Library;
use Gate;

class MassDestroyLibraryRequest extends FormRequest
{
   public function authorize()
    {
        return abort_if(Gate::denies('library_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:librarys,id',
        ];
    }
}
