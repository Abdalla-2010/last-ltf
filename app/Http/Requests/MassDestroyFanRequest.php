<?php

namespace App\Http\Requests;

use App\Fam;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyFanRequest extends FormRequest
{
    public function authorize()
    {
        return abort_if(Gate::denies('fan_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:fans,id',
        ];
    }
}
