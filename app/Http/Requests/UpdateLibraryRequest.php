<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Library;


class UpdateLibraryRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('library_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
        ];
    }
}
