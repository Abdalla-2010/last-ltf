<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Visitor;

class UpdateVisitorRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('visitor_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
        ];
    }
}
