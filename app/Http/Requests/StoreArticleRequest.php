<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Article;

class StoreArticleRequest extends FormRequest
{
     public function authorize()
    {
        return \Gate::allows('article_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
        ];
    }
}
