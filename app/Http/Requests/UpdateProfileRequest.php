<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required'],
            'phone' => ['required'],
            'designation' => ['required'],
            'company' => ['required'],
            'website' => ['required'],
            'facebook' => ['required'],
            'zip_code' => ['required'],
            'title' => ['required'],
            'country' => ['required'],
            'company_email' => ['required'],
            'company_type' => ['required'],
            'travel' => ['required'],
            'languages' => ['required'],
            'destination' => ['required'],
            'status' => ['nullable'],
            'password' => ['nullable'],
            'linkedin' => ['nullable'],
            'twitter' => ['nullable'],
            'instgram' => ['nullable'],
            'imgPersonal' => ['nullable', 'image'],
            'imgCompany' => ['nullable', 'image'],
            'payment_status' => ['nullable'],
            'youtubeLink' => ['nullable'],
            'company_profile' => ['nullable', 'mimes:pdf'],
            'brochure' => ['nullable', 'image'],
        ];
    }
}
