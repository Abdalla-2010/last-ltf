<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Exhibitor;

class UpdateExhibitorRequest extends FormRequest
{
   public function authorize()
    {
        return \Gate::allows('exhibitor_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
        ];
    }
}
